using System.Collections;
using System.Collections.Generic;
using Packages.RedSunsetCore.Runtime.Core;
using Packages.RedSunsetCore.Runtime.Leaderboard;
using Packages.RedSunsetCore.Runtime.Leaderboard.Interfaces;
using Packages.RedSunsetCore.Runtime.Services.Async;
using Packages.RedSunsetCore.Runtime.Services.Database;
using Packages.RedSunsetCore.Runtime.Services.Database.Events;
using Packages.RedSunsetCore.Runtime.Services.Event.ExtensionMethods;
using Packages.RedSunsetCore.Samples.Firebase.FirebaseRealtimeDatabase.Data;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Packages.RedSunsetCore.Samples.Firebase.FirebaseRealtimeDatabase
{
    public class ExampleFirebaseBasedDatabase : RedsunsetCoreBehaviour
    {
        private const string USERS_LIST = "usersList";
        private const string USERS_DATA = "usersData";
        private const string LEADERBOARD = "leaderboard";
        
        [Inject]
        private IDatabaseService firebaseDatabase;
        private ILeaderboard<ExampleUser> leaderboard = new SimpleLeaderboard<ExampleUser>();
        private ExampleUser me;

        [SerializeField]
        private Button addUserBtn;
        [SerializeField]
        private Button updateLeaderboardBtn;
        [SerializeField]
        private InputField newUserId;
        [SerializeField]
        private InputField newUserScore;
        [SerializeField]
        private RectTransform usersContainer;
        [SerializeField]
        private GameObject userPrefab;
        
        private bool isInitialized = false;


        override protected void Awake()
        {
            base.Awake();
            addUserBtn.interactable = false;
            updateLeaderboardBtn.interactable = false;
        }

        private void Start()
        {
            StartCoroutine(CheckInitialization());
            firebaseDatabase.AddListener<DatabaseInitializedEvent>(OnDatabaseInitialized);
            firebaseDatabase.Initialize();
        }

        private void OnDatabaseInitialized(DatabaseInitializedEvent databaseInitializedEvent)
        {
            addUserBtn.onClick.AddListener(OnNewUser);
            updateLeaderboardBtn.onClick.AddListener(OnUpdateLeaderboard);

            string myId = SystemInfo.deviceUniqueIdentifier;
            Future<List<string>> databaseLeaderboard = firebaseDatabase.ReadFuture<List<string>>($"{LEADERBOARD}");
            databaseLeaderboard.WithValue(users =>
            {
                foreach (string user in users)
                {
                    Future<ExampleUser> userFuture = firebaseDatabase.ReadFuture<ExampleUser>($"{USERS_DATA}/{user}");
                    userFuture.WithValue(AddUserToLeaderboard);
                }
            });
            
            addUserBtn.interactable = true;
            updateLeaderboardBtn.interactable = true;
        }

        private IEnumerator CheckInitialization()
        {
            while (!isInitialized)
            {
                yield return null;
            }
            
            addUserBtn.interactable = true;
            updateLeaderboardBtn.interactable = true;
            
            var userDataField = Instantiate(userPrefab, usersContainer).GetComponent<ExampleLeaderboardUserField>();
            userDataField.Initialize(me);
        }

        private void OnNewUser()
        {
            var newUser = new ExampleUser { Id = newUserId.text, Score = int.Parse(newUserScore.text) };
            AddUserToLeaderboard(newUser);
            firebaseDatabase.Write($"{USERS_DATA}/{newUser.Id}", newUser);
            
            Debug.Log($"New User Added: Id: {newUser.Id} - Score: {newUser.Score}");
        }

        private void AddUserToLeaderboard(ExampleUser newUser)
        {
            leaderboard.AddUser(newUser);

            var userDataField = Instantiate(userPrefab, usersContainer).GetComponent<ExampleLeaderboardUserField>();
            userDataField.Initialize(newUser);
        }
        
        private void OnUpdateLeaderboard()
        {
            IEnumerable<ExampleUser> allUsers = leaderboard.GetUsers();
            foreach (ExampleUser user in allUsers)
            {
                firebaseDatabase.Write($"{USERS_DATA}/{user.Id}", user);
            }
            
            Debug.Log($"Leaderboard Updated!");
        }
    }
}