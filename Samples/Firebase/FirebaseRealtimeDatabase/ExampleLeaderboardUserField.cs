using Packages.RedSunsetCore.Samples.Firebase.FirebaseRealtimeDatabase.Data;
using UnityEngine;
using UnityEngine.UI;

namespace Packages.RedSunsetCore.Samples.Firebase.FirebaseRealtimeDatabase
{
    public class ExampleLeaderboardUserField : MonoBehaviour
    {
        [SerializeField]
        private Text userIdTxt;
        [SerializeField]
        private InputField scoreInput;

        public ExampleUser User { get; private set; }
        
        private void Awake()
        {
            scoreInput.onValueChanged.AddListener(OnSetScore);
        }

        public void Initialize(ExampleUser user)
        {
            User = user;
            userIdTxt.text = user.Id;
            scoreInput.text = user.Score.ToString();
        }

        private void OnSetScore(string value)
        {
            User.Score = int.Parse(value);
        }
    }
}