using System;
using System.Collections.Generic;
using IObservableUser = Packages.RedSunsetCore.Runtime.DesignPatterns.ObserverPattern.IObservable<Packages.RedSunsetCore.Samples.Firebase.FirebaseRealtimeDatabase.Data.ExampleLeaderboard>;

namespace Packages.RedSunsetCore.Samples.Firebase.FirebaseRealtimeDatabase.Data
{
    [Serializable]
    public class ExampleLeaderboard : List<ExampleUser>, Runtime.DesignPatterns.ObserverPattern.IObservable<ExampleLeaderboard>
    {
        public List<Action<IObservableUser>> Listeners { get; } = new List<Action<IObservableUser>>();
    }
}