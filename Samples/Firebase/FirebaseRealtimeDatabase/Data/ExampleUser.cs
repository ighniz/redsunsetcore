using System;
using System.Collections.Generic;
using Packages.RedSunsetCore.Runtime.Leaderboard.Interfaces;
using UnityEngine;

namespace Packages.RedSunsetCore.Samples.Firebase.FirebaseRealtimeDatabase.Data
{
    [Serializable]
    public class ExampleUser : ILeaderboardUser<ExampleUser>
    {
        List<Action<Runtime.DesignPatterns.ObserverPattern.IObservable<ExampleUser>>> Runtime.DesignPatterns.ObserverPattern.IObservable<ExampleUser>.Listeners { get; } = new List<Action<Runtime.DesignPatterns.ObserverPattern.IObservable<ExampleUser>>>();
        
        [SerializeField]
        private string id;
        public string Id
        {
            get => id;
            set => id = value;
        }

        [SerializeField]
        private int score;
        public int Score
        {
            get => score;
            set => score = value;
        }

        int IComparable<ExampleUser>.CompareTo(ExampleUser other)
        {
            return score > other.score ? 1 : -1;
        }
    }
}