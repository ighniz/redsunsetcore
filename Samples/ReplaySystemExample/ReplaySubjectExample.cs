using System.IO;
using Packages.RedSunsetCore.Runtime.ExtensionMethods;
using Packages.RedSunsetCore.Runtime.GameTools.Replay;
using Packages.RedSunsetCore.Runtime.GameTools.Replay.Interface;
using Packages.RedSunsetCore.Runtime.Services.Async.Interfaces;
using UnityEngine;

namespace Packages.RedSunsetCore.Samples.ReplaySystemExample
{
    public class ReplaySubjectExample : MonoBehaviour, IMemoryStreamRecordable, IUpdateReceiver
    {
        public int Id { get; }
        
        [SerializeField]
        private float speed;
        private IReplaySystem<ReplaySubjectExample> replaySystem;

        private void Start()
        {
            replaySystem = new MemoryStreamReplaySystem<ReplaySubjectExample>();
            replaySystem.Register(this);
            replaySystem.StartRecord();
            this.AddToUpdateList();
        }

        void IMemoryStreamRecordable.OnRecord(BinaryWriter writer)
        {
            Vector2 position = transform.position;
            writer.Write(position.x);
            writer.Write(position.y);
        }

        void IMemoryStreamRecordable.OnReplay(BinaryReader reader)
        {
            Vector2 position;
            position.x = reader.ReadSingle();
            position.y = reader.ReadSingle();
            transform.position = position;
        }

        void IUpdateReceiver.OnUpdate()
        {
            replaySystem.Update();
            if (replaySystem.IsRecording)
            {
                var direction = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
                transform.position += direction * speed * Time.deltaTime;

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    replaySystem.StartReplay();
                }
            }
        }
    }
}