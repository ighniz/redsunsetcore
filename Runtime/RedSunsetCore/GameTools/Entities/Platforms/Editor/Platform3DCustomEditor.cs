using System;
using UnityEditor;
using UnityEngine;

namespace RedSunsetCore.GameTools.Entities.Platforms.Editor
{
	[CustomEditor(typeof(Platform3D))]
	public class Platform3DCustomEditor : UnityEditor.Editor
	{
		private Platform3D mTargetPlatform;
		private GUIStyle mStyle;

		private void OnEnable()
		{
			mTargetPlatform = (Platform3D)target;
			mStyle = new GUIStyle
			{
				normal =
				{
					textColor = Color.green
				}
			};
		}

		override public void OnInspectorGUI()
		{
			DrawDefaultInspector();
			if (GUILayout.Button("Set Waypoint"))
			{
				var newWaypoint = new PlatformWaypoint
				{
					position = mTargetPlatform.transform.position,
					rotation = mTargetPlatform.transform.rotation
				};
				mTargetPlatform.Waypoints.Add(newWaypoint);
			}

			if (GUILayout.Button("Clear Waipoints"))
			{
				mTargetPlatform.Waypoints.Clear();
			}
		}

		private void OnSceneGUI()
		{
			if (mTargetPlatform != null)
			{
				for (var i = 0; i < mTargetPlatform.Waypoints.Count; i++)
				{
					Color currentColor = Handles.color;
					Handles.color = Color.green;
					Handles.Label(mTargetPlatform.Waypoints[i].position, $"( {i} )", mStyle);
					Handles.color = currentColor;
				}
			}
		}
	}
}