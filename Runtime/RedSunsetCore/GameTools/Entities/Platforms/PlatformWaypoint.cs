using System;
using RedSunsetCore.CorePlugins.iTweenPlugin;
using UnityEngine;

namespace RedSunsetCore.GameTools.Entities.Platforms
{
	[Serializable]
	public class PlatformWaypoint
	{
		public Vector3 position;
		public Quaternion rotation;
		public iTween.EaseType easeType;
	}
}