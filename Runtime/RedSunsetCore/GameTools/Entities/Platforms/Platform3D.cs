using System.Collections;
using System.Collections.Generic;
using RedSunsetCore.Core;
using RedSunsetCore.CorePlugins.iTweenPlugin;
using UnityEngine;

namespace RedSunsetCore.GameTools.Entities.Platforms
{
	public class Platform3D : RedsunsetCoreBehaviour
	{
		[SerializeField]
		private List<PlatformWaypoint> waypoints = new ();
		public List<PlatformWaypoint> Waypoints => waypoints;

		[SerializeField]
		private float speed = 1;
		public float Speed => speed;

		private int mCurrentWaypointIndex = 0;

		override protected void Start()
		{
			base.Start();

			gameObject.transform.position = waypoints[0].position;
			gameObject.transform.rotation = waypoints[0].rotation;

			StartNewTween();
		}

		private void StartNewTween()
		{
			if (mCurrentWaypointIndex < waypoints.Count - 1)
			{
				mCurrentWaypointIndex++;
				PlatformWaypoint currentWaypoint = waypoints[mCurrentWaypointIndex];
				float timeBasedOnSpeed = Vector3.Distance(currentWaypoint.position, transform.position) / speed;
				iTween.MoveTo(gameObject, new Hashtable
				{
					["position"] = currentWaypoint.position,
					["speed"] = speed,
					["easetype"] = currentWaypoint.easeType,
					["oncomplete"] = "StartNewTween"
				});
				iTween.RotateTo(gameObject, new Hashtable
				{
					["rotation"] = currentWaypoint.rotation.eulerAngles,
					["time"] = timeBasedOnSpeed,
					["easetype"] = currentWaypoint.easeType
				});
			}
		}

		private void OnDrawGizmosSelected()
		{
			Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
			foreach (PlatformWaypoint waypoint in waypoints)
			{
				Gizmos.DrawWireMesh(mesh, waypoint.position, waypoint.rotation, transform.localScale);
			}
		}
	}
}