using System.Collections.Generic;
using RedSunsetCore.Core;
using RedSunsetCore.GameTools.Scene.Enums;
using RedSunsetCore.GameTools.Scene.Events;
using UnityEngine;

namespace RedSunsetCore.GameTools.Scene
{
    abstract public class InfiniteScene : RedsunsetCoreBehaviour
    {
        private List<Transform> currentChunks = new List<Transform>();
        private Transform LastChunk => currentChunks[currentChunks.Count - 1];

        private Dictionary<InfiniteSceneDirection, Vector3> directionToVectorTable = new Dictionary<InfiniteSceneDirection, Vector3>
        {
            [InfiniteSceneDirection.Right] = Vector3.right,
            [InfiniteSceneDirection.Down] = Vector3.down,
            [InfiniteSceneDirection.Left] = Vector3.left,
            [InfiniteSceneDirection.Up] = Vector3.up
        };

        [SerializeField]
        private InfiniteSceneChunk[] chunksPrefabs;
        [SerializeField]
        private Transform chunksContainer;
        [SerializeField]
        private InfiniteSceneChunk startChunk;
        [SerializeField]
        private InfiniteSceneDirection direction;
        [SerializeField]
        private float speed;
        [SerializeField]
        private int startAmountOfChunk;
        
        public Vector3 VectorDirection => directionToVectorTable[direction];

        override protected void Awake()
        {
            base.Awake();
            currentChunks.Add(startChunk.transform);
            startChunk.Dispatcher.AddListener<InfiniteSceneOutOfCameraEvent>(OnChunkOut);
            for (var i = 0; i < startAmountOfChunk; i++)
            {
                SpawnNewChunk();
            }
        }

        public void Move()
        {
            foreach (Transform chunk in currentChunks)
            {
                chunk.position += VectorDirection * speed * Time.deltaTime;
            }
        }

        protected void SpawnNewChunk()
        {
            GameObject selectedPrefab = chunksPrefabs[Random.Range(0, chunksPrefabs.Length)].gameObject;
            var newChunk = Instantiate(selectedPrefab, chunksContainer).GetComponent<RedsunsetCoreBehaviour>();
            
            Vector3 newChunkSize = newChunk.GetComponent<Renderer>().bounds.size;
            float newChunkAxisSize = direction == InfiniteSceneDirection.Horizontal ? newChunkSize.x : newChunkSize.y;
            
            Vector3 lastChunkSize = LastChunk.GetComponent<Renderer>().bounds.size;
            float lastChunkAxisSize = direction == InfiniteSceneDirection.Horizontal ? lastChunkSize.x : lastChunkSize.y;

            newChunk.transform.position = LastChunk.position - VectorDirection * (newChunkAxisSize/2 + lastChunkAxisSize/2);
            
            currentChunks.Add(newChunk.transform);
            newChunk.Dispatcher.AddListener<InfiniteSceneOutOfCameraEvent>(OnChunkOut);
        }

        private void OnChunkOut(InfiniteSceneOutOfCameraEvent infiniteSceneOutOfCameraEvent)
        {
            infiniteSceneOutOfCameraEvent.Chunk.Dispatcher.RemoveListener<InfiniteSceneOutOfCameraEvent>(OnChunkOut);
            Destroy(infiniteSceneOutOfCameraEvent.Chunk.gameObject);
            currentChunks.Remove(infiniteSceneOutOfCameraEvent.Chunk.transform);
            SpawnNewChunk();
        }
        
        private void Update()
        {
            Move();
        }
    }
}