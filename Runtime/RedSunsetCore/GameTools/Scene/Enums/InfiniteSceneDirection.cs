namespace RedSunsetCore.GameTools.Scene.Enums
{
    public enum InfiniteSceneDirection
    {
        Right,
        Down,
        Left,
        Up,
        Horizontal = Left | Right,
        Vertical = Down | Up
    }
}