using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.GameTools.Scene.Events
{
    public class InfiniteSceneOutOfCameraEvent : ICustomEvent
    {
        public InfiniteSceneChunk Chunk { get; set; }
        
        public void Reset()
        {
            
        }
    }
}