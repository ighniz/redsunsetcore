using RedSunsetCore.Core;
using RedSunsetCore.GameTools.Scene.Events;

namespace RedSunsetCore.GameTools.Scene
{
    public class InfiniteSceneChunk : RedsunsetCoreBehaviour
    {
        protected void OnBecameInvisible()
        {
            Dispatcher.Dispatch<InfiniteSceneOutOfCameraEvent>(x => x.Chunk = this);
        }
    }
}