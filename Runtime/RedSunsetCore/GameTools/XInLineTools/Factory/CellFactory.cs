using RedSunsetCore.GameTools.XInLineTools.Interfaces;
using RedSunsetCore.Services.Zenject;
using Zenject;

namespace RedSunsetCore.GameTools.XInLineTools.Factory
{
    public class CellFactory<TCell, TTile> : ICellFactory<TCell, TTile>
        where TCell : class, IXInLineCell<TCell, TTile>, new()
        where TTile : class, IXInLineTileComponent<TTile>
    {
        [Inject]
        protected IInjectingService injector;
        
        public TCell Create()
        {
            return injector.Create<TCell>();
        }
    }
}