using System.Collections.Generic;
using RedSunsetCore.GameTools.XInLineTools.Interfaces;
using RedSunsetCore.GameTools.XInLineTools.Matrix;

namespace RedSunsetCore.GameTools.XInLineTools.Factory
{
    public interface IMatrixCellFactory<TCell, TTile> : ICellFactory<TCell, TTile>
        where TCell : class, IXInLineCell<TCell, TTile>
        where TTile : class, IXInLineTileComponent<TTile>
    {
        TCell Create(HashSet<TCell> futureNeighbors, CellPosition position, int repetitionsCountAllowed);
    }
}