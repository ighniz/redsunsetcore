using System.Collections.Generic;
using RedSunsetCore.GameTools.XInLineTools.Interfaces;
using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.GameTools.XInLineTools.Events
{
    public class XInLineCellsRemovedAndMovedEvent<TCell, TTile> : ICustomEvent
        where TCell : class, IXInLineCell<TCell, TTile>
        where TTile : class, IXInLineTileComponent<TTile>
    {
        public HashSet<TCell> MovedCells { get; set; } = new HashSet<TCell>();
        
        public void Reset()
        {
            
        }
    }
}