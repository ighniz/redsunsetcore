using RedSunsetCore.GameTools.XInLineTools.Interfaces;
using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.GameTools.XInLineTools.Events
{
    public class XInLineSwapEvent<TCell, TTile> : ICustomEvent
        where TCell : class, IXInLineCell<TCell, TTile>
        where TTile : class, IXInLineTileComponent<TTile>
    {
        public TCell A { get; set; }
        public TCell B { get; set; }
        
        public void Reset()
        {
            
        }
    }
}