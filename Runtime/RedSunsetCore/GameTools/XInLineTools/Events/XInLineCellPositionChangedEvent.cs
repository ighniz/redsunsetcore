using RedSunsetCore.GameTools.XInLineTools.Interfaces;
using RedSunsetCore.GameTools.XInLineTools.Matrix;
using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.GameTools.XInLineTools.Events
{
    public class XInLineCellPositionChangedEvent<TCell, TTile> : ICustomEvent
        where TCell : class, IXInLineCell<TCell, TTile>
        where TTile : class, IXInLineTileComponent<TTile>
    {
        public TCell targetCell;
        public CellPosition lastPosition;
        
        public void Reset()
        {
            
        }
    }
}