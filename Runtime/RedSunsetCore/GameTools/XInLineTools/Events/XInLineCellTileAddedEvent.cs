﻿using RedSunsetCore.GameTools.XInLineTools.Interfaces;
using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.GameTools.XInLineTools.Events
{
    public class XInLineCellTileAddedEvent<TTile> : ICustomEvent
    where TTile : IXInLineTileComponent<TTile>
    {
        public IXInLineTileComponent<TTile> tile;
        
        public void Reset()
        {
            
        }
    }
}