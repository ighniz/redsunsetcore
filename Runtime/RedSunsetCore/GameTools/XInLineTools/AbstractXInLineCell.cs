using System;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.GameTools.XInLineTools.Events;
using RedSunsetCore.GameTools.XInLineTools.Interfaces;
using RedSunsetCore.Services.Event.Interfaces;
using Zenject;

namespace RedSunsetCore.GameTools.XInLineTools
{
    abstract public class AbstractXInLineCell<TCell, TTile> : IXInLineCell<TCell, TTile>, IObjectEventDispatcher, IComparable<TCell>
        where TCell : class, IXInLineCell<TCell, TTile>
        where TTile : class, IXInLineTileComponent<TTile>
    {
        public HashSet<TTile> AllTiles { get; } = new ();
        virtual public HashSet<TCell> DirectNeighbors { get; } = new ();
        public TCell Killer { get; private set; }
        public bool IsMoving { get; internal set; }
        public bool IsPopped { get; internal set; }

        [Inject]
        public IEventDispatcher Dispatcher { get; }
        
        virtual public HashSet<TCell> GetMatchableNeighbors(bool includeItself)
        {
            var matchableNeighbors = new HashSet<TCell>();
            var pendingList = new HashSet<TCell>();
            pendingList.UnionWith(DirectNeighbors);

            while (pendingList.Count > 0)
            {
                TCell neighbor = pendingList.First();
                if (IsMatchableWith(neighbor) && (neighbor != this || includeItself))
                {
                    matchableNeighbors.Add(neighbor);
                    pendingList.UnionWith(neighbor.DirectNeighbors);
                }

                pendingList.Remove(neighbor);
                pendingList.ExceptWith(matchableNeighbors);
            }

            return matchableNeighbors;
        }

        virtual public int GetDistanceTo(TCell cell)
        {
            throw new NotImplementedException();
        }

        abstract public bool IsMatchableWith(TCell cell);

        virtual public void AddTile(TTile tile)
        {
            AllTiles.Add(tile);
            Dispatcher.Dispatch<XInLineCellTileAddedEvent<TTile>>(initializer =>
            {
                initializer.tile = tile;
            });
        }

        virtual public void RemoveTile(TTile tile)
        {
            AllTiles.Remove(tile);
        }

        public void RemoveTiles(HashSet<TTile> tiles)
        {
            AllTiles.ExceptWith(tiles);
        }

        public void RemoveAllTiles()
        {
            AllTiles.Clear();
        }

        public void Pop(TCell killer = null)
        {
            if (!IsPopped)
            {
                IsPopped = true;
                Killer = killer;
                foreach (TTile tile in AllTiles)
                {
                    tile.OnCellPopped();
                }

                Dispatcher.Dispatch<XInLineCellPoppedEvent<TCell, TTile>>(initializer =>
                {
                    initializer.target = this as TCell;
                });
            }
        }

        virtual public void Connect(TCell cell, bool mutual = true)
        {
            if (!DirectNeighbors.Contains(cell))
            {
                DirectNeighbors.Add(cell);
                if (mutual)
                {
                    cell.Connect(this as TCell, false);
                }
            }
        }

        virtual public void Connect(HashSet<TCell> neighbors, bool mutual = true)
        {
            DirectNeighbors.UnionWith(neighbors);
            if (mutual)
            {
                var casted = this as TCell;
                foreach (TCell neighbor in neighbors)
                {
                    neighbor.Connect(casted, false);
                }
            }
        }

        virtual public void Disconnect(TCell cell, bool mutual = true)
        {
            DirectNeighbors.Remove(cell);

            if (mutual)
            {
                cell.Disconnect(this as TCell, false);
            }
        }

        virtual public void DisconnectAll(bool mutual = true)
        {
            if (mutual)
            {
                var meCasted = this as TCell;
                foreach (TCell neighbor in DirectNeighbors)
                {
                    neighbor.Disconnect(meCasted, false);
                }
            }
            DirectNeighbors.Clear();
        }
        
        int IComparable<TCell>.CompareTo(TCell other)
        {
            return other.AllTiles.Count - AllTiles.Count;
        }
    }
}