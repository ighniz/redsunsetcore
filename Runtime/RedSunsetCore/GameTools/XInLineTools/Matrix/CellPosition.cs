using System;
using System.Collections.Generic;
using RedSunsetCore.GameTools.Orientation;
using UnityEngine;

namespace RedSunsetCore.GameTools.XInLineTools.Matrix
{
    [Serializable]
    public struct CellPosition
    {
        public bool Equals(CellPosition other)
        {
            return row == other.row && column == other.column;
        }

        public override bool Equals(object obj)
        {
            return obj is CellPosition other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (row * 397) ^ column;
            }
        }

        static readonly private Dictionary<CellPosition, DPadOrientation> cellPositionConvertionTable;
        
        static public CellPosition Down => new CellPosition(-1, 0);
        static public CellPosition Left => new CellPosition(0, -1);
        static public CellPosition Right => new CellPosition(0, 1);
        static public CellPosition Up => new CellPosition(1, 0);
        static public CellPosition DownLeft => new CellPosition(1, -1);
        static public CellPosition DownRight => new CellPosition(1, 1);
        static public CellPosition UpLeft => new CellPosition(-1, -1);
        static public CellPosition UpRight => new CellPosition(-1, 1);

        [SerializeField]
        private int row;
        [SerializeField]
        private int column;
        
        public int Row
        {
            get => row;
            set => row = value;
        }

        public int Column
        {
            get => column;
            set => column = value;
        }

        public int SqrMagnitude => row * row + column * column;
        public CellPosition Normalized => new CellPosition(Mathf.Clamp(row, -1, 1), Mathf.Clamp(column, -1, 1));
        public CellPosition Opposite => new CellPosition(row * -1, column * -1);

        public CellPosition(int row, int column)
        {
            this.row = row;
            this.column = column;
        }

        static CellPosition()
        {
            cellPositionConvertionTable = new Dictionary<CellPosition, DPadOrientation>
            {
                {new CellPosition(0, 1), DPadOrientation.Right},
                {new CellPosition(0, -1), DPadOrientation.Left},
                {new CellPosition(1, 0), DPadOrientation.Up},
                {new CellPosition(-1, 0), DPadOrientation.Down},
                {new CellPosition(-1, 1), DPadOrientation.DownRight},
                {new CellPosition(1, -1), DPadOrientation.UpLeft},
                {new CellPosition(-1, -1), DPadOrientation.DownLeft},
                {new CellPosition(1, 1), DPadOrientation.UpRight}
            };
        }

        static public CellPosition operator +(CellPosition a, CellPosition b)
        {
            return new CellPosition(a.Row + b.Row, a.Column + b.Column);
        }
        
        static public CellPosition operator -(CellPosition a, CellPosition b)
        {
            return new CellPosition(a.Row - b.Row, a.Column - b.Column);
        }

        static public CellPosition operator *(CellPosition a, int scalar)
        {
            return new CellPosition(a.row * scalar, a.column * scalar);
        }

        static public bool operator ==(CellPosition a, CellPosition b)
        {
            return a.Row == b.Row && a.Column == b.Column;
        }
        
        static public bool operator !=(CellPosition a, CellPosition b)
        {
            return a.Row != b.Row || a.Column != b.Column;
        }
        
        public DPadOrientation ToDPadOrientation()
        {
            return cellPositionConvertionTable[this];
        }
        
        override public string ToString()
        {
            return $"(Row: {Row} ; Column: {Column})";
        }

        public string ToShortString()
        {
            return $"({Row}-{Column})";
        }

        public Vector2Int ToVector2Int()
        {
            return new Vector2Int(Column, Row);
        }
        
        public Vector2 ToVector2()
        {
            return new Vector2(Column, Row);
        }
    }
}