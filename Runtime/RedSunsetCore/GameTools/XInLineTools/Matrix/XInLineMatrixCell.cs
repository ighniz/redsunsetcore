using System.Collections.Generic;
using RedSunsetCore.GameTools.Orientation;
using RedSunsetCore.GameTools.XInLineTools.Events;
using RedSunsetCore.GameTools.XInLineTools.Interfaces;
using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.GameTools.XInLineTools.Matrix
{
    abstract public class XInLineMatrixCell<TCell, TTile> : AbstractXInLineCell<TCell, TTile>
        where TCell : XInLineMatrixCell<TCell, TTile>
        where TTile : class, IXInLineTileComponent<TTile>
    {
        private IObjectEventDispatcher ThisImplXInLineCell => this;
        
        private CellPosition position;
        public CellPosition Position
        {
            get => position;
            set
            {
                CellPosition lastValue = position;
                position = value;
                ThisImplXInLineCell.Dispatcher.Dispatch<XInLineCellPositionChangedEvent<TCell, TTile>>(ev =>
                {
                    ev.targetCell = this as TCell;
                    ev.lastPosition = lastValue;
                });
            }
        }
        public IDictionary<DPadOrientation, TCell> NeighborsByOrientation { get; private set; } = new Dictionary<DPadOrientation, TCell>();

        override public void Connect(TCell cell, bool mutual = true)
        {
            base.Connect(cell, mutual);

            CellPosition direction = (cell.Position - Position).Normalized;
            NeighborsByOrientation[direction.ToDPadOrientation()] = cell;
        }

        override public void Connect(HashSet<TCell> neighbors, bool mutual = true)
        {
            foreach (TCell targetCell in neighbors)
            {
                Connect(targetCell, mutual);
            }
        }

        override public void Disconnect(TCell cell, bool mutual = true)
        {
            base.Disconnect(cell, mutual);
            CellPosition direction = (cell.Position - Position).Normalized;
            NeighborsByOrientation.Remove(direction.ToDPadOrientation());
        }

        override public void DisconnectAll(bool mutual = true)
        {
            base.DisconnectAll(mutual);
            NeighborsByOrientation.Clear();
        }
    }
}