using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.GameTools.Orientation;
using RedSunsetCore.GameTools.Orientation.ExtensionMethods;
using RedSunsetCore.GameTools.XInLineTools.Events;
using RedSunsetCore.GameTools.XInLineTools.ExtensionMethods;
using RedSunsetCore.GameTools.XInLineTools.Factory;
using RedSunsetCore.GameTools.XInLineTools.Interfaces;
using RedSunsetCore.Services.Event.ExtensionMethods;

namespace RedSunsetCore.GameTools.XInLineTools.Matrix
{
    public class XInLineMatrix<TCell, TTile, TConnection> : AbstractXInLineGraph<TCell, TTile, TConnection>, IXInlineMatrix<TCell, TTile, TConnection>
        where TCell : XInLineMatrixCell<TCell, TTile>
        where TTile : class, IXInLineTileComponent<TTile>
        where TConnection : class, ICellConnection<TCell, TTile>
    {
        public int RowsCount { get; private set; }
        public int ColumnsCount { get; private set; }
        public DPadOrientation NeighborsOrientation { get; set; }

        private TCell[,] matrix;
        private HashSet<CellPosition> availablePositions;

        override public int Capacity
        {
            get => RowsCount * ColumnsCount;
            set{/*To Implement*/}
        }

        public TCell this[int row, int column]
        {
            get
            {
                TCell target = null;
                if (row >= 0 && row < RowsCount &&
                    column >= 0 && column < ColumnsCount)
                {
                    target = matrix[row, column];
                }
                return target;
            }

            set => matrix[row, column] = value;
        }

        public TCell this[CellPosition position]
        {
            get => this[position.Row, position.Column];
            set => this[position.Row, position.Column] = value;
        }

        public XInLineMatrix(int rowsCount, int columnsCount, DPadOrientation neighborsOrientation)
        {
            RowsCount = rowsCount;
            ColumnsCount = columnsCount;
            NeighborsOrientation = neighborsOrientation;
            
            matrix = new TCell[rowsCount,columnsCount];
            availablePositions = new HashSet<CellPosition>();
            
            for (var row = 0; row < rowsCount; row++)
            {
                for (var column = 0; column < columnsCount; column++)
                {
                    availablePositions.Add(new CellPosition(row, column));
                }
            }
        }

        public HashSet<TCell> GetMatchesOf(CellPosition cellPosition)
        {
            TCell targetCell = this[cellPosition];
            HashSet<TCell> matches;
            if (targetCell != null)
            {
                matches = targetCell.GetMatchableNeighbors(false);
            }
            else
            {
                matches = new HashSet<TCell>();
                List<CellPosition> validNeighborsPositions = cellPosition.GetDPadValidNeighbors(RowsCount, ColumnsCount, NeighborsOrientation);
                foreach (CellPosition position in validNeighborsPositions)
                {
                    TCell validNeighbor = this[position];
                    if (validNeighbor != null)
                    {
                        matches.UnionWith(validNeighbor.GetMatchableNeighbors(true));
                    }
                }
            }

            return matches;
        }
        
        virtual public HashSet<TCell> Fill(IMatrixCellFactory<TCell, TTile> matrixCellFactory, bool autoConnect, int repetitionsCountAllowed)
        {
            var allNewCells = new HashSet<TCell>();
            while (Count < Capacity)
            {
                CellPosition targetCell = availablePositions.First();
                HashSet<TCell> matchableNeighbors = GetMatchesOf(targetCell);
                
                TCell newCell = matrixCellFactory.Create(matchableNeighbors, targetCell, repetitionsCountAllowed);
                Add(newCell, targetCell);
                allNewCells.Add(newCell);

                if (autoConnect)
                {
                    ConnectCellWithItsNeighbors(newCell);
                }
            }
            
            Dispatcher.Dispatch<XInLineFillCompleteEvent<TCell, TTile>>(ev =>
            {
                ev.CellsAdded = allNewCells;
            });

            return allNewCells;
        }
        
        override public HashSet<TCell> Fill(CellCreatorDelegate cellCreator, bool autoConnect)
        {
            var allNewCells = new HashSet<TCell>();
            while (Count < Capacity)
            {
                CellPosition targetCellPosition = availablePositions.First();
                HashSet<TCell> matchableNeighbors = GetMatchesOf(targetCellPosition);

                TCell newCell = cellCreator.Invoke(matchableNeighbors, targetCellPosition);
                Add(newCell, targetCellPosition);
                allNewCells.Add(newCell);

                if (autoConnect)
                {
                    ConnectCellWithItsNeighbors(newCell);
                }
            }
            
            Dispatcher.Dispatch<XInLineFillCompleteEvent<TCell, TTile>>(ev =>
            {
                ev.CellsAdded = allNewCells;
            });

            return allNewCells;
        }
        
        override public void ConnectAllCells()
        {
            //To avoid multiples calls to the same property.
            int rowsCount = RowsCount;
            int columnsCount = ColumnsCount;
            
            for (var row = 0; row < rowsCount; row++)
            {
                for (var column = 0; column < columnsCount; column++)
                {
                    var cellPosition = new CellPosition(row, column);
                    ConnectCellWithItsNeighbors(this[cellPosition]);
                }
            }
        }

        override public void ConnectCellWithItsNeighbors(TCell cell)
        {
            CellPosition cellPosition = cell.Position;
            List<CellPosition> validPositions = cellPosition.GetDPadValidNeighbors(RowsCount, ColumnsCount, NeighborsOrientation);
            foreach (CellPosition position in validPositions)
            {
                TCell neighbor = this[position.Row, position.Column];
                    
                if (neighbor != null && !cell.DirectNeighbors.Contains(neighbor))
                {
                    cell.Connect(neighbor);
                }
            }
            
            Dispatcher.Dispatch<XInLineCellConnectedEvent<TCell, TTile>>(ev =>
            {
                ev.CellConnected = cell;
            });
        }

        override public void Add(HashSet<TCell> cells)
        {
            if (Count < Capacity)
            {
                foreach (TCell cell in cells)
                {
                    Add(cell);
                }
            }
        }

        public void Add(IEnumerable<TCell> cells, bool useItsPositions)
        {
            if (Count < Capacity)
            {
                foreach (TCell cell in cells)
                {
                    if (useItsPositions)
                    {
                        Add(cell, cell.Position);
                    }
                    else
                    {
                        Add(cell);
                    }
                }
            }
        }
        
        override public void Add(TCell cell)
        {
            if (Count < Capacity)
            {
                CellPosition position = availablePositions.First();
                Add(cell, position);
            }
        }

        public void Add(TCell cell, CellPosition position)
        {
            AllCells.Add(cell);
            cell.Position = position;
            this[position] = cell;
            availablePositions.Remove(position);
            cell.AddListener<XInLineCellPositionChangedEvent<TCell, TTile>>(OnCellPositionChanged);

            Dispatcher.Dispatch<XInLineCellAddedEvent<TCell, TTile>>(ev =>
            {
                ev.Cell = cell;
            });
        }

        private void OnCellPositionChanged(XInLineCellPositionChangedEvent<TCell, TTile> cellPositionChanged)
        {
            TCell targetCell = cellPositionChanged.targetCell;
            CellPosition lastPosition = cellPositionChanged.lastPosition;
            Dispatcher.Dispatch<XInLineCellPositionChangedEvent<TCell, TTile>>(ev =>
            {
                ev.targetCell = targetCell;
                ev.lastPosition = lastPosition;
            });
        }

        override public void Remove(TCell cell, TCell killer = null)
        {
            base.Remove(cell, killer);
            
            this[cell.Position] = null;
            availablePositions.Add(cell.Position);
            cell.RemoveListener<XInLineCellPositionChangedEvent<TCell, TTile>>(OnCellPositionChanged);

            Dispatcher.Dispatch<XInLineCellRemovedEvent<TCell, TTile>>(ev =>
            {
                ev.RemovedCell = cell;
            });
        }

        virtual public HashSet<TCell> RemoveAndMove(HashSet<TCell> cells, DPadOrientation moveTo, TCell killer = null)
        {
            Remove(cells, killer);

            DPadOrientation oppositeOrientation = moveTo.Opposite();
            CellPosition oppositeCellDirection = oppositeOrientation.ToCellDirection();
            Queue<CellPosition> emptyCellsWithSomethingOver = GetEmptyCellsWithSomethingOver(oppositeCellDirection);
            var dirtyCells = new HashSet<TCell>();

            do
            {
                while (emptyCellsWithSomethingOver.Count > 0)
                {
                    CellPosition backupPosition = emptyCellsWithSomethingOver.Dequeue();
                    TCell backupCell = this[backupPosition + oppositeCellDirection];
                    
                    backupCell.DisconnectAll();
                    availablePositions.Add(backupCell.Position);
                    this[backupCell.Position] = null;

                    backupCell.IsMoving = true;
                    backupCell.Position = backupPosition;
                    backupCell.IsMoving = false;
                    this[backupPosition] = backupCell;
                    availablePositions.Remove(backupPosition);
                    
                    ConnectCellWithItsNeighbors(backupCell);
                    
                    dirtyCells.Add(backupCell);
                }
                
                emptyCellsWithSomethingOver = GetEmptyCellsWithSomethingOver(oppositeCellDirection);
            } while (emptyCellsWithSomethingOver.Count > 0);
            
            Dispatcher.Dispatch<XInLineCellsRemovedAndMovedEvent<TCell, TTile>>(ev =>
            {
                ev.MovedCells = dirtyCells;
            });
            
            return dirtyCells;
        }

        private Queue<CellPosition> GetEmptyCellsWithSomethingOver(CellPosition cellDirection)
        {
            List<CellPosition> emptyCellWithElementsOver = availablePositions.ToList();
            var emptyCellsAfterCheck = new Queue<CellPosition>();
            for (int i = emptyCellWithElementsOver.Count - 1; i >= 0; i--)
            {
                CellPosition currentPosition = emptyCellWithElementsOver[i] + cellDirection;
                if (IsValidPosition(currentPosition) && this[currentPosition] != null)
                {
                    emptyCellsAfterCheck.Enqueue(emptyCellWithElementsOver[i]);
                }
            }

            return emptyCellsAfterCheck;
        }
        
        public List<TCell> GetAllNeighborsOf(TCell cell)
        {
            List<CellPosition> validNeighborsPositions = cell.Position.GetDPadValidNeighbors(RowsCount, ColumnsCount, NeighborsOrientation);

            return validNeighborsPositions.Select(position => matrix[position.Row, position.Column]).Where(neighbor => neighbor != null).ToList();
        }

        public bool IsValidPosition(CellPosition position)
        {
            return position.Row >= 0 &&
                   position.Row < RowsCount &&
                   position.Column >= 0 &&
                   position.Column < ColumnsCount;
        }
        
        public HashSet<TCell> GetMatchesByOrientation(CellPosition position, DPadOrientation orientation)
        {
            Dictionary<DPadOrientation,HashSet<TCell>> matchesGroup = GetMatchesGroupByOrientation(position, orientation);
            var totalMatches = new HashSet<TCell>();
            foreach (KeyValuePair<DPadOrientation,HashSet<TCell>> group in matchesGroup)
            {
                totalMatches.UnionWith(group.Value);
            }

            return totalMatches;
        }

        public HashSet<TCell> GetMatchesBySelectedOrientation(CellPosition cellPosition)
        {
            return GetMatchesByOrientation(cellPosition, NeighborsOrientation);
        }

        public Dictionary<DPadOrientation, HashSet<TCell>> GetMatchesGroupByOrientation(CellPosition position, DPadOrientation orientation)
        {
            DPadOrientation[] allOrientations = orientation.BreakDown();

            var matches = new Dictionary<DPadOrientation, HashSet<TCell>>();
            foreach (DPadOrientation currentOrientation in allOrientations)
            {
                TCell targetCell = this[position];
                
                var currentMatchesList = new HashSet<TCell>();
                matches.Add(currentOrientation, currentMatchesList);
                
                while (targetCell.NeighborsByOrientation.TryGetValue(currentOrientation, out TCell neighbor) &&
                       targetCell.IsMatchableWith(neighbor))
                {
                    currentMatchesList.Add(neighbor);
                    targetCell = neighbor;
                }
            }

            return matches;
        }

        public Dictionary<DPadOrientation, HashSet<TCell>> GetMatchesGroupBySelectedOrientation(CellPosition position)
        {
            return GetMatchesGroupByOrientation(position, NeighborsOrientation);
        }
        
        public HashSet<TCell> SearchAndGetMatches(CellPosition targetCellPosition, int targetCount)
        {
            //TODO: THIS ALGORITHM
            TCell cell = this[targetCellPosition];

            Dictionary<DPadOrientation, HashSet<TCell>> matchesGroup = GetMatchesGroupBySelectedOrientation(cell.Position);
            var horizontalMatches = new HashSet<TCell>();
            horizontalMatches.UnionWith(matchesGroup[DPadOrientation.Left]);
            horizontalMatches.UnionWith(matchesGroup[DPadOrientation.Right]);
            horizontalMatches.Add(cell);
                
            var verticalMatches = new HashSet<TCell>();
            verticalMatches.UnionWith(matchesGroup[DPadOrientation.Down]);
            verticalMatches.UnionWith(matchesGroup[DPadOrientation.Up]);
            verticalMatches.Add(cell);
                
            var totalMatches = new HashSet<TCell>();

            if (horizontalMatches.Count >= targetCount)
            {
                totalMatches.UnionWith(horizontalMatches);
            }

            if (verticalMatches.Count >= targetCount)
            {
                totalMatches.UnionWith(verticalMatches);
            }

            return totalMatches;
        }

        override public void Swap(TCell cellA, TCell cellB)
        {
            cellA.DisconnectAll();
            cellB.DisconnectAll();

            (cellA.Position, cellB.Position) = (cellB.Position, cellA.Position);

            this[cellA.Position] = cellA;
            this[cellB.Position] = cellB;
            
            ConnectCellWithItsNeighbors(cellA);
            ConnectCellWithItsNeighbors(cellB);
            
            Dispatcher.Dispatch<XInLineSwapEvent<TCell, TTile>>(ev =>
            {
                ev.A = cellA;
                ev.B = cellB;
            });
        }
    }
}