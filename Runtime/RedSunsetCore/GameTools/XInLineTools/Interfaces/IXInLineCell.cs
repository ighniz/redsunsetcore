using System.Collections.Generic;
using RedSunsetCore.DesignPatterns.BuilderPattern.Product;

namespace RedSunsetCore.GameTools.XInLineTools.Interfaces
{
    public interface IXInLineCell<TCell, TTile> : IProduct
        where TCell : class, IXInLineCell<TCell, TTile>
        where TTile : class, IXInLineTileComponent<TTile>
    {
        HashSet<TTile> AllTiles { get; }
        /// <summary>
        ///     Gets only directs neighbors.
        /// </summary>
        HashSet<TCell> DirectNeighbors { get; }
        TCell Killer { get; }
        bool IsMoving { get; }
        bool IsPopped { get; }
        HashSet<TCell> GetMatchableNeighbors(bool includeItself);
        int GetDistanceTo(TCell cell);
        bool IsMatchableWith(TCell cell);
        void AddTile(TTile tile);
        void RemoveTile(TTile tile);
        void RemoveTiles(HashSet<TTile> tiles);
        void RemoveAllTiles();
        void Pop(TCell killer = null);
        void Connect(TCell cell, bool mutual = true);
        void Connect(HashSet<TCell> neighbors, bool mutual = true);
        void Disconnect(TCell cell, bool mutual = true);
        void DisconnectAll(bool mutual = true);
    }
}