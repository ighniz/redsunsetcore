namespace RedSunsetCore.GameTools.XInLineTools.Interfaces
{
    public interface IXInLineTileComponent<in TTile>
        where TTile : IXInLineTileComponent<TTile>
    {
        bool IsMatchableWith(TTile other);
        void OnCellPopped();
    }
}