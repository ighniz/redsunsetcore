using System.Collections.Generic;
using RedSunsetCore.GameTools.Orientation;
using RedSunsetCore.GameTools.XInLineTools.Matrix;

namespace RedSunsetCore.GameTools.XInLineTools.Interfaces
{
    public interface IXInlineMatrix<TCell, TTile, TConnection> : IXInLineGraph<TCell, TTile, TConnection>
        where TCell : class, IXInLineCell<TCell, TTile>
        where TTile : class, IXInLineTileComponent<TTile>
        where TConnection : class, ICellConnection<TCell, TTile>
    {
        HashSet<TCell> GetMatchesOf(CellPosition cellPosition);
        void Add(IEnumerable<TCell> cells, bool useItsPositions);
        void Add(TCell cell, CellPosition position);
        HashSet<TCell> RemoveAndMove(HashSet<TCell> cells, DPadOrientation moveTo, TCell killer = null);
        List<TCell> GetAllNeighborsOf(TCell cell);
        bool IsValidPosition(CellPosition position);
        HashSet<TCell> GetMatchesByOrientation(CellPosition position, DPadOrientation orientation);
        HashSet<TCell> GetMatchesBySelectedOrientation(CellPosition cellPosition);
        Dictionary<DPadOrientation, HashSet<TCell>> GetMatchesGroupByOrientation(CellPosition position, DPadOrientation orientation);
        Dictionary<DPadOrientation, HashSet<TCell>> GetMatchesGroupBySelectedOrientation(CellPosition position);
        HashSet<TCell> SearchAndGetMatches(CellPosition targetCellPosition, int targetCount);
    }
}