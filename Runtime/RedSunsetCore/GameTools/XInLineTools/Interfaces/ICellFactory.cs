using RedSunsetCore.DesignPatterns.FactoryPattern;

namespace RedSunsetCore.GameTools.XInLineTools.Interfaces
{
    public interface ICellFactory<out TCell, TTile> : IFactory<TCell>
        where TCell : class, IXInLineCell<TCell, TTile>
        where TTile : class, IXInLineTileComponent<TTile>
    {
        
    }
}