using System;
using System.Collections.Generic;

namespace RedSunsetCore.GameTools.XInLineTools.Interfaces
{
    public interface IXInLineGraph<TCell, TTile, TConnection>
        where TCell : class, IXInLineCell<TCell, TTile>
        where TTile : class, IXInLineTileComponent<TTile>
        where TConnection : class, ICellConnection<TCell, TTile>
    {
        int Capacity { get; set; }
        int Count { get; }
        HashSet<TCell> AllCells { get; }
        Dictionary<TCell, TConnection> ConnectionsMap { get; }
        HashSet<TCell> GetMatches();
        HashSet<TCellFilter> GetMatchesOf<TCellFilter>(bool includeItSelf=false) where TCellFilter : TCell;
        HashSet<TCell> GetMatchesOf(TCell cell, bool includeItSelf=false);
        void ConnectAllCells();
        void ConnectCellWithItsNeighbors(TCell cell);
        void Add(TCell cell);
        void Add(HashSet<TCell> cells);
        void Remove(TCell cell, TCell killer = null);
        void Remove(HashSet<TCell> cells, TCell killer = null);
        HashSet<TCell> Fill(ICellFactory<TCell, TTile> generator, bool autoConnect);
        void Fill(Func<TCell> generator);
        void Swap(TCell cellA, TCell cellB);
    }
}