using System.Collections.Generic;

namespace RedSunsetCore.GameTools.XInLineTools.Interfaces
{
    public interface ICellConnection<TCell, TTile>
        where TTile : class, IXInLineTileComponent<TTile>
        where TCell : class, IXInLineCell<TCell, TTile>
    {
        HashSet<TCell> ConnectedCells { get; }
    }
}