﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace RedSunsetCore.GameTools.XInLineTools.Reactions
{
    public interface IXInLineReactionMaker
    {
        /// <summary>
        ///     Start a new reaction. Recommended to use into a Coroutine.
        /// </summary>
        /// <param name="reactionableList">Represents a group.</param>
        /// <param name="source">Source that trigger reaction.</param>
        /// <returns></returns>
        IEnumerator StartReaction(IEnumerable<IGrouping<int, IXInlineReactionable>> reactionableList, IXInlineReactionable source);
    }
}