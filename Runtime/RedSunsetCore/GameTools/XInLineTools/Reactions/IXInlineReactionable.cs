﻿using UnityEngine.Events;

namespace RedSunsetCore.GameTools.XInLineTools.Reactions
{
    public interface IXInlineReactionable
    {
        void React(IXInlineReactionable source);
        event UnityAction<IXInlineReactionable> ReactionCompletedEvent;
    }
}