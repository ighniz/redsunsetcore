using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.GameTools.Orientation;
using RedSunsetCore.GameTools.Orientation.ExtensionMethods;
using RedSunsetCore.GameTools.XInLineTools.Matrix;

namespace RedSunsetCore.GameTools.XInLineTools.ExtensionMethods
{
    static public class XInLineMatrixExtensionMethods
    {
        static public readonly Dictionary<DPadOrientation, CellPosition> DPadDirectionsMap;

        static XInLineMatrixExtensionMethods()
        {
            DPadDirectionsMap = new Dictionary<DPadOrientation, CellPosition>
            {
                {DPadOrientation.Down,      CellPosition.Down},
                {DPadOrientation.DownLeft,  CellPosition.DownLeft},
                {DPadOrientation.Left,      CellPosition.Left},
                {DPadOrientation.UpLeft,    CellPosition.UpLeft},
                {DPadOrientation.Up,        CellPosition.Up},
                {DPadOrientation.UpRight,   CellPosition.UpRight},
                {DPadOrientation.Right,     CellPosition.Right},
                {DPadOrientation.DownRight, CellPosition.DownRight}
            };
        }
        
        static public IEnumerable<CellPosition> GetDPadNeighbors(this CellPosition  source, DPadOrientation orientations)
        {
            DPadOrientation[] splitOrientation = orientations.BreakDown();
            
            var result = new CellPosition[splitOrientation.Length];
            for (var i = 0; i < result.Length; i++)   
            {
                result[i] = source + DPadDirectionsMap[splitOrientation[i]];
            }

            return result;
        }

        static public List<CellPosition> GetDPadValidNeighbors(this CellPosition source, int rowsCount, int columnsCount, DPadOrientation orientations)
        {
            List<CellPosition> dPadNeighbors = source.GetDPadNeighbors(orientations).ToList();
            var result = new CellPosition[dPadNeighbors.Count];
            for (int i = dPadNeighbors.Count-1; i >= 0 ; i--)
            {
                CellPosition position = dPadNeighbors[i];
                if (position.Row < 0 || position.Column < 0 ||
                    position.Row >= rowsCount || position.Column >= columnsCount)
                {
                    dPadNeighbors.RemoveAt(i);
                }
            }

            return dPadNeighbors;
        }
    }
}