using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.Core;
using RedSunsetCore.GameTools.XInLineTools.Events;
using RedSunsetCore.GameTools.XInLineTools.Interfaces;
using RedSunsetCore.GameTools.XInLineTools.Matrix;
using RedSunsetCore.Services.Event.Interfaces;
using Zenject;

namespace RedSunsetCore.GameTools.XInLineTools
{
    abstract public class AbstractXInLineGraph<TCell, TTile, TConnection> : RedSunsetCoreObject, IXInLineGraph<TCell, TTile, TConnection>, IEnumerable<TCell>, IObjectEventDispatcher
        where TCell : class, IXInLineCell<TCell, TTile>
        where TTile : class, IXInLineTileComponent<TTile>
        where TConnection : class, ICellConnection<TCell, TTile>
    {
        public delegate TCell CellCreatorDelegate(HashSet<TCell> matchableNeighbors, CellPosition targetCellPosition);
        
        virtual public int Capacity { get; set; }
        virtual public int Count => AllCells.Count;
        virtual public HashSet<TCell> AllCells { get; } = new HashSet<TCell>();
        public Dictionary<TCell, TConnection> ConnectionsMap { get; } = new Dictionary<TCell, TConnection>();

        [Inject]
        public IEventDispatcher Dispatcher { get; }
        
        virtual public HashSet<TCell> GetMatches()
        {
            throw new NotImplementedException();
        }

        virtual public HashSet<TCellFilter> GetMatchesOf<TCellFilter>(bool includeItSelf=false) where TCellFilter : TCell
        {
            throw new NotImplementedException();
        }

        virtual public HashSet<TCell> GetMatchesOf(TCell cell, bool includeItSelf=false)
        {
            return cell.GetMatchableNeighbors(includeItSelf);
        }

        abstract public void ConnectAllCells();
        abstract public void ConnectCellWithItsNeighbors(TCell cell);

        virtual public void Add(TCell cell)
        {
            if (Count < Capacity)
            {
                AllCells.Add(cell);
                Dispatcher.Dispatch<XInLineCellAddedEvent<TCell, TTile>>(ev =>
                {
                    ev.Cell = cell;
                });
            }
        }

        virtual public void Add(HashSet<TCell> cells)
        {
            if (Count < Capacity)
            {
                AllCells.UnionWith(cells);
                foreach (TCell addedCell in cells)
                {
                    Dispatcher.Dispatch<XInLineCellAddedEvent<TCell, TTile>>(ev =>
                    {
                        ev.Cell = addedCell;
                    });
                }
            }
        }

        virtual public void Remove(TCell cell, TCell killer = null)
        {
            AllCells.Remove(cell);
            cell.DisconnectAll();
            cell.Pop(killer);
        }

        virtual public void Remove(HashSet<TCell> cells, TCell killer = null)
        {
            foreach (TCell targetCell in cells.OrderByDescending(cell => cell.AllTiles.Count))
            {
                Remove(targetCell, killer);
            }
        }

        virtual public HashSet<TCell> Fill(ICellFactory<TCell, TTile> generator, bool autoConnect)
        {
            var allNewCells = new HashSet<TCell>();
            while (Count < Capacity)
            {
                TCell newCell = generator.Create();
                Add(newCell);
                allNewCells.Add(newCell);
            }

            if (autoConnect)
            {
                foreach (TCell newCell in allNewCells)
                {
                    ConnectCellWithItsNeighbors(newCell);
                }
            }

            Dispatcher.Dispatch<XInLineFillCompleteEvent<TCell, TTile>>(ev =>
            {
                ev.CellsAdded = allNewCells;
            });
            
            return allNewCells;
        }

        virtual public void Fill(Func<TCell> generator)
        {
            var allNewCells = new HashSet<TCell>();
            while (Count < Capacity)
            {
                Add(generator.Invoke());
            }
            
            Dispatcher.Dispatch<XInLineFillCompleteEvent<TCell, TTile>>(ev =>
            {
                ev.CellsAdded = allNewCells;
            });
        }

        abstract public HashSet<TCell> Fill(CellCreatorDelegate cellCreator, bool autoConnect);

        abstract public void Swap(TCell cellA, TCell cellB);

        public IEnumerator<TCell> GetEnumerator()
        {
            return ((IEnumerable<TCell>) AllCells).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}