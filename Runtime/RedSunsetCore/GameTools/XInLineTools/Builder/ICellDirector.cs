using System.Collections.Generic;
using RedSunsetCore.DesignPatterns.BuilderPattern.Director;
using RedSunsetCore.GameTools.XInLineTools.Interfaces;
using RedSunsetCore.GameTools.XInLineTools.Matrix;

namespace RedSunsetCore.GameTools.XInLineTools.Builder
{
    public interface ICellDirector<TCell, TTile> : IDirector<TCell>
        where TCell : class, IXInLineCell<TCell, TTile>
        where TTile : class, IXInLineTileComponent<TTile> 
    {
        TCell Build(HashSet<TCell> futureNeighbors, CellPosition position, int repetitionsCountAllowed);
    }
}