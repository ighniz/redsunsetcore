namespace RedSunsetCore.GameTools.XInLineTools.Enums
{
    public enum XInLineMatrixState
    {
        Empty,
        AvailableStorage,
        Busy,
        Full
    }
}