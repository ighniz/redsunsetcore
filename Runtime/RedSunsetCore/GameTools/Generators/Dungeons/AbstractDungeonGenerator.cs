using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class AbstractDungeonGenerator : MonoBehaviour
{
	[SerializeField]
	protected TilemapVisualizer tilemapVisualizer = null;

	[SerializeField]
	protected Vector2Int startPosition = Vector2Int.zero;

	public void GenerateDungeon()
	{
		tilemapVisualizer.Clear();
		/*StartCoroutine(RunProceduralGeneration());*/
		RunProceduralGeneration();
	}

	abstract protected IEnumerator RunProceduralGeneration();
}