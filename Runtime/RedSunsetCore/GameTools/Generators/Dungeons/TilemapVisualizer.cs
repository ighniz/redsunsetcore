using System;
using System.Collections;
using System.Collections.Generic;
using RedSunsetCore.GameTools.Generators.Dungeons;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapVisualizer : MonoBehaviour
{
	[SerializeField]
	private Tilemap floorTilemap, wallTilemap;
	[SerializeField]
	private TileBase floorTile,
		wallTop,
		wallSideRight,
		wallSideLeft,
		wallBottom,
		wallFull,
		wallInnerCornerDownLeft,
		wallInnerCornerDownRight,
		wallDiagonalCornerDownRight,
		wallDiagonalCornerDownLeft,
		wallDiagonalCornerUpRight,
		wallDiagonalCornerUpLeft;

	public void PaintFloorTiles(IEnumerable<Vector2Int> floorPositions)
	{
		//StartCoroutine(PaintTiles(floorPositions, floorTilemap, floorTile));
		PaintTiles(floorPositions, floorTilemap, floorTile);
	}

	private IEnumerator PaintTiles(IEnumerable<Vector2Int> positions, Tilemap tilemap, TileBase tile)
	{
		foreach (Vector2Int pos in positions) PaintSingleTile(tilemap, tile, pos);
		//yield return null;
		return null;
	}

	private void PaintSingleTile(Tilemap tilemap, TileBase tile, Vector2Int pos)
	{
		Vector3Int tilePosition = tilemap.WorldToCell((Vector3Int)pos);
		tilemap.SetTile(tilePosition, tile);
	}

	public void Clear()
	{
		floorTilemap.ClearAllTiles();
		wallTilemap.ClearAllTiles();
	}

	public void PaintSingleBasicWall(Vector2Int position, string binaryType)
	{
		var typeAsInt = Convert.ToInt32(binaryType, 2);
		TileBase tile = null;
		if (WallTypesHelper.WallTop.Contains(typeAsInt))
			tile = wallTop;
		else if (WallTypesHelper.WallSideRight.Contains(typeAsInt))
			tile = wallSideRight;
		else if (WallTypesHelper.WallSideLeft.Contains(typeAsInt))
			tile = wallSideLeft;
		else if (WallTypesHelper.WallBottom.Contains(typeAsInt))
			tile = wallBottom;
		else if (WallTypesHelper.WallFull.Contains(typeAsInt)) tile = wallFull;

		if (tile != null)
			PaintSingleTile(wallTilemap, tile, position);
		else
			Debug.Log($"[PaintSingleBasicWall] No tile at: {position}. binaryType is {binaryType}.");
	}

	public void PaintSingleCornerWall(Vector2Int position, string binaryType)
	{
		var typeAsInt = Convert.ToInt32(binaryType, 2);
		TileBase tile = null;

		if (WallTypesHelper.WallInnerCornerDownLeft.Contains(typeAsInt))
			tile = wallInnerCornerDownLeft;
		else if (WallTypesHelper.WallInnerCornerDownRight.Contains(typeAsInt))
			tile = wallInnerCornerDownRight;
		else if (WallTypesHelper.WallDiagonalCornerDownLeft.Contains(typeAsInt))
			tile = wallDiagonalCornerDownLeft;
		else if (WallTypesHelper.WallDiagonalCornerDownRight.Contains(typeAsInt))
			tile = wallDiagonalCornerDownRight;
		else if (WallTypesHelper.WallDiagonalCornerUpRight.Contains(typeAsInt))
			tile = wallDiagonalCornerUpRight;
		else if (WallTypesHelper.WallDiagonalCornerUpLeft.Contains(typeAsInt))
			tile = wallDiagonalCornerUpLeft;
		else if (WallTypesHelper.WallFullEightDirections.Contains(typeAsInt))
			tile = wallFull;
		else if (WallTypesHelper.WallBottomEightDirections.Contains(typeAsInt)) tile = wallBottom;

		if (tile != null)
			PaintSingleTile(wallTilemap, tile, position);
		else
			Debug.Log($"[PaintSingleCornerWall] No tile at: {position}. binaryType is {binaryType}.");
	}
}