using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.GameTools.Generators.Dungeons.Config;
using RedSunsetCore.GameTools.Generators.Dungeons.Data;
using RedSunsetCore.GameTools.Generators.Dungeons.Enums;
using RedSunsetCore.GameTools.Generators.Dungeons.ExtensionMethods;
using RedSunsetCore.GameTools.Generators.Dungeons.Interfaces;
using UnityEngine;
using UnityEngine.Assertions;

namespace RedSunsetCore.GameTools.Generators.Dungeons
{
	public class DungeonGeneratorFactory : IDungeonGenerator
	{ 
		private DungeonGenerationConfig mConfig;
		readonly private Dictionary<Vector2Int, DungeonTile> mFloorPositions = new(); //This should be a dictionary because we need to update the references in every part of the process. If we use a HashSet the new reference on the same position will not be added.
		readonly private Dictionary<Vector2Int, DungeonTile> mAllRoomTiles = new();
		readonly private List<Dictionary<Vector2Int, DungeonTile>> mAllCorridorTiles = new();
		readonly private Dictionary<Vector2Int, DungeonEntrance> mAllEntrancesTiles = new();
		readonly private Dictionary<Guid, DungeonRoom> mAllRooms = new();
		readonly private Dictionary<Guid, DungeonCorridor> mAllCorridors = new();
		readonly private Dictionary<Guid, DungeonEntrance> mAllEntrances = new();
		readonly private Dictionary<Vector2Int, DungeonItem> mAllItems = new();
		private System.Random randomGenerator;
		private int biggerRoomTileCount;

		public Dictionary<Guid, DungeonRoom> AllRooms => mAllRooms;
		public Dictionary<Guid, DungeonCorridor> AllCorridors => mAllCorridors;
		public Dictionary<Guid, DungeonEntrance> AllEntrances => mAllEntrances;
		public DungeonRoom BiggerRoom { get; private set; }
		
		public IDungeonGenerator ProcessMod(IDungeonGeneratorMod mod)
		{
			return this;
		}

		public IDungeonGenerator GenerateCorridorsFirst()
		{
			randomGenerator = new System.Random(mConfig.Seed.GetHashCode());

			ClearData();
			HashSet<DungeonTile> potentialRoomPositions = new();

			CreateCorridors(potentialRoomPositions);
			CreateRooms(potentialRoomPositions);

			List<DungeonTile> deadEnds = FindAllDeadEnds(mFloorPositions);
			CreateRoomsAtDeadEnds(deadEnds);

			CleanEmptyRooms();

			ConnectTiles();
			SetSidesAndPossibleRooms();
			VerifyCorridors();
			CollectAllEntrances();
			CreateItems();
			return this;
		}

		private void CreateItems()
		{
			foreach ((Guid id, DungeonRoom room) in mAllRooms)
			{
				var sidesCandidates = new [] { DungeonPointSide.Left, DungeonPointSide.Right, DungeonPointSide.Bottom, DungeonPointSide.Top };
				List<DungeonTile> tilesCandidates = room.Where(x => x.PointSide.HasJustOneOf(sidesCandidates)).ToList();
				if (tilesCandidates.Count > 0)
				{
					//TODO: Generates items based on certain parameters.
					DungeonTile tile = tilesCandidates[randomGenerator.Next(0, tilesCandidates.Count)];
					var item = new DungeonItem();
					tile.AddItem(item);
					mAllItems[tile.Position] = item;
				}
			}
		}

		public IEnumerator<DungeonTile> GetEnumerator()
		{
			foreach ((Vector2Int _, DungeonTile tile) in mFloorPositions) yield return tile;
		}

		public IDungeonGenerator WithConfig(DungeonGenerationConfig config)
		{
			mConfig = config;
			return this;
		}

		public void ClearData()
		{
			mFloorPositions.Clear();
			mAllEntrancesTiles.Clear();
			mAllCorridorTiles.Clear();

			mAllRoomTiles.Clear();
			mAllCorridors.Clear();
			mAllEntrances.Clear();
			mAllRooms.Clear();
			mAllRooms.Add(Guid.Empty, new DungeonRoom(Guid.Empty));
		}

		private void CleanEmptyRooms()
		{
			foreach ((Guid id, DungeonRoom room) in new Dictionary<Guid, DungeonRoom>(mAllRooms))
			{
				if (room.TileCount == 0)
				{
					mAllRooms.Remove(id);
				}
			}
		}

		private void ConnectTiles()
		{
			foreach ((Vector2Int position, DungeonTile tile) in mFloorPositions)
			{
				foreach (Vector2Int direction in Directions2D.EightDirectionsList)
				{
					if (mFloorPositions.TryGetValue(position + direction, out DungeonTile neighbor))
					{
						tile.AddNeighbor(neighbor, direction);
					}
				}
			}
		}

		private void SetSidesAndPossibleRooms()
		{
			var floorPositionsCopy = new Dictionary<Vector2Int, DungeonTile>(mFloorPositions);
			foreach ((Vector2Int _, DungeonTile tile) in floorPositionsCopy)
			{
				var neighborBinaryType = 0;
				for (var index = 0; index < Directions2D.EightDirectionsList.Count; index++)
				{
					Vector2Int direction = Directions2D.EightDirectionsList[index];
					Vector2Int neighborPosition = tile.Position + direction;
					if (tile.Neighbors.Any(x => x.Value.Position == neighborPosition))
					{
						neighborBinaryType |= 1 << index;
					}
				}

				tile.PointSide = (DungeonPointSide)neighborBinaryType;
				if ((tile.PointSide & DungeonPointSide.All) == DungeonPointSide.All || tile.PointType == DungeonPointType.Room)
				{
					var bestNeighbor = tile.Neighbors.FirstOrDefault(x => x.Value.RoomId != Guid.Empty).Value;
					if (bestNeighbor != null)
					{
						RegisterRoomTile(bestNeighbor.RoomId, tile);
					}
				}
			}
			UpdateCorridorList();
		}

		private void VerifyCorridors()
		{
			var corridorsForConversion = new List<DungeonTile>();
			foreach (Dictionary<Vector2Int,DungeonTile> corridor in mAllCorridorTiles)
			{
				foreach ((Vector2Int _, DungeonTile point) in corridor)
				{
					if (IsThereAtLeastOne(point.Neighbors, DungeonPointType.Room))
					{
						//We need to collect all first, because if we convert them every pass we are going to convert all at once.
						corridorsForConversion.Add(point);
					}
				}
			}

			foreach (DungeonTile tile in corridorsForConversion)
			{
				var bestNeighbor = tile.Neighbors.FirstOrDefault(x => x.Value.RoomId != Guid.Empty).Value;
				Assert.IsNotNull(bestNeighbor, "The tile should has at least one neighbor!");
				UnregisterCorridorTiles(tile);
				RegisterRoomTile(bestNeighbor.RoomId, tile);
			}

			UpdateCorridorList();

			/*foreach (DungeonPoint point in mFloorPositions)
			{
				if (point.PointType == DungeonPointType.Corridor)
				{
					if (point.Neighbors.Count(x => x.Value.PointType == DungeonPointType.Room) == 1 &&
					    point.Neighbors.All(x => x.Value.PointType != DungeonPointType.Entrance))
					{
						point.PointType = DungeonPointType.Entrance;
					}else if (point.Neighbors.Any(x => x.Value.PointType == DungeonPointType.Room))
					{
						//point.PointType = DungeonPointType.Room;
					}
				}else if (point.Neighbors.Any(x => x.Value.PointType == DungeonPointType.Room))
				{
					point.PointType = DungeonPointType.Wall;
				}
			}*/
		}

		private void CollectAllEntrances()
		{
			foreach (Dictionary<Vector2Int,DungeonTile> allCorridors in mAllCorridorTiles)
			{
				foreach ((Vector2Int _, DungeonTile tile) in allCorridors)
				{
					if (tile.Neighbors.Count(x => x.Value.PointType == DungeonPointType.Room) == 1 &&
					    tile.Neighbors.All(x => x.Value.PointType != DungeonPointType.Entrance))
					{
						UnregisterCorridorTiles(tile);
						RegisterEntrance(tile);
					}
				}
			}

			UpdateCorridorList();
		}

		private bool IsThereAtLeastOne(Dictionary<Vector2Int, DungeonTile> tiles, DungeonPointType type)
		{
			return tiles.Any(x => x.Value.PointType == type);
		}

		private bool AreAll(Dictionary<Vector2Int, DungeonTile> tiles, DungeonPointType type)
		{
			return tiles.All(x => x.Value.PointType == type);
		}

		private void RegisterRoomTile(Guid roomId, DungeonTile tile)
		{
			mFloorPositions[tile.Position] = tile;
			tile.PointType = DungeonPointType.Room;
			tile.RoomId = roomId;
			mAllRoomTiles[tile.Position] = tile;
			if (!mAllRooms.TryGetValue(roomId, out DungeonRoom room))
			{
				room = new DungeonRoom(roomId);
				mAllRooms.Add(roomId, room);
			}
			room.AddTile(tile);

			if (room.TileCount > biggerRoomTileCount)
			{
				biggerRoomTileCount = room.TileCount;
				BiggerRoom = room;
			}
		}

		private void RegisterRoom(Guid roomId, IEnumerable<DungeonTile> tiles)
		{
			foreach (DungeonTile dungeonTile in tiles)
			{
				RegisterRoomTile(roomId, dungeonTile);
			}
		}

		private void RegisterCorridor(Guid corridorId, Dictionary<Vector2Int, DungeonTile> rawCorridorData)
		{
			if (!mAllCorridors.TryGetValue(corridorId, out DungeonCorridor corridor))
			{
				corridor = new DungeonCorridor(corridorId);
				mAllCorridors.Add(corridorId, corridor);
			}

			foreach ((Vector2Int position, DungeonTile tile) in rawCorridorData)
			{
				UnregisterCorridorTiles(tile);
				mFloorPositions[position] = tile;
				corridor.AddTile(tile);
			}
			mAllCorridorTiles.Add(rawCorridorData);
		}

		private void UnregisterCorridorTiles(params DungeonTile[] tiles)
		{
			foreach (DungeonTile tile in tiles)
			{
				mFloorPositions.Remove(tile.Position);
				tile.CorridorId = Guid.Empty;
				foreach ((Guid id, DungeonCorridor corridor) in new Dictionary<Guid, DungeonCorridor>(mAllCorridors))
				{
					if (corridor.TryRemoveTile(tile))
					{
						if (corridor.TileCount == 0)
						{
							mAllCorridors.Remove(id);
						}
						break;
					}
				}
			}
		}

		private void RegisterEntrance(DungeonTile tile)
		{
			mFloorPositions[tile.Position] = tile;
			tile.PointType = DungeonPointType.Entrance;
			List<KeyValuePair<Vector2Int, DungeonTile>> neighbors = tile.Neighbors.ToList();
			Assert.AreEqual(neighbors.Count, 2, "The entrance should have 2 neighbors.");
			var entrance = new DungeonEntrance(tile, neighbors[0].Value, neighbors[1].Value);
			mAllEntrancesTiles[tile.Position] = entrance;
			mAllEntrances[entrance.Id] = entrance;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		private void CreateCorridors(HashSet<DungeonTile> potentialRoomPositions)
		{
			var currentPoint = new DungeonTile(mConfig.StartPosition, DungeonPointType.Corridor);
			potentialRoomPositions.Add(currentPoint);

			for (var i = 0; i < mConfig.CorridorCount; i++)
			{
				HashSet<DungeonTile> corridor = RandomWalkCorridor(currentPoint, mConfig.CorridorLength);
				currentPoint = corridor.Last();
				potentialRoomPositions.Add(currentPoint);

				RegisterCorridor(Guid.NewGuid(), corridor.ToDictionary(x => x.Position));
			}
		}

		private void UpdateCorridorList()
		{
			foreach (Dictionary<Vector2Int,DungeonTile> corridorMap in mAllCorridorTiles)
			{
				int corridorTilesCount = corridorMap.Count;
				
				//If we have only MinCorridorLength or less tiles in this corridor, we transform those tiles into a room.
				//This is to avoid corridors with just one or two tiles. In those cases every corridor could
				//have just one or any entrance.
				if (corridorTilesCount <= mConfig.MinCorridorLength)
				{
					foreach ((Vector2Int _, DungeonTile point) in corridorMap)
					{
						RegisterRoomTile(point.RoomId, point);
					}
				}

				List<Vector2Int> allInvalidCorridors = corridorMap
					.Where(x => x.Value.PointType != DungeonPointType.Corridor)
					.Select(x => x.Key)
					.ToList();

				foreach (Vector2Int invalidCorridor in allInvalidCorridors)
				{
					corridorMap.Remove(invalidCorridor);
				}
			}
		}

		private void CreateRooms(HashSet<DungeonTile> potentialRoomPositions)
		{
			HashSet<DungeonTile> allRoomPositions = new();
			int roomToCreateCount = Mathf.RoundToInt(potentialRoomPositions.Count * mConfig.RoomPercent);

			HashSet<DungeonTile> roomToCreate = potentialRoomPositions.OrderBy(x => randomGenerator.Next()).Take(roomToCreateCount).ToHashSet();

			foreach (DungeonTile roomPosition in roomToCreate)
			{
				roomPosition.PointType = DungeonPointType.Room;
				HashSet<DungeonTile> roomFloor = RunRandomWalk(mConfig, roomPosition);
				RegisterRoom(Guid.NewGuid(), roomFloor.ToArray());
				allRoomPositions.UnionWith(roomFloor);
			}
		}

		private HashSet<DungeonTile> RunRandomWalk(DungeonGenerationConfig randomWalkSo, DungeonTile tile)
		{
			DungeonTile currentTile = tile;
			var floorPositions = new HashSet<DungeonTile>();
			for (var i = 0; i < randomWalkSo.Iterations; i++)
			{
				HashSet<DungeonTile> path = SimpleRandomWalk(currentTile, randomWalkSo.WalkLength, tile.PointType);
				floorPositions.UnionWith(path);
				if (randomWalkSo.StartRandomlyEachIteration) currentTile = floorPositions.ElementAt(randomGenerator.Next(0, floorPositions.Count));
			}

			return floorPositions;
		}

		private List<DungeonTile> FindAllDeadEnds(Dictionary<Vector2Int, DungeonTile> floorPositions)
		{
			List<DungeonTile> deadEnds = new();
			
			foreach ((Guid _, DungeonCorridor corridor) in mAllCorridors)
			{
				foreach (DungeonTile tile in corridor)
				{
					var neighborsCount = 0;
					foreach (Vector2Int direction in Directions2D.CardinalDirectionList)
					{
						if (floorPositions.ContainsKey(tile.Position + direction))
						{
							neighborsCount++;
						}
					}

					if (neighborsCount == 1)
					{
						deadEnds.Add(tile);
					}
				}
			}
			
			/*foreach ((Vector2Int position, DungeonTile tile) in floorPositions)
			{
				var neighborsCount = 0;
				foreach (Vector2Int direction in Directions2D.CardinalDirectionList)
				{
					if (floorPositions.Any(x => x.Value.Position == position + direction))
					{
						neighborsCount++;
					}
				}

				if (neighborsCount == 1)
				{
					deadEnds.Add(tile);
				}
			}*/

			return deadEnds;
		}

		private void CreateRoomsAtDeadEnds(List<DungeonTile> deadEnds)
		{
			foreach (DungeonTile tile in deadEnds)
			{
				tile.PointType = DungeonPointType.Room;
				HashSet<DungeonTile> roomFloor = RunRandomWalk(mConfig, tile);
				RegisterRoom(Guid.NewGuid(), roomFloor.ToArray());
			}
		}

		private HashSet<DungeonTile> SimpleRandomWalk(DungeonTile startTile, int walkLength, DungeonPointType pointType)
		{
			HashSet<DungeonTile> mPath = new() { startTile };

			DungeonTile previousPosition = startTile;
			for (var i = 0; i < walkLength; i++)
			{
				var newPoint = new DungeonTile(previousPosition.Position + Directions2D.GetRandomCardinalDirection(randomGenerator), pointType);
				mPath.Add(newPoint);
				previousPosition = newPoint;
			}

			return mPath;
		}

		private HashSet<DungeonTile> RandomWalkCorridor(DungeonTile startTile, int corridorLength)
		{
			HashSet<DungeonTile> corridor = new();
			Vector2Int direction = Directions2D.GetRandomCardinalDirection(randomGenerator);
			DungeonTile currentTile = startTile;
			corridor.Add(currentTile);

			for (var i = 0; i < corridorLength; i++)
			{
				currentTile = new DungeonTile(currentTile.Position + direction, DungeonPointType.Corridor);
				corridor.Add(currentTile);
			}

			return corridor;
		}
	}
}