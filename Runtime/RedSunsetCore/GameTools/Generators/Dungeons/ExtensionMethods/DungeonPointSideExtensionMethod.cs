using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.GameTools.Generators.Dungeons.Enums;
using UnityEngine;

namespace RedSunsetCore.GameTools.Generators.Dungeons.ExtensionMethods
{
	static public class DungeonPointSideExtensionMethod
	{
		/*
		 * None = -1, // This should be less than -1. Otherwise when you choose "All" will include "None".
		Top = 1 << 0, // 0b_0000_0001 = 1
		TopRight = 1 << 1, // 0b_0000_0010 = 2
		Right = 1 << 2, // 0b_0000_0100 = 4
		BottomRight = 1 << 3, // 0b_0000_1000 = 8
		Bottom = 1 << 4, // 0b_0001_0000 = 16
		BottomLeft = 1 << 5, // 0b_0010_0000 = 32
		Left = 1 << 6, // 0b_0100_0000 = 64
		TopLeft = 1 << 7, // 0b_1000_0000 = 128,
		Cross = TopRight | BottomRight | BottomLeft | TopLeft,
		Horizontal = Right | Left,
		Vertical = Bottom | Top,
		All = Horizontal | Vertical | Cross
		 */
		
		static readonly private Dictionary<DungeonPointSide, Vector2Int> directions = new Dictionary<DungeonPointSide, Vector2Int>
		{
			{ DungeonPointSide.None, Vector2Int.zero },
			{ DungeonPointSide.Top, Vector2Int.up },
			{ DungeonPointSide.TopRight, Vector2Int.up + Vector2Int.right},
			{ DungeonPointSide.Right, Vector2Int.right },
			{ DungeonPointSide.BottomRight, Vector2Int.down + Vector2Int.right },
			{ DungeonPointSide.Bottom, Vector2Int.down },
			{ DungeonPointSide.BottomLeft, Vector2Int.down + Vector2Int.left },
			{ DungeonPointSide.Left, Vector2Int.left },
			{ DungeonPointSide.TopLeft, Vector2Int.up + Vector2Int.left }
		};

		static public Vector2Int[] ToDirectionVectors(this DungeonPointSide source)
		{
			var result = new List<Vector2Int>();

			foreach (var pair in directions)
			{
				if (source.HasFlag(pair.Key))
					result.Add(pair.Value);
			}
  
			return result.ToArray();
		}

		static public List<DungeonPointSide> GetDpadDirections(this DungeonPointSide source)
		{
			var dpadDirections = new List<DungeonPointSide>();
			DungeonPointSide[] splitSource = source.Split();
			foreach (DungeonPointSide side in splitSource)
			{
				if (side == DungeonPointSide.Left || side == DungeonPointSide.Right ||
				    side == DungeonPointSide.Bottom || side == DungeonPointSide.Top)
				{
					dpadDirections.Add(side);
				}
			}

			return dpadDirections;
		}

		static public bool HasOneOf(this DungeonPointSide source, params DungeonPointSide[] sides)
		{
			return source.Split().Any(side => sides.Any(option => side == option));
		}

		static public bool HasJustOneOf(this DungeonPointSide source, params DungeonPointSide[] sides)
		{
			return source.ToDirectionVectors().Length == 1;
		}
	}
}