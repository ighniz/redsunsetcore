using System;
using System.Collections;
using System.Collections.Generic;
using RedSunsetCore.GameTools.Generators.Dungeons.Config;
using RedSunsetCore.GameTools.Generators.Dungeons.Data;
using RedSunsetCore.GameTools.Generators.Dungeons.Interfaces;

namespace RedSunsetCore.GameTools.Generators.Dungeons
{
	public class DungeonRoom : IDungeonEnvironment
	{
		public Guid Id { get; }
		public int TileCount => mAllTiles.Count;

		readonly private HashSet<DungeonTile> mAllTiles = new();
		readonly private HashSet<DungeonTile> mAllEntrances = new();

		public DungeonRoom(Guid id)
		{
			Id = id;
		}

		public void AddTile(DungeonTile tile)
		{
			mAllTiles.Add(tile);
		}

		public void AddTiles(IEnumerable<DungeonTile> tiles)
		{
			mAllTiles.UnionWith(tiles);
		}

		public void RemoveTile(DungeonTile tile)
		{
			mAllTiles.Remove(tile);
		}

		public bool TryRemoveTile(DungeonTile tile)
		{
			var success = false;
			if (mAllTiles.Contains(tile))
			{
				RemoveTile(tile);
				success = true;
			}

			return success;
		}

		public void AddEntrance(DungeonTile entrance)
		{
			mAllEntrances.Add(entrance);
		}

		public IEnumerator<DungeonTile> GetEnumerator()
		{
			foreach (DungeonTile tile in mAllTiles)
			{
				yield return tile;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}