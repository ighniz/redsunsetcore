using System;
using System.Collections;
using System.Collections.Generic;
using RedSunsetCore.GameTools.Generators.Dungeons.Data;
using RedSunsetCore.GameTools.Generators.Dungeons.Interfaces;

namespace RedSunsetCore.GameTools.Generators.Dungeons
{
	public class DungeonCorridor : IDungeonEnvironment
	{
		private HashSet<DungeonTile> mAllTiles = new();

		public Guid Id { get; }
		public int TileCount => mAllTiles.Count;

		public DungeonCorridor(Guid id)
		{
			Id = id;
		}

		public void AddTile(DungeonTile tile)
		{
			mAllTiles.Add(tile);
		}

		public void AddTiles(IEnumerable<DungeonTile> tiles)
		{
			mAllTiles.UnionWith(tiles);
		}

		public void RemoveTile(DungeonTile tile)
		{
			mAllTiles.Remove(tile);
		}

		public bool TryRemoveTile(DungeonTile tile)
		{
			var success = false;
			if (mAllTiles.Contains(tile))
			{
				RemoveTile(tile);
				success = true;
			}

			return success;
		}

		public IEnumerator<DungeonTile> GetEnumerator()
		{
			foreach (DungeonTile tile in mAllTiles)
			{
				yield return tile;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}