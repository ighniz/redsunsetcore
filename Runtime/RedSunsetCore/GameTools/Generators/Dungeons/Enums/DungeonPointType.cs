namespace RedSunsetCore.GameTools.Generators.Dungeons.Enums
{
	public enum DungeonPointType
	{
		Undefined,
		Corridor,
		Room,
		DeadEnd,
		Wall,
		Entrance
	}
}