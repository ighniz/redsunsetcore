using System;

namespace RedSunsetCore.GameTools.Generators.Dungeons.Enums
{
	[Flags]
	public enum DungeonPointSide
	{
		None = -1, // This should be less than -1. Otherwise when you choose "All" will include "None".
		Top = 1 << 0, // 0b_0000_0001 = 1
		TopRight = 1 << 1, // 0b_0000_0010 = 2
		Right = 1 << 2, // 0b_0000_0100 = 4
		BottomRight = 1 << 3, // 0b_0000_1000 = 8
		Bottom = 1 << 4, // 0b_0001_0000 = 16
		BottomLeft = 1 << 5, // 0b_0010_0000 = 32
		Left = 1 << 6, // 0b_0100_0000 = 64
		TopLeft = 1 << 7, // 0b_1000_0000 = 128,
		Cross = TopRight | BottomRight | BottomLeft | TopLeft,
		Horizontal = Right | Left,
		Vertical = Bottom | Top,
		All = Horizontal | Vertical | Cross
	}
}