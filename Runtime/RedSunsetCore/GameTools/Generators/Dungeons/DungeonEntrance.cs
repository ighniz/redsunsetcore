using System;
using System.Collections;
using System.Collections.Generic;
using RedSunsetCore.GameTools.Generators.Dungeons.Data;
using RedSunsetCore.GameTools.Generators.Dungeons.Interfaces;

namespace RedSunsetCore.GameTools.Generators.Dungeons
{
	public class DungeonEntrance : IDungeonEnvironment, IDungeonConnector
	{
		private DungeonTile mTile;
		private DungeonTile mSideA;
		private DungeonTile mSideB;

		public Guid Id { get; }
		public int TileCount => 3;

		public DungeonEntrance(DungeonTile tile, DungeonTile sideA, DungeonTile sideB)
		{
			Id = Guid.NewGuid();
			mTile = tile;
			mSideA = sideA;
			mSideB = sideB;
		}

		public IEnumerator<DungeonTile> GetEnumerator()
		{
			yield return mTile;
			yield return mSideA;
			yield return mSideB;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public void AddTile(DungeonTile tile)
		{
			// Not Needed.
		}

		public void AddTiles(IEnumerable<DungeonTile> tiles)
		{
			// Not Needed.
		}

		public void RemoveTile(DungeonTile tile)
		{
			// Not Needed.
		}

		public bool TryRemoveTile(DungeonTile tile)
		{
			throw new NotImplementedException();
		}
	}
}