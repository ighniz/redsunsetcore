using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using UnityEngine;

namespace RedSunsetCore.GameTools.Generators.Dungeons.Config
{
	[CreateAssetMenu(menuName = StringConstants.Config.MENU_CREATE_DUNGEON_DATA, fileName = nameof(DungeonGenerationConfig))]
	public class DungeonGenerationConfig : RedSunsetScriptable<DungeonGenerationConfig>
	{
		[SerializeField]
		private int iterations = 10;
		public int Iterations => iterations;

		[SerializeField]
		private int walkLength = 10;
		public int WalkLength => walkLength;

		[SerializeField]
		private bool startRandomlyEachIteration = true;
		public bool StartRandomlyEachIteration => startRandomlyEachIteration;

		[SerializeField]
		private Vector2Int startPosition;
		public Vector2Int StartPosition => startPosition;

		[SerializeField]
		private int corridorCount;
		public int CorridorCount => corridorCount;

		[SerializeField]
		private int corridorLength;
		public int CorridorLength => corridorLength;

		[SerializeField]
		private float roomPercent;
		public float RoomPercent => roomPercent;

		[SerializeField]
		private int minCorridorLength;
		public int MinCorridorLength => minCorridorLength;

		[SerializeField]
		private DungeonItem[] possibleDungeonItems;
		public DungeonItem[] PossibleDungeonItems => possibleDungeonItems;

		[SerializeField]
		private string seed;
		public string Seed => seed;

		override public void Reset()
		{
			iterations = 10;
			startRandomlyEachIteration = true;
			startPosition = Vector2Int.zero;
			corridorCount = 0;
			corridorLength = 0;
			roomPercent = .8f;
		}
	}
}