using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.GameTools.Generators.Dungeons;
using RedSunsetCore.GameTools.Generators.Dungeons.Config;
using RedSunsetCore.GameTools.Generators.Dungeons.Data;
using RedSunsetCore.GameTools.Generators.Dungeons.Enums;
using UnityEngine;

public class SimpleRandomWalkDungeonGenerator : AbstractDungeonGenerator
{
	[SerializeField]
	protected DungeonGenerationConfig randomWalkParameters;

	override protected IEnumerator RunProceduralGeneration()
	{
		var startPoint = new DungeonTile(startPosition, DungeonPointType.Undefined);
		HashSet<DungeonTile> floorPositions = RunRandomWalk(randomWalkParameters, startPoint);
		yield return WallGenerator.CreateWalls(floorPositions);
	}

	protected HashSet<DungeonTile> RunRandomWalk(DungeonGenerationConfig randomWalkSo, DungeonTile tile)
	{
		DungeonTile currentPosition = tile;
		var floorPositions = new HashSet<DungeonTile>();
		for (var i = 0; i < randomWalkSo.Iterations; i++)
		{
			HashSet<DungeonTile> path = ProceduralGenerationAlgorithms.SimpleRandomWalk(currentPosition, randomWalkSo.WalkLength, tile.PointType);
			floorPositions.UnionWith(path);
			if (randomWalkSo.StartRandomlyEachIteration) currentPosition = floorPositions.ElementAt(Random.Range(0, floorPositions.Count));
		}

		return floorPositions;
	}
}