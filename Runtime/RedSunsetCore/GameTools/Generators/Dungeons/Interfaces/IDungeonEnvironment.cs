 using System;
using System.Collections.Generic;
using RedSunsetCore.GameTools.Generators.Dungeons.Data;

namespace RedSunsetCore.GameTools.Generators.Dungeons.Interfaces
{
    public interface IDungeonEnvironment : IDungeonObject, IEnumerable<DungeonTile>
    {
        Guid Id { get; }
        int TileCount { get; }
        void AddTile(DungeonTile tile);
        void AddTiles(IEnumerable<DungeonTile> tiles);
        void RemoveTile(DungeonTile tile);
        bool TryRemoveTile(DungeonTile tile);
    }
}