using System;
using System.Collections.Generic;
using RedSunsetCore.GameTools.Generators.Dungeons.Config;
using RedSunsetCore.GameTools.Generators.Dungeons.Data;

namespace RedSunsetCore.GameTools.Generators.Dungeons.Interfaces
{
    public interface IDungeonGenerator : IEnumerable<DungeonTile>
    {
        IDungeonGenerator GenerateCorridorsFirst();
        IDungeonGenerator WithConfig(DungeonGenerationConfig config);
        void ClearData();
        Dictionary<Guid, DungeonRoom> AllRooms { get; }
        Dictionary<Guid, DungeonCorridor> AllCorridors { get; }
        Dictionary<Guid, DungeonEntrance> AllEntrances { get; }
        DungeonRoom BiggerRoom { get; }
        IDungeonGenerator ProcessMod(IDungeonGeneratorMod mod);
    }
}