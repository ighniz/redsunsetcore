using System.Collections.Generic;
using UnityEngine;

namespace RedSunsetCore.GameTools.Generators.Dungeons
{
	static public class Directions2D
	{
		static readonly public List<Vector2Int> CardinalDirectionList = new()
		{
			new Vector2Int(0, 1), //UP
			new Vector2Int(1, 0), //RIGHT
			new Vector2Int(0, -1), //DOWN
			new Vector2Int(-1, 0) //LEFT
		};

		static readonly public List<Vector2Int> DiagonalDirectionList = new()
		{
			new Vector2Int(1, 1), //UP-RIGHT
			new Vector2Int(1, -1), //RIGHT-DOWN
			new Vector2Int(-1, -1), //DOWN-LEFT
			new Vector2Int(-1, 1) //LEFT-UP
		};

		static readonly public List<Vector2Int> EightDirectionsList = new()
		{
			new Vector2Int(0, 1), //UP
			new Vector2Int(1, 1), //UP-RIGHT
			new Vector2Int(1, 0), //RIGHT
			new Vector2Int(1, -1), //RIGHT-DOWN
			new Vector2Int(0, -1), //DOWN
			new Vector2Int(-1, -1), //DOWN-LEFT
			new Vector2Int(-1, 0), //LEFT
			new Vector2Int(-1, 1) //LEFT-UP
		};

		static public Vector2Int GetTrueRandomCardinalDirection()
		{
			return CardinalDirectionList[Random.Range(0, CardinalDirectionList.Count)];
		}

		static public Vector2Int GetTrueRandomEightDirection()
		{
			return EightDirectionsList[Random.Range(0, EightDirectionsList.Count)];
		}

		static public Vector2Int GetRandomCardinalDirection(System.Random generator)
		{
			return CardinalDirectionList[generator.Next(0, CardinalDirectionList.Count)];
		}

		static public Vector2Int GetRandomEightDirection(System.Random generator)
		{
			return EightDirectionsList[generator.Next(0, EightDirectionsList.Count)];
		}
	}
}