using System;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.GameTools.Generators.Dungeons.Enums;
using UnityEngine;

namespace RedSunsetCore.GameTools.Generators.Dungeons.Data
{
	public class DungeonTile
	{
		public Vector2Int Position { get; }
		public Vector2Int Origin { get; set; }
		public DungeonPointType PointType { get; set; }
		public DungeonPointSide PointSide { get; set; } = DungeonPointSide.None;
		public Guid Uid { get; } = Guid.NewGuid();
		public Guid RoomId { get; set; }
		public Guid CorridorId { get; set; }
		public Dictionary<Vector2Int, DungeonTile> Neighbors { get; } = new();
		public List<DungeonItem> Items { get; private set; } = new();

		public DungeonTile(Vector2Int position, DungeonPointType pointType)
		{
			Position = position;
			PointType = pointType;
		}

		public bool IsAccessible()
		{
			return Neighbors.Any(x => x.Value.PointType == DungeonPointType.Room);
		}

		public void AddNeighbor(DungeonTile neighbor, Vector2Int direction)
		{
			Neighbors.Add(direction, neighbor);
		}

		public void AddItem(DungeonItem item)
		{
			Items.Add(item);
		}

		override public int GetHashCode()
		{
			return Position.GetHashCode();
		}

		override public bool Equals(object obj)
		{
			return obj is DungeonTile other && Position == other.Position;
		}
	}
}