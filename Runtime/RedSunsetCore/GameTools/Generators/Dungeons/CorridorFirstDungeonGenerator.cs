using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.GameTools.Generators.Dungeons.Data;
using RedSunsetCore.GameTools.Generators.Dungeons.Enums;
using UnityEngine;

namespace RedSunsetCore.GameTools.Generators.Dungeons
{
	public class CorridorFirstDungeonGenerator : SimpleRandomWalkDungeonGenerator
	{
		[SerializeField]
		private int corridorLength = 14, corridorCount = 5;
		[SerializeField]
		[Range(.1f, 1f)]
		private float roomPercent = .8f;

		override protected IEnumerator RunProceduralGeneration()
		{
			CorridorFirstGeneration();
			//yield return null;
			return null;
		}

		private void CorridorFirstGeneration()
		{
			HashSet<DungeonTile> floorPositions = new();
			HashSet<DungeonTile> potentialRoomPositions = new();

			CreateCorridors(floorPositions, potentialRoomPositions);
			HashSet<DungeonTile> roomPositions = CreateRooms(potentialRoomPositions);

			List<DungeonTile> deadEnds = FindAllDeadEnds(floorPositions);
			CreateRoomsAtDeadEnds(deadEnds, roomPositions);

			floorPositions.UnionWith(roomPositions);

			//tilemapVisualizer.PaintFloorTiles(floorPositions);
			/*StartCoroutine(WallGenerator.CreateWalls(floorPositions, tilemapVisualizer));*/
			WallGenerator.CreateWalls(floorPositions);
		}

		private void CreateRoomsAtDeadEnds(List<DungeonTile> deadEnds, HashSet<DungeonTile> allRoomFloors)
		{
			foreach (DungeonTile point in deadEnds)
				if (!allRoomFloors.Contains(point))
				{
					HashSet<DungeonTile> roomFloor = RunRandomWalk(randomWalkParameters, point);
					allRoomFloors.UnionWith(roomFloor);
				}
		}

		private List<DungeonTile> FindAllDeadEnds(HashSet<DungeonTile> floorPositions)
		{
			List<DungeonTile> deadEnds = new();
			foreach (DungeonTile point in floorPositions)
			{
				var neighborsCount = 0;
				foreach (Vector2Int direction in Directions2D.CardinalDirectionList)
				{
					DungeonTile neighbor = floorPositions.FirstOrDefault(x => x.Position == point.Position + direction);
					if (neighbor != null) neighborsCount++;
				}

				if (neighborsCount == 1) deadEnds.Add(point);
			}

			return deadEnds;
		}

		private HashSet<DungeonTile> CreateRooms(HashSet<DungeonTile> potentialRoomPositions)
		{
			HashSet<DungeonTile> roomPositions = new();
			int roomToCreateCount = Mathf.RoundToInt(potentialRoomPositions.Count * roomPercent);

			List<DungeonTile> roomToCreate = potentialRoomPositions.OrderBy(x => x.Uid).Take(roomToCreateCount).ToList();

			foreach (DungeonTile roomPosition in roomToCreate)
			{
				HashSet<DungeonTile> roomFloor = RunRandomWalk(randomWalkParameters, roomPosition);
				roomPositions.UnionWith(roomFloor);
			}

			return roomPositions;
		}

		private void CreateCorridors(HashSet<DungeonTile> floorPositions, HashSet<DungeonTile> potentialRoomPositions)
		{
			var currentCorridorTile = new DungeonTile(startPosition, DungeonPointType.Corridor);
			potentialRoomPositions.Add(currentCorridorTile);

			for (var i = 0; i < corridorCount; i++)
			{
				List<DungeonTile> corridor = ProceduralGenerationAlgorithms.RandomWalkCorridor(currentCorridorTile, corridorLength);
				currentCorridorTile = corridor[^1];
				potentialRoomPositions.Add(currentCorridorTile);
				floorPositions.UnionWith(corridor);
			}
		}
	}
}