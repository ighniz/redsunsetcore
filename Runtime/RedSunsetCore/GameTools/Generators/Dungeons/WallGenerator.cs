using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.GameTools.Generators.Dungeons.Data;
using RedSunsetCore.GameTools.Generators.Dungeons.Enums;
using UnityEngine;

namespace RedSunsetCore.GameTools.Generators.Dungeons
{
	static public class WallGenerator
	{
		static public IEnumerator CreateWalls(HashSet<DungeonTile> floorPositions)
		{
			/*HashSet<DungeonPoint> basicWallPositions = FindWallsInDirections(floorPositions, Directions2D.CardinalDirectionList);
		HashSet<DungeonPoint> cornerWallPositions = FindWallsInDirections(floorPositions, Directions2D.DiagonalDirectionList);*/

			/*CreateBasicWall(basicWallPositions, floorPositions);
		CreateCornerWalls(cornerWallPositions, floorPositions);*/

			//HashSet<DungeonPoint> wallsPositions = FindWallsInDirections(floorPositions, Directions2D.EightDirectionsList);
			CreateAllWalls(floorPositions);
			return null;
		}

		static private void CreateAllWalls(HashSet<DungeonTile> floorPositions)
		{
			foreach (DungeonTile point in floorPositions)
			{
				var neighborBinaryType = 0;
				for (var index = 0; index < Directions2D.EightDirectionsList.Count; index++)
				{
					Vector2Int direction = Directions2D.EightDirectionsList[index];
					Vector2Int neighborPosition = point.Position + direction;
					if (floorPositions.Any(x => x.Position == neighborPosition))
					{
						neighborBinaryType |= 1 << index;
					}
				}

				point.PointSide = (DungeonPointSide)neighborBinaryType;
				if ((point.PointSide & DungeonPointSide.Cross) == DungeonPointSide.Cross)
				{
					point.PointType = DungeonPointType.Room;
				}/*else if (point.PointSide == DungeonPointSide.Horizontal || point.PointSide == DungeonPointSide.Vertical)
			{
				point.PointType = DungeonPointType.Corridor;
			}
			else
			{
				point.PointType = DungeonPointType.Wall;
			}*/
			}
		}

		static private void CreateCornerWalls(HashSet<DungeonTile> cornerWallPositions, HashSet<DungeonTile> floorPositions)
		{
			foreach (DungeonTile point in cornerWallPositions)
			{
				var neighborBinaryType = 0;
				for (var index = 0; index < Directions2D.DiagonalDirectionList.Count; index++)
				{
					Vector2Int direction = Directions2D.DiagonalDirectionList[index];
					Vector2Int neighborPosition = point.Position + direction;
					if (floorPositions.Any(x => x.Position == neighborPosition)) neighborBinaryType |= 1 << index;
				}

				point.PointSide = (DungeonPointSide)neighborBinaryType;
			}
		}

		static private void CreateBasicWall(HashSet<DungeonTile> basicWallPositions, HashSet<DungeonTile> floorPositions)
		{
			foreach (DungeonTile point in basicWallPositions)
			{
				var neighborBinaryType = 0;
				for (var index = 0; index < Directions2D.CardinalDirectionList.Count; index++)
				{
					Vector2Int direction = Directions2D.CardinalDirectionList[index];
					Vector2Int neighborPosition = point.Position + direction;
					if (floorPositions.Any(x => x.Position == neighborPosition)) neighborBinaryType |= 1 << index;
				}

				point.PointSide = (DungeonPointSide)neighborBinaryType;
			}
		}

		static private HashSet<DungeonTile> FindWallsInDirections(HashSet<DungeonTile> floorPositions, List<Vector2Int> directionList)
		{
			HashSet<DungeonTile> wallPositions = new();
			foreach (DungeonTile point in floorPositions)
			{
				foreach (Vector2Int direction in directionList)
				{
					if (floorPositions.All(x => x.Position != point.Position + direction))
					{
						wallPositions.Add(point);
						point.PointType = DungeonPointType.Wall;
					}
				}
			}

			return wallPositions;
		}
	}
}