using System.Collections;
using System.Collections.Generic;
using RedSunsetCore.GameTools.Generators.Dungeons;
using RedSunsetCore.GameTools.Generators.Dungeons.Data;
using RedSunsetCore.GameTools.Generators.Dungeons.Enums;
using UnityEngine;

public class RoomFirstDungeonGenerator : SimpleRandomWalkDungeonGenerator
{
	[SerializeField]
	private int minRoomWidth = 4, minRoomHeight = 4;

	[SerializeField]
	private int dungeonWidth = 20, dungeonHeight = 20;

	[SerializeField]
	[Range(0, 10)]
	private int offset = 1;

	[SerializeField]
	private bool randomWalkRooms = false;

	override protected IEnumerator RunProceduralGeneration()
	{
		CreateRooms();
		return null;
	}

	private void CreateRooms()
	{
		List<BoundsInt> roomsList = ProceduralGenerationAlgorithms.BinarySpacePartitioning(
			new BoundsInt((Vector3Int)startPosition, new Vector3Int(dungeonWidth, dungeonHeight, 0)),
			minRoomWidth,
			minRoomHeight);

		HashSet<DungeonTile> floor;
		if (randomWalkRooms)
			floor = CreateRoomsRandomly(roomsList);
		else
			floor = CreateSimpleRooms(roomsList);

		List<Vector2Int> roomCenters = new();
		foreach (BoundsInt room in roomsList) roomCenters.Add((Vector2Int)Vector3Int.RoundToInt(room.center));

		HashSet<DungeonTile> corridors = ConnectRooms(roomCenters);
		floor.UnionWith(corridors);

		//tilemapVisualizer.PaintFloorTiles(floor);
		StartCoroutine(WallGenerator.CreateWalls(floor));
	}

	private HashSet<DungeonTile> CreateRoomsRandomly(List<BoundsInt> roomsList)
	{
		HashSet<DungeonTile> floor = new();
		for (var i = 0; i < roomsList.Count; i++)
		{
			BoundsInt roomBounds = roomsList[i];
			var roomCenter = new Vector2Int(Mathf.RoundToInt(roomBounds.center.x), Mathf.RoundToInt(roomBounds.center.y));
			var roomTile = new DungeonTile(roomCenter, DungeonPointType.Room);
			HashSet<DungeonTile> roomFloor = RunRandomWalk(randomWalkParameters, roomTile);
			foreach (DungeonTile point in roomFloor)
				if (point.Position.x >= roomBounds.xMin + offset &&
				    point.Position.x <= roomBounds.xMax - offset &&
				    point.Position.y >= roomBounds.yMin - offset &&
				    point.Position.y <= roomBounds.yMax - offset)
					floor.Add(point);
		}

		return floor;
	}

	private HashSet<DungeonTile> ConnectRooms(List<Vector2Int> roomCenters)
	{
		HashSet<DungeonTile> corridors = new();
		Vector2Int currentRoomCenter = roomCenters[Random.Range(0, roomCenters.Count)];
		roomCenters.Remove(currentRoomCenter);

		while (roomCenters.Count > 0)
		{
			Vector2Int closest = FindClosestPoint(currentRoomCenter, roomCenters);
			roomCenters.Remove(closest);
			HashSet<DungeonTile> newCorridor = CreateCorridor(currentRoomCenter, closest);
			currentRoomCenter = closest;
			corridors.UnionWith(newCorridor);
		}

		return corridors;
	}

	private HashSet<DungeonTile> CreateCorridor(Vector2Int currentRoomCenter, Vector2Int destination)
	{
		HashSet<DungeonTile> corridor = new();
		Vector2Int position = currentRoomCenter;
		var corridorTile = new DungeonTile(position, DungeonPointType.Corridor);
		corridor.Add(corridorTile);
		while (position.y != destination.y)
		{
			if (destination.y > position.y)
				position += Vector2Int.up;
			else if (destination.y < position.y) position += Vector2Int.down;

			corridor.Add(new DungeonTile(position, DungeonPointType.Corridor));
		}

		while (position.x != destination.x)
		{
			if (destination.x > position.x)
				position += Vector2Int.right;
			else if (destination.x < position.x) position += Vector2Int.left;

			corridor.Add(new DungeonTile(position, DungeonPointType.Corridor));
		}

		return corridor;
	}

	private Vector2Int FindClosestPoint(Vector2Int currentRoomCenter, List<Vector2Int> roomCenters)
	{
		Vector2Int closest = Vector2Int.zero;
		var distance = float.MaxValue;
		foreach (Vector2Int position in roomCenters)
		{
			float currentDistance = Vector2.Distance(position, currentRoomCenter);
			if (currentDistance < distance)
			{
				distance = currentDistance;
				closest = position;
			}
		}

		return closest;
	}

	private HashSet<DungeonTile> CreateSimpleRooms(List<BoundsInt> roomsLists)
	{
		HashSet<DungeonTile> floor = new();
		foreach (BoundsInt room in roomsLists)
			for (var col = 0; col < room.size.x - offset; col++)
			for (int row = offset; row < room.size.y - offset; row++)
			{
				Vector2Int position = (Vector2Int)room.min + new Vector2Int(col, row);
				var roomTile = new DungeonTile(position, DungeonPointType.Room);
				floor.Add(roomTile);
			}

		return floor;
	}
}