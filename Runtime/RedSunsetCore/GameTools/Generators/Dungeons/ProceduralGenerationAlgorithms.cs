using System.Collections.Generic;
using RedSunsetCore.GameTools.Generators.Dungeons;
using RedSunsetCore.GameTools.Generators.Dungeons.Data;
using RedSunsetCore.GameTools.Generators.Dungeons.Enums;
using UnityEngine;

static public class ProceduralGenerationAlgorithms
{
	static public HashSet<DungeonTile> SimpleRandomWalk(DungeonTile startTile, int walkLength, DungeonPointType pointType)
	{
		HashSet<DungeonTile> mPath = new() { startTile };

		DungeonTile previousPosition = startTile;
		for (var i = 0; i < walkLength; i++)
		{
			var newPoint = new DungeonTile(previousPosition.Position + Directions2D.GetTrueRandomCardinalDirection(), pointType);
			mPath.Add(newPoint);
			previousPosition = newPoint;
		}

		return mPath;
	}

	static public List<DungeonTile> RandomWalkCorridor(DungeonTile startTile, int corridorLength)
	{
		List<DungeonTile> corridor = new();
		Vector2Int direction = Directions2D.GetTrueRandomCardinalDirection();
		DungeonTile currentTile = startTile;
		corridor.Add(currentTile);

		for (var i = 0; i < corridorLength; i++)
		{
			currentTile = new DungeonTile(currentTile.Position + direction, DungeonPointType.Corridor);
			corridor.Add(currentTile);
		}

		return corridor;
	}

	static public List<BoundsInt> BinarySpacePartitioning(BoundsInt spaceToSplit, int minWidth, int minHeight)
	{
		Queue<BoundsInt> roomsQueue = new();
		List<BoundsInt> roomsList = new();
		roomsQueue.Enqueue(spaceToSplit);
		while (roomsQueue.Count > 0)
		{
			BoundsInt room = roomsQueue.Dequeue();
			if (room.size.y >= minWidth && room.size.x >= minHeight)
			{
				if (Random.value < .5)
				{
					//Can it contains two spaces?
					if (room.size.y >= minHeight * 2)
						SplitHorizontally(minHeight, roomsQueue, room);
					else if (room.size.x >= minWidth * 2)
						SplitVertically(minWidth, roomsQueue, room);
					else
						roomsList.Add(room);
				}
				else //Here we are going to check vertical first.
				{
					//Can it contains two spaces?
					if (room.size.y >= minHeight * 2)
						SplitVertically(minWidth, roomsQueue, room);
					else if (room.size.x >= minWidth * 2)
						SplitHorizontally(minHeight, roomsQueue, room);
					else
						roomsList.Add(room);
				}
			}
		}

		return roomsList;
	}

	static private void SplitVertically(int minWidth, Queue<BoundsInt> roomsQueue, BoundsInt room)
	{
		int xSplit = Random.Range(1, room.size.x);
		var room1 = new BoundsInt(room.min, new Vector3Int(xSplit, room.size.y, room.size.z));
		var room2 = new BoundsInt(
			new Vector3Int(room.min.x + xSplit, room.min.y, room.min.z),
			new Vector3Int(room.size.x - xSplit, room.size.y, room.size.x));

		roomsQueue.Enqueue(room1);
		roomsQueue.Enqueue(room2);
	}

	static private void SplitHorizontally(int minHeight, Queue<BoundsInt> roomsQueue, BoundsInt room)
	{
		int ySplit = Random.Range(1, room.size.y); //Another way: (minHeight, room.size.y - minHeight) fit always two rooms together.
		var room1 = new BoundsInt(room.min, new Vector3Int(room.size.x, ySplit, room.size.z));
		var room2 = new BoundsInt(
			new Vector3Int(room.min.x, room.min.y + ySplit, room.min.z),
			new Vector3Int(room.size.x, room.size.y - ySplit, room.size.z));

		roomsQueue.Enqueue(room1);
		roomsQueue.Enqueue(room2);
	}
}