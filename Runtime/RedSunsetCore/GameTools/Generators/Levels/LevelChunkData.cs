﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace RedSunsetCore.GameTools.Generators.Levels
{
	/// <summary>
	///		Represents a chunk data when the level is generated.
	/// </summary>
	[Serializable]
	public class LevelChunkData
	{
		public GameObject gameObject;
		public List<LevelChunkData> connections = new();

		[JsonIgnore]
		public LevelChunkConfig config;

		[JsonProperty]
		public string ConfigName => config.FileName;
	}
}