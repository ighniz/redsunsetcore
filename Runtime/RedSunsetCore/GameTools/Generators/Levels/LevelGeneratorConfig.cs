﻿using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using UnityEngine;

namespace RedSunsetCore.GameTools.Generators.Levels
{
	[CreateAssetMenu(fileName = nameof(LevelGeneratorConfig), menuName = StringConstants.Config.MENU_CONFIG + nameof(LevelGeneratorConfig))]
	public class LevelGeneratorConfig : RedSunsetScriptableConfig<LevelGeneratorConfig>
	{
		[SerializeField]
		private LevelChunkConfig startChunk;
		public LevelChunkConfig StartChunk => startChunk;

		[SerializeField]
		private int levelLength;
		public int LevelLength => levelLength;

		override public void Reset()
		{
		}
	}
}