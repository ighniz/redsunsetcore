using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.ExtensionMethods;
using UnityEngine;
using UnityEngine.Assertions;

namespace RedSunsetCore.GameTools.Generators.Levels
{
	public class LevelGenerator
	{
		static public LevelGenerator Create(LevelGeneratorConfig config)
		{
			return new LevelGenerator().WithConfig(config);
		}

		private LevelGeneratorConfig mConfig;
		private int mRemainingConnections;
		private List<LevelChunkData> mAvailableConnections = new();

		private LevelGenerator()
		{
		}

		public LevelGenerator WithConfig(LevelGeneratorConfig config)
		{
			mConfig = config;
			mRemainingConnections = config.LevelLength;
			return this;
		}

		public LevelData GenerateLevelData()
		{
			var levelData = new LevelData();
			LevelChunkConfig currentChunkConfig = mConfig.StartChunk;
			levelData.startingChunk = CreateChunk(currentChunkConfig);
			while (mRemainingConnections > 0) //for (int i = 0; i < mConfig.LevelLength; i++)
			{
				Assert.IsTrue(mAvailableConnections.Count > 0, "There is no connections available.");
				LevelChunkData currentChunkData = mAvailableConnections[Random.Range(0, mAvailableConnections.Count)];
				currentChunkConfig = currentChunkData.config;
				LevelChunkData newChunkData = CreateChunk(currentChunkConfig);
				currentChunkData.connections.Add(newChunkData);
			}

			return levelData;
		}

		private LevelChunkData CreateChunk(LevelChunkConfig config)
		{
			var chunk = new LevelChunkData();
			int amountOfConnections = Random.Range(config.MinimumConnections, config.MaximumConnections);
			IEnumerable<LevelChunkConfig> allowedConnections = config.AllowedConnections.GetShuffled().Take(amountOfConnections);

			foreach (LevelChunkConfig levelChunkConfig in allowedConnections)
			{
				var chunkConnection = new LevelChunkData
				{
					//gameObject = GameObject.Instantiate(levelChunkConfig.Prefab),
					config = levelChunkConfig
				};
				chunk.connections.Add(chunkConnection);
				mAvailableConnections.Add(chunkConnection);
				mRemainingConnections--;
			}

			//chunk.gameObject = GameObject.Instantiate(config.Prefab);
			chunk.config = config;
			return chunk;
		}
	}
}