﻿using System;
using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using System.Collections.Generic;
using UnityEngine;

namespace RedSunsetCore.GameTools.Generators.Levels
{
	[CreateAssetMenu(fileName = nameof(LevelChunkConfig), menuName = StringConstants.Config.MENU_CONFIG + nameof(LevelChunkConfig))]
	public class LevelChunkConfig : RedSunsetScriptableConfig<LevelChunkConfig>
	{
		[SerializeField]
		private GameObject prefab;
		public GameObject Prefab => prefab;

		[SerializeField]
		private int minimumConnections;
		public int MinimumConnections => minimumConnections;

		[SerializeField]
		private int maximumConnections;
		public int MaximumConnections => maximumConnections;

		[SerializeField]
		private List<LevelChunkConfig> allowedConnections;
		public List<LevelChunkConfig> AllowedConnections => allowedConnections;

		override public void Reset()
		{
		}
	}
}