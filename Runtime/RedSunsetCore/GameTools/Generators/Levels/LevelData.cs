﻿using System;
using RedSunsetCore.Utils.Interfaces.JSON;

namespace RedSunsetCore.GameTools.Generators.Levels
{
	[Serializable]
	public class LevelData : IJson
	{
		public LevelChunkData startingChunk;
	}
}