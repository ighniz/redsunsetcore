using System;
using System.Collections.Generic;
using RedSunsetCore.GameTools.Replay.Interface;

namespace RedSunsetCore.GameTools.Replay
{
    public class SerializableReplaySystem<T> : IReplaySystem<T> where T : struct, IRecordableObject<T>
    {
        private List<T> frames = new List<T>();
        
        private bool isRecording;
        bool IReplaySystem<T>.IsRecording => isRecording;

        private bool isReplaying;
        bool IReplaySystem<T>.IsReplaying => isReplaying;

        void IReplaySystem<T>.StartRecord()
        {
            throw new NotImplementedException();
        }

        void IReplaySystem<T>.Stop()
        {
            throw new NotImplementedException();
        }

        void IReplaySystem<T>.StartReplay()
        {
            throw new NotImplementedException();
        }

        public void Update()
        {
            throw new NotImplementedException();
        }

        void IReplaySystem<T>.Register(T recordable)
        {
            throw new NotImplementedException();
        }

        void IReplaySystem<T>.Unregister(T recordable)
        {
            throw new NotImplementedException();
        }

        void IReplaySystem<T>.InvertDataRecorded()
        {
            throw new NotImplementedException();
        }

        private void RecordFrame()
        {
            
        }

        private void ReplayFrame()
        {
            
        }
    }
}