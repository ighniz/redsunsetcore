﻿namespace RedSunsetCore.GameTools.Replay.Interface
{
    public interface IReplaySystem<TRecordable> where TRecordable : IRecordable
    {
        bool IsRecording { get; }
        bool IsReplaying { get; }
        void StartRecord();
        void Stop();
        void StartReplay();
        void Update();
        void Register(TRecordable recordable);
        void Unregister(TRecordable recordable);
        void InvertDataRecorded();
    }
}