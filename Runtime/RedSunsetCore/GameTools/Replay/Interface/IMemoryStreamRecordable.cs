using System.IO;

namespace RedSunsetCore.GameTools.Replay.Interface
{
    public interface IMemoryStreamRecordable : IRecordable
    {
        void OnRecord(BinaryWriter writer);
        void OnReplay(BinaryReader reader);
    }
}