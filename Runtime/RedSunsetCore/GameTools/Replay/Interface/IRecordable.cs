﻿namespace RedSunsetCore.GameTools.Replay.Interface
{
    public interface IRecordable
    {
        int Id { get; }
    }
}