﻿using System;
using System.Collections.Generic;
using System.IO;
using RedSunsetCore.GameTools.Replay.Interface;

namespace RedSunsetCore.GameTools.Replay
{
    public class MemoryStreamReplaySystem<TRecordable> : IReplaySystem<TRecordable> where TRecordable : IMemoryStreamRecordable
    {
        private IReplaySystem<TRecordable> ThisImpl => this;
        
        private MemoryStream memoryStream;
        private BinaryReader reader;
        private BinaryWriter writer;
        private Action currentProcessAction;
        private List<TRecordable> recordables = new List<TRecordable>();

        private bool isRecording;
        bool IReplaySystem<TRecordable>.IsRecording => isRecording;

        private bool isReplaying;
        bool IReplaySystem<TRecordable>.IsReplaying => isReplaying;

        public MemoryStreamReplaySystem()
        {
            memoryStream = new MemoryStream();
            writer = new BinaryWriter(memoryStream);
            reader = new BinaryReader(memoryStream);
        }
        
        private void Reset()
        {
            memoryStream.Seek(0, SeekOrigin.Begin);
            writer.Seek(0, SeekOrigin.Begin);
        }

        void IReplaySystem<TRecordable>.StartRecord()
        {
            isReplaying = false;
            isRecording = true;
            memoryStream.SetLength(0);
            Reset();
            currentProcessAction = RecordObjects;
        }
        
        void IReplaySystem<TRecordable>.Stop()
        {
            isRecording = false;
            isReplaying = false;
            currentProcessAction = () => { };
        }

        void IReplaySystem<TRecordable>.StartReplay()
        {
            isRecording = false;
            isReplaying = true;
            Reset();
            currentProcessAction = ReplayObjects;
        }

        public void Update()
        {
            currentProcessAction.Invoke();
        }

        void IReplaySystem<TRecordable>.Register(TRecordable recordable)
        {
            recordables.Add(recordable);
        }

        void IReplaySystem<TRecordable>.Unregister(TRecordable recordable)
        {
            recordables.Remove(recordable);
        }

        void IReplaySystem<TRecordable>.InvertDataRecorded()
        {
            /*To be implemented...*/
        }

        private void RecordObjects()
        {
            foreach (TRecordable recordable in recordables)
            {
                recordable.OnRecord(writer);
            }
        }
        
        private void ReplayObjects()
        {
            if (memoryStream.Position < memoryStream.Length)
            {
                foreach (TRecordable recordable in recordables)
                {
                    recordable.OnReplay(reader);
                }
            }
            else
            {
                
            }
        }
    }
}