using RedSunsetCore.GameTools.Replay.Interface;

namespace RedSunsetCore.GameTools.Replay
{
    public interface IRecordableObject<out T> : IRecordable where T : struct
    {
        T GetData();
    }
}