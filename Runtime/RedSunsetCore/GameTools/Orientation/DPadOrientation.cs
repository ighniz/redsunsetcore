using System;

namespace RedSunsetCore.GameTools.Orientation
{
    [Flags]
    public enum DPadOrientation
    {
        None = 0,
        Down = 1,
        Left = 2,
        Up = 4,
        Right = 8,
        DownLeft = 16,
        DownRight = 32,
        UpLeft = 64,
        UpRight = 128,
        Plus = Down | Left | Up | Right,
        Cross = DownRight | DownLeft | UpLeft | UpRight,
        All = Plus | Cross
    }
}