
using System.Collections.Generic;
using RedSunsetCore.GameTools.XInLineTools.Matrix;
using UnityEngine;

namespace RedSunsetCore.GameTools.Orientation.ExtensionMethods
{
    static public class VectorExtensionMethods
    {
        static private Dictionary<DPadOrientation, Vector2> vector2ByDPadOrientationTable = new();

        static VectorExtensionMethods()
        {
            vector2ByDPadOrientationTable = new Dictionary<DPadOrientation, Vector2>
            {
                [DPadOrientation.None] = Vector2.zero,
                [DPadOrientation.Left] = Vector2.left,
                [DPadOrientation.Right] = Vector2.right,
                [DPadOrientation.Down] = Vector2.down,
                [DPadOrientation.Up] = Vector2.up,
                [DPadOrientation.DownLeft] = new Vector2(-1, -1).normalized,
                [DPadOrientation.DownRight] = new Vector2(1, -1).normalized,
                [DPadOrientation.UpLeft] = new Vector2(-1, 1).normalized,
                [DPadOrientation.UpRight] = new Vector2(1, 1).normalized
            };
        }
        
        static public DPadOrientation ToOrientation(this Vector2 source, float accuracy=.9f)
        {
            var result = DPadOrientation.None;
            
            bool isRight = Vector2.Dot(Vector2.right, source) > accuracy;
            bool isLeft = Vector2.Dot(Vector2.left, source) > accuracy;
            bool isDown = Vector2.Dot(Vector2.down, source) > accuracy;
            bool isUp = Vector2.Dot(Vector2.up, source) > accuracy;
                
            if (isRight)
                result |= DPadOrientation.Right;
            else if (isLeft)
                result |= DPadOrientation.Left;

            if (isDown)
                result |= DPadOrientation.Down;
            else if (isUp)
                result |= DPadOrientation.Up;
            
            return result;
        }

        static public Vector2 ToOrientationVector(this Vector2 source, float accuracy=.9f)
        {
            return vector2ByDPadOrientationTable[ToOrientation(source, accuracy)];
        }

        static public CellPosition ToCellPosition(this Vector2Int source)
        {
            return new CellPosition(source.y, source.x);
        }
        
        static public CellPosition ToCellPosition(this Vector2 source)
        {
            return new CellPosition((int)source.y, (int)source.x);
        }
    }
}