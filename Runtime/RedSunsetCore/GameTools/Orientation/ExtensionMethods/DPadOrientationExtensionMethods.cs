using System;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.GameTools.XInLineTools.Matrix;

namespace RedSunsetCore.GameTools.Orientation.ExtensionMethods
{
    static public class DPadOrientationExtensionMethods
    {
        static private Dictionary<DPadOrientation, DPadOrientation[]> combinationsMap;
        static private Dictionary<DPadOrientation, DPadOrientation[]> basicMap;
        static readonly private Dictionary<DPadOrientation, CellPosition> dpadOrientationConvertionTable;
        static DPadOrientationExtensionMethods()
        {
            combinationsMap = new Dictionary<DPadOrientation, DPadOrientation[]>();
            basicMap = new Dictionary<DPadOrientation, DPadOrientation[]>
            {
                {DPadOrientation.None, Array.Empty<DPadOrientation>()},
                {DPadOrientation.Down, new[] {DPadOrientation.Down}},
                {DPadOrientation.Left, new[] {DPadOrientation.Left}},
                {DPadOrientation.Up, new[] {DPadOrientation.Up}},
                {DPadOrientation.Right, new[] {DPadOrientation.Right}},
                {DPadOrientation.DownLeft, new[] {DPadOrientation.Down, DPadOrientation.Left}},
                {DPadOrientation.DownRight, new[] {DPadOrientation.Down, DPadOrientation.Right}},
                {DPadOrientation.UpLeft, new[] {DPadOrientation.Up, DPadOrientation.Left}},
                {DPadOrientation.UpRight, new[] {DPadOrientation.Up, DPadOrientation.Right}},
                {
                    DPadOrientation.Plus,
                    new[] {DPadOrientation.Down, DPadOrientation.Left, DPadOrientation.Up, DPadOrientation.Right}
                },
                {
                    DPadOrientation.Cross,
                    new[]
                    {
                        DPadOrientation.DownRight, DPadOrientation.DownLeft, DPadOrientation.UpLeft,
                        DPadOrientation.UpRight
                    }
                }
            };
            basicMap.Add(DPadOrientation.All, basicMap[DPadOrientation.Plus].Concat(basicMap[DPadOrientation.Cross]).ToArray());
            
            dpadOrientationConvertionTable = new Dictionary<DPadOrientation, CellPosition>
            {
                {DPadOrientation.Right,     new CellPosition(0, 1)},
                {DPadOrientation.Left,      new CellPosition(0, -1)},
                {DPadOrientation.Up,        new CellPosition(1, 0)},
                {DPadOrientation.Down,      new CellPosition(-1, 0)},
                {DPadOrientation.DownRight, new CellPosition(-1, 1)},
                {DPadOrientation.UpLeft,    new CellPosition(1, -1)},
                {DPadOrientation.DownLeft,  new CellPosition(-1, -1)},
                {DPadOrientation.UpRight,   new CellPosition(1, 1)}
            };
        }
        
        static public DPadOrientation[] BreakDown(this DPadOrientation source)
        {
            DPadOrientation[] splitOrientations;
            if (basicMap.TryGetValue(source, out DPadOrientation[] basicMapResult))
            {
                splitOrientations = basicMapResult;
            }
            else if (combinationsMap.TryGetValue(source, out DPadOrientation[] combinationsMapResult))
            {
                splitOrientations = combinationsMapResult;
            }
            else
            {
                var orientations = new HashSet<DPadOrientation>();

                string[] split = source.ToString().Split(new []{","}, StringSplitOptions.RemoveEmptyEntries);
                foreach (string valueName in split)
                {
                    var enumValue = (DPadOrientation) Enum.Parse(typeof(DPadOrientation), valueName);
                    orientations.UnionWith(basicMap[enumValue]);
                }

                splitOrientations = orientations.ToArray();
                combinationsMap.Add(source, splitOrientations);
            }

            return splitOrientations;
        }

        static public CellPosition ToCellDirection(this DPadOrientation source)
        {
            return dpadOrientationConvertionTable[source];
        }

        static public DPadOrientation Opposite(this DPadOrientation source)
        {
            return source.ToCellDirection().Opposite.ToDPadOrientation();
        }
    }
}