using System;
using RedSunsetCore.GameTools.GameEntitiesBehaviours.Interfaces;

namespace RedSunsetCore.GameTools.GameEntitiesBehaviours
{
	public class GameBehaviourProcessor
	{
		public TGameBehaviour AddBehaviour<TGameBehaviour>() where TGameBehaviour : IGameEntityBehaviour
		{
			Type behaviourType = typeof(TGameBehaviour);
			return default;
		}

		public void RemoveBehaviour<TGameBehaviour>(TGameBehaviour behaviour) where TGameBehaviour : IGameEntityBehaviour
		{
			
		}
	}
}