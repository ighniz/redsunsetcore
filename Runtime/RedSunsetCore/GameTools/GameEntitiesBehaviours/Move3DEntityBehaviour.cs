using RedSunsetCore.Core;
using RedSunsetCore.GameTools.GameEntitiesBehaviours.Interfaces;
using RedSunsetCore.Services.ModelView.Controller;
using RedSunsetCore.Services.ModelView.Interfaces;
using RedSunsetCore.Services.ModelView.Model;
using RedSunsetCore.Services.ModelView.View;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.GameTools.GameEntitiesBehaviours
{
	[RequireComponent(typeof(Rigidbody))]
	public class Move3DEntityBehaviour : RedsunsetCoreBehaviour, IGameEntityBehaviour
	{
		[Inject]
		private IModelViewService mModelViewService;
		
		override protected void Awake()
		{
			base.Awake();
			mModelViewService.InstantiateSystem<ModelGameActor, ViewGameActor, ControllerGameActor>(gameObject);
		}
	}
}