using System.Collections;
using RedSunsetCore.Core;
using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.GameTools.Entities;
using RedSunsetCore.GameTools.GameEntitiesBehaviours.Interfaces;
using RedSunsetCore.Services.ModelView.View;
using UnityEngine;
using UnityEngine.Assertions;

namespace RedSunsetCore.GameTools.GameEntitiesBehaviours
{
	public class GrabEntityBehaviour : RedsunsetCoreBehaviour, IGameEntityBehaviour
	{
		private Collider mCollider;
		private GameObject mCachedGameObject;

		override protected void Awake()
		{
			base.Awake();
			mCachedGameObject = gameObject;
			mCollider = mCachedGameObject.GetComponent<Collider>();
			Assert.IsNotNull(mCollider, $"The game object {mCachedGameObject.name} hasn't a collider.");
		}

		private void OnCollisionEnter(Collision other)
		{
			PickItemFromGameObject(other.gameObject);
		}

		private void OnCollisionEnter2D(Collision2D other)
		{
			PickItemFromGameObject(other.gameObject);
		}

		private ViewGameItem PickItemFromGameObject(GameObject go)
		{
			Assert.IsNotNull(go, "The game object shouldn't be null.".ToAssertFormat());
			return go.GetComponent<ViewGameItem>();
		}

		public void Grab()
		{
			mCollider.enabled = true;
			StartCoroutine(DisableCollider());
		}

		private IEnumerator DisableCollider()
		{
			yield return null;
			mCollider.enabled = false;
		}

		public void Process()
		{
			throw new System.NotImplementedException();
		}
	}
}