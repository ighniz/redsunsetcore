using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.GameTools.SlidePuzzle.Events
{
    public class SlidePuzzleTileMoveToNextValidPositionEvent : ICustomEvent
    {
        public SlidePuzzleTileData SlidePuzzleTile { get; set; }

        public void Reset()
        {
        }
    }
}