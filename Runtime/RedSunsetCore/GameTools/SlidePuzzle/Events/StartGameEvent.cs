using RedSunsetCore.Services.Event.Interfaces;
using UnityEngine;

namespace RedSunsetCore.GameTools.SlidePuzzle.Events
{
    public class StartGameEvent : ICustomEvent
    {
        public Texture TargetTexture { get; set; }

        public void Reset()
        {
        }
    }
}