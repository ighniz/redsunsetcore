using RedSunsetCore.GameTools.XInLineTools.Matrix;
using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.GameTools.SlidePuzzle.Events
{
    public class SlidePuzzleUpdatedEvent : ICustomEvent
    {
        public SlidePuzzleTileData Data { get; set; }
        public CellPosition From { get; set; }
        public CellPosition To { get; set; }

        public void Reset()
        {
        }
    }
}