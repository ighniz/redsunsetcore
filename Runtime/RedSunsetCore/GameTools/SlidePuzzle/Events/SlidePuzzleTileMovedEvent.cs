using RedSunsetCore.GameTools.XInLineTools.Matrix;
using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.GameTools.SlidePuzzle.Events
{
    public class SlidePuzzleTileMovedEvent : ICustomEvent
    {
        public CellPosition TargetDirection { get; set; }
        public SlidePuzzleTileData SlidePuzzleTile { get; set; }

        public void Reset()
        {
        }
    }
}