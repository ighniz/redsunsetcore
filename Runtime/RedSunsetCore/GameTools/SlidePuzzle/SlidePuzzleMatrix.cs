using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.GameTools.SlidePuzzle.Events;
using RedSunsetCore.GameTools.SlidePuzzle.Interfaces;
using RedSunsetCore.GameTools.XInLineTools.Matrix;
using RedSunsetCore.Services.Event.ExtensionMethods;
using RedSunsetCore.Services.Event.Interfaces;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.GameTools.SlidePuzzle
{
    public class SlidePuzzleMatrix : ISlidePuzzleMatrix
    {
        readonly private SlidePuzzleTileData[] matrix;

        public SlidePuzzleMatrix(in int rowCount, in int columnCount)
        {
            matrix = new SlidePuzzleTileData[rowCount * columnCount];
            RowCount = rowCount;
            ColumnCount = columnCount;
        }

        private ISlidePuzzleMatrix ThisImpl => this;

        public int RowCount { get; }
        public int ColumnCount { get; }

        [Inject]
        IEventDispatcher IObjectEventDispatcher.Dispatcher { get; }

        public bool IsSolved(in int solvedRowCount, in int solvedColumnCount)
        {
            int remainingTilesToCheck = matrix.Count(tile => tile is not null);
            SlidePuzzleTileData currentSlidePuzzleTile =
                matrix.FirstOrDefault(tile => tile is not null && tile.Id == 0);

            if (currentSlidePuzzleTile != null)
            {
                CellPosition origin = currentSlidePuzzleTile.Position;
                int currentId = -1;

                for (var row = 0; row < solvedRowCount; row++)
                for (var column = 0; column < solvedColumnCount; column++)
                {
                    currentSlidePuzzleTile = ThisImpl[row + origin.Row, column + origin.Column];
                    if (currentSlidePuzzleTile is not null && currentSlidePuzzleTile.Id > currentId)
                    {
                        currentId = currentSlidePuzzleTile.Id;
                        remainingTilesToCheck--;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return remainingTilesToCheck == 0;
        }

        bool ISlidePuzzleMatrix.IsSolved()
        {
            return IsSolved(RowCount, ColumnCount);
        }

        public bool IsEmpty(CellPosition position)
        {
            return ThisImpl[position] == null;
        }

        public bool MoveTileToNextValidPosition(SlidePuzzleTileData slidePuzzleTileData)
        {
            CellPosition targetValidPosition;

            if (ThisImpl.IsValidPosition(slidePuzzleTileData.Position + CellPosition.Down) &&
                IsEmpty(slidePuzzleTileData.Position + CellPosition.Down))
                targetValidPosition = slidePuzzleTileData.Position + CellPosition.Down;
            else if (ThisImpl.IsValidPosition(slidePuzzleTileData.Position + CellPosition.Up) &&
                     IsEmpty(slidePuzzleTileData.Position + CellPosition.Up))
                targetValidPosition = slidePuzzleTileData.Position + CellPosition.Up;
            else if (ThisImpl.IsValidPosition(slidePuzzleTileData.Position + CellPosition.Left) &&
                     IsEmpty(slidePuzzleTileData.Position + CellPosition.Left))
                targetValidPosition = slidePuzzleTileData.Position + CellPosition.Left;
            else
                targetValidPosition = slidePuzzleTileData.Position + CellPosition.Right;

            return ThisImpl.MoveTile(slidePuzzleTileData.Position, targetValidPosition);
        }

        bool ISlidePuzzleMatrix.IsValidPosition(CellPosition position)
        {
            return ThisImpl.IsValidPosition(position.Row, position.Column);
        }

        bool ISlidePuzzleMatrix.IsValidPosition(int row, int column)
        {
            return row >= 0 &&
                   row < RowCount &&
                   column >= 0 &&
                   column < ColumnCount;
        }

        bool ISlidePuzzleMatrix.Insert(SlidePuzzleTileData data)
        {
            var operationResult = false;
            for (var i = 0; i < matrix.Length; i++)
                if (matrix[i] is null)
                {
                    ThisImpl[i] = data;
                    operationResult = true;
                    break;
                }

            return operationResult;
        }

        SlidePuzzleTileData ISlidePuzzleMatrix.this[CellPosition index]
        {
            get
            {
                SlidePuzzleTileData data = null;
                if (ThisImpl.IsValidPosition(index)) data = matrix[index.Row * ColumnCount + index.Column];
                return data;
            }
            set
            {
                int arrayIndex = index.Row * ColumnCount + index.Column;
                matrix[arrayIndex] = value;
                if (value is not null)
                {
                    value.Position = index;
                    value.CurrentIndex = arrayIndex;
                }
            }
        }

        SlidePuzzleTileData ISlidePuzzleMatrix.this[int row, int column]
        {
            get
            {
                SlidePuzzleTileData data = null;
                if (ThisImpl.IsValidPosition(row, column)) data = matrix[row * ColumnCount + column];
                return data;
            }
            set
            {
                int arrayIndex = row * ColumnCount + column;
                matrix[arrayIndex] = value;
                if (value is not null)
                {
                    value.Position = new CellPosition(row, column);
                    value.CurrentIndex = arrayIndex;
                }
            }
        }

        SlidePuzzleTileData ISlidePuzzleMatrix.this[int index]
        {
            get
            {
                SlidePuzzleTileData data = null;
                if (index >= 0 && index < matrix.Length) data = matrix[index];
                return data;
            }
            set
            {
                int row = index / ColumnCount;
                int column = index % ColumnCount;
                matrix[index] = value;
                if (value is not null)
                {
                    value.Position = new CellPosition(row, column);
                    value.CurrentIndex = index;
                }
            }
        }

        bool ISlidePuzzleMatrix.MoveTile(CellPosition from, CellPosition to)
        {
            var operationResult = false;

            if (!ThisImpl[from].IsLocked &&
                ThisImpl[to] is null &&
                ThisImpl.IsValidPosition(to))
            {
                ThisImpl[to] = ThisImpl[from];

                ThisImpl[from] = null;
                operationResult = true;
                ThisImpl.Dispatch<SlidePuzzleUpdatedEvent>(ev =>
                {
                    ev.Data = ThisImpl[to];
                    ev.From = from;
                    ev.To = to;
                });
            }

            return operationResult;
        }

        void ISlidePuzzleMatrix.Swap(CellPosition from, CellPosition to)
        {
            (ThisImpl[to], ThisImpl[from]) = (ThisImpl[from], ThisImpl[to]);
            (ThisImpl[from].Position, ThisImpl[to].Position) = (ThisImpl[to].Position, ThisImpl[from].Position);
        }

        void ISlidePuzzleMatrix.Shuffle()
        {
            List<SlidePuzzleTileData> lockedTiles = new();
            List<SlidePuzzleTileData> unlockedTiles = new();

            //Split tiles into two lists.
            for (var i = 0; i < matrix.Length; i++)
            {
                SlidePuzzleTileData slidePuzzleTileData = matrix[i];
                if (slidePuzzleTileData is not null)
                {
                    if (slidePuzzleTileData.IsLocked)
                    {
                        lockedTiles.Add(slidePuzzleTileData);
                    }
                    else
                    {
                        unlockedTiles.Add(slidePuzzleTileData);
                        matrix[i] = null;
                    }
                }
            }

            //Shuffle unlocked tiles.
            unlockedTiles = unlockedTiles.OrderBy(_ => Random.value).ToList();

            foreach (SlidePuzzleTileData tile in unlockedTiles)
            {
                CellPosition currentPosition = tile.Position;
                ThisImpl.Insert(tile);
                ThisImpl.Dispatch<SlidePuzzleUpdatedEvent>(ev =>
                {
                    ev.Data = tile;
                    ev.From = currentPosition;
                    ev.To = tile.Position;
                });
            }
        }
    }
}