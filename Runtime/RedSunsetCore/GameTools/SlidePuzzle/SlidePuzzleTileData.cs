using RedSunsetCore.GameTools.XInLineTools.Matrix;

namespace RedSunsetCore.GameTools.SlidePuzzle
{
    public class SlidePuzzleTileData
    {
        public SlidePuzzleTileData(int id)
        {
            Id = id;
        }

        public int CurrentIndex { get; set; }
        public int Id { get; }
        public CellPosition Position { get; set; }
        public bool IsLocked { get; set; }
    }
}