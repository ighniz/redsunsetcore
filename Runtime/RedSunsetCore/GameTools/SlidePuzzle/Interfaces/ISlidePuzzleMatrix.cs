using RedSunsetCore.GameTools.XInLineTools.Matrix;
using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.GameTools.SlidePuzzle.Interfaces
{
    public interface ISlidePuzzleMatrix : IObjectEventDispatcher
    {
        SlidePuzzleTileData this[CellPosition index] { get; set; }
        SlidePuzzleTileData this[int row, int column] { get; set; }
        SlidePuzzleTileData this[int index] { get; set; }
        bool MoveTile(CellPosition from, CellPosition to);
        void Swap(CellPosition from, CellPosition to);
        void Shuffle();
        bool IsValidPosition(CellPosition position);
        bool IsValidPosition(int row, int column);
        bool Insert(SlidePuzzleTileData data);
        bool IsSolved(in int solvedRowCount, in int solvedColumnCount);
        bool IsSolved();
        bool IsEmpty(CellPosition position);
        bool MoveTileToNextValidPosition(SlidePuzzleTileData slidePuzzleTileData);
    }
}