using UnityEngine;

namespace RedSunsetCore.GameTools.SlidePuzzle
{
    static public class ImageCutterTools
    {
        static public (Sprite img, int row, int column)[] ImageToSpriteMatrix(Texture2D texture, int rowAmount,
            int columnAmount)
        {
            var fragmentSize = new Vector2Int(texture.width / columnAmount, texture.height / rowAmount);
            var result = new (Sprite img, int row, int column)[rowAmount * columnAmount];

            for (var row = 0; row < rowAmount; row++)
            for (var column = 0; column < columnAmount; column++)
            {
                var area = new Rect(
                    column * fragmentSize.x,
                    row * fragmentSize.y,
                    fragmentSize.x,
                    fragmentSize.y);

                (Sprite img, int row, int column) fragmentData = new()
                {
                    img = Sprite.Create(texture, area, new Vector2(.5f, .5f)),
                    row = rowAmount - 1 - row,
                    column = column
                };

                result[row * rowAmount + column] = fragmentData;
            }

            return result;
        }
    }
}