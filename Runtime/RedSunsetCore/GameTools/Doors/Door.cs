using RedSunsetCore.GameTools.Doors.Events;
using RedSunsetCore.GameTools.Doors.Interfaces;
using RedSunsetCore.Services.Event.Interfaces;
using Zenject;

namespace RedSunsetCore.GameTools.Doors
{
    public class Door<TKey> : IDoor<TKey> where TKey : IKey
    {
        private IDoor<TKey> ThisImpl => this;

        string IDoor<TKey>.Id { get; }
        string IDoor<TKey>.RequiredKey { get; set; }

        private bool isLocked;
        bool IDoor<TKey>.IsLocked => isLocked;

        private bool isOpened;
        bool IDoor<TKey>.IsOpened => isOpened;

        bool IDoor<TKey>.Unlock(TKey key)
        {
            var operationSuccess = false;
            if (ThisImpl.RequiredKey == key.Code)
            {
                isLocked = false;
                operationSuccess = true;
                ThisImpl.Dispatcher.Dispatch<DoorUnlockedEvent<TKey>>(ev =>
                {
                    ev.Door = this;
                });
            }
            
            return operationSuccess;
        }

        bool IDoor<TKey>.Lock(TKey key)
        {
            var operationSuccess = false;
            if (ThisImpl.RequiredKey == key.Code)
            {
                isLocked = true;
                operationSuccess = true;
                ThisImpl.Dispatcher.Dispatch<DoorLockedEvent<TKey>>(ev =>
                {
                    ev.Door = this;
                });
            }
            
            return operationSuccess;
        }

        void IDoor<TKey>.Open()
        {
            if (!ThisImpl.IsLocked)
            {
                isOpened = true;
                ThisImpl.Dispatcher.Dispatch<DoorOpenedEvent<TKey>>(ev =>
                {
                    ev.Door = this;
                });
            }
        }

        void IDoor<TKey>.Close()
        {
            if (!ThisImpl.IsLocked)
            {
                isOpened = false;
                ThisImpl.Dispatcher.Dispatch<DoorClosedEvent<TKey>>(ev =>
                {
                    ev.Door = this;
                });
            }
        }

        [Inject]
        IEventDispatcher IObjectEventDispatcher.Dispatcher { get; }
    }
}