using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.GameTools.Doors.Events
{
	abstract public class DoorEvent : ICustomEvent
	{
		abstract public void Reset();
	}
}