using RedSunsetCore.GameTools.Doors.Interfaces;

namespace RedSunsetCore.GameTools.Doors.Events
{
    abstract public class DoorOpenedEvent : DoorEvent {}
    
    public class DoorOpenedEvent<TKey> : DoorOpenedEvent where TKey : IKey
    {
        public IDoor<TKey> Door { get; set; }
        
        override public void Reset()
        {
            Door = null;
        }
    }
}