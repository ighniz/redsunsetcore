using RedSunsetCore.GameTools.Doors.Interfaces;

namespace RedSunsetCore.GameTools.Doors.Events
{
    abstract public class DoorLockedEvent : DoorEvent {}
    
    public class DoorLockedEvent<TKey> : DoorLockedEvent where TKey : IKey
    {
        public IDoor<TKey> Door { get; set; }
        
        override public void Reset()
        {
            Door = null;
        }
    }
}