using RedSunsetCore.GameTools.Doors.Interfaces;

namespace RedSunsetCore.GameTools.Doors.Events
{
    abstract public class DoorClosedEvent : DoorEvent {}
    
    public class DoorClosedEvent<TKey> : DoorClosedEvent where TKey : IKey
    {
        public IDoor<TKey> Door { get; set; }

        override public void Reset()
        {
            Door = null;
        }
    }
}