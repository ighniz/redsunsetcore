using RedSunsetCore.GameTools.Doors.Interfaces;

namespace RedSunsetCore.GameTools.Doors.Events
{
    abstract public class DoorUnlockedEvent : DoorEvent {}
    
    public class DoorUnlockedEvent<TKey> : DoorUnlockedEvent where TKey : IKey
    {
        public IDoor<TKey> Door { get; set; }
        
        override public void Reset()
        {
            Door = null;
        }
    }
}