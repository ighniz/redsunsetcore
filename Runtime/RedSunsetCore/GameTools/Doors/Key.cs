using RedSunsetCore.GameTools.Doors.Interfaces;

namespace RedSunsetCore.GameTools.Doors
{
    public class Key : IKey
    {
        string IKey.Code { get; set; }

        static public TKey BuildKey<TKey, TKeyPart>(params TKeyPart[] keys)
            where TKey : IKey, new()
            where TKeyPart : IKeyPart
        {
            var key = new TKey();
            foreach (TKeyPart keyPart in keys)
            {
                key.Code += keyPart.Id;
            }

            return key;
        }
    }
}