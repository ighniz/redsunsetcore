using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.GameTools.Doors.Interfaces
{
    public interface IDoor<TKey> : IObjectEventDispatcher where TKey : IKey
    {
        string Id { get; }
        string RequiredKey { get; set; }
        bool IsLocked { get; }
        bool IsOpened { get; }

        bool Unlock(TKey key);
        bool Lock(TKey key);
        void Open();
        void Close();
    }
}