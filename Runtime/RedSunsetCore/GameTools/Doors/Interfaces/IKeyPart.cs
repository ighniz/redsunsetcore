namespace RedSunsetCore.GameTools.Doors.Interfaces
{
    public interface IKeyPart
    {
        public int Id { get; set; }
    }
}