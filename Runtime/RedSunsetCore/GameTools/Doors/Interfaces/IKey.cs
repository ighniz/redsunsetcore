namespace RedSunsetCore.GameTools.Doors.Interfaces
{
    public interface IKey
    {
        string Code { get; set; }
    }
}