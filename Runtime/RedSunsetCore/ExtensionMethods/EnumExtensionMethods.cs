using System;
using System.Linq;

namespace RedSunsetCore.ExtensionMethods
{
	static public class EnumExtensionMethods
	{
		static public bool Contains<T>(this T source, T value) where T : Enum
		{
			return source.Split().Contains(value);
		}

		static public T[] Split<T>(this T source) where T : Enum
		{
			string[] splitValues = source.ToString().Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
			var result = new T[splitValues.Length];
			for (var i = 0; i < result.Length; i++) result[i] = (T)Enum.Parse(typeof(T), splitValues[i]);

			return result;
		}
	}
}