using RedSunsetCore.DesignPatterns.ObserverPattern;

namespace RedSunsetCore.ExtensionMethods
{
	static public class ObservableExtensionMethod
	{
		static public void AddListener<T>(this IObservable<T> source, System.Action<IObservable<T>> action) where T : IObservable<T>
		{
			source.Listeners.Add(action);
		}

		static public void RemoveListener<T>(this IObservable<T> source, System.Action<IObservable<T>> action) where T : IObservable<T>
		{
			source.Listeners.Remove(action);
		}

		static public void NotifyListener<T>(this IObservable<T> source) where T : IObservable<T>
		{
			for (int i = source.Listeners.Count - 1; i >= 0; i--) source.Listeners[i].Invoke(source);
		}

		static public void ClearListeners<T>(this IObservable<T> source) where T : IObservable<T>
		{
			source.Listeners.Clear();
		}
	}
}