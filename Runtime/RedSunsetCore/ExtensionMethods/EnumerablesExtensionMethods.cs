﻿using System;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.MathTools.Interfaces;

namespace RedSunsetCore.ExtensionMethods
{
	static public class EnumerablesExtensionMethods
	{
		static private Random randomGenerator = new();

		static public T GetRandom<T>(this IList<T> source) where T : IWeighted
		{
			int totalWeight = source.Sum(item => item.Weight);
			int roll = randomGenerator.Next(totalWeight);

			foreach (T item in source)
			{
				if (roll < item.Weight) return item;

				roll -= item.Weight;
			}

			return default;
		}

		static public T GetRandomElement<T>(this IList<T> source)
		{
			int randomIndex = randomGenerator.Next(0, source.Count);
			return source[randomIndex];
		}

		static public IEnumerable<T> GetShuffled<T>(this IEnumerable<T> source)
		{
			return source.OrderBy(x => UnityEngine.Random.value);
		}
	}
}