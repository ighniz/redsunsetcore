using Defective.JSON;
using Newtonsoft.Json;
using RedSunsetCore.Utils.Interfaces.JSON;
using UnityEngine;

namespace RedSunsetCore.ExtensionMethods
{
	static public class JsonExtensionMethods
	{
		static public string ToJson(this IToJson source, bool pretty = false)
		{
			return JsonConvert.SerializeObject(source, pretty ? Formatting.Indented : Formatting.None);
		}

		static public JSONObject ToJsonObject(this IToJson source)
		{
			return JSONObject.Create(source.ToJson());
		}

		static public void FromJson(this IFromJson source, string json)
		{
			JsonConvert.PopulateObject(json, source);
		}

		static public void FromJson(this IFromJson source, JSONObject jsonObject)
		{
			JsonConvert.PopulateObject(jsonObject.ToString(), source);
		}
	}
}