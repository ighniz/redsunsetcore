using System.Collections.Generic;

namespace RedSunsetCore.ExtensionMethods
{
	static public class DictionariesExtensionMethods
	{
		static public bool AddIfNotExist<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
		{
			bool exist = dictionary.ContainsKey(key);
			if (!exist) dictionary.Add(key, value);

			return !exist;
		}
	}
}