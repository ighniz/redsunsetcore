using RedSunsetCore.Services.Async;
using RedSunsetCore.Services.Async.Interfaces;

namespace RedSunsetCore.ExtensionMethods
{
	static public class AsyncServiceInterfacesExtensionMethods
	{
		/// <summary>
		///     Adds an object to awake list.
		/// </summary>
		/// <param name="target">Target.</param>
		static public void AddToAwakeList(this IAwakeReceiver target)
		{
			int index = AsyncManager.instance.FindIndexForTarget(target, AsyncManager.instance.awakes);
			AsyncManager.instance.awakes[index].Add(target);
		}

		/// <summary>
		///     Removes an object to awake list.
		/// </summary>
		/// <param name="target">Target.</param>
		static public void RemoveFromAwakeList(this IAwakeReceiver target)
		{
			int index = AsyncManager.instance.FindIndexForTarget(target, AsyncManager.instance.awakes);
			AsyncManager.instance.awakes[index].Remove(target);
		}

		/// <summary>
		///     Adds an object to start list.
		/// </summary>
		/// <param name="target">Target.</param>
		static public void AddToStartList(this IStartReceiver target)
		{
			int index = AsyncManager.instance.FindIndexForTarget(target, AsyncManager.instance.starts);
			AsyncManager.instance.starts[index].Add(target);
		}

		/// <summary>
		///     Removes an object to start list.
		/// </summary>
		/// <param name="target">Target.</param>
		static public void RemoveFromStartList(this IStartReceiver target)
		{
			int index = AsyncManager.instance.FindIndexForTarget(target, AsyncManager.instance.starts);
			AsyncManager.instance.starts[index].Remove(target);
		}

		/// <summary>
		///     Adds an object to update list.
		/// </summary>
		/// <param name="target">Target.</param>
		static public void AddToUpdateList(this IUpdateReceiver target)
		{
			int index = AsyncManager.instance.FindIndexForTarget(target, AsyncManager.instance.updates);
			AsyncManager.instance.updates[index].Add(target);
		}

		/// <summary>
		///     Removes an object to update list.
		/// </summary>
		/// <param name="target">Target.</param>
		static public void RemoveFromUpdateList(this IUpdateReceiver target)
		{
			int index = AsyncManager.instance.FindIndexForTarget(target, AsyncManager.instance.updates);
			AsyncManager.instance.updates[index].Remove(target);
		}

		/// <summary>
		///     Adds an object to late update list.
		/// </summary>
		/// <param name="target">Target.</param>
		static public void AddToLateUpdateList(this ILateUpdateReceiver target)
		{
			int index = AsyncManager.instance.FindIndexForTarget(target, AsyncManager.instance.lateUpdates);
			AsyncManager.instance.lateUpdates[index].Add(target);
		}

		/// <summary>
		///     Removes an object to late update list.
		/// </summary>
		/// <param name="target">Target.</param>
		static public void RemoveFromLateUpdateList(this ILateUpdateReceiver target)
		{
			int index = AsyncManager.instance.FindIndexForTarget(target, AsyncManager.instance.lateUpdates);
			AsyncManager.instance.lateUpdates[index].Remove(target);
		}

		/// <summary>
		///     Adds an object to fixed update list.
		/// </summary>
		/// <param name="target">Target.</param>
		static public void AddToFixedUpdateList(this IFixedUpdateReceiver target)
		{
			int index = AsyncManager.instance.FindIndexForTarget(target, AsyncManager.instance.lateUpdates);
			AsyncManager.instance.fixedUpdates[index].Add(target);
		}

		/// <summary>
		///     Removes an object to fixed update list.
		/// </summary>
		/// <param name="target">Target.</param>
		static public void RemoveFromFixedUpdateList(this IFixedUpdateReceiver target)
		{
			int index = AsyncManager.instance.FindIndexForTarget(target, AsyncManager.instance.lateUpdates);
			AsyncManager.instance.fixedUpdates[index].Remove(target);
		}

		/// <summary>
		///     Adds an object to simulated fixed update list.
		/// </summary>
		/// <param name="target">Target.</param>
		static public void AddToSimulatedFixedUpdateList(this ISimulatedFixedUpdateReceiver target)
		{
			int index = AsyncManager.instance.FindIndexForTarget(target, AsyncManager.instance.simulatedFixedUpdates);
			AsyncManager.instance.simulatedFixedUpdates[index].Add(target);
		}

		/// <summary>
		///     Removes an object to simulated fixed update list.
		/// </summary>
		/// <param name="target">Target.</param>
		static public void RemoveFromSimulatedFixedUpdateList(this ISimulatedFixedUpdateReceiver target)
		{
			int index = AsyncManager.instance.FindIndexForTarget(target, AsyncManager.instance.simulatedFixedUpdates);
			AsyncManager.instance.simulatedFixedUpdates[index].Remove(target);
		}
	}
}