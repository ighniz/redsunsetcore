using System;
using System.Collections.Generic;
using System.Linq;

namespace RedSunsetCore.ExtensionMethods
{
	static public class TypeExtensionMethods
	{
		static public IEnumerable<Type> GetAllSubtypes<T>(this T source)
		{
			return typeof(T).GetAllSubtypes();
		}

		static public IEnumerable<Type> GetAllSubtypes(this Type source)
		{
			return AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(p => source.IsAssignableFrom(p) && !p.IsInterface && !p.IsAbstract);
		}
	}
}