using RedSunsetCore.Constants;

namespace RedSunsetCore.ExtensionMethods
{
	static public class StringExtensionMethods
	{
		static public string GetFormatted(this string source, params object[] format)
		{
			return string.Format(source, format);
		}

		static public string ToAssertFormat(this string source)
		{
			return StringConstants.Debug.GetFormattedMessage(source);
		}
	}
}