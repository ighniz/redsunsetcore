using UnityEngine;

namespace RedSunsetCore.ExtensionMethods
{
	static public class Vector3ExtensionMethods
	{
		static public Vector2 xy(this Vector3 source)
		{
			return new Vector2(source.x, source.y);
		}
	}
}