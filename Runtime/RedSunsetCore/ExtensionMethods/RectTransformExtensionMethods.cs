using UnityEngine;

namespace RedSunsetCore.ExtensionMethods
{
	static public class RectTransformExtensionMethods
	{
		static public void Stretch(this RectTransform source)
		{
			// Set the overlay to cover the whole screen.
			source.anchorMin = Vector2.zero;
			source.anchorMax = Vector2.one;
			source.sizeDelta = Vector2.zero;
			source.anchoredPosition = Vector2.zero;
		}
	}
}