using UnityEngine;

namespace RedSunsetCore.ExtensionMethods
{
	static public class GameObjectExtensionMethods
	{
		static public string GetHierarchyPath(this GameObject gameObject)
		{
			return gameObject.transform.parent == null ? gameObject.name : gameObject.transform.parent.gameObject.GetHierarchyPath() + "/" + gameObject.name;
		}
	}
}