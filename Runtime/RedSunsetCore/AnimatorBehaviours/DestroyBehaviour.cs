using RedSunsetCore.Core;
using UnityEngine;

namespace RedSunsetCore.AnimatorBehaviours
{
    public class DestroyBehaviour : RedsunsetAnimatorBehaviour
    {
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            Destroy(animator.gameObject);
        }
    }
}