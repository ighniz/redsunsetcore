using System;
using System.Collections;
using RedSunsetCore.Services.Async;
using UnityEngine;

namespace RedSunsetCore.CorePlugins.iTweenPlugin
{
    static public class TweenExtensionMethods
    {
        static public void AddSettings(ref Hashtable settings, object[] parameters)
        {
            if (parameters.Length == 0 || parameters.Length % 2 == 0)
            {
                for (var i = 0; i < parameters.Length; i += 2)
                {
                    settings.Add(parameters[i], parameters[i+1]);
                }
            }
            else
            {
                throw new Exception("The parameters must be a multiple of 2.");
            }
        }
        
        static public void MoveTo(this GameObject source, params object[] parameters)
        {
            AsyncManager.instance.StartCoroutine(TweenCoroutine(source, () =>
            {
                var hashtable = new Hashtable();
                AddSettings(ref hashtable, parameters);
                iTween.MoveTo(source, hashtable);
            }));
        }

        static private IEnumerator TweenCoroutine(GameObject source, Action initializer)
        {
            iTween.Stop(source);
            yield return null;
            initializer.Invoke();
        }

        static public void MoveTo(this GameObject source, Vector3 position, float time,
                                  iTween.EaseType easeType = iTween.EaseType.linear, params object[] parameters)
        {
            AsyncManager.instance.StartCoroutine(TweenCoroutine(source, () =>
            {
                var hashtable = new Hashtable
                {
                    {"position", position},
                    {"time", time},
                    {"easetype", easeType}
                };
            
                AddSettings(ref hashtable, parameters);
            
                iTween.MoveTo(source, hashtable);
            }));
        }
        
        static public void ScaleTo(this GameObject source, params object[] parameters)
        {
            AsyncManager.instance.StartCoroutine(TweenCoroutine(source, () =>
            {
                var hashtable = new Hashtable();
                AddSettings(ref hashtable, parameters);
                iTween.ScaleTo(source, hashtable);
            }));
        }

        static public void ScaleTo(this GameObject source, Vector3 scale, float time, iTween.EaseType easeType, params object[] parameters)
        {
            AsyncManager.instance.StartCoroutine(TweenCoroutine(source, () =>
            {
                var hashtable = new Hashtable
                {
                    {"scale", scale},
                    {"time", time},
                    {"easetype", easeType}
                };
            
                AddSettings(ref hashtable, parameters);
            
                iTween.ScaleTo(source, hashtable);
            }));
        }
    }
}