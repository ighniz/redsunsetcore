using System.Collections.Generic;
using RedSunsetCore.ExtensionMethods;
using UnityEngine;

namespace RedSunsetCore.MathTools
{
	static public class VectorTools
	{
		static public List<Vector2Int> Cardinals = new()
		{
			Vector2Int.up,
			Vector2Int.right,
			Vector2Int.down,
			Vector2Int.left
		};

		static public List<Vector2Int> Diagonals = new()
		{
			new Vector2Int(1, 1),
			new Vector2Int(1, -1),
			new Vector2Int(-1, -1),
			new Vector2Int(-1, 1)
		};

		static public List<Vector2Int> Directions2D = new()
		{
			Vector2Int.up,
			new Vector2Int(1, 1),
			Vector2Int.right,
			new Vector2Int(1, -1),
			Vector2Int.down,
			new Vector2Int(-1, -1),
			Vector2Int.left,
			new Vector2Int(-1, 1)
		};

		static public Vector2Int GetRandomCardinalDirection()
		{
			return Cardinals.GetRandomElement();
		}
	}
}