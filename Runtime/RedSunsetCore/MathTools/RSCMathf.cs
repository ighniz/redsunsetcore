using UnityEngine;

namespace RedSunsetCore.MathTools
{
    static public class RSCMathf
    {
        static public void RecalculateBasedOnAspectRatio(ref this Vector3 source, float aspectRatio, float designAspectRatio)
        {
            float ratioDifference = aspectRatio / designAspectRatio;
            source.x *= ratioDifference;
        }
        
        static public Vector3 GetEquivalentBasedOnAspectRatio(this Vector3 source, float aspectRatio, float designAspectRatio)
        {
            float ratioDifference = aspectRatio / designAspectRatio;
            return new Vector3(source.x * ratioDifference, source.y, source.z);
        }
        
        static public void RecalculateBasedOnAspectRatio(ref this Vector2 source, float aspectRatio, float designAspectRatio)
        {
            float ratioDifference = aspectRatio / designAspectRatio;
            source.x *= ratioDifference;
        }
        
        static public Vector2 GetEquivalentBasedOnAspectRatio(this Vector2 source, float aspectRatio, float designAspectRatio)
        {
            float ratioDifference = aspectRatio / designAspectRatio;
            return new Vector2(source.x * ratioDifference, source.y);
        }
    }
}