namespace RedSunsetCore.MathTools.Interfaces
{
    public interface IWeighted
    {
        int Weight { get; }
    }
}