using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RedSunsetCore.Attributes
{
    public class ClassEnumAttribute : PropertyAttribute
    {
        public string[] allSubclassesAsString;
        
        public ClassEnumAttribute(params Type[] allTypes)
        {
            List<Type> tempList = new List<Type>();
            foreach (var t in allTypes)
            {
                IEnumerable<Type> types = AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(s => s.GetTypes())
                    .Where(p => t.IsAssignableFrom(p) && !p.IsInterface && !p.IsAbstract);
                
                tempList.AddRange(types.ToList());
            }

            allSubclassesAsString = new string[tempList.Count+1];
            for (var i = 0; i < tempList.Count; i++)
            {
                allSubclassesAsString[i+1] = tempList[i].FullName;
            }

            allSubclassesAsString[0] = "None";
        }
    }
}