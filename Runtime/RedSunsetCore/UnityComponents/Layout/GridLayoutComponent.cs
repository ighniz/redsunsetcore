using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.Core.Events;
using RedSunsetCore.CorePlugins.iTweenPlugin;
using RedSunsetCore.GameTools.XInLineTools.Matrix;
using RedSunsetCore.Services.Event.ExtensionMethods;
using RedSunsetCore.UnityComponents.Layout.Base;
using RedSunsetCore.UnityComponents.Layout.Events;
using RedSunsetCore.Utils.YieldsInstructions;
using UnityEngine;

namespace RedSunsetCore.UnityComponents.Layout
{
    public class GridLayoutComponent : LayoutComponent
    {
        public enum OrderPriorityType
        {
            Rows,
            Columns
        }
        
        public int rowsCount = 1;
        public int columnsCount = 1;
        public Vector2 spacing = Vector2.zero;
        public Vector2 cellSize = Vector2.one;
        public OrderPriorityType orderPriority;
        [HideInInspector]
        public float timeToPosition;
        [HideInInspector]
        public float timeToScale;
        public iTween.EaseType easeType;
        public bool invertRows;
        public bool invertColumns;

        private Dictionary<CellPosition, GridLayoutElementComponent> elementsByCellPosition;
        
        private List<GridLayoutElementComponent> allChildren;
        public List<GridLayoutElementComponent> AllChildren => allChildren;

        protected HashSet<GridLayoutElementComponent> elementsMoving = new();

        override protected void Awake()
        {
            base.Awake();
            
            allChildren = new List<GridLayoutElementComponent>();
            elementsByCellPosition = new Dictionary<CellPosition, GridLayoutElementComponent>();
            
            UpdateLayout();
        }

        override protected void Validate()
        {
            rowsCount = Mathf.Max(1, rowsCount);
            columnsCount = Mathf.Max(1, columnsCount);
            cellSize.x = Mathf.Max(0.00001f, cellSize.x);
            cellSize.y = Mathf.Max(0.00001f, cellSize.y);
            timeToPosition = Mathf.Max(0, timeToPosition);
            timeToScale = Mathf.Max(0, timeToScale);
            
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                timeToPosition = 0;
                timeToScale = 0;
            }
#endif
        }

        public T GetChild<T>(CellPosition position) where T : GridLayoutElementComponent
        {
            return elementsByCellPosition[position] as T;
        }

        public void AddElement(GridLayoutElementComponent element)
        {
            allChildren.Add(element);
            element.AddListener<RedSunsetCoreBehaviourDestroyEvent<GridLayoutElementComponent>>(OnElementDestroyed);
        }

        public void AddElement(Transform elementTransform)
        {
            var element = elementTransform.GetComponent<GridLayoutElementComponent>();
            if (element == null)
            {
                element = elementTransform.gameObject.AddComponent<GridLayoutElementComponent>();
            }
            
            AddElement(element);
        }

        public void RemoveElement(GridLayoutElementComponent element)
        {
            allChildren.Remove(element);
            elementsMoving.Remove(element);
        }

        public void RemoveElement(Transform elementTransform)
        {
            var element = elementTransform.gameObject.GetComponent<GridLayoutElementComponent>();
            if (element != null)
            {
                RemoveElement(element);
            }
        }

        private void OnElementDestroyed(RedSunsetCoreBehaviourDestroyEvent<GridLayoutElementComponent> destroyEvent)
        {
            elementsMoving.Remove(destroyEvent.TargetBehaviour);
            allChildren.Remove(destroyEvent.TargetBehaviour);
        }

        override public void UpdateLayout()
        {
            base.UpdateLayout();
            
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                allChildren = GetComponentsInChildren<GridLayoutElementComponent>().ToList();
            }
#endif
            
            UpdateLayout(allChildren);
        }

        override public void UpdateLayout<T>(List<T> elements)
        {
            base.UpdateLayout(elements);

            StartCoroutine(InternalUpdateLayout(elements));
        }

        virtual protected IEnumerator InternalUpdateLayout<T>(List<T> elements)
        {
            int childCount = elements.Count;
            int matrixSize = rowsCount * columnsCount;
            int matrixSizeExcess = Mathf.Max(0, childCount - matrixSize);
            int limitRows = rowsCount;
            int limitColumns = columnsCount;

            if (orderPriority == OrderPriorityType.Rows)
            {
                limitRows += matrixSizeExcess;
            }
            else
            {
                limitColumns += matrixSizeExcess;
            }

            var index = 0;
            if (childCount > 0)
            {
                for (var row = 0; row < limitRows; row++)
                {
                    for (var column = 0; column < limitColumns; column++)
                    {
                        var child = elements[index] as GridLayoutElementComponent;
                        if (child != null && child.isDirty)
                        {
                            if (!child.IgnoreLayout)
                            {
                                child.Position = new CellPosition(row, column);
                            }

                            ApplyPosition(child);
                            ApplyScale(child);
                            
                            IEnumerator yieldForApplyMovement = GetYieldInstructionAfterApplyMovement(child);

                            if (yieldForApplyMovement is not NoWaitYield)
                            {
                                yield return yieldForApplyMovement;
                            }
                            
                            elementsByCellPosition[child.Position] = child;
                            
                            child.isDirty = false;
                        }

                        index++;
                        if (index >= childCount)
                        {
                            goto Finish;
                        }
                    }
                }
            }

            Finish: ;
        }

        private void ApplyPosition(GridLayoutElementComponent gridLayoutElementComponent)
        {
            Transform elementTransform = gridLayoutElementComponent.transform;

            Vector3 targetPosition = GetPositionFrom(gridLayoutElementComponent.Position, elementTransform.localPosition.z);

            if (gridLayoutElementComponent.IgnoreLayoutMovementToPosition)
            {
                float movementUnitAmount =
                    gridLayoutElementComponent.unitMovementType == GridLayoutElementComponent.UnitMovementType.Time
                        ? gridLayoutElementComponent.timeToPosition
                        : gridLayoutElementComponent.speedToPosition;
                
                CreateMovement(
                    gridLayoutElementComponent, 
                    transform.TransformPoint(targetPosition),
                    movementUnitAmount, 
                    gridLayoutElementComponent.IgnoreEaseType ? gridLayoutElementComponent.easeType : easeType
                    );
            }else if (timeToPosition > 0)
            {
                CreateMovement(gridLayoutElementComponent, transform.TransformPoint(targetPosition), timeToPosition, easeType);
            }
            else
            {
                elementTransform.localPosition = targetPosition;
            }
        }

        virtual protected void CreateMovement(GridLayoutElementComponent gridLayoutElementComponent, Vector3 targetPosition, float targetTimeOrSpeed, iTween.EaseType targetEaseType)
        {
            elementsMoving.Add(gridLayoutElementComponent);
            gridLayoutElementComponent.gameObject.MoveTo(
                "position", targetPosition,
                gridLayoutElementComponent.unitMovementType == GridLayoutElementComponent.UnitMovementType.Time ? "time" : "speed", targetTimeOrSpeed,
                "easeType", targetEaseType,
                "oncompletetarget", gameObject,
                "oncomplete", nameof(OnElementMovementComplete),
                "oncompleteparams", gridLayoutElementComponent);
        }

        virtual protected IEnumerator GetYieldInstructionAfterApplyMovement(GridLayoutElementComponent element)
        {
            return null;
        }

        virtual protected void OnElementMovementComplete(GridLayoutElementComponent elementCompleted)
        {
            elementsMoving.Remove(elementCompleted);
            if (elementsMoving.Count <= 0)
            {
                OnAllMovementsComplete();
            }
        }

        virtual protected void OnAllMovementsComplete()
        {
            Dispatcher.Dispatch<GridLayoutComponentAllMovementsCompleteEvent>();
        }

        private void ApplyScale(GridLayoutElementComponent gridLayoutElementComponent)
        {
            Transform elementTransform = gridLayoutElementComponent.transform;
            Bounds childBounds = elementTransform.GetComponent<Renderer>().bounds;
            float finalScaleX = cellSize.x / (childBounds.size.x + 0.000001f);
            float finalScaleY = cellSize.y / (childBounds.size.y + 0.000001f);
                        
            Vector3 currentScale = elementTransform.localScale;
            currentScale.x *= finalScaleX;
            currentScale.y *= finalScaleY;

            if (!gridLayoutElementComponent.IgnoreScale)
            {
                if (timeToScale > 0)
                {
                    gridLayoutElementComponent.gameObject.ScaleTo(currentScale, timeToScale, iTween.EaseType.easeOutElastic);
                }
                else
                {
                    elementTransform.localScale = currentScale;
                }
            }
        }

        public Vector3 GetPositionFrom(int row, int column, float z)
        {
            return new Vector3(
                column * spacing.x,
                row * spacing.y, z);
        }

        public Vector3 GetPositionFrom(CellPosition position, float z)
        {
            return new Vector3(
                position.Column * spacing.x,
                position.Row * spacing.y, z);
        }

        public CellPosition GetMatrixPositionFrom(Vector3 worldPoint)
        {
            Vector3 localPoint = transform.InverseTransformPoint(worldPoint);
            
            return new CellPosition(Mathf.RoundToInt(localPoint.y / spacing.y), Mathf.RoundToInt(localPoint.x / spacing.x));
        }
    }
}