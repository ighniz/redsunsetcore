using RedSunsetCore.Core;
using UnityEngine;

namespace RedSunsetCore.UnityComponents.Layout.Base
{
    public class LayoutElementComponent : RedsunsetCoreBehaviour
    {
        [SerializeField]
        private bool ignoreLayout;
        public bool IgnoreLayout
        {
            get => ignoreLayout;
            set => ignoreLayout = value;
        }
        
        [HideInInspector]
        public bool isDirty;
    }
}