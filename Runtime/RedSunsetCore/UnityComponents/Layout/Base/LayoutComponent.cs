using System.Collections.Generic;
using RedSunsetCore.Core;
using RedSunsetCore.UnityComponents.Layout.Interfaces;

namespace RedSunsetCore.UnityComponents.Layout.Base
{
    abstract public class LayoutComponent : RedsunsetCoreBehaviour, IUnityComponentLayout
    {
        abstract protected void Validate();

        virtual public void UpdateLayout()
        {
            Validate();
        }
        
        virtual public void UpdateLayout<T>(List<T> elements) where T : LayoutElementComponent
        {
            Validate();
        }
    }
}