using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using UnityEngine;

namespace RedSunsetCore.UnityComponents.Layout.Responsive
{
    /// <summary>
    ///     This class save the config to keep the design layout.
    /// </summary>
    [CreateAssetMenu(fileName = StringConstants.Files.RESPONSIVE_CONFIG, menuName = StringConstants.Config.MENU_CREATE_RESPONSIVE_CONFIG)]
    public class ResponsiveConfig : RedSunsetScriptableConfig<ResponsiveConfig>
    {
        [SerializeField]
        private float designAspectRatio;
        public float DesignAspectRatio => designAspectRatio;

        //TODO: Temporal fix.
        public float TargetAspectRatio => Camera.main.aspect;

        override public void Reset()
        {
            
        }
    }
}