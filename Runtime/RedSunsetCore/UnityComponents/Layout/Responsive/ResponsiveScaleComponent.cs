using RedSunsetCore.Core;
using RedSunsetCore.MathTools;
using UnityEngine;

namespace RedSunsetCore.UnityComponents.Layout.Responsive
{
    public class ResponsiveScaleComponent : RedsunsetCoreBehaviour
    {
        [SerializeField]
        private ResponsiveConfig responsiveConfig;
        public ResponsiveConfig ResponsiveConfig
        {
            get => responsiveConfig;
            set => responsiveConfig = value;
        }

        override protected void Awake()
        {
            base.Awake();
            transform.localScale = transform.localScale.GetEquivalentBasedOnAspectRatio(responsiveConfig.TargetAspectRatio, responsiveConfig.DesignAspectRatio);
        }
    }
}