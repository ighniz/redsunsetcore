using RedSunsetCore.Core.Events;
using RedSunsetCore.CorePlugins.iTweenPlugin;
using RedSunsetCore.GameTools.XInLineTools.Matrix;
using RedSunsetCore.UnityComponents.Layout.Base;
using UnityEngine;

namespace RedSunsetCore.UnityComponents.Layout
{
    public class GridLayoutElementComponent : LayoutElementComponent
    {
        public enum UnitMovementType
        {
            Time,
            Speed
        }
        
        [SerializeField]
        private CellPosition position;
        public CellPosition Position
        {
            get => position;
            set
            {
                position = value;
                isDirty = true;
            }
        }

        [SerializeField]
        private bool ignoreScale;
        public bool IgnoreScale
        {
            get => ignoreScale;
            set
            {
                ignoreScale = value;
                isDirty = true;
            }
        }
        
        [SerializeField]
        private bool ignoreEaseType;
        public bool IgnoreEaseType
        {
            get => ignoreEaseType;
            set
            {
                ignoreEaseType = value;
                isDirty = true;
            }
        }

        public iTween.EaseType easeType;
        
        [SerializeField]
        private bool ignoreLayoutMovementToPosition;
        public bool IgnoreLayoutMovementToPosition
        {
            get => ignoreLayoutMovementToPosition;
            set
            {
                ignoreLayoutMovementToPosition = value;
                isDirty = true;
            }
        }
        
        public float timeToPosition;
        public float speedToPosition;
        public int elementId = -1;
        
        public UnitMovementType unitMovementType;
        
        override protected void OnNotifyDestroy()
        {
            Dispatcher.Dispatch<RedSunsetCoreBehaviourDestroyEvent<GridLayoutElementComponent>>(ev =>
            {
                ev.TargetBehaviour = this;
            });
        }
    }
}