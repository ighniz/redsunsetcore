namespace RedSunsetCore.UnityComponents.Layout.Interfaces
{
    public interface IUnityComponentLayout
    {
        void UpdateLayout();
    }
}