using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace RedSunsetCore.AI
{
	public class MarkovChain
	{
		readonly private Dictionary<string, Dictionary<string, int>> mLookupDictionary = new();

		public void Train(string text)
		{
			string[] words = text.Split(' ');

			for (var i = 0; i < words.Length - 1; i++)
			{
				if (mLookupDictionary.ContainsKey(words[i]))
				{
					if (mLookupDictionary[words[i]].ContainsKey(words[i + 1]))
					{
						mLookupDictionary[words[i]][words[i + 1]]++;
					}
					else
					{
						mLookupDictionary[words[i]].Add(words[i + 1], 1);
					}
				}
				else
				{
					var newDictEntry = new Dictionary<string, int> { { words[i + 1], 1 } };
					mLookupDictionary.Add(words[i], newDictEntry);
				}
			}
		}

		public string GenerateResponse(string userInput, int sentenceLength)
		{
			var sentence = new StringBuilder();
			var random = new Random();
			string[] inputWords = userInput.Split(' ');
			string currentWord = inputWords[^1];

			if (!mLookupDictionary.ContainsKey(currentWord))
			{
				var words = new List<string>(mLookupDictionary.Keys);
				currentWord = words[random.Next(words.Count)];
			}

			sentence.Append(currentWord);

			for (var i = 1; i < sentenceLength; i++)
				if (mLookupDictionary.ContainsKey(currentWord))
				{
					var nextWordList = new List<string>(mLookupDictionary[currentWord].Keys);
					var totalOccurences = 0;
					foreach (string word in nextWordList) totalOccurences += mLookupDictionary[currentWord][word];

					int randomIndex = random.Next(totalOccurences);
					var nextWord = "";
					foreach (string word in nextWordList)
						if (randomIndex < mLookupDictionary[currentWord][word])
						{
							nextWord = word;
							break;
						}
						else
						{
							randomIndex -= mLookupDictionary[currentWord][word];
						}

					sentence.Append(" " + nextWord);
					currentWord = nextWord;
				}
				else
				{
					var words = new List<string>(mLookupDictionary.Keys);
					currentWord = words[random.Next(words.Count)];
					sentence.Append(" " + currentWord);
				}

			return sentence.ToString();
		}

		public string GetJsonData()
		{
			return JsonConvert.SerializeObject(mLookupDictionary, Formatting.Indented);
		}
	}
}