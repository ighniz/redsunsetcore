using System.Collections.Generic;
using Newtonsoft.Json;
using RedSunsetCore.Config;
using RedSunsetCore.DesignPatterns.ObserverPattern;
using RedSunsetCore.Utils.Interfaces.Common;
using UnityEngine;

namespace RedSunsetCore.Scriptables
{
    abstract public class RedSunsetScriptable<T> : RedSunsetScriptable, IObservable<T> where T : IObservable<T>
    {
        public List<System.Action<IObservable<T>>> Listeners { get; } = new ();
    }

    abstract public class RedSunsetScriptable : ScriptableObject, IResettable, IConfig
    {
        [SerializeField]
        private bool usePrettyJsonFormat;
        public bool UsePrettyJsonFormat
        {
            get => usePrettyJsonFormat;
            set => usePrettyJsonFormat = value;
        }

        virtual public void FromJson(string json)
        {
            JsonConvert.PopulateObject(json, this);
        }
        
        virtual public string ToJson()
        {
            return JsonConvert.SerializeObject(this, usePrettyJsonFormat ? Formatting.Indented : Formatting.None);
        }

        abstract public void Reset();
    }
}