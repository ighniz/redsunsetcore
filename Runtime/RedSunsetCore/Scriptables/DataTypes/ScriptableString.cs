using RedSunsetCore.Constants;
using UnityEngine;

namespace RedSunsetCore.Scriptables.DataTypes
{
    [CreateAssetMenu(fileName = StringConstants.ScriptablesDataType.STRING_NEW_FILE, menuName = StringConstants.ScriptablesDataType.MENU_CREATE_STRING)]
    public class ScriptableString : ScriptableVariable<string>
    {
        static public string operator +(ScriptableString left, string right)
        {
            return left.Value + right;
        }

        static public implicit operator string(ScriptableString other)
        {
            return other.Value;
        }
    }
}