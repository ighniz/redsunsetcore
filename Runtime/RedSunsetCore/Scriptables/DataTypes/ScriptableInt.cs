using RedSunsetCore.Constants;
using UnityEngine;

namespace RedSunsetCore.Scriptables.DataTypes
{
    [CreateAssetMenu(fileName = StringConstants.ScriptablesDataType.INT_NEW_FILE, menuName = StringConstants.ScriptablesDataType.MENU_CREATE_INT)]
    public class ScriptableInt : ScriptableVariable<int>
    {
        static public int operator +(ScriptableInt left, int right)
        {
            return left.Value + right;
        }

        static public ScriptableInt operator ++(ScriptableInt other)
        {
            other.Value++;
            return other;
        }

        static public int operator -(ScriptableInt left, int right)
        {
            return left.Value - right;
        }

        static public ScriptableInt operator --(ScriptableInt other)
        {
            other.Value--;
            return other;
        }

        static public int operator *(ScriptableInt left, int right)
        {
            return left.Value * right;
        }
        
        static public int operator /(ScriptableInt left, int right)
        {
            return left.Value / right;
        }

        static public int operator /(int left, ScriptableInt right)
        {
            return left / right.Value;
        }

        static public implicit operator int(ScriptableInt other)
        {
            return other.Value;
        }
    }
}