using RedSunsetCore.Constants;
using UnityEngine;

namespace RedSunsetCore.Scriptables.DataTypes
{
    [CreateAssetMenu(fileName = StringConstants.ScriptablesDataType.BOOL_NEW_FILE, menuName = StringConstants.ScriptablesDataType.MENU_CREATE_BOOL)]
    public class ScriptableBool : ScriptableVariable<bool>
    {
        static public implicit operator bool(ScriptableBool other)
        {
            return other.Value;
        }
    }
}