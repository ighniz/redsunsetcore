using RedSunsetCore.Constants;
using UnityEngine;

namespace RedSunsetCore.Scriptables.DataTypes
{
    [CreateAssetMenu(fileName = StringConstants.ScriptablesDataType.FLOAT_NEW_FILE, menuName = StringConstants.ScriptablesDataType.MENU_CREATE_FLOAT)]
    public class ScriptableFloat : ScriptableVariable<float>
    {
        static public float operator +(ScriptableFloat left, float right)
        {
            return left.Value + right;
        }

        static public ScriptableFloat operator ++(ScriptableFloat other)
        {
            other.Value++;
            return other;
        }

        static public float operator -(ScriptableFloat left, float right)
        {
            return left.Value - right;
        }

        static public ScriptableFloat operator --(ScriptableFloat other)
        {
            other.Value--;
            return other;
        }

        static public float operator *(ScriptableFloat left, float right)
        {
            return left.Value * right;
        }
        
        static public float operator /(ScriptableFloat left, float right)
        {
            return left.Value / right;
        }
        
        static public float operator /(long left, ScriptableFloat right)
        {
            return left / right.Value;
        }

        static public implicit operator float(ScriptableFloat other)
        {
            return other.Value;
        }
    }
}