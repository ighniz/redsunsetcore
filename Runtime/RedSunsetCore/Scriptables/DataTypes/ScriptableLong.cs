using RedSunsetCore.Constants;
using UnityEngine;

namespace RedSunsetCore.Scriptables.DataTypes
{
    [CreateAssetMenu(fileName = StringConstants.ScriptablesDataType.LONG_NEW_FILE, menuName = StringConstants.ScriptablesDataType.MENU_CREATE_LONG)]
    public class ScriptableLong : ScriptableVariable<long>
    {
        static public long operator +(ScriptableLong left, long right)
        {
            return left.Value + right;
        }

        static public ScriptableLong operator ++(ScriptableLong other)
        {
            other.Value++;
            return other;
        }

        static public long operator -(ScriptableLong left, long right)
        {
            return left.Value - right;
        }

        static public ScriptableLong operator --(ScriptableLong other)
        {
            other.Value--;
            return other;
        }

        static public long operator *(ScriptableLong left, long right)
        {
            return left.Value * right;
        }
        
        static public long operator /(ScriptableLong left, long right)
        {
            return left.Value / right;
        }

        static public long operator /(long left, ScriptableLong right)
        {
            return left / right.Value;
        }

        static public implicit operator long(ScriptableLong other)
        {
            return other.Value;
        }
    }
}