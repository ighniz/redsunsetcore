using RedSunsetCore.Constants;
using UnityEngine;

namespace RedSunsetCore.Scriptables.DataTypes
{
    [CreateAssetMenu(fileName = StringConstants.ScriptablesDataType.VECTOR_3_NEW_FILE, menuName = StringConstants.ScriptablesDataType.MENU_CREATE_VECTOR_3)]
    public class ScriptableVector3 : ScriptableVariable<Vector3>
    {
        static public ScriptableVector3 operator ++(ScriptableVector3 other)
        {
            other.Value += Vector3.one;
            return other;
        }

        static public ScriptableVector3 operator --(ScriptableVector3 other)
        {
            other.Value -= Vector3.one;
            return other;
        }

        static public Vector3 operator *(ScriptableVector3 left, int right)
        {
            return left.Value * right;
        }
        
        static public Vector3 operator *(ScriptableVector3 left, float right)
        {
            return left.Value * right;
        }

        static public implicit operator Vector3(ScriptableVector3 other)
        {
            return other.Value;
        }
    }
}