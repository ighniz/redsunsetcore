using RedSunsetCore.Constants;
using UnityEngine;

namespace RedSunsetCore.Scriptables.DataTypes
{
    [CreateAssetMenu(fileName = StringConstants.ScriptablesDataType.VECTOR_2_NEW_FILE, menuName = StringConstants.ScriptablesDataType.MENU_CREATE_VECTOR_2)]
    public class ScriptableVector2 : ScriptableVariable<Vector2>
    {
        static public ScriptableVector2 operator ++(ScriptableVector2 other)
        {
            other.Value += Vector2.one;
            return other;
        }

        static public ScriptableVector2 operator --(ScriptableVector2 other)
        {
            other.Value -= Vector2.one;
            return other;
        }

        static public Vector2 operator *(ScriptableVector2 left, int right)
        {
            return left.Value * right;
        }
        
        static public Vector2 operator *(ScriptableVector2 left, float right)
        {
            return left.Value * right;
        }

        static public implicit operator Vector2(ScriptableVector2 other)
        {
            return other.Value;
        }
    }
}