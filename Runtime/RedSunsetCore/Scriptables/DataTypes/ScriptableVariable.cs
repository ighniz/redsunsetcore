using Newtonsoft.Json;
using UnityEngine;

namespace RedSunsetCore.Scriptables.DataTypes
{
    public class ScriptableVariable<TType> : RedSunsetScriptable<ScriptableVariable<TType>>
    {
        [SerializeField]
        protected TType value;
        [JsonIgnore]
        public TType Value
        {
            get => value;
            set => this.value = value;
        }

        override public void Reset()
        {
            Value = default;
        }
        
        override public string ToString()
        {
            return Value.ToString();
        }
    }
}