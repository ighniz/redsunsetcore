using System;
using RedSunsetCore.Services;
using RedSunsetCore.Services.Core;

namespace RedSunsetCore.Scriptables
{
    [Serializable]
    public class RedSunsetScriptableServiceConfig : RedSunsetScriptable<RedSunsetScriptableServiceConfig>
    {
        override public void Reset()
        {
            
        }
    }
    
    [Serializable]
    public class RedSunsetScriptableServiceConfig<TService, TServiceConfig> : RedSunsetScriptableServiceConfig
        where TService : IService<TService>
        where TServiceConfig : IServiceConfig<TService>
    {
        
    }
}