using Newtonsoft.Json;
using RedSunsetCore.Config;
using RedSunsetCore.DesignPatterns.ObserverPattern;

namespace RedSunsetCore.Scriptables
{
    abstract public class RedSunsetScriptableConfig<T> : RedSunsetScriptable<T>, IConfig where T : IObservable<T>
    {
        [JsonProperty]
        public string FileName => name;
    }
}