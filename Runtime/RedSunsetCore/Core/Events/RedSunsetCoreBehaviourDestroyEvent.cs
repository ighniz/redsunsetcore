using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Core.Events
{
    public class RedSunsetCoreBehaviourDestroyEvent<TType> : ICustomEvent where TType : RedsunsetCoreBehaviour
    {
        public TType TargetBehaviour { get; set; }

        public void Reset()
        {
            
        }
    }
}