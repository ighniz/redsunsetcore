using RedSunsetCore.Services.Event.Interfaces;
using UnityEngine;
using UnityEngine.Animations;

namespace RedSunsetCore.Core.Events
{
	public class AnimatorBehaviourEvent : ICustomEvent
	{
		public Animator Animator { get; set; }
		public AnimatorStateInfo StateInfo { get; set; }
		public int LayerIndex { get; set; }
		public AnimatorControllerPlayable Controller { get; set; }

		public void Reset()
		{
			Animator = null;
			StateInfo = new AnimatorStateInfo();
			LayerIndex = -1;
			Controller = AnimatorControllerPlayable.Null;
		}
	}
}