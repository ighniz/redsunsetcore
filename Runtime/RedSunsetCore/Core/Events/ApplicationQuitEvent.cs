using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Core.Events
{
    public class ApplicationQuitEvent : ICustomEvent
    {
        public void Reset()
        {
            
        }
    }
}