using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Core.Events
{
    public class ApplicationFocusEvent : ICustomEvent
    {
        public bool HasFocus { get; set; }

        public void Reset()
        {
            
        }
    }
}