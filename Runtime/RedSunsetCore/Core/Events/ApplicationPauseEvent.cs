using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Core.Events
{
    public class ApplicationPauseEvent : ICustomEvent
    {
        public bool IsPaused { get; set; }

        public void Reset()
        {
            
        }
    }
}