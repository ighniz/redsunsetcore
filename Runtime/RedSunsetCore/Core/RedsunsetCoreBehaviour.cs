using RedSunsetCore.Constants;
using RedSunsetCore.Core.Events;
using RedSunsetCore.Services.DebugTools;
using RedSunsetCore.Services.Event;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Zenject;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Core
{
    public class RedsunsetCoreBehaviour : MonoBehaviour, IObjectEventDispatcher
    {
        protected IEventService mEventService;
        protected IInjectingService mInjector;
        protected IDebugService mDebugService;

        private Transform mTransform;
        new public Transform transform
        {
            get
            {
                if (mTransform == null)
                    mTransform = base.transform;
                return mTransform;
            }

            set => mTransform = value;
        }
        [HideInInspector] public Animator animator;
        [HideInInspector] public SpriteRenderer spriteRenderer;
        [HideInInspector] public Rigidbody2D rigidbody2d;
        [HideInInspector] new public Rigidbody rigidbody;

        public IEventDispatcher Dispatcher { get; private set; }

        virtual protected void Awake()
        {
            //Declared as virtual just in case you want to override it, but taking into account that could be used in the future.
            mInjector = IInjectingService.Instance;
            mEventService = IEventService.Instance;

            animator = GetComponent<Animator>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            rigidbody = GetComponent<Rigidbody>();
            rigidbody2d = GetComponent<Rigidbody2D>();
            Dispatcher = mInjector.Create<EventDispatcher>();
        }
        
        virtual protected void Start()
        {
            //Declared as virtual just in case you want to override it, but taking into account that could be used in the future.
        }

        protected void OnDestroy()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                Dispatcher.Clear();
            }
            else
            {
#endif
                OnNotifyDestroy();
#if UNITY_EDITOR
            }
#endif
#if UNITY_EDITOR
            if (Application.isPlaying)
            {
                Dispatcher?.Dispatch<RedSunsetCoreBehaviourDestroyEvent<RedsunsetCoreBehaviour>>(ev =>
                {
                    ev.TargetBehaviour = this;
                });
            }
#else
            Dispatcher?.Dispatch<RedSunsetCoreBehaviourDestroyEvent<RedsunsetCoreBehaviour>>(ev =>
            {
                ev.TargetBehaviour = this;
            });
#endif

            if (Dispatcher?.SubscribersCount > 0 && Application.isPlaying)
            {
                Debug.LogError(StringConstants.Debug.GetFormattedMessage($"There is some subscribers in \"{nameof(Dispatcher)}\" on GameObject called \"{gameObject.name}\". Clean them to avoid issues."));
                Dispatcher?.Clear();
            }

            mEventService = null;
        }

        virtual protected void OnNotifyDestroy()
        {

        }
    }
}