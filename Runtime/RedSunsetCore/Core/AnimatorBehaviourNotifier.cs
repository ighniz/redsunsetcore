using RedSunsetCore.Core.Events;
using RedSunsetCore.Services.Event.Interfaces;
using UnityEngine;
using UnityEngine.Animations;
using Zenject;

namespace RedSunsetCore.Core
{
	public class AnimatorBehaviourNotifier : StateMachineBehaviour
	{
		[Inject]
		private IEventService mEventService;

		override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			mEventService.Dispatch<AnimatorBehaviourEnterEvent>(ev =>
			{
				ev.Animator = animator;
				ev.Controller = AnimatorControllerPlayable.Null;
				ev.StateInfo = stateInfo;
				ev.LayerIndex = layerIndex;
			});
		}

		override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
			mEventService.Dispatch<AnimatorBehaviourEnterEvent>(ev =>
			{
				ev.Animator = animator;
				ev.Controller = controller;
				ev.StateInfo = stateInfo;
				ev.LayerIndex = layerIndex;
			});
		}

		override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
			mEventService.Dispatch<AnimatorBehaviourExitEvent>(ev =>
			{
				ev.Animator = animator;
				ev.Controller = controller;
				ev.StateInfo = stateInfo;
				ev.LayerIndex = layerIndex;
			});
		}

		override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			mEventService.Dispatch<AnimatorBehaviourExitEvent>(ev =>
			{
				ev.Animator = animator;
				ev.Controller = AnimatorControllerPlayable.Null;
				ev.StateInfo = stateInfo;
				ev.LayerIndex = layerIndex;
			});
		}
	}
}