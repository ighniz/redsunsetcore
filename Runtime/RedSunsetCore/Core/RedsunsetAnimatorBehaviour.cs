using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Zenject;
using UnityEngine;
using UnityEngine.Animations;
using Zenject;

namespace RedSunsetCore.Core
{
    public class RedsunsetAnimatorBehaviour : StateMachineBehaviour
    {
        public ActivationType activationType;

        protected Animator mainAnimator;
        
        [Inject]
        protected IEventService eventService;
        
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            IInjectingService.Instance.InjectDependencies(this);
            mainAnimator = animator;
        }

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex,
            AnimatorControllerPlayable controller)
        {
            IInjectingService.Instance.InjectDependencies(this);
            mainAnimator = animator;
        }

        public enum ActivationType
        {
            Enter,
            Exit
        }
    }
}