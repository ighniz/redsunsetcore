using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace RedSunsetCore.Core
{
	public class RedSunsetReverseMask : Image
	{
		static readonly private int StencilComp = Shader.PropertyToID("_StencilComp");
		override public Material materialForRendering
		{
			get
			{
				var mat = new Material(base.materialForRendering);
				mat.SetInt(StencilComp, (int)CompareFunction.NotEqual);
				return mat;
			}
		}
	}
}