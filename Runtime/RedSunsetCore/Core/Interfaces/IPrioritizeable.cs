namespace RedSunsetCore.Core.Interfaces
{
	public interface IPrioritizeable
	{
		int Priority { get; set; }
	}
}