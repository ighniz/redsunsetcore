namespace RedSunsetCore.Core.Interfaces
{
	public interface ITask
	{
		string Id { get; }
		string Description { get; set; }
		int StartValue { get; set; }
		int Progress { get; }
		bool IsCompleted { get; }
		int PercentageComplete { get; }
		int TargetProgress { get; set; }
		int Step { get; set; }

		void Activate();
		void MakeProgress();
		void Complete();
	}
}