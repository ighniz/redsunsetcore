using System;
using System.Collections.Generic;
using ModestTree;
using RedSunsetCore.DesignPatterns.PoolPattern.Interfaces;
using RedSunsetCore.Services.Zenject;

namespace RedSunsetCore.DesignPatterns.PoolPattern
{
	public class ObjectPool<TObject> : IObjectPool<TObject>
		where TObject : IPooleableObject
	{
		public int AvailableObjects => mPooled.Count;
		public int BusyObjects => mBusy.Count;
		public int TotalObjects => mPooled.Count + mBusy.Count;
		public bool AllowExpansion { get; private set; }

		private IObjectPool<TObject> mThisImpl;
		private IInjectingService injectingService;
		private Queue<TObject> mPooled = new();
		private HashSet<TObject> mBusy = new();

		private Func<TObject> mCreatorCallback;
		public Func<TObject> CreatorCallback
		{
			get => mCreatorCallback;
			set => mCreatorCallback = value;
		}

		/// <summary>
		///		This constructor was thought for just for tests cases.
		/// </summary>
		public ObjectPool()
		{
			mThisImpl = this;
			injectingService = IInjectingService.Instance;
		}
		
		public ObjectPool(Func<TObject> creatorCallback, uint initialCapacity, bool allowExpansion)
		{
			mThisImpl = this;
			injectingService = IInjectingService.Instance;
			mCreatorCallback = creatorCallback;
			AllowExpansion = allowExpansion;

			mThisImpl.CreateCache(initialCapacity);
		}
		
		TObject IObjectPool<TObject>.Pick()
		{
			TObject targetInstance;

			if (mPooled.IsEmpty())
			{
				targetInstance = mCreatorCallback();
				injectingService.InjectDependencies(targetInstance);
			}
			else
			{
				targetInstance = mPooled.Dequeue();
			}

			mBusy.Add(targetInstance);
			targetInstance.OnPicked();
			return targetInstance;
		}

		void IObjectPool<TObject>.Release(TObject targetInstance)
		{
			if (targetInstance != null)
			{
				mBusy.Remove(targetInstance);
				mPooled.Enqueue(targetInstance);
				targetInstance.OnReleased();
			}
		}

		void IObjectPool<TObject>.CreateCache(uint amount)
		{
			for (var i = 0; i < amount; i++)
			{
				TObject newObj = mCreatorCallback();
				injectingService.InjectDependencies(newObj);
				mPooled.Enqueue(newObj);
			}
		}
	}
}