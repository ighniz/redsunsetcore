namespace RedSunsetCore.DesignPatterns.PoolPattern.Enums
{
	public enum PooleableObjectState
	{
		Picked,
		Released
	}
}