using System;

namespace RedSunsetCore.DesignPatterns.PoolPattern.Interfaces
{
	public interface IObjectPool<TObject> where TObject : IPooleableObject
	{
		public int AvailableObjects { get; }
		public int BusyObjects { get; }
		public int TotalObjects { get; }
		public bool AllowExpansion { get; }

		Func<TObject> CreatorCallback { get; set; }
		void CreateCache(uint amount);
		TObject Pick();
		void Release(TObject targetInstance);
	}
}