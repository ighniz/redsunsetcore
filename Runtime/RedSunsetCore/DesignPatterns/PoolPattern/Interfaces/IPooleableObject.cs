using RedSunsetCore.DesignPatterns.PoolPattern.Enums;

namespace RedSunsetCore.DesignPatterns.PoolPattern.Interfaces
{
	public interface IPooleableObject
	{
		PooleableObjectState State { get; }
		void OnPicked();
		void OnReleased();
	}
}