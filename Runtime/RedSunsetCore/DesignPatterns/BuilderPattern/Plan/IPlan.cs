using RedSunsetCore.DesignPatterns.BuilderPattern.Product;

namespace RedSunsetCore.DesignPatterns.BuilderPattern.Plan
{
    public interface IPlan<TProduct> where TProduct : IProduct
    {
        
    }
}