using RedSunsetCore.DesignPatterns.BuilderPattern.Product;

namespace RedSunsetCore.DesignPatterns.BuilderPattern.Builder
{
    public interface IBuilder<out TProduct> where TProduct : IProduct
    {
        TProduct Build();
    }
}