using RedSunsetCore.DesignPatterns.BuilderPattern.Builder;
using RedSunsetCore.DesignPatterns.BuilderPattern.Product;

namespace RedSunsetCore.DesignPatterns.BuilderPattern.Director
{
    public interface IDirector<out TProduct> : IBuilder<TProduct> where TProduct : IProduct
    {
        
    }
}