using System;
using RedSunsetCore.DesignPatterns.BuilderPattern.Product;

namespace RedSunsetCore.DesignPatterns.FactoryPattern
{
    public interface IFactoryWithCatalogue<out TProduct> : IFactory<TProduct> where TProduct : IProduct
    {
        TProduct Create(Func<IFactoryCatalogue, IFactoryCatalogueResult> selector);
    }
}