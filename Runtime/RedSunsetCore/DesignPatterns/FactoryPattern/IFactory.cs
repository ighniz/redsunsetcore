using RedSunsetCore.DesignPatterns.BuilderPattern.Product;

namespace RedSunsetCore.DesignPatterns.FactoryPattern
{
    public interface IFactory<out TProduct> where TProduct : IProduct
    {
        TProduct Create();
    }
}