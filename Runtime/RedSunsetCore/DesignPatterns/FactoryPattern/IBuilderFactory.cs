using RedSunsetCore.DesignPatterns.BuilderPattern.Product;

namespace RedSunsetCore.DesignPatterns.FactoryPattern
{
    public interface IBuilderFactory<out TProduct> where TProduct : IProduct
    {
        IFactory<TProduct> GetBuilder();
    }
}