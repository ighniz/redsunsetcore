using System;
using System.Collections.Generic;
using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.DesignPatterns.ObserverPattern
{
    public interface IObservable<T> where T : IObservable<T>
    {
        List<Action<IObservable<T>>> Listeners { get; }
    }

    public interface IObservable : IObjectEventDispatcher
    {
        
    }
}