using System;
using System.Collections.Generic;

namespace RedSunsetCore.DesignPatterns.ObserverPattern
{
    public interface IObservableByActions
    {
        List<Action> CallbacksObservers { get; }
    }
    
    public interface IObservableByActions<T> where T : IObservableByActions<T>
    {
        List<Action<IObservableByActions<T>>> CallbacksObservers { get; }
    }
}