using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.DesignPatterns.ObserverPattern.Events
{
    public class ObservableChangedEvent<T> : ICustomEvent
    {
        public T Observable { get; set; }
        
        public void Reset()
        {
            
        }
    }
}