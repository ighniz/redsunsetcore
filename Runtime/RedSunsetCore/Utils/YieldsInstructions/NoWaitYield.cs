using UnityEngine;

namespace RedSunsetCore.Utils.YieldsInstructions
{
	public class NoWaitYield : CustomYieldInstruction
	{
		override public bool keepWaiting => false;
	}
}