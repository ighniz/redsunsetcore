namespace RedSunsetCore.Utils.Interfaces.Common
{
    public interface IResettable
    {
        void Reset();
    }
}