using System;

namespace RedSunsetCore.Utils
{
    static public class TimeUtils
    {
        static public DateTime EpochOrigin = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        
        static public long GetEpoch()
        {
            DateTime now = DateTime.UtcNow;
            TimeSpan elapsedTime = now - EpochOrigin;
            return (long)elapsedTime.TotalSeconds;
        }
    }
}