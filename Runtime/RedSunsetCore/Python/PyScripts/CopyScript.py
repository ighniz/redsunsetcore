import argparse
from shutil import copy

argumentsParser = argparse.ArgumentParser(description="Copy file from SOURCE to DESTINATION")
argumentsParser.add_argument("-s", "--source", type=str, required=True, help="Source file")
argumentsParser.add_argument("-d", "--destination", type=str, required=True, help="Destination path")
arguments = argumentsParser.parse_args()

copy(arguments.source, arguments.destination)