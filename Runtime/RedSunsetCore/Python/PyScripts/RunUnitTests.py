import shutil
import os
import sys
import argparse
import shutil
import xml.etree.ElementTree as ET
from colorama import Fore

fileName = os.path.basename(__file__)
argumentsParser = argparse.ArgumentParser(description="Run all unit tests.")
argumentsParser.add_argument("-p", "--platform", type=str, required=True, help="Target Platform")
argumentsParser.add_argument("-c", "--create", action="store_true", help="Should it be created an Unity Project?")
arguments = argumentsParser.parse_args()

if arguments.create == True:
    allPaths = os.listdir('.')
    if fileName in allPaths:
        allPaths.remove(fileName)
    if ".git" in allPaths:
        allPaths.remove(".git")
    if ".gitignore" in allPaths:
        allPaths.remove(".gitignore")
    if(".gitmodules" in allPaths):
        allPaths.remove(".gitmodules")
    currentDir = os.getcwd() + "/"
    frameworkDirectory = currentDir + "Assets/RedSunsetCore/"
    os.mkdir("Assets")
    os.mkdir(frameworkDirectory)
    os.mkdir("ProjectSettings")
    for file in allPaths:
        shutil.move(currentDir + file, frameworkDirectory + file)

resultFile = "results.xml"
os.system(os.getenv("UNITY_EXE") + " -logFile - -nographics -batchmode -runTests -projectPath ./ -testPlatform " + arguments.platform + " -testResults ./" + resultFile)

tree = ET.parse(resultFile)
root = tree.getroot()

failedTestsCount = 0
for child in root.iter("test-suite"):
    if child.get("result") == "Failed":
        for failedGroup in child.findall("test-case"):
            if failedGroup.get("result") == "Failed": #Gets only methods that failed.
                failedTestsCount += 1
                failureData = failedGroup.find("failure")
                print(Fore.RED + "*********************************************")
                print(Fore.RED + "- FULL NAME: "   + Fore.RESET + failedGroup.get("fullname"))
                print(Fore.RED + "- CLASS NAME: "  + Fore.RESET + failedGroup.get("classname"))
                print(Fore.RED + "- START TIME: "  + Fore.RESET + failedGroup.get("start-time"))
                print(Fore.RED + "- END TIME: "    + Fore.RESET + failedGroup.get("end-time"))
                print(Fore.RED + "- DURATION: "    + Fore.RESET + failedGroup.get("duration"))
                print(Fore.RED + "- MESSAGE: "     + Fore.RESET + failureData.find("message").text)
                print(Fore.RED + "- STACK TRACE: " + Fore.RESET + failureData.find("stack-trace").text)
                print()

if failedTestsCount > 0:
    raise Exception(Fore.RED + "Some tests were failed!")
else:
    print(Fore.GREEN + "*******************************" + Fore.RESET)
    print(Fore.GREEN + "All tests were pass!"            + Fore.RESET)
    print(Fore.GREEN + "*******************************" + Fore.RESET)