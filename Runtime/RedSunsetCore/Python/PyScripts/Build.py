#This script requires:
#UNITY_EXE ======> Path to Unity executable.
#UNITY_USER =====> User.
#UNITY_PASSWORD => Password.

import os
import sys
import argparse
from colorama import Fore

fileName = os.path.basename(__file__)
argumentsParser = argparse.ArgumentParser(description="Build the project.")
argumentsParser.add_argument("-p", "--platform", type=str, required=True, help="Target Platform")
arguments = argumentsParser.parse_args()

VAR_UNITY_EXE = os.getenv("UNITY_EXE")
VAR_UNITY_USER = os.getenv("UNITY_USER")
VAR_UNITY_PASSWORD = os.getenv("UNITY_PASSWORD")

os.system(VAR_UNITY_EXE + " -username '" + VAR_UNITY_USER + "' -password '" + VAR_UNITY_PASSWORD + "' -logFile - -nographics -batchmode -projectPath ./ -buildTarget " + arguments.platform + " -executeMethod RedSunsetCore.Editor.Core.Builder.CoreBuilder.Build" + arguments.platform)

#print(Fore.GREEN + "*******************************" + Fore.RESET)
#print(Fore.GREEN + "All tests were pass!"            + Fore.RESET)
#print(Fore.GREEN + "*******************************" + Fore.RESET)