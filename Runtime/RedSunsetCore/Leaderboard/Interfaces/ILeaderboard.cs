using System;
using System.Collections.Generic;

namespace RedSunsetCore.Leaderboard.Interfaces
{
    public interface ILeaderboard<TUser> where TUser : ILeaderboardUser<TUser>
    {
        int Count { get; }
        void AddUser(TUser user);
        void RemoveUser(TUser user);
        IEnumerable<TUser> GetUsers();
        TUser GetUser(Predicate<TUser> searchFunc);
        void Clear();
    }
}