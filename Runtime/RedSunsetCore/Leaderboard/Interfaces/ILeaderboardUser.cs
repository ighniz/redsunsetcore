using System;

namespace RedSunsetCore.Leaderboard.Interfaces
{
    public interface ILeaderboardUser<TUser> : IComparable<TUser>, DesignPatterns.ObserverPattern.IObservable<TUser> where TUser : ILeaderboardUser<TUser>
    {
        string Id { get; set; }
    }
}