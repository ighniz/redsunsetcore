using System;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.Leaderboard.Interfaces;

namespace RedSunsetCore.Leaderboard
{
    public class SimpleLeaderboard<TUser> : ILeaderboard<TUser> where TUser : class, ILeaderboardUser<TUser>
    {
        readonly private SortedSet<TUser> users = new SortedSet<TUser>();

        public int Count => users.Count;

        void ILeaderboard<TUser>.AddUser(TUser user)
        {
            users.Add(user);
        }

        void ILeaderboard<TUser>.RemoveUser(TUser user)
        {
            users.Remove(user);
        }

        IEnumerable<TUser> ILeaderboard<TUser>.GetUsers()
        {
            return users.ToArray();
        }

        public TUser GetUser(Predicate<TUser> searchFunc)
        {
            return users.FirstOrDefault(user => searchFunc(user));
        }

        public void Clear()
        {
            users.Clear();
        }
    }
}