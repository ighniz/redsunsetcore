using System;
using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.Assets.Data;
using UnityEngine;

namespace RedSunsetCore.Config
{
    [Serializable]
    [CreateAssetMenu(fileName = StringConstants.Files.ASSETS_CONFIG, menuName = StringConstants.Config.MENU_CREATE_ASSETS_CONFIG)]
    public class AssetsConfig : RedSunsetScriptableConfig<AssetsConfig>
    {
        public AssetBundlesSourceDataDictionary bundles;
        public AssetSourceDataByLabels assetsByLabels;
        public AssetSourceDataDictionary assetsByPath;

        override public void Reset()
        {
            bundles = new AssetBundlesSourceDataDictionary();
            assetsByLabels = new AssetSourceDataByLabels();
            assetsByPath = new AssetSourceDataDictionary();
        }
    }
}