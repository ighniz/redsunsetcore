using System.Collections.Generic;
using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using UnityEngine;

namespace RedSunsetCore.Config
{
    [CreateAssetMenu(fileName = StringConstants.Files.STRING_POOL_CONFIG, menuName = StringConstants.Config.MENU_CREATE_STRING_POOL_CONFIG)]
    public class StringPoolConfig : RedSunsetScriptableConfig<StringPoolConfig>
    {
        public List<string> pool = new List<string>();

        override public void Reset()
        {
            pool = new List<string>();
        }
    }
}