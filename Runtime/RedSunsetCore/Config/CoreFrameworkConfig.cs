using System;
using System.Collections.Generic;
using System.Reflection;
using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.Config;
using RedSunsetCore.Services.Core;
using UnityEditor;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Config
{
    [Serializable]
    [CreateAssetMenu(fileName = StringConstants.Files.CORE_FRAMEWORK_CONFIG, menuName = StringConstants.Config.MENU_CREATE_CORE_FRAMEWORK_CONFIG)]
    public class CoreFrameworkConfig : RedSunsetScriptableConfig<CoreFrameworkConfig>, IConfigService
    {
#if UNITY_EDITOR
        static public IConfigService MainCoreFrameworkConfig =>
            AssetDatabase.LoadAssetAtPath<CoreFrameworkConfig>(StringConstants.Paths.CONFIG_DEFAULT_FOLDER   +
                                                               "Default" + 
                                                               nameof(CoreFrameworkConfig) +
                                                               ".asset");
#endif

        [SerializeField]
        private List<RedSunsetScriptable> configs;
        private Dictionary<Type, IConfig> configsByType;

        T IConfigService.GetConfig<T>()
        {
            if (configsByType == null)
            {
                configsByType = new Dictionary<Type, IConfig>();
                
                //Adds config in this file.
                foreach (IConfig config in configs)
                {
                    configsByType.Add(config.GetType(), config);
                }

                //Adds additional configs in GameConfig.
                var gameConfig = Resources.Load<GameConfig>(nameof(GameConfig));
                if (gameConfig)
                {
                    foreach (RedSunsetScriptable additionalConfig in gameConfig.Configurations)
                    {
                        configsByType.Add(additionalConfig.GetType(), additionalConfig);
                    }
                }
                else
                {
                    Debug.LogWarning("[WARNING] There is not a GameConfig file. No additional configs will be loaded.");
                }
            }

            configsByType.TryGetValue(typeof(T), out IConfig targetConfig);
            return (T)targetConfig;
        }

        override public void Reset()
        {
            foreach (RedSunsetScriptable config in configs)
            {
                config.Reset();
            }
        }
    }
}