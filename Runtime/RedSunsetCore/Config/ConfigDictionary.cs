using System;
using RedSunsetCore.Scriptables;

namespace RedSunsetCore.Config
{
    [Serializable]
    public class ConfigDictionary : SerializableDictionary<Type, RedSunsetScriptable>
    {
        
    }
}