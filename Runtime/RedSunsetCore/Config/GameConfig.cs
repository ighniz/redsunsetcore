using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services;
using RedSunsetCore.Services.Core;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Config
{
    [CreateAssetMenu(fileName = StringConstants.Files.GAME_CONFIG, menuName = StringConstants.Config.MENU_CREATE_GAME_CONFIG)]
    public class GameConfig : RedSunsetScriptableConfig<GameConfig>
    {
        [Header("WARNING: Only ONE instance of this scriptable object should exist in the project and should be in the Resources folder.")]
        [SerializeField]
        private List<RedSunsetScriptable> configurations = new ();
        public List<RedSunsetScriptable> Configurations => configurations;

        [SerializeField]
        private List<ScriptableObjectInstaller> gameServices = new ();
        public List<ScriptableObjectInstaller> GameServices => gameServices;
        
        [SerializeField]
        private List<RedSunsetScriptableServiceConfig> overrideServiceConfig = new ();
        public List<RedSunsetScriptableServiceConfig> OverrideServiceConfig => overrideServiceConfig;

        [SerializeField]
        private JsonLibraryConfig mJsonLibraryConfig;
        public JsonLibraryConfig JsonLibraryConfig => mJsonLibraryConfig;
        
        static public bool TryGetInstance(out GameConfig gameConfig)
        {
            gameConfig = Resources.Load<GameConfig>(nameof(GameConfig));
            return gameConfig != null;
        }
        
        public void ApplyJsonLibraryConfig()
        {
            var jsonSettings = new JsonSerializerSettings();
            if (mJsonLibraryConfig.useStringEnumConverterForJsonLibrary)
            {
                jsonSettings.Converters.Add(new StringEnumConverter());
            }
            JsonConvert.DefaultSettings = () => jsonSettings;
        }

        public TServiceConfig GetServiceConfig<TService, TServiceConfig>()
            where TService : IService<TService>
            where TServiceConfig : class, IServiceConfig<TService>
        {
            return overrideServiceConfig.FirstOrDefault(config => config is TServiceConfig) as TServiceConfig;
        }

        override public void Reset()
        {
            //
        }
    }
    
    [Serializable]
    public class JsonLibraryConfig
    {
        public bool useStringEnumConverterForJsonLibrary;
    }
}