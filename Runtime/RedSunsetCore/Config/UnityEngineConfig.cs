using System;
using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using UnityEngine;

namespace RedSunsetCore.Config
{
    [Serializable]
    [CreateAssetMenu(fileName = StringConstants.Files.UNITY_ENGINE_CONFIG, menuName = StringConstants.Config.MENU_CREATE_UNITY_ENGINE_CONFIG)]
    public class UnityEngineConfig : RedSunsetScriptableConfig<UnityEngineConfig>
    {
        public string version;
        public string nickName;
        public string location;
        public string environmentVarName;
        [HideInInspector]
        public string lastEnvironmentVarName;

        override public void Reset()
        {
            
        }
    }
}