using System;
using RedSunsetCore.Attributes;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.Database
{
    [CreateAssetMenu(fileName = "DatabaseServiceInstaller", menuName = "RedSunsetCore/Installers/DatabaseServiceInstaller")]
    public class DatabaseInstaller : ScriptableObjectInstaller<DatabaseInstaller>
    {
        [ClassEnum(typeof(IDatabaseService))]
        public string serviceType;
        
        override public void InstallBindings()
        {
            var databaseType = Type.GetType(serviceType);
            if (databaseType != null)
            {
                Container.Bind<IDatabaseService>().To(databaseType).AsSingle();
            }
            else
            {
                Debug.Log("There is no valid database selected.");
            }
        }
    }
}