using System.Threading.Tasks;
using RedSunsetCore.Services.Async;
using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Services.Database
{
    public interface IDatabaseService : IService, IObjectEventDispatcher
    {
        void Initialize();
        void Write<TValue>(string path, TValue value);
        void Add<TValue>(string path, TValue value);
        void Remove<TValue>(string path);
        Task<TValue> Read<TValue>(string path);
        Future<TValue> ReadFuture<TValue>(string path);
        void Update<TValue>(string path, TValue value);
        void Flush();
    }
}