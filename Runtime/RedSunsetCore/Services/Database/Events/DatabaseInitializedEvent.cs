using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Services.Database.Events
{
    public class DatabaseInitializedEvent : ICustomEvent
    {
        public void Reset()
        {
            
        }
    }
}