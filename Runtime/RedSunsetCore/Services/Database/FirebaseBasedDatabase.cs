#if RED_SUNSET_CORE_FIREBASE && (UNITY_ANDROID || UNITY_IOS)
using System;
using System.Collections;
using System.Threading.Tasks;
using Firebase;
using Firebase.Database;
using Newtonsoft.Json;
using RedSunsetCore.Services.Async;
using RedSunsetCore.Services.Async.Interfaces;
using RedSunsetCore.Services.Database.Events;
using RedSunsetCore.Services.Event;
using RedSunsetCore.Services.Event.Interfaces;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.Database
{
    public class FirebaseBasedDatabase : IDatabaseService
    {
        [Inject]
        private IAsyncService asyncService;

        [Inject]
        private IEventService eventService;
        
        private DatabaseReference reference;
        private bool isInitialized;
        
        public EventDispatcher Dispatcher { get; } = new EventDispatcher();
        
        void IDatabaseService.Initialize()
        {
            asyncService.StartCoroutine(WaitForFirebaseInitialization());
        }

        private IEnumerator WaitForFirebaseInitialization()
        {
            Task<DependencyStatus> dependencyStatus = FirebaseApp.CheckAndFixDependenciesAsync();
            var wait = new WaitUntil(() => dependencyStatus.IsCompleted);

            yield return wait;
            
            if (dependencyStatus.Exception != null)
            {
                throw new Exception($"Firebase can't be initialized. Error: {dependencyStatus.Exception.Message}");
            }
            reference = FirebaseDatabase.DefaultInstance.RootReference;
            isInitialized = true;
            Dispatcher.Dispatch<DatabaseInitializedEvent>();
        }

        void IDatabaseService.Write<TValue>(string path, TValue value)
        {
            GetNode(path).SetRawJsonValueAsync(JsonConvert.SerializeObject(value));
        }

        void IDatabaseService.Add<TValue>(string path, TValue value)
        {
            GetNode(path).Push().SetRawJsonValueAsync(JsonConvert.SerializeObject(value));
        }

        void IDatabaseService.Remove<TValue>(string path)
        {
            throw new NotImplementedException();
        }

        async public Task<TValue> Read<TValue>(string path)
        {
            TValue asyncResult = default;
            DatabaseReference node = GetNode(path);
            
            await node
                .GetValueAsync()
                .ContinueWith(task =>
                {
                    asyncResult = ExtractDataFromTask<TValue>(task);
                });

            return asyncResult;
        }

        public Future<TValue> ReadFuture<TValue>(string path)
        {
            var result = new Future<TValue>();
            DatabaseReference node = GetNode(path);

            asyncService.StartCoroutine(WaitForNode(node, result));
            
            return result;
        }

        private IEnumerator WaitForNode<TValue>(Query node, Future<TValue> futureToFill)
        {
            Task<DataSnapshot> task = node.GetValueAsync();
            var wait = new WaitUntil(() => task.IsCompleted);
            yield return wait;
            futureToFill.Value = ExtractDataFromTask<TValue>(task);
        }

        static private TValue ExtractDataFromTask<TValue>(Task<DataSnapshot> task)
        {
            TValue asyncResult = default;
            if (task.IsFaulted)
            {
                Debug.Log(task.Exception?.Message);
            }
            else if (task.IsCompleted)
            {
                try
                {
                    string jsonResult = task.Result.GetRawJsonValue();
                    asyncResult = JsonConvert.DeserializeObject<TValue>(jsonResult);
                }
                catch (Exception e)
                {
                    Debug.Log(e.Message);
                }
            }

            return asyncResult;
        }

        public void Update<TValue>(string path, TValue value)
        {
            GetNode(path).SetValueAsync(JsonConvert.SerializeObject(value));
        }

        void IDatabaseService.Flush()
        {
            //Not needed.
        }

        private DatabaseReference GetNode(string path)
        {
            string[] splitPath = path.Split(new []{ "/" }, System.StringSplitOptions.RemoveEmptyEntries);
            DatabaseReference node = reference.Child(splitPath[0]);
            for (var i = 1; i < splitPath.Length; i++)
            {
                node = node.Child(splitPath[i]);
            }

            return node;
        }
    }
}
#endif