using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.Advertisement
{
    [CreateAssetMenu(fileName = "AdvertisementServiceInstaller", menuName = "RedSunsetCore/Installers/AdvertisementServiceInstaller")]
    public class AdvertisementInstaller : ScriptableObjectInstaller<AdvertisementInstaller>
    {
        override public void InstallBindings()
        {
            
        }
    }
}