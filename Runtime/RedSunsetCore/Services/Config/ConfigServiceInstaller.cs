using Newtonsoft.Json;
using RedSunsetCore.Config;
using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.Core;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.Config
{
    [CreateAssetMenu(fileName = StringConstants.Files.CONFIG_SERVICE_INSTALLER, menuName = StringConstants.Installers.OPTION_CONFIG_SERVICE_INSTALLER)]
    public class ConfigServiceInstaller : RedSunsetInstaller<ConfigServiceInstaller, IConfigService>
    {
        [SerializeField]
        private CoreFrameworkConfig coreFrameworkConfig;
        
        private JsonSerializerSettings jsonSettings;
        
        override public void InstallBindings()
        {
            Container.Bind<IConfigService>().FromMethod(() => coreFrameworkConfig).AsSingle();
            
            var gameConfig = Resources.Load<GameConfig>(nameof(GameConfig));
            if (gameConfig != null)
            {
                gameConfig.ApplyJsonLibraryConfig();

                foreach (ScriptableObjectInstaller gameService in gameConfig.GameServices)
                {
                    Container.Inject(gameService);
                    gameService.InstallBindings();
                }
                
                foreach (RedSunsetScriptable configuration in gameConfig.Configurations)
                {
                    Container.Inject(configuration);
                    Container.Bind(configuration.GetType()).FromInstance(configuration).AsSingle();
                }
            }
            else
            {
                Debug.LogWarning("[WARNING] There is not a GameConfig scriptable object. No additional services will be installed.");
            }
        }
    }
}