using RedSunsetCore.Config;

namespace RedSunsetCore.Services.Config
{
    public interface IConfigService : IService<IConfigService>
    {
        T GetConfig<T>() where T : IConfig;
    }
}