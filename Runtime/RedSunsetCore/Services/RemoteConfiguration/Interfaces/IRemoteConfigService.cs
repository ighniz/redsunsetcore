namespace RedSunsetCore.Services.RemoteConfiguration.Interfaces
{
    public interface IRemoteConfigService : IService<IRemoteConfigService>
    {
        string[] DebugUsers { get; }
        bool AmIDebugUser { get; }
        bool IsInitialized { get; }
    }
}