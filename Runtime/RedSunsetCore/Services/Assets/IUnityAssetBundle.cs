using UnityEngine;

namespace RedSunsetCore.Services.Assets
{
    public interface IUnityAssetBundle
    {
        Sprite LoadImage(string path);
        void LoadEntireBundle();
    }
}