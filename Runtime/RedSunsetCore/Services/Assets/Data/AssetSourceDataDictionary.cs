using System;

namespace RedSunsetCore.Services.Assets.Data
{
    [Serializable]
    public class AssetSourceDataDictionary : SerializableDictionary<string, AssetSourceData>
    {
        
    }
}