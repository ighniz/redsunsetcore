using System;
using UnityEngine;

namespace RedSunsetCore.Services.Assets.Data
{
    [Serializable]
    public class AssetBundleSourceData : AssetSourceData
    {
        private AssetSourceDataDictionary assets = new AssetSourceDataDictionary();
        public AssetSourceDataDictionary Assets => assets;
        public Hash128 hash128;
        
        public AssetBundleSourceData(string completeName) : base(completeName)
        {
            sourceType = AssetSourceType.AssetBundle;
        }

        public void AddAssetSourceData(AssetSourceData assetSourceData)
        {
            if (!assets.ContainsKey(assetSourceData.name))
            {
                assets.Add(assetSourceData.name, assetSourceData);
                assetSourceData.assetBundleRelativePath = completeName;
            }
        }
        
        public AssetSourceData GetAssetSourceData(string name)
        {
            name = name.ToLowerInvariant();
            return assets.TryGetValue(name, out AssetSourceData assetSourceData) ? assetSourceData : null;
        }
    }
}