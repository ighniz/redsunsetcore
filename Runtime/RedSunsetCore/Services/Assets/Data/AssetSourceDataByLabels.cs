using System;

namespace RedSunsetCore.Services.Assets.Data
{
    [Serializable]
    public class AssetSourceDataByLabels : SerializableDictionary<string, AssetSourceDataDictionary>
    {
        
    }
}