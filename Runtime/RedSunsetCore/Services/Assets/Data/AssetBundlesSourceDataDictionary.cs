using System;

namespace RedSunsetCore.Services.Assets.Data
{
    [Serializable]
    public class AssetBundlesSourceDataDictionary : SerializableDictionary<string, AssetBundleSourceData>
    {
        
    }
}