using System;
using System.Collections.Generic;
using Defective.JSON;
using RedSunsetCore.Utils.Interfaces.JSON;

namespace RedSunsetCore.Services.Assets.Data
{
    [Serializable]
    public class AssetSourceData : IJson
    {
        public string name;
        public string completeName;
        public string extension;
        public List<string> dependencies = new List<string>();
        public AssetSourceType sourceType;
        public string dataType;
        public string relativePath;
        public string assetBundleRelativePath;

        public AssetSourceData(string completeName)
        {
            this.completeName = completeName;
            
            name = completeName;
            extension = string.Empty;
            int extensionIndex = completeName.LastIndexOf(".", StringComparison.Ordinal);
            if (extensionIndex != -1)
            {
                extensionIndex++; //To skip dot.
                extension = completeName.Substring(extensionIndex, completeName.Length - extensionIndex);
                name = completeName.Replace("." + extension, string.Empty);
            }
        }
        
        public string ToJson()
        {
            JSONObject jsonData = JSONObject.Create(JSONObject.Type.Object);
            JSONObject jsonDependencies = JSONObject.Create(JSONObject.Type.Array);
            
            foreach (string assetDependency in dependencies)
            {
                jsonDependencies.Add(assetDependency);
            }
            
            jsonData.AddField(nameof(dependencies), jsonDependencies);
            jsonData.AddField(nameof(sourceType), sourceType.ToString());
            jsonData.AddField(nameof(dataType), dataType);
            jsonData.AddField(nameof(relativePath), relativePath);

            return jsonData.ToString(true);
        }

        public JSONObject ToJsonObject()
        {
            throw new NotImplementedException();
        }

        public void FromJson(string json)
        {
            JSONObject jsonData = new JSONObject(json);
            
            JSONObject dependenciesJson = jsonData.GetField(nameof(dependencies));
            foreach (JSONObject dependencyData in dependenciesJson)
            {
                dependencies.Add(dependencyData.stringValue);
            }

            jsonData.GetField(out string enumValue, nameof(dataType), AssetSourceType.Unknown.ToString());
            sourceType = (AssetSourceType)Enum.Parse(typeof(AssetSourceType), enumValue);

            jsonData.GetField(out dataType, nameof(dataType), string.Empty);
            jsonData.GetField(out relativePath, nameof(relativePath), string.Empty);
        }

        public void FromJson(JSONObject jsonObject)
        {
            throw new NotImplementedException();
        }
    }
}