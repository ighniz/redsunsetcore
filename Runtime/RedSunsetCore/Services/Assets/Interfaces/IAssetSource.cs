using System;

namespace RedSunsetCore.Services.Assets.Interfaces
{
    public interface IAssetSource<T>
    {
        AssetSourceType SourceType { get; set; }
        Type DataType { get; set; }
        string RelativePath { get; set; }
        string AbsolutePath { get; set; }
    }
}