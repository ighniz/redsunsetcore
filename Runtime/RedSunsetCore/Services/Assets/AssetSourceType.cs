namespace RedSunsetCore.Services.Assets
{
    public enum AssetSourceType
    {
        Unknown,
        UnityObject,
        Sprite,
        Sound,
        Text,
        Texture,
        Binary,
        Json,
        AssetBundle
    }
}