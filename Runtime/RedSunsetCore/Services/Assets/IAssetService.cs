using RedSunsetCore.Config;
using UnityEngine.SceneManagement;

namespace RedSunsetCore.Services.Assets
{
    public interface IAssetService : IService<IAssetService>
    {
        AssetsConfig CurrentAssetsConfig { get; set; }
        T LoadAsset<T>(string   assetPath, string label =null, string variation =null);
        void  LoadAssetAsync(string assetPath, string label =null, string variation =null);
        void  LoadAssetAsync<T>(string assetPath, string label =null, string variation =null);
        Scene LoadScene(string      scenePath, string label =null, string variation =null);
        void  LoadSceneAsync(string scenePath, string label =null, string variation =null);
        void UnloadAsset<T>(string assetPath);
        void UnloadScene(string assetPath);
    }
}