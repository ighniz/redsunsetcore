using RedSunsetCore.Attributes;
using RedSunsetCore.Config;
using RedSunsetCore.Constants;
using RedSunsetCore.Services.Core;
using UnityEngine;

namespace RedSunsetCore.Services.Assets
{
    [CreateAssetMenu(fileName = StringConstants.Files.ASSETS_SERVICE_INSTALLER, menuName = StringConstants.Installers.OPTION_ASSETS_SERVICE_INSTALLER)]
    public class AssetsServiceInstaller : RedSunsetInstaller<AssetsServiceInstaller, IAssetService>
    {
        public AssetsConfig assetsConfig;
        
        [ClassEnum(typeof(IAssetService))]
        public string defaultServiceType = StringConstants.Core.DEFAULT_NAME_NONE_SERVICE;
        
        override public void InstallBindings()
        {
            BasicInstallation(defaultServiceType, service =>
            {
                service.CurrentAssetsConfig = assetsConfig;
            }).AsSingle();
        }
    }
}