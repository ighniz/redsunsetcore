using System;
using RedSunsetCore.Config;
using UnityEngine.SceneManagement;

namespace RedSunsetCore.Services.Assets
{
    public class AddressableAssetsManager : IAssetService
    {
        public AssetsConfig CurrentAssetsConfig { get; set; }
        public T LoadAsset<T>(string assetPath, string label = null, string variation = null)
        {
            throw new NotImplementedException();
        }

        public void LoadAssetAsync(string assetPath, string label = null, string variation = null)
        {
            //Addressables.LoadAssetAsync<>()
        }

        public void LoadAssetAsync<T>(string assetPath, string label = null, string variation = null)
        {
            throw new NotImplementedException();
        }

        public Scene LoadScene(string scenePath, string label = null, string variation = null)
        {
            throw new NotImplementedException();
        }

        public void LoadSceneAsync(string scenePath, string label = null, string variation = null)
        {
            throw new NotImplementedException();
        }

        public void UnloadAsset<T>(string assetPath)
        {
            throw new NotImplementedException();
        }

        public void UnloadScene(string assetPath)
        {
            throw new NotImplementedException();
        }
    }
}