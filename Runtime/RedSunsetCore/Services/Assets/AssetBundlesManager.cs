using System;
using RedSunsetCore.Config;
using RedSunsetCore.Services.Assets.Data;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace RedSunsetCore.Services.Assets
{
    public class AssetBundlesManager : IAssetService
    {
        public AssetBundlesManager()
        {
        }
        
        public AssetsConfig CurrentAssetsConfig { get; set; }
        public T LoadAsset<T>(string assetPath, string label = null, string variation = null)
        {
            if (CurrentAssetsConfig.assetsByPath.TryGetValue(assetPath, out AssetSourceData assetSourceData))
            {
                using (UnityWebRequest request =
                    UnityWebRequestAssetBundle.GetAssetBundle(assetSourceData.assetBundleRelativePath))
                {
                    //TODO: Load synchronously with an IEnumerator inside a "while" sentence.
                    //request.SendWebRequest()
                }
            }

            return default;
        }

        public void LoadAssetAsync(string assetPath, string label = null, string variation = null)
        {
            throw new NotImplementedException();
        }

        public void LoadAssetAsync<T>(string assetPath, string label = null, string variation = null)
        {
            throw new NotImplementedException();
        }

        public Scene LoadScene(string scenePath, string label = null, string variation = null)
        {
            throw new NotImplementedException();
        }

        public void LoadSceneAsync(string scenePath, string label = null, string variation = null)
        {
            throw new NotImplementedException();
        }

        /*private IEnumerator InternalLoadAssetBundle()
        {
            using (UnityWebRequest uwr = UnityWebRequestAssetBundle.GetAssetBundle("http://www.my-server.com/mybundle"))
            {
                yield return uwr.SendWebRequest();

                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    //Debug.Log(uwr.error);
                }
                else
                {
                    // Get downloaded asset bundle
                    //AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(uwr);
                }
            }
        }*/

        public void UnloadAsset<T>(string assetPath)
        {
            throw new NotImplementedException();
        }

        public void UnloadScene(string assetPath)
        {
            throw new NotImplementedException();
        }
    }
}