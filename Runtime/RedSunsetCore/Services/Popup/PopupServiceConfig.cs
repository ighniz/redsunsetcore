using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.Popup.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.Popup
{
    [CreateAssetMenu(fileName = StringConstants.Files.POPUP_SERVICE_CONFIG, menuName = StringConstants.Config.MENU_CREATE_POPUP_SERVICE_CONFIG)]
    public class PopupServiceConfig : RedSunsetScriptableServiceConfig<IPopupService, PopupServiceConfig>, IServiceConfig<IPopupService>
    {
        public PopupSkinData popupSkinData;
        
        override public void Reset()
        {
            
        }
    }
}