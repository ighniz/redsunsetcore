using RedSunsetCore.Attributes;
using RedSunsetCore.Constants;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.Popup.Interfaces;
using RedSunsetCore.Services.Popup.View;
using RedSunsetCore.Services.Transforms;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.Popup
{
    [CreateAssetMenu(fileName = StringConstants.Files.POPUP_SERVICE_INSTALLER, menuName = StringConstants.Installers.OPTION_POPUP_SERVICE_INSTALLER)]
    public class PopupServiceInstaller : RedSunsetInstaller<PopupServiceInstaller, IPopupService, PopupServiceConfig>
    {
        [Inject]
        private ITransformService mTransformService;
        
        [ClassEnum(typeof(IPopupService))]
        public string serviceType;

        override public void InstallBindings()
        {
            //PopupServiceConfig config = ServiceConfig; //Cache the config to avoid multiple lookups.
            BasicInstallation(serviceType, service =>
            {
                Transform persistableCanvas = mTransformService.Get(nameof(SystemTransformsEnum.PersistableCanvas));
                var popupContainer = Container.InstantiateComponentOnNewGameObject<ViewPopupManager>("PopupContainer");
                var popupContainerRectTransform = popupContainer.GetComponent<RectTransform>();
                popupContainerRectTransform.SetParent(persistableCanvas, false);
                popupContainerRectTransform.localPosition = Vector3.zero;
                var viewPopupManager = popupContainer.gameObject.GetComponent<ViewPopupManager>();
                viewPopupManager.popupSkinData = service.Config.popupSkinData;
            }).AsSingle().NonLazy();
        }
    }
}