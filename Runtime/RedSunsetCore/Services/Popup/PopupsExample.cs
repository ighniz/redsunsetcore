﻿using System.Globalization;
using RedSunsetCore.Services.Popup.Interfaces;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.Popup
{
    public class PopupsExample : MonoBehaviour
    {
        [Inject]
        private IPopupService popupService;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                var popupGroup = Time.time.ToString(CultureInfo.InvariantCulture);
                
                PopupConfig popupYes = new PopupConfig()
                    .WithTitle($"[Group:{popupGroup}] Respuesta Afirmativa")
                    .WithBody("Usted está seguro.")
                    .WithGroup(popupGroup)
                    .AddButton("Aceptar", () => {});
                
                PopupConfig popupNop = new PopupConfig()
                    .WithTitle($"[Group:{popupGroup}] Respuesta Negativa")
                    .WithBody("Usted no está seguro de nada.")
                    .WithGroup(popupGroup)
                    .AddButton("Aceptar", () => {});

                PopupConfig popupQuestion = new PopupConfig()
                    .WithTitle($"[Group:{popupGroup}] Pregunta de prueba")
                    .WithBody("¿Está seguro?")
                    .WithGroup(popupGroup)
                    .AddButton("Sí", () => popupService.Enqueue(popupYes))
                    .AddButton("No", () => popupService.Enqueue(popupNop));
                
                popupService.Enqueue(popupQuestion);
            }
            
        }
    }
}