using RedSunsetCore.Constants;
using UnityEngine;

namespace RedSunsetCore.Services.Popup
{
    [CreateAssetMenu(menuName = StringConstants.Popups.MENU_POPUPS_FACTORY, fileName = StringConstants.Files.NEW_POPUP_FACTORY)]
    sealed public class PopupFactory : ScriptableObject
    {

    }
}