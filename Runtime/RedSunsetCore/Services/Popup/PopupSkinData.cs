using UnityEngine;

namespace RedSunsetCore.Services.Popup
{
    [CreateAssetMenu(fileName = "PopupSkin", menuName = "RedSunsetCore/Popups/PopupSkin")]
    public class PopupSkinData : ScriptableObject
    {
        public GameObject overlayPrefab;
        public GameObject skin;
    }
}