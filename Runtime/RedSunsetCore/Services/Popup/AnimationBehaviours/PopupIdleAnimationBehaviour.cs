using RedSunsetCore.Core;
using RedSunsetCore.Services.Popup.Events;
using UnityEngine;
using UnityEngine.Animations;

namespace RedSunsetCore.Services.Popup.AnimationBehaviours
{
    public class PopupIdleAnimationBehaviour : RedsunsetAnimatorBehaviour
    {
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex,
            AnimatorControllerPlayable controller)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex, controller);
            
            eventService.Dispatch<PopupIdleAnimationEvent>();
        }
    }
}