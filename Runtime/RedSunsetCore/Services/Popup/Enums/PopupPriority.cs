﻿namespace RedSunsetCore.Services.Popup.Enums
{
    public enum PopupPriority
    {
        Low = 1,
        Normal = 2,
        High = 4,
        VeryHigh = 8,
        System = 16,
        SuperUser = 32
    }
}