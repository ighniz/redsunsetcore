﻿using System;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Popup.Events;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace RedSunsetCore.Services.Popup.View
{
    public class ViewPopup : MonoBehaviour
    {
        static readonly private int ContinueTrigger = Animator.StringToHash("Continue");
        
        public bool EnableBehaviour { get; set; }

        [SerializeField]
        private GameObject buttonPrefab;
        [SerializeField]
        private Text titleTxt;
        [SerializeField]
        private Text bodyTxt;
        [SerializeField]
        private RectTransform buttonContainer;
        [SerializeField]
        private Button closeButton;
        [SerializeField]
        private RectTransform closeButtonContainer;
        
        [Inject]
        private IEventService eventService;

        private Animator animator;
        public PopupConfig PopupConfig { get; private set; }

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        public void Initialize(PopupConfig popupConfig)
        {
            PopupConfig = popupConfig;
            ConfigureCloseButton();
            ConfigureText();
            ConfigureButtons();
        }

        private void ConfigureText()
        {
            titleTxt.text = PopupConfig.Title;
            bodyTxt.text = PopupConfig.Body;
        }

        private void ConfigureButtons()
        {
            foreach ((string key, Action value) in PopupConfig.ActionsTable)
            {
                GameObject newButtonGameObject = Instantiate(buttonPrefab, buttonContainer);
                newButtonGameObject.SetActive(true);
                var newButton = newButtonGameObject.GetComponent<Button>();
                var newButtonText = newButton.gameObject.GetComponentInChildren<Text>();
                newButtonText.text = key;
                
                newButton.onClick.AddListener(() =>
                {
                    if (EnableBehaviour)
                    {
                        EnableBehaviour = false;
                        newButton.onClick.RemoveAllListeners();

                        eventService.Dispatch<PopupButtonClickedEvent>(ev =>
                        {
                            ev.ButtonName = key;
                            ev.PopupConfig = PopupConfig;
                        });
                    }
                });
            }
        }
        
        private void ConfigureCloseButton()
        {
            closeButtonContainer.gameObject.SetActive(PopupConfig.UseCloseButton);
            if (PopupConfig.UseCloseButton)
            {
                closeButton.onClick.AddListener(() =>
                {
                    if (EnableBehaviour)
                    {
                        eventService.Dispatch<PopupButtonClickedEvent>(ev =>
                        {
                            ev.ButtonName = string.Empty;
                            ev.PopupConfig = PopupConfig;
                        });
                    }
                });
            }
        }

        public void Close()
        {
            animator.SetTrigger(ContinueTrigger);
        }
    }
}