﻿using RedSunsetCore.Core;
using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.Services.Popup.Events;
using RedSunsetCore.Services.Transforms;
using RedSunsetCore.Services.Zenject;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace RedSunsetCore.Services.Popup.View
{
    [RequireComponent(typeof(RectTransform), typeof(CanvasRenderer))]
    public class ViewPopupManager : RedsunsetCoreBehaviour
    {
        [HideInInspector]
        public PopupSkinData popupSkinData;

        [Inject]
        private IInjectingService mInjectingService;
        [Inject]
        private ITransformService mTransformService;

        private GameObject currentPopup;
        private PopupConfig currentPopupConfig;
        private PopupConfig nextPopupConfig;
        private ViewPopup currentPopupComponent;
        private GameObject overlay;
        private RectTransform overlayRectTransform;
        private RectTransform rectTransform;

        override protected void Start()
        {
            base.Start();

            rectTransform = GetComponent<RectTransform>();
            overlay = Instantiate(popupSkinData.overlayPrefab, rectTransform);
            overlay.SetActive(false);
            rectTransform.Stretch();
            overlay.GetComponent<RectTransform>().Stretch();

            mEventService.AddListener<PopupOpenedEvent>(OnPopupOpened);
            mEventService.AddListener<PopupClosedEvent>(OnPopupClosed);
        }

        private void OnPopupOpened(PopupOpenedEvent popupOpenedEvent)
        {
            if (currentPopup != null)
            {
                nextPopupConfig = popupOpenedEvent.PopupConfig;
            }
            else
            {
                Open(popupOpenedEvent.PopupConfig);
            }
        }

        private void Open(PopupConfig popupConfig)
        {
            overlay.SetActive(true);
            currentPopupConfig = popupConfig;
            currentPopup = Instantiate(popupSkinData.skin, transform);
            currentPopupComponent = currentPopup.GetComponent<ViewPopup>();
            Assert.IsNotNull(currentPopupComponent, $"The game object popup should have a {nameof(ViewPopup)} component.");
            mInjectingService.InjectDependencies(currentPopupComponent);
            currentPopupComponent.Initialize(currentPopupConfig);
            
            mEventService.AddListener<PopupIdleAnimationEvent>(OnPopupIdle);
        }

        private void OnPopupIdle(PopupIdleAnimationEvent popupIdleAnimationEvent)
        {
            mEventService.RemoveListener<PopupIdleAnimationEvent>(OnPopupIdle);
            currentPopupComponent.EnableBehaviour = true;
        }

        private void OnPopupClosed(PopupClosedEvent popupClosedEvent)
        {
            mEventService.AddListener<PopupEndOutroAnimationEvent>(OnPopupEndOutroAnimation);
            currentPopupComponent.Close();
        }

        private void OnPopupEndOutroAnimation()
        {
            mEventService.RemoveListener<PopupEndOutroAnimationEvent>(OnPopupEndOutroAnimation);
            Destroy(currentPopup);
            
            if (nextPopupConfig != null)
            {
                Open(nextPopupConfig);
                nextPopupConfig = null;
            }
            else
            {
                overlay.SetActive(false);
            }
        }
    }
}