using RedSunsetCore.Services.Popup.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.Popup
{
    public class PopupContainer : MonoBehaviour, IPopupContainer
    {
        [SerializeField]
        private GameObject overlay;
        [SerializeField]
        private PopupSkinData defaultSkinData;
        
        public GameObject Overlay => overlay;
        public PopupSkinData DefaultSkinData => defaultSkinData;
        public RectTransform RectTransform => GetComponent<RectTransform>();

        public RectTransform Parent
        {
            get => transform.parent as RectTransform;
            set => transform.parent = value;
        }
    }
}