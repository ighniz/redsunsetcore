using System;
using System.Collections.Generic;
using RedSunsetCore.Config;

namespace RedSunsetCore.Services.Popup
{
    public class PopupConfig : IConfig
    {
        public string Title { get; private set; }
        public string Body { get; private set; }
        public string Group { get; private set; } = string.Empty;
        public bool UseCloseButton { get; private set; } = true;
        
        public Dictionary<string, Action> ActionsTable { get; private set; } = new();

        public PopupConfig AddButton(string buttonName, Action action)
        {
            ActionsTable.Add(buttonName, action);
            return this;
        }

        public PopupConfig AddButtons(params (string buttonName, Action action)[] buttons)
        {
            foreach ((string buttonName, Action action) in buttons)
            {
                AddButton(buttonName, action);
            }

            return this;
        }

        public PopupConfig RemoveButton(string buttonName)
        {
            ActionsTable.Remove(buttonName);
            return this;
        }

        public PopupConfig WithTitle(string title)
        {
            Title = title;
            return this;
        }

        public PopupConfig WithBody(string body)
        {
            Body = body;
            return this;
        }

        public PopupConfig WithCloseButton()
        {
            UseCloseButton = true;
            return this;
        }

        public PopupConfig WithoutCloseButton()
        {
            UseCloseButton = false;
            return this;
        }

        public PopupConfig WithGroup(string group)
        {
            Group = group;
            return this;
        }
    }
}