using RedSunsetCore.Attributes;
using RedSunsetCore.Constants;
using RedSunsetCore.Services.Popup.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.Popup
{
    [CreateAssetMenu(menuName = StringConstants.Popups.MENU_POPUP_DEFINITION, fileName = StringConstants.Files.NEW_POPUP_DEFINITION)]
    public class PopupDefinition : ScriptableObject
    {
        [ClassEnum(typeof(IPopup))]
        public string type;
    }
}