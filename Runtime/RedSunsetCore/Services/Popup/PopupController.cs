using System;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Popup.Events;
using RedSunsetCore.Services.Popup.Interfaces;
using Zenject;

namespace RedSunsetCore.Services.Popup
{
    public class PopupController : IPopupService
    {
        [Inject]
        private IEventService eventService;
        readonly private Dictionary<string, Queue<PopupConfig>> popupsQueue = new ();

        public PopupServiceConfig Config { get; set; }
        public bool IsThereAnyPopupEnqueue => popupsQueue.Count != 0;
        public PopupConfig CurrentPopup { get; private set; }
        
        [Inject]
        public void Initialize()
        {
            eventService.AddListener<PopupButtonClickedEvent>(OnPopupButtonClicked);
        }

        private void OnPopupButtonClicked(PopupButtonClickedEvent popupButtonClickedEvent)
        {
            if (CurrentPopup.ActionsTable.TryGetValue(popupButtonClickedEvent.ButtonName, out Action action))
            {
                action.Invoke();
            }
            
            //Notify this popup was closed.
            eventService.Dispatch<PopupClosedEvent>(ev => ev.PopupConfig = CurrentPopup);
            
            //There is more popups queued.
            if (IsThereAnyPopupEnqueue)
            {
                if (popupsQueue.TryGetValue(CurrentPopup.Group, out Queue<PopupConfig> popups))
                {
                    PopupConfig newPopup = popups.Dequeue();
                    if (popups.Count == 0)
                    {
                        popupsQueue.Remove(newPopup.Group);
                    }

                    OpenPopup(newPopup);
                }else
                {
                    OpenPopup(popupsQueue.First().Value.Dequeue());
                }
            }
            else
            {
                CurrentPopup = null;
            }
        }
        
        public void Enqueue(PopupConfig popupConfig)
        {
            if (CurrentPopup == null)
            {
                OpenPopup(popupConfig);
            }
            else
            {
                if (popupsQueue.TryGetValue(popupConfig.Group, out Queue<PopupConfig> popups))
                {
                    popups.Enqueue(popupConfig);
                }
                else
                {
                    popups = new Queue<PopupConfig>();
                    popups.Enqueue(popupConfig);
                    popupsQueue.Add(popupConfig.Group, popups);
                }
            }
        }

        public void Dequeue(PopupConfig popupConfig)
        {
            
        }

        private void OpenPopup(PopupConfig popupConfig)
        {
            CurrentPopup = popupConfig;
            eventService.Dispatch<PopupOpenedEvent>(ev => ev.PopupConfig = CurrentPopup);
        }
    }
}