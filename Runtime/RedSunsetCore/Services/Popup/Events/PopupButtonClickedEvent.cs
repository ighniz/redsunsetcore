﻿using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Services.Popup.Events
{
    public class PopupButtonClickedEvent : ICustomEvent
    {
        public string ButtonName { get; set; }
        public PopupConfig PopupConfig { get; set; }
        
        public void Reset()
        {
            
        }
    }
}