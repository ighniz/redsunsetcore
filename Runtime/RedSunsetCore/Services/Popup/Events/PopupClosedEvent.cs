﻿using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Services.Popup.Events
{
    public class PopupClosedEvent : ICustomEvent
    {
        public PopupConfig PopupConfig { get; set; }

        public void Reset()
        {
            
        }
    }
}