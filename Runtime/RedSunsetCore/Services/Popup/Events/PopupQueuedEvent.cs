﻿using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Services.Popup.Events
{
    public class PopupQueuedEvent : ICustomEvent
    {
        public PopupConfig PopupConfig{ get; set; }
        
        public void Reset()
        {
            
        }
    }
}