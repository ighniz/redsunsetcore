using Zenject;

namespace RedSunsetCore.Services.Popup.Interfaces
{
    public interface IPopupService : IService<IPopupService, PopupServiceConfig>, IInitializable
    {
        bool IsThereAnyPopupEnqueue { get; }
        PopupConfig CurrentPopup { get; }
        void Enqueue(PopupConfig popupConfig);
        void Dequeue(PopupConfig popupConfig);
    }
}