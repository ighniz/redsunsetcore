using UnityEngine;

namespace RedSunsetCore.Services.Popup.Interfaces
{
    public interface IPopupContainer
    {
        GameObject Overlay { get; }
        PopupSkinData DefaultSkinData { get; }
        RectTransform RectTransform { get; }
        RectTransform Parent { get; set; }
    }
}