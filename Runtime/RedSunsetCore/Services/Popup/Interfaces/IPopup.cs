namespace RedSunsetCore.Services.Popup.Interfaces
{
    public interface IPopup
    {
        PopupDefinition Definition { get; set; }
        PopupConfig Config { get; set; }
    }
}