using IngameDebugConsole;
using RedSunsetCore.Constants;
using RedSunsetCore.Core;
using RedSunsetCore.Services.Scenes.Events;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace RedSunsetCore.Services.DebugTools
{
    public class DebugService : RedsunsetCoreBehaviour, IDebugService
    {
        private DebugLogManager mLogManager;
        DebugLogManager IDebugService.LogManager => mLogManager;

        private Image[] mAllImages;
        private Text[] mAllTexts;

        override protected void Awake()
        {
            base.Awake();
            mEventService.AddListener<SceneLoadedEvent>(OnSceneLoaded);
            mLogManager = GetComponent<DebugLogManager>();
            SearchDuplicatedEventSystem();
            
            //Workaround to hide the debug console because the method SetActive(false) doesn't work (PART 1).
            mAllImages = mLogManager.gameObject.GetComponentsInChildren<Image>();
            mAllTexts = mLogManager.gameObject.GetComponentsInChildren<Text>();

            SetActive(false);
        }

        public void SetActive(bool active)
        {
            //Workaround to hide the debug console because the method SetActive(false) doesn't work (PART 2).
            //If we disable the Canvas or CanvasGroup the console reactive in certain moments them.
            //If we disable the GameObject the console doesn't receive the logs events and we lost the first logs. Maybe we can move the console initialization.
            foreach (Image img in mAllImages)
            {
                img.enabled = active;
            }

            foreach (Text txt in mAllTexts)
            {
                txt.enabled = active;
            }
        }

        public void Log(string message)
        {
            string formattedMessage = StringConstants.Debug.GetFormattedMessage(message);
            Debug.Log(formattedMessage);
        }

        public void LogWarning(string message)
        {
            string formattedMessage = StringConstants.Debug.GetFormattedMessage(message);
            Debug.LogWarning(formattedMessage);
        }

        public void LogError(string message)
        {
            string formattedMessage = StringConstants.Debug.GetFormattedMessage(message);
            Debug.LogError(formattedMessage);
        }

        private void OnSceneLoaded(SceneLoadedEvent ev)
        {
            SearchDuplicatedEventSystem();
        }

        private void SearchDuplicatedEventSystem()
        {
            EventSystem[] allEventSystems = FindObjectsByType<EventSystem>(FindObjectsInactive.Include, FindObjectsSortMode.None);
            gameObject.GetComponentInChildren<EventSystem>(true).enabled = allEventSystems.Length <= 1;
        }
    }
}