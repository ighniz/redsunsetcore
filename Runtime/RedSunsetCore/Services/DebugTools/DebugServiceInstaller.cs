using System;
using IngameDebugConsole;
using RedSunsetCore.Attributes;
using RedSunsetCore.Constants;
using RedSunsetCore.Services.Core;
using UnityEngine;

namespace RedSunsetCore.Services.DebugTools
{
    [CreateAssetMenu(fileName = StringConstants.Files.DEBUG_SERVICE_INSTALLER, menuName = StringConstants.Installers.OPTION_DEBUG_SERVICE_INSTALLER)]
    public class DebugServiceInstaller : RedSunsetInstaller<DebugServiceInstaller, IDebugService>
    {
        [ClassEnum(typeof(IDebugService))]
        public string defaultServiceType = StringConstants.Core.DEFAULT_NAME_NONE_SERVICE;

        [SerializeField]
        private DebugLogManager debugLogManagerPrefab;
        
        override public void InstallBindings()
        {
            if (debugLogManagerPrefab != null)
            {
                Container.Bind<IDebugService>()
                    .FromMethod(() =>
                    {
                        GameObject debugLogGameObject = Container.InstantiatePrefab(debugLogManagerPrefab);
                        var service = Container.InstantiateComponent(Type.GetType(defaultServiceType), debugLogGameObject) as IDebugService;
                        return service;
                    })
                    .AsSingle()
                    .NonLazy();
            }
            else
            {
                Debug.LogError($"The variable {nameof(debugLogManagerPrefab)} can't be null.");
            }
        }
    }
}