using IngameDebugConsole;

namespace RedSunsetCore.Services.DebugTools
{
    public interface IDebugService : IService<IDebugService>
    {
        DebugLogManager LogManager{ get; }
        void SetActive(bool active);
        void Log(string message);
        void LogWarning(string message);
        void LogError(string message);
    }
}