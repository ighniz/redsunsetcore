using RedSunsetCore.Scriptables.DataTypes;
using UnityEngine;

namespace RedSunsetCore.Services.Transforms
{
    public interface ITransformService : IService<ITransformService, TransformServiceConfig>
    {
        void Register(Transform transform, string usage);
        void Register(Transform transform, ScriptableString usage);
        void Unregister(string usage);
        Transform Get(string usage);
    }
}