using System.Collections.Generic;
using RedSunsetCore.Attributes;
using RedSunsetCore.Services.Core;
using UnityEngine;

namespace RedSunsetCore.Services.Transforms
{
    [CreateAssetMenu(fileName = "TransformServiceInstaller", menuName = "RedSunsetCore/Installers/TransformServiceInstaller")]
    public class TransformServiceInstaller : RedSunsetInstaller<TransformServiceInstaller, ITransformService, TransformServiceConfig>
    {
        [ClassEnum(typeof(ITransformService))]
        public string serviceType;

        override public void InstallBindings()
        {
            BasicInstallation(serviceType, service =>
            {
                var persistableCanvas = Instantiate(service.Config.persistableCanvasPrefab).GetComponent<RectTransform>();
                service.Register(persistableCanvas, nameof(SystemTransformsEnum.PersistableCanvas));
                persistableCanvas.SetParent(Container.DefaultParent, false);
                
                foreach (KeyValuePair<string, Transform> transform in service.Config.registeredTransform)
                {
                    service.Register(transform.Value, transform.Key);
                }
            }).AsSingle();
        }
    }
}