using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Scriptables.DataTypes;
using UnityEngine;

namespace RedSunsetCore.Services.Transforms
{
    [CreateAssetMenu(fileName = nameof(TransformUsagesConfig), menuName = StringConstants.Config.MENU_CONFIG + nameof(TransformUsagesConfig))]
    public class TransformUsagesConfig : RedSunsetScriptableConfig<TransformUsagesConfig>
    {
        [SerializeField]
        private ScriptableString[] usages;
        public ScriptableString[] Usages => usages;

        override public void Reset()
        {
            
        }
    }
}