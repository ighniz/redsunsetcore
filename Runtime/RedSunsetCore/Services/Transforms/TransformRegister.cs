using System.Collections.Generic;
using RedSunsetCore.Scriptables.DataTypes;
using UnityEngine;

namespace RedSunsetCore.Services.Transforms
{
    public class TransformRegister : ITransformService
    {
        public TransformServiceConfig Config { get; set; }

        private Dictionary<string, Transform> registry = new ();
        
        public void Register(Transform transform, string usage)
        {
            registry[usage] = transform;
        }

        public void Register(Transform transform, ScriptableString usage)
        {
            Register(transform, usage.Value);
        }

        public void Unregister(string usage)
        {
            registry.Remove(usage);
        }

        public Transform Get(string usage)
        {
            if (registry.TryGetValue(usage, out Transform transform))
            {
                return transform;
            }

            return null;
        }
    }
}