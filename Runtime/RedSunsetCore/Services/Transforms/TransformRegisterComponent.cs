using RedSunsetCore.Core;
using RedSunsetCore.Scriptables.DataTypes;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.Transforms
{
    public class TransformRegisterComponent : RedsunsetCoreBehaviour
    {
        [Inject]
        private ITransformService mTransformService;
        
        public enum RegisterMethod
        {
            Awake,
            Start
        }
        [SerializeField]
        private RegisterMethod registerMethod;
        [SerializeField]
        private ScriptableString usage;

        override protected void Awake()
        {
            base.Awake();
            if (registerMethod == RegisterMethod.Awake)
            {
                mTransformService.Register(transform, usage);
            }
        }

        override protected void Start()
        {
            base.Start();
            if (registerMethod == RegisterMethod.Start)
            {
                mTransformService.Register(transform, usage);
            }
        }
    }
}