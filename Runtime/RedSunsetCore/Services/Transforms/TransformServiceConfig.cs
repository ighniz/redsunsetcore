using System;
using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.Core;
using UnityEngine;

namespace RedSunsetCore.Services.Transforms
{
    [CreateAssetMenu(fileName = nameof(TransformServiceConfig), menuName = StringConstants.Config.MENU_CONFIG + nameof(TransformServiceConfig))]
    public class TransformServiceConfig : RedSunsetScriptableServiceConfig<ITransformService, TransformServiceConfig>, IServiceConfig<ITransformService>
    {
        public Canvas persistableCanvasPrefab;
        public DictionaryStringTransform registeredTransform;

        [Serializable]
        public class DictionaryStringTransform : SerializableDictionary<string, Transform>{}
    }
}