﻿using System;
using System.Collections.Generic;
using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.Services.DebugTools;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Inputs.Events;
using RedSunsetCore.Services.Inputs.ExtensionMethods;
using RedSunsetCore.Services.Inputs.Interfaces;
using UnityEngine.InputSystem;
using Zenject;

namespace RedSunsetCore.Services.Inputs
{
	public class InputActionManager : IInputActionService
	{
		private IInputActionService mThisImpl;
		private HashSet<IInputActionHandler> mAllActionInputHandlers = new ();
		private Dictionary<string, HashSet<Action<InputAction.CallbackContext>>> mAllActionListenersTable = new ();
		[Inject]
		private IDebugService mDebugService;

		[Inject]
		public IEventDispatcher Dispatcher { get; }
		public InputServiceConfig Config { get; set; }

		private IInputActionHandlerGroup mInputActionHandlerGroup;
		IInputActionHandlerGroup IInputActionService.InputActionHandlerGroup
		{
			get => mInputActionHandlerGroup;
			set
			{
				if (mInputActionHandlerGroup != value)
				{
					mInputActionHandlerGroup = value;
					Dispatcher.Dispatch<InputActionHandlerGroupChangedEvent>(ev =>
					{
						ev.NewInputActionHandler = mInputActionHandlerGroup;
					});
				}
			}

		}

		void IInputActionService.Initialize()
		{
			mThisImpl = this;

			InputSystem.onDeviceChange += OnDeviceChange;
			foreach (InputActionMap actionMap in Config.InputActionAsset.actionMaps)
			{
				actionMap.actionTriggered += InternalOnActionTriggered;
				mThisImpl.ActivateInputMap(actionMap.name);
			}
		}

		void IInputActionService.ActivateInputMap(string mapName)
		{
			Config.InputActionAsset.FindActionMap(mapName)?.Enable();
		}

		void IInputActionService.DeactivateInputMap(string mapName)
		{
			Config.InputActionAsset.FindActionMap(mapName)?.Disable();
		}

		void IInputActionService.AddInputActionHandler(IInputActionHandler handler)
		{
			mAllActionInputHandlers.Add(handler);
		}

		void IInputActionService.RemoveInputActionHandler(IInputActionHandler handler)
		{
			mAllActionInputHandlers.Remove(handler);
		}

		void IInputActionService.AddListenerForAction(string actionName, Action<InputAction.CallbackContext> callback)
		{
			if (mAllActionListenersTable.TryGetValue(actionName, out HashSet<Action<InputAction.CallbackContext>> handlers))
			{
				handlers.Add(callback);
			}
			else
			{
				var newHandlersList = new HashSet<Action<InputAction.CallbackContext>> { callback };
				mAllActionListenersTable.Add(actionName, newHandlersList);
			}
		}

		void IInputActionService.RemoveListenerForAction(string actionName, Action<InputAction.CallbackContext> callback)
		{
			if (mAllActionListenersTable.TryGetValue(actionName, out HashSet<Action<InputAction.CallbackContext>> handlers))
			{
				handlers.Remove(callback);
				if (handlers.Count == 0)
				{
					mAllActionListenersTable.Remove(actionName);
				}
			}
			else
			{
				mDebugService.LogWarning($"The callback you are trying to remove for \"{actionName}\" is not registered.".ToAssertFormat());
			}
		}

		private void InternalOnActionTriggered(InputAction.CallbackContext inputActionContext)
		{
			if (mAllActionListenersTable.TryGetValue(inputActionContext.GetCompleteName(), out HashSet<Action<InputAction.CallbackContext>> handlers))
			{
				foreach (Action<InputAction.CallbackContext> handler in handlers)
				{
					handler.Invoke(inputActionContext);
				}
			}

			if (mThisImpl.InputActionHandlerGroup != null)
			{
				mThisImpl.InputActionHandlerGroup.OnActionTriggered(inputActionContext, mAllActionInputHandlers);
			}
			else
			{
				mDebugService.LogWarning("There is not an InputActionHandlerGroup to handle inputs.".ToAssertFormat());
			}
		}

		private void OnDeviceChange(InputDevice device, InputDeviceChange changeType)
		{
			//
		}
	}
}