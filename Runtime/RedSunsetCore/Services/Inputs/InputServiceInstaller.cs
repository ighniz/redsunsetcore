using RedSunsetCore.Attributes;
using RedSunsetCore.Constants;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.Inputs.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.Inputs
{
	[CreateAssetMenu(fileName = nameof(InputServiceInstaller), menuName = StringConstants.Installers.MENU_INSTALLERS + nameof(InputServiceInstaller))]
	public class InputServiceInstaller : RedSunsetInstaller<InputServiceInstaller, IInputActionService, InputServiceConfig>
	{
		[ClassEnum(typeof(IInputActionService))]
		public string serviceType = StringConstants.Core.DEFAULT_NAME_NONE_SERVICE;

		override public void InstallBindings()
		{
			BasicInstallation(serviceType, service =>
			{
				IInputActionService.Instance = service;
				service.Initialize();
				
			}).AsSingle().NonLazy();
		}
	}
}