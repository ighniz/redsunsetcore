﻿using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.Inputs.Interfaces;
using UnityEngine;
using UnityEngine.InputSystem;

namespace RedSunsetCore.Services.Inputs
{
	[CreateAssetMenu(fileName = nameof(InputServiceConfig), menuName = StringConstants.Config.MENU_CONFIG + nameof(InputServiceConfig))]
	public class InputServiceConfig : RedSunsetScriptableServiceConfig<IInputActionService, InputServiceConfig>, IServiceConfig<IInputActionService>
	{
		[SerializeField]
		private InputActionAsset inputActionAsset;
		public InputActionAsset InputActionAsset => inputActionAsset;
	}
}