﻿using UnityEngine;

namespace RedSunsetCore.Services.Inputs
{
    public class InputData
    {
        public Vector3 position;
        public bool isDone;
    }
}