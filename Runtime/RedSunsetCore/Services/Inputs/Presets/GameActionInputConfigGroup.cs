using System;
using UnityEngine;

namespace RedSunsetCore.Services.Inputs.Presets
{
    [Serializable]
    public class GameActionInputConfigGroup
    {
        public KeyCode key;
        public GameActionInputConfig[] actions;
    }
}