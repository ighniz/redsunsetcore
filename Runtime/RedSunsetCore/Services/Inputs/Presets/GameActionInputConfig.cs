using System;
using RedSunsetCore.Attributes;
using RedSunsetCore.Services.GameActionSystem;
using RedSunsetCore.Services.Inputs.Enums;

namespace RedSunsetCore.Services.Inputs.Presets
{
    [Serializable]
    public class GameActionInputConfig
    {
        [ClassEnum(typeof(GameAction))]
        public string actionName;
        public InputTriggerType triggerType;
    }
}