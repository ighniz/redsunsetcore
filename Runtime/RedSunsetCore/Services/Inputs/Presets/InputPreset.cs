using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using UnityEngine;

namespace RedSunsetCore.Services.Inputs.Presets
{
    [CreateAssetMenu(fileName = StringConstants.Files.INPUT_PRESET, menuName = StringConstants.Installers.OPTION_INPUT_PRESET)]
    public class InputPreset : RedSunsetScriptableConfig<InputPreset>
    {
        public GameActionInputConfigGroup config;
        
        override public void Reset()
        {
            
        }

        
    }
}