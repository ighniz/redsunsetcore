using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Inputs.Interfaces;

namespace RedSunsetCore.Services.Inputs.Events
{
	public class InputActionHandlerGroupChangedEvent : ICustomEvent
	{
		public IInputActionHandlerGroup NewInputActionHandler { get; set; }

		public void Reset()
		{
			NewInputActionHandler = null;
		}
	}
}