namespace RedSunsetCore.Services.Inputs.Enums
{
    public enum InputGestureType
    {
        Touch,
        Swipe,
        Pinch,
        Rotate,
        Pan
    }
}