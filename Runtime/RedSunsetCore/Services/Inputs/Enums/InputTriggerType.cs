using System;

namespace RedSunsetCore.Services.Inputs.Enums
{
    [Flags]
    public enum InputTriggerType
    {
        Press = 1,
        Release = 2,
        Hold = 4
    }
}