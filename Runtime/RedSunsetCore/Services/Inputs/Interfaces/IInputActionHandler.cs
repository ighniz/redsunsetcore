﻿using UnityEngine.InputSystem;

namespace RedSunsetCore.Services.Inputs.Interfaces
{
	public interface IInputActionHandler
	{
		bool OnActionTriggered(InputAction.CallbackContext inputActionContext);
	}
}