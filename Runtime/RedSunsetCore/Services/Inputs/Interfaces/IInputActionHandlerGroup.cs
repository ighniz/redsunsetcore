using System.Collections.Generic;
using UnityEngine.InputSystem;

namespace RedSunsetCore.Services.Inputs.Interfaces
{
	public interface IInputActionHandlerGroup
	{
		void OnActionTriggered(InputAction.CallbackContext inputActionContext, HashSet<IInputActionHandler> allHandlers);
	}
}