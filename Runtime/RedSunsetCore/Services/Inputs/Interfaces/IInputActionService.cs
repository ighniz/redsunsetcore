﻿using System;
using RedSunsetCore.Services.Event.Interfaces;
using UnityEngine.InputSystem;

namespace RedSunsetCore.Services.Inputs.Interfaces
{
	public interface IInputActionService : IService<IInputActionService, InputServiceConfig>,
									 IObjectEventDispatcher
	{
		static IInputActionService Instance { get; set; }

		IInputActionHandlerGroup InputActionHandlerGroup { get; set; }

		void Initialize();
		void ActivateInputMap(string mapName);
		void DeactivateInputMap(string mapName);
		void AddInputActionHandler(IInputActionHandler handler);
		void RemoveInputActionHandler(IInputActionHandler handler);
		void AddListenerForAction(string actionName, Action<InputAction.CallbackContext> callback);
		void RemoveListenerForAction(string actionName, Action<InputAction.CallbackContext> callback);
	}
}