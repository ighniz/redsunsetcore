using System;
using UnityEngine.InputSystem;

namespace RedSunsetCore.Services.Inputs.ExtensionMethods
{
	static public class InputActionExtensionMethods
	{
		static public bool Is(this InputAction.CallbackContext source, Enum targetEnum)
		{
			return GetCompleteName(source) == targetEnum.ToString();
		}

		/// <summary>
		///		This method returns a unique name that represents this specific callback context.
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		static public string GetCompleteName(this InputAction.CallbackContext source)
		{
			return $"{source.action.actionMap.name}_{source.action.name}";
		}
	}
}