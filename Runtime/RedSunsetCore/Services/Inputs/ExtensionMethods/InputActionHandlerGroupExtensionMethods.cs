using RedSunsetCore.Services.Inputs.Interfaces;

namespace RedSunsetCore.Services.Inputs.ExtensionMethods
{
	static public class InputActionHandlerGroupExtensionMethods
	{
		static public void SetAsInputHandlerGroup(this IInputActionHandlerGroup source)
		{
			IInputActionService.Instance.InputActionHandlerGroup = source;
		}
	}
}