using RedSunsetCore.Services.Inputs.Interfaces;

namespace RedSunsetCore.Services.Inputs.ExtensionMethods
{
	static public class InputActionHandlerExtensionMethods
	{
		/// <summary>
		///		Start to send events for this object.
		///
		///		NOTE: This object is going to be in a group of receiver, so this group is going to manage it. If this
		///		object receive or not events is going to depends on this group manager.
		/// </summary>
		/// <param name="source"></param>
		static public void StartListeningInputs(this IInputActionHandler source)
		{
			IInputActionService.Instance.AddInputActionHandler(source);
		}

		static public void StopListening(this IInputActionHandler source)
		{
			IInputActionService.Instance.RemoveInputActionHandler(source);
		}
	}
}