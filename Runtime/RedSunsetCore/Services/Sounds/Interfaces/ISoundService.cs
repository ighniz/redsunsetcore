namespace RedSunsetCore.Services.Sounds.Interfaces
{
	public interface ISoundService : IService<ISoundService, SoundServiceConfig>
	{
		IAudioPlayer PeekAudioPlayer(SoundDefinition soundDefinition);
	}
}