using RedSunsetCore.Services.Event.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.Sounds.Interfaces
{
	public interface IAudioPlayer : IObjectEventDispatcher
	{
		Vector3 Position { get; set; }
		bool IsCompleted { get; }
		bool Autorelease { get; set; }
		SoundDefinition Definition { get; }

		void Play(int loops = 0);
		void PlayXShots(int amountOfShoots);
		void Stop();
		void Pause();
		void Resume();
		void Initialize(SoundDefinition definition);
		void Destroy();
		void SetActive(bool active);
		void Release();
	}
}