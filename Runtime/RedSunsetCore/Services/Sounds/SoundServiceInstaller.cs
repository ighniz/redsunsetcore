using RedSunsetCore.Attributes;
using RedSunsetCore.Constants;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.Sounds.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.Sounds
{
	[CreateAssetMenu(fileName = nameof(SoundServiceInstaller), menuName = StringConstants.Installers.MENU_INSTALLERS + nameof(SoundServiceInstaller))]
	public class SoundServiceInstaller : RedSunsetInstaller<SoundServiceInstaller, ISoundService, SoundServiceConfig>
	{
		[ClassEnum(typeof(ISoundService))]
		public string serviceType;

		override public void InstallBindings()
		{
			BasicInstallation(serviceType, service =>
			{
				var soundContainerGo = new GameObject("SoundContainer");
				soundContainerGo.transform.SetParent(Container.DefaultParent);
				service.Config.Container = soundContainerGo.transform;
			}).AsSingle();
		}
	}
}