using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.Sounds.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.Sounds
{
	[CreateAssetMenu(fileName = nameof(SoundServiceConfig), menuName = StringConstants.Config.MENU_CONFIG + nameof(SoundServiceConfig))]
	public class SoundServiceConfig : RedSunsetScriptableServiceConfig<ISoundService, SoundServiceConfig>, IServiceConfig<ISoundService>
	{
		[SerializeField, Range(1, 32)]
		private int channelsCount;
		public int ChannelsCount => channelsCount;

		public Transform Container { get; set; }
	}
}