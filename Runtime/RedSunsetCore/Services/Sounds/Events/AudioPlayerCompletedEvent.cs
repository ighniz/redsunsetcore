using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Sounds.Interfaces;

namespace RedSunsetCore.Services.Sounds.Events
{
	public class AudioPlayerCompletedEvent : ICustomEvent
	{
		public IAudioPlayer Target { get; set; }

		public void Reset()
		{
			
		}
	}
}