using RedSunsetCore.Constants;
using RedSunsetCore.Services.Event.ExtensionMethods;
using RedSunsetCore.Services.Sounds.Events;
using RedSunsetCore.Services.Sounds.Interfaces;
using RedSunsetCore.Services.Zenject;
using UnityEngine.Assertions;
using UnityEngine.Pool;
using Zenject;

namespace RedSunsetCore.Services.Sounds
{
	public class SoundManager : ISoundService
	{
		private ISoundService ThisImpl => this;
		private ObjectPool<IAudioPlayer> mAudioPlayers;
		private IInjectingService mInjector;

		public SoundServiceConfig Config { get; set; }

		[Inject]
		private void Initialize(IInjectingService injector)
		{
			mInjector = injector;

			mAudioPlayers = new ObjectPool<IAudioPlayer>(
				OnCreateNewAudioPlayer,
				OnGetAudioPlayer,
				OnReleaseAudioPlayer,
				OnDestroyAudioPlayer,
				true,
				1,
				Config.ChannelsCount);
		}

		private IAudioPlayer OnCreateNewAudioPlayer()
		{
			var channel = mInjector.InstantiateComponentOnNewGameObject<AudioPlayer>();
			channel.transform.SetParent(Config.Container);
			channel.name = $"Channel{Config.Container.childCount}";
			return channel;
		}

		private void OnGetAudioPlayer(IAudioPlayer audioPlayer)
		{
			audioPlayer.SetActive(true);
		}

		private void OnReleaseAudioPlayer(IAudioPlayer audioPlayer)
		{
			audioPlayer.RemoveListener<AudioPlayerCompletedEvent>(OnAudioPlayerCompleted);
			audioPlayer.SetActive(false);
		}

		private void OnDestroyAudioPlayer(IAudioPlayer audioPlayer)
		{
			audioPlayer.RemoveListener<AudioPlayerCompletedEvent>(OnAudioPlayerCompleted);
			audioPlayer.Destroy();
		}

		IAudioPlayer ISoundService.PeekAudioPlayer(SoundDefinition soundDefinition)
		{
			IAudioPlayer player = mAudioPlayers.Get();
			Assert.IsNotNull(soundDefinition, StringConstants.Debug.GetFormattedMessage($"The parameter \"{nameof(soundDefinition)}\" shouldn't be null."));
			Assert.IsNotNull(player, StringConstants.Debug.GetFormattedMessage($"There is no available channels (max = {Config.ChannelsCount})"));

			player.AddListener<AudioPlayerCompletedEvent>(OnAudioPlayerCompleted);
			player.Initialize(soundDefinition);

			return player;
		}

		private void OnAudioPlayerCompleted(AudioPlayerCompletedEvent ev)
		{
			mAudioPlayers.Release(ev.Target);
		}
	}
}