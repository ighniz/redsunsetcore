using System;
using System.Collections;
using System.Collections.Generic;
using RedSunsetCore.Constants;
using RedSunsetCore.Core;
using RedSunsetCore.Services.Sounds.Enums;
using RedSunsetCore.Services.Sounds.Events;
using RedSunsetCore.Services.Sounds.Interfaces;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

namespace RedSunsetCore.Services.Sounds
{
	/// <summary>
	///     When you play a sound the ISoundService returns an instance of that sound with the data into the system.
	/// </summary>
	[RequireComponent(typeof(AudioSource))]
	public class AudioPlayer : RedsunsetCoreBehaviour, IAudioPlayer
	{
		private Coroutine mInternalPlayCoroutine;
		private AudioSource mSource;
		private int mVariationIndex;
		readonly private List<AudioClip> mAllClips = new ();

		//Interface members.
		public Vector3 Position { get; set; }
		public bool IsCompleted => !mSource.isPlaying;
		public bool Autorelease { get; set; } = false;
		public SoundDefinition Definition { get; set; }

		override protected void Awake()
		{
			base.Awake();
			mSource = GetComponent<AudioSource>();
			mSource.playOnAwake = false;
		}

		public void Initialize(SoundDefinition definition)
		{
			Position = Vector3.zero;
			Definition = definition;

			//Reset values.
			mSource.loop = false;
			mSource.mute = false;
			mSource.pitch = 1;
			mSource.time = 0;
			mSource.volume = 1;

			mAllClips.Add(Definition.Clip);
			mAllClips.AddRange(Definition.Variations);
		}

		private IEnumerator InternalPlay(int loops = 0)
		{
			var waitYield = new WaitUntil(() => !mSource.isPlaying);
			int remainingLoops = loops;

			if (loops == -1)
			{
				SelectClipVariation();
				mSource.loop = true;
				mSource.Play();
				yield return waitYield;
			}
			else
			{
				//Loops.
				do
				{
					remainingLoops--;
					SelectClipVariation();
					mSource.loop = false;
					mSource.Play();
					yield return waitYield;
				} while (remainingLoops > 0);
			}

			if (Autorelease)
			{
				Release();
			}
		}

		private void SelectClipVariation()
		{
			if (Definition.Variations.Length == 0)
			{
				mVariationIndex = 0;
			}
			else if(Definition.VariationOrderType == SoundVariationOrderType.Random)
			{
				mVariationIndex = Random.Range(0, mAllClips.Count);
			}
			else
			{
				mVariationIndex = (mVariationIndex + (int)Definition.VariationOrderType) % mAllClips.Count;
			}

			mSource.clip = mAllClips[Mathf.Abs(mVariationIndex)];
		}

		public void Play(int loops = 0)
		{
			mInternalPlayCoroutine = StartCoroutine(InternalPlay(loops));
		}

		public void PlayXShots(int amountOfShoots)
		{
			Autorelease = true;
			mInternalPlayCoroutine = StartCoroutine(InternalPlay(amountOfShoots));
		}

		public void Stop()
		{
			mSource.Stop();
		}

		public void Pause()
		{
			mSource.Pause();
		}

		public void Resume()
		{
			mSource.UnPause();
		}

		public void SetActive(bool active)
		{
			gameObject.SetActive(active);
		}

		public void Release()
		{
			if (mInternalPlayCoroutine != null)
			{
				StopCoroutine(mInternalPlayCoroutine);
			}
			mInternalPlayCoroutine = null;
			mAllClips.Clear();

			Dispatcher.Dispatch<AudioPlayerCompletedEvent>(ev =>
			{
				ev.Target = this;
			});
		}

		public void Destroy()
		{
			if (mInternalPlayCoroutine != null)
			{
				StopCoroutine(mInternalPlayCoroutine);
			}
			Destroy(gameObject);
		}
	}
}