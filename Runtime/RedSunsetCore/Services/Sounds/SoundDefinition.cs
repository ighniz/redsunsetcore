using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.Sounds.Enums;
using UnityEngine;

namespace RedSunsetCore.Services.Sounds
{
	[CreateAssetMenu(menuName = StringConstants.Core.MENU_DEFINITIONS + nameof(SoundDefinition), fileName = nameof(SoundDefinition))]
	public class SoundDefinition : RedSunsetScriptableConfig<SoundDefinition>
	{
		[SerializeField]
		private AudioClip clip;
		public AudioClip Clip => clip;

		[SerializeField]
		private SoundType soundType;
		public SoundType SoundType => soundType;

		[Header("Different versions of the AudioClip")]
		[SerializeField]
		private AudioClip[] variations;
		public AudioClip[] Variations => variations;

		[SerializeField]
		private SoundVariationOrderType variationOrderType;
		public SoundVariationOrderType VariationOrderType => variationOrderType;

		override public void Reset()
		{
		}
	}
}