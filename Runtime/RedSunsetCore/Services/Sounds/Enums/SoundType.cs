namespace RedSunsetCore.Services.Sounds.Enums
{
	public enum SoundType
	{
		Invalid = -1,
		None,
		Background,
		Voice,
		FX,
		Music,
	}
}