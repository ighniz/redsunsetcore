namespace RedSunsetCore.Services.Sounds.Enums
{
	public enum SoundVariationOrderType
	{
		Inverted = -1,
		Random = 0,
		Order = 1
	}
}