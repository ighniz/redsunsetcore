using System;
using System.Collections.Generic;

namespace RedSunsetCore.Services
{
    static public class ServiceLocator
    {
        static private Dictionary<Type, IService> repository = new Dictionary<Type, IService>();

        static public void Register<T>(IService instance)
        {
            repository.Add(instance.GetType(), instance);
        }

        static public TService Get<TService>() where TService : IService
        {
            return (TService)repository[typeof(TService)];
        }
    }
}