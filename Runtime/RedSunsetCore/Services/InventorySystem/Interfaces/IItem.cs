namespace RedSunsetCore.Services.InventorySystem.Interfaces
{
	public interface IItem
	{
		string ID { get; }
	}
}