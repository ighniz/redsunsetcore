using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.InventorySystem.Interfaces;
using RedSunsetCore.Services.ModelView.Interfaces;
using Zenject;

namespace RedSunsetCore.Services.InventorySystem
{
	public class InventorySlot : IModel<InventorySlot>
	{
		[Inject]
		IEventDispatcher IObjectEventDispatcher.Dispatcher { get; }

		private IItem mItem;
		public IItem Item => mItem;

		private int mAmount;
		public int Amount => mAmount;

		void IModel<InventorySlot>.OnInitializeModel()
		{
			throw new System.NotImplementedException();
		}

		void IModel<InventorySlot>.OnDestroyModel()
		{
			throw new System.NotImplementedException();
		}
	}
}