using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.InventorySystem.Interfaces;

namespace RedSunsetCore.Services.InventorySystem
{
	public class InventoryServiceConfig : RedSunsetScriptableServiceConfig<IInventoryService, InventoryServiceConfig>, IServiceConfig<IInventoryService>
	{
		
	}
}