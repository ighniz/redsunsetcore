using System;
using UnityEngine;

namespace RedSunsetCore.Services.InventorySystem
{
	[Serializable]
	public class ItemDatabase
	{
		[SerializeField]
		private SerializableDictionary<string, Item> database = new ();
	}
}