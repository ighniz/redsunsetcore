using RedSunsetCore.Services.InventorySystem.Interfaces;

namespace RedSunsetCore.Services.InventorySystem
{
	public class Item : IItem
	{
		private string mId;
		public string ID => mId;
	}
}