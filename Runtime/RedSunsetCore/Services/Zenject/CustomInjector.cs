﻿using System;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace RedSunsetCore.Services.Zenject
{
	public class CustomInjector : IInjectingService
	{
		readonly private DiContainer diContainer;
		public bool IsInjectorReady { get; private set; } = false;

		[Inject]
		private CustomInjector(DiContainer diContainer)
		{
			this.diContainer = diContainer;
			IInjectingService.Instance = this;
		}

		T IInjectingService.Create<T>(params object[] parameters)
		{
			return diContainer.Instantiate<T>(parameters);
		}

		public T CreateFromType<T>(Type type)
		{
			return (T)diContainer.Instantiate(type);
		}

		void IInjectingService.InjectDependencies(object target)
		{
			diContainer.Inject(target);
		}

		GameObject IInjectingService.InstantiatePrefab(Object prefab)
		{
			return diContainer.InstantiatePrefab(prefab);
		}

		GameObject IInjectingService.InstantiatePrefab(Object prefab, Transform parentTransform)
		{
			return diContainer.InstantiatePrefab(prefab, parentTransform);
		}

		GameObject IInjectingService.InstantiatePrefab(Object prefab, Vector3 position, Quaternion rotation, Transform parentTransform)
		{
			return diContainer.InstantiatePrefab(prefab, position, rotation, parentTransform);
		}

		GameObject IInjectingService.InstantiatePrefab(Object prefab, GameObjectCreationParameters gameObjectBindInfo)
		{
			return diContainer.InstantiatePrefab(prefab, gameObjectBindInfo);
		}

		T IInjectingService.GetService<T>()
		{
			return diContainer.Resolve<T>();
		}

		public T InstantiateComponentOnNewGameObject<T>() where T : Component
		{
			return diContainer.InstantiateComponentOnNewGameObject<T>();
		}

		[Inject]
		public void Initialize()
		{
			IsInjectorReady = true;
		}
	}
}