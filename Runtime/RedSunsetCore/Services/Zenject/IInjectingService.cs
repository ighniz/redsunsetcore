﻿using System;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace RedSunsetCore.Services.Zenject
{
	public interface IInjectingService : IInitializable
	{
		static IInjectingService Instance { get; set; }
		bool IsInjectorReady { get; }
		T Create<T>(params object[] parameters);
		T CreateFromType<T>(Type type);
		void InjectDependencies(object target);
		GameObject InstantiatePrefab(Object prefab);
		GameObject InstantiatePrefab(Object prefab, Transform parentTransform);
		GameObject InstantiatePrefab(Object prefab, Vector3 position, Quaternion rotation, Transform parentTransform);
		GameObject InstantiatePrefab(Object prefab, GameObjectCreationParameters gameObjectBindInfo);
		T GetService<T>();
		T InstantiateComponentOnNewGameObject<T>() where T : Component;
	}
}