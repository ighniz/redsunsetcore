using RedSunsetCore.Core;
using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.GameTools.Entities.Interfaces;
using RedSunsetCore.Services.Async.Interfaces;
using RedSunsetCore.Services.ModelView.Interfaces;
using RedSunsetCore.Services.ModelView.Model;
using UnityEngine;

namespace RedSunsetCore.Services.ModelView.View
{
	public class ViewGameActor : RedsunsetCoreBehaviour, IGameEntity, IView<ModelGameActor>, IUpdateReceiver
	{
		private ModelGameActor mModel;

		void IView<ModelGameActor>.OnInitializeView(ModelGameActor model)
		{
			mModel = model;
			this.AddToUpdateList();
		}

		void IView<ModelGameActor>.OnModelUpdated(ModelGameActor model)
		{
			
		}

		void IView<ModelGameActor>.OnDestroyView()
		{
			
		}

		public void OnUpdate()
		{
			transform.LookAt(transform.position + mModel.CurrentMovementDirection);
			rigidbody.linearVelocity = Vector3.ClampMagnitude(mModel.CurrentMovementDirection, 1) * mModel.Speed;
		}
	}
}