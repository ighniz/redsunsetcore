using RedSunsetCore.Services.ModelView.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.ModelView
{
	public class ModelViewData : ModelData
	{
		private IView mView;

		public TView GetViewAs<TView>()
			where TView : Component, IView<IModel>
		{
			return (TView)mView;
		}
	}

	public class ModelViewData<TModel, TView> : ModelData<TModel>
		where TModel : class, IModel<TModel>
		where TView : Component, IView<TModel>
	{
		public TView View { get; private set; }
		public IModelViewService Service { get; private set; }

		public ModelViewData(TModel model, TView view, IModelViewService service) : base(model)
		{
			View = view;
			Service = service;
		}
	}
}