using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.ModelView.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.ModelView.Config
{
	[CreateAssetMenu(fileName = nameof(ModelViewServiceConfig), menuName = StringConstants.Config.MENU_CONFIG + nameof(ModelViewServiceConfig))]
	public class ModelViewServiceConfig : RedSunsetScriptableServiceConfig<IModelViewService, ModelViewServiceConfig>, IServiceConfig<IModelViewService>
	{
		public ModelViewBindConfig[] bindingCandidates;
	}
}