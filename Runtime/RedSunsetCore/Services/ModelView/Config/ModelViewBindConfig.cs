using System;
using RedSunsetCore.Attributes;
using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.ModelView.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.ModelView.Config
{
	[CreateAssetMenu(fileName = nameof(ModelViewBindConfig), menuName = StringConstants.Config.MENU_CONFIG + nameof(ModelViewBindConfig))]
	public class ModelViewBindConfig : RedSunsetScriptableConfig<ModelViewBindConfig>
	{
		[SerializeField, ClassEnum(typeof(IView))]
		private string viewClass;

		[SerializeField, ClassEnum(typeof(IModel))]
		private string modelClass;
		public Type ViewClass => Type.GetType(viewClass);
		public Type ModelClass => Type.GetType(modelClass);

		override public void Reset()
		{
			
		}
	}
}