using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Services.ModelView.Interfaces
{
	public interface IModel : IObjectEventDispatcher
	{
		
	}

	public interface IModel<TModel> : IModel
		where TModel : IModel
	{
		void OnInitializeModel();
		void OnDestroyModel();
	}
}