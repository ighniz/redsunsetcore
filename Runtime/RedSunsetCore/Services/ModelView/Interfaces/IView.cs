namespace RedSunsetCore.Services.ModelView.Interfaces
{
	public interface IView
	{
		
	}

	public interface IView<in TModel> : IView
		where TModel : class, IModel
	{
		void OnInitializeView(TModel model);
		void OnModelUpdated(TModel model);
		void OnDestroyView();
	}
}