using RedSunsetCore.Services.ModelView.Config;
using UnityEngine;

namespace RedSunsetCore.Services.ModelView.Interfaces
{
	public interface IModelViewService : IService<IModelViewService, ModelViewServiceConfig>
	{
		ModelViewData<TModel, TView> InstantiateSystem<TModel, TView>(GameObject attachToGameObject)
			where TModel : class, IModel<TModel>
			where TView : Component, IView<TModel>;

		ModelViewControllerData<TModel, TView, TController> InstantiateSystem<TModel, TView, TController>(GameObject attachToGameObject)
			where TModel : class, IModel<TModel>
			where TView : Component, IView<TModel>
			where TController : class, IController<TController, TModel>;

		void DestroySystem<TModel, TView>((TModel model, TView view) system)
			where TModel : class, IModel<TModel>
			where TView : Component, IView<TModel>;

		void UpdateView<TModel>(TModel model)
			where TModel : class, IModel;
	}
}