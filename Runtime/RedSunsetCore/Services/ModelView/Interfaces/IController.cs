namespace RedSunsetCore.Services.ModelView.Interfaces
{
	public interface IController
	{
		
	}

	public interface IController<TController, TModel> : IController
		where TController : class, IController<TController, TModel>
		where TModel : class, IModel<TModel>
	{
		void OnInitializeController(TModel model);
	}
}