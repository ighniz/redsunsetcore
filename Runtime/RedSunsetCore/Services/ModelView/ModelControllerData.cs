using RedSunsetCore.Services.ModelView.Interfaces;

namespace RedSunsetCore.Services.ModelView
{
	public class ModelControllerData : ModelData
	{
		
	}

	public class ModelControllerData<TModel, TController> : ModelData<TModel>
		where TModel : class, IModel<TModel>
		where TController : class, IController<TController, TModel>
	{
		public TController Controller { get; private set; }

		public ModelControllerData(TModel model, TController controller) : base(model)
		{
			Controller = controller;
		}
	}
}