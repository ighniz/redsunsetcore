using System;
using System.Collections.Generic;
using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.Services.ModelView.Config;
using RedSunsetCore.Services.ModelView.Events;
using RedSunsetCore.Services.ModelView.Interfaces;
using RedSunsetCore.Services.Zenject;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
using Object = UnityEngine.Object;

namespace RedSunsetCore.Services.ModelView
{
	public class ModelViewManager : IModelViewService
	{
		private IInjectingService mInjector;
		private Dictionary<IView, IModel> mViewModelTableByInstance = new();
		private Dictionary<IModel, IView> mModelViewTableByInstance = new();

		public ModelViewServiceConfig Config { get; set; }

		private TModel InstantiateModel<TModel>()
			where TModel : class, IModel<TModel>
		{
			Type modelType = typeof(TModel);
			
			var modelInstance = mInjector.CreateFromType<TModel>(modelType);
			return modelInstance;
		}

		private TView InstantiateView<TView>(GameObject attachToGameObject)
			where TView : Component, IView
		{
			var viewInstance = attachToGameObject.AddComponent<TView>();
			mInjector.InjectDependencies(viewInstance);
			return viewInstance;
		}

		private TController InstantiateController<TController>()
			where TController : class, IController
		{
			Type controllerType = typeof(TController);
			
			var controllerInstance = mInjector.CreateFromType<TController>(controllerType);
			return controllerInstance;
		}

		private void OnUpdateView<TModel>(ModelEvent<TModel> modelEvent) where TModel : class, IModel
		{
			UpdateView(modelEvent.Model);
		}

		public ModelViewData<TModel, TView> InstantiateSystem<TModel, TView>(GameObject attachToGameObject)
			where TModel : class, IModel<TModel>
			where TView : Component, IView<TModel>
		{
			var model = InstantiateModel<TModel>();
			var view = InstantiateView<TView>(attachToGameObject);

			mViewModelTableByInstance[view] = model;
			mModelViewTableByInstance[model] = view;

			model.OnInitializeModel();
			view.OnInitializeView(model);

			model.Dispatcher.AddListener<ModelEvent<TModel>>(OnUpdateView);

			return new ModelViewData<TModel, TView>(model, view, this);
		}

		public ModelViewControllerData<TModel, TView, TController> InstantiateSystem<TModel, TView, TController>(GameObject attachToGameObject)
			where TModel : class, IModel<TModel>
			where TView : Component, IView<TModel>
			where TController : class, IController<TController, TModel>
		{
			var model = InstantiateModel<TModel>();
			var view = InstantiateView<TView>(attachToGameObject);
			var controller = InstantiateController<TController>();

			model.OnInitializeModel();
			view.OnInitializeView(model);
			controller.OnInitializeController(model);
			return new ModelViewControllerData<TModel, TView, TController>(model, view, controller, this);
		}

		public void DestroySystem<TModel, TView>((TModel model, TView view) system)
			where TModel : class, IModel<TModel>
			where TView : Component, IView<TModel>
		{
			Assert.IsTrue(system.model != null && system.view != null, "At least one of the model/view shouldn't be null.".ToAssertFormat());
			if (system.model != null)
			{
				mModelViewTableByInstance.Remove(system.model);
				system.model.Dispatcher.RemoveListener<ModelEvent<TModel>>(OnUpdateView);
				system.model.OnDestroyModel();
			}

			if (system.view != null)
			{
				mViewModelTableByInstance.Remove(system.view);
				Object.Destroy(system.view);
			}
		}

		public void UpdateView<TModel>(TModel model) where TModel : class, IModel
		{
			Assert.IsNotNull(model, "The model shouldn't be null.".ToAssertFormat());
			var view = (IView<TModel>)mModelViewTableByInstance[model];
			Assert.IsNotNull(view, $"There is not any view for the model {typeof(TModel)}.".ToAssertFormat());
			view.OnModelUpdated(model);
		}

		[Inject]
		public void Initialize(IInjectingService injector)
		{
			mInjector = injector;
		}
	}
}