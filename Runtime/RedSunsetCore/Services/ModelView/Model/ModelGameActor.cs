using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.ModelView.Interfaces;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.ModelView.Model
{
	public class ModelGameActor : IModel<ModelGameActor>
	{
		[Inject]
		private IEventDispatcher mDispatcher;
		public IEventDispatcher Dispatcher => mDispatcher;
		
		public Vector3 CurrentMovementDirection { get; set; }
		public float Speed { get; set; } = 5;
		
		void IModel<ModelGameActor>.OnInitializeModel()
		{
			
		}

		void IModel<ModelGameActor>.OnDestroyModel()
		{
			
		}
	}
}