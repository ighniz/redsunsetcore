using System;
using RedSunsetCore.Attributes;
using RedSunsetCore.Constants;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.ModelView.Config;
using RedSunsetCore.Services.ModelView.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.ModelView
{
	[CreateAssetMenu(fileName = nameof(ModelViewServiceInstaller), menuName = StringConstants.Installers.MENU_INSTALLERS + nameof(ModelViewServiceInstaller))]
	public class ModelViewServiceInstaller : RedSunsetInstaller<ModelViewServiceInstaller, IModelViewService, ModelViewServiceConfig>
	{
		[ClassEnum(typeof(IModelViewService))]
		public string serviceType;

		override public void InstallBindings()
		{
			var type = Type.GetType(serviceType);
			Container.Bind<IModelViewService>().To(type).AsSingle();
		}
	}
}