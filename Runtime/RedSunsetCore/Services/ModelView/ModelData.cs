using RedSunsetCore.Services.ModelView.Interfaces;

namespace RedSunsetCore.Services.ModelView
{
	public class ModelData
	{
		private IModel mModel;
		public TModel GetModelAs<TModel>()
			where TModel : class, IModel<TModel>
		{
			return (TModel)mModel;
		}
	}

	public class ModelData<TModel> : ModelData
		where TModel : class, IModel<TModel>
	{
		public TModel Model { get; private set; }

		public ModelData(TModel model)
		{
			Model = model;
		}

		virtual public void UpdateSystem()
		{
			
		}
	}
}