using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.ModelView.Interfaces;

namespace RedSunsetCore.Services.ModelView.Events
{
	public class ModelEvent<TModel> : ICustomEvent
		where TModel : IModel
	{
		public TModel Model { get; set; }

		public void Reset()
		{
			
		}
	}
}