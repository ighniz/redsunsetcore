using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.Services.Async.Interfaces;
using RedSunsetCore.Services.ModelView.Interfaces;
using RedSunsetCore.Services.ModelView.Model;
using UnityEngine;

namespace RedSunsetCore.Services.ModelView.Controller
{
	public class ControllerGameActor : IController<ControllerGameActor, ModelGameActor>, IUpdateReceiver
	{
		private ModelGameActor mModel;

		public void OnInitializeController(ModelGameActor model)
		{
			mModel = model;
			this.AddToUpdateList();
		}

		public void OnUpdate()
		{
			float xAxis = Input.GetAxis("Horizontal");
			float yAxis = Input.GetAxis("Vertical");
			if (xAxis != 0 || yAxis != 0)
			{
				mModel.CurrentMovementDirection = new Vector3
				{
					x = xAxis,
					z = yAxis
				};
			}
		}
	}
}