using RedSunsetCore.Services.ModelView.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.ModelView
{
	public class ModelViewControllerData<TModel, TView, TController> : ModelViewData<TModel, TView>
		where TModel : class, IModel<TModel>
		where TView : Component, IView<TModel>
		where TController : class, IController<TController, TModel>
	{
		public TController Controller { get; private set; }

		public ModelViewControllerData(TModel model, TView view, TController controller, IModelViewService service) : base(model, view, service)
		{
			Controller = controller;
		}
	}
}