using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.ModelView.Interfaces;
using Zenject;

namespace RedSunsetCore.Services.ModelView.Predefined
{
    public class GenericModel : IModel<GenericModel>
    {
        [Inject]
        private IEventDispatcher mDispatcher;
        public IEventDispatcher Dispatcher => mDispatcher;
        void IModel<GenericModel>.OnInitializeModel()
        {
            throw new System.NotImplementedException();
        }

        void IModel<GenericModel>.OnDestroyModel()
        {
            throw new System.NotImplementedException();
        }
    }
}