﻿using System;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.Core;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Zenject;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.Event
{
    /// <summary>
    ///     Represents a simple event dispatcher: Add, Remove and Dispatchs events.
    /// </summary>
    public class EventDispatcher : RedSunsetCoreObject, IEventDispatcher
    {
        
        [Inject]
        private IInjectingService injector;
        private DynamicEventPool dynamicEventPool;
        readonly private Dictionary<Type, HashSet<ICaller>> subscribers = new ();

        public int SubscribersCount => subscribers.Count;

        [Inject]
        override protected void Initialize()
        {
            base.Initialize();
            dynamicEventPool = injector.Create<DynamicEventPool>();
        }

        public void AddListener<TEventType>(Action<TEventType> listener) where TEventType : class, ICustomEvent
        {
            Type eventType = typeof(TEventType);
            AddListener(eventType, listener);
        }

        public void AddListener<TEventType>(Action listener) where TEventType : class, ICustomEvent
        {
            Type eventType = typeof(TEventType);
            var newCaller = new Caller<TEventType> {MethodWithOutParams = listener};
            if (!subscribers.TryGetValue(eventType, out HashSet<ICaller> allListeners))
            {
                allListeners = new HashSet<ICaller>();
                subscribers[eventType] = allListeners;
                allListeners.Add(newCaller);
            }
            else if (!allListeners.Any(caller => ((Caller<TEventType>)caller).MethodWithOutParams.Equals(listener)))
            {
                allListeners.Add(newCaller);
            }
            
#if UNITY_EDITOR
            else
            {
                Debug.LogWarning($"This listener already exists for the type {eventType}.");
            }
#endif
        }

        public void AddListener<TEventType>(Type eventType, Action<TEventType> listener)
            where TEventType : ICustomEvent
        {
            var newCaller = new Caller<TEventType> {Method = listener};
            if (!subscribers.TryGetValue(eventType, out HashSet<ICaller> allListeners))
            {
                allListeners = new HashSet<ICaller>();
                subscribers[eventType] = allListeners;
                allListeners.Add(newCaller);
            }
            else if (!allListeners.Any(caller => ((Caller<TEventType>)caller).Method.Equals(listener)))
            {
                allListeners.Add(newCaller);
            }
            
#if UNITY_EDITOR
            else
            {
                Debug.LogWarning($"This listener already exists for the type {eventType}.");
            }
#endif
        }

        public void RemoveListener<TEventType>(Action<TEventType> listener) where TEventType : class, ICustomEvent
        {
            Type eventType = typeof(TEventType);
            RemoveListener(eventType, listener);
        }

        public void RemoveListener<TEventType>(Action listener) where TEventType : class, ICustomEvent
        {
            HashSet<ICaller> callersOf = GetCallersOf<TEventType>();

            if (callersOf != null)
            {
                ICaller targetCaller = callersOf.FirstOrDefault( caller => ((Caller<TEventType>)caller).MethodWithOutParams == listener );
                callersOf.Remove(targetCaller);
                Type eventType = typeof(TEventType);
                if (subscribers.TryGetValue(eventType, out HashSet<ICaller> allListeners))
                {
                    if (allListeners.Count == 0)
                    {
                        subscribers.Remove(eventType);
                    }
                }
            }
        }

        public void RemoveListener<TEventType>(Type eventType, Action<TEventType> listener) where TEventType : class, ICustomEvent
        {
            HashSet<ICaller> callersOf = GetCallersOf<TEventType>();

            if (callersOf != null)
            {
                ICaller targetCaller = callersOf.FirstOrDefault( caller => ((Caller<TEventType>)caller).Method == listener );
                callersOf.Remove(targetCaller);
                if (subscribers.TryGetValue(eventType, out HashSet<ICaller> allListeners))
                {
                    if (allListeners.Count == 0)
                    {
                        subscribers.Remove(eventType);
                    }
                }
            }
        }

        public void RemoveAllListenersOf<TEventType>() where TEventType : class, ICustomEvent
        {
            HashSet<ICaller> callersOf = GetCallersOf<TEventType>();
            callersOf.Clear();
        }

        private HashSet<ICaller> GetCallersOf<T>()
        {
            Type eventType = typeof(T);
            return subscribers.TryGetValue(eventType, out HashSet<ICaller> allListeners) ? allListeners : new HashSet<ICaller>();
        }

        public void Dispatch<TEventType>(Action<TEventType> initializer = null) where TEventType : class, ICustomEvent
        {
            Type eventType = typeof(TEventType);
            if (subscribers.TryGetValue(eventType, out HashSet<ICaller> allListeners))
            {
                TEventType eventToDispatch = dynamicEventPool.Get<TEventType>();
                eventToDispatch.Reset();

                initializer?.Invoke(eventToDispatch);

                foreach (ICaller listener in allListeners.ToList())
                {
                    listener.Call(eventToDispatch);
                }
                
                dynamicEventPool.Release(eventToDispatch);
            }
        }

        public void Clear()
        {
            foreach (KeyValuePair<Type, HashSet<ICaller>> keyValuePair in subscribers)
            {
                keyValuePair.Value.Clear();
            }
            subscribers.Clear();
        }
    }
}