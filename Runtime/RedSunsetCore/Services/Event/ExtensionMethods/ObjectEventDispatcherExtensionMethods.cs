using System;
using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Services.Event.ExtensionMethods
{
    static public class ObjectEventDispatcherExtensionMethods
    {
        static public void AddListener<TEventType>(this IObjectEventDispatcher source, Action<TEventType> listener) where TEventType : class, ICustomEvent
        {
            source.Dispatcher.AddListener(listener);
        }

        static public void RemoveListener<TEventType>(this IObjectEventDispatcher source, Action<TEventType> listener)
            where TEventType : class, ICustomEvent
        {
            source.Dispatcher.RemoveListener(listener);
        }

        static public void RemoveAllListenersOf<TEventType>(this IObjectEventDispatcher source) where TEventType : class, ICustomEvent
        {
            source.Dispatcher.RemoveAllListenersOf<TEventType>();
        }

        static public void Dispatch<TEventType>(this IObjectEventDispatcher source, Action<TEventType> initializer = null)
            where TEventType : class, ICustomEvent
        {
            source.Dispatcher.Dispatch(initializer);
        }

        static public void Clean(this IObjectEventDispatcher source)
        {
            source.Dispatcher.Clear();
        }
    }
}