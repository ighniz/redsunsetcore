using System;
using RedSunsetCore.Attributes;
using RedSunsetCore.Constants;
using RedSunsetCore.Services.Event.Interfaces;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.Event
{
    [CreateAssetMenu(fileName = StringConstants.Installers.OPTION_EVENT_DISPATCHER_FACTORY, menuName = "RedSunsetCore/Installers/EventServiceInstaller")]
    public class EventServiceInstaller : ScriptableObjectInstaller<EventServiceInstaller>
    {
        [ClassEnum(typeof(IEventService))]
        public string serviceType;
        
        override public void InstallBindings()
        {
            Type eventServiceType = Type.GetType(serviceType);
            Container.Bind<IEventService>().To(eventServiceType).AsSingle();
        }
    }
}