﻿using System;

namespace RedSunsetCore.Services.Event.Interfaces
{
    public interface ICaller<T> : ICaller
    {
        Action<T> Method { get; set; }
        Action MethodWithOutParams { get; set; }
    }

    public interface ICaller
    {
        void Call(ICustomEvent customEvent);
    }
}