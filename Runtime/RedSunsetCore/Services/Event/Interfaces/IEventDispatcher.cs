﻿using System;

namespace RedSunsetCore.Services.Event.Interfaces
{
    public interface IEventDispatcher
    {
        int SubscribersCount { get; }
        void AddListener<TEventType>(Action<TEventType> listener) where TEventType : class, ICustomEvent;
        void AddListener<TEventType>(Action listener) where TEventType : class, ICustomEvent;
        void AddListener<TEventType>(Type eventType, Action<TEventType> listener) where TEventType : ICustomEvent;
        void RemoveListener<TEventType>(Action<TEventType> listener) where TEventType : class, ICustomEvent;
        void RemoveListener<TEventType>(Action listener) where TEventType : class, ICustomEvent;
        void RemoveListener<TEventType>(Type eventType, Action<TEventType> listener) where TEventType : class, ICustomEvent;
        void RemoveAllListenersOf<TEventType>() where TEventType : class, ICustomEvent;
        void Dispatch<TEventType>(Action<TEventType> initializer = null) where TEventType : class, ICustomEvent;
        void Clear();
    }
}