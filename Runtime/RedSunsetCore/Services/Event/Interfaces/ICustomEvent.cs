﻿namespace RedSunsetCore.Services.Event.Interfaces
{
    public interface ICustomEvent
    {
        void Reset();
    }
}