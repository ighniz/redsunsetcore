﻿namespace RedSunsetCore.Services.Event.Interfaces
{
    public interface IEventService : IService, IEventDispatcher
    {
        static IEventService Instance { get; set; }
        void ChangeDefaultDispatcher(IEventDispatcher defaultDispatcher);
    }
}