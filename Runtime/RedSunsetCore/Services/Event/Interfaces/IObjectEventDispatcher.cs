namespace RedSunsetCore.Services.Event.Interfaces
{
    public interface IObjectEventDispatcher
    {
        IEventDispatcher Dispatcher { get; }
    }
}