﻿using System;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Zenject;
using Zenject;

namespace RedSunsetCore.Services.Event
{
    public class EventSystem : IEventService
    {
        private IInjectingService injectingService;
        public IEventDispatcher CurrentEventDispatcher { get; private set; }

        public int SubscribersCount => CurrentEventDispatcher.SubscribersCount;

        public EventSystem()
        {
            IEventService.Instance = this;
            injectingService = IInjectingService.Instance;
            CurrentEventDispatcher = injectingService.Create<EventDispatcher>();
        }
        
        public void ChangeDefaultDispatcher(IEventDispatcher defaultDispatcher)
        {
            CurrentEventDispatcher = defaultDispatcher;
        }

        public void AddListener<TEventType>(Action<TEventType> listener) where TEventType : class, ICustomEvent
        {
            CurrentEventDispatcher.AddListener(listener);
        }

        public void AddListener<TEventType>(Action listener) where TEventType : class, ICustomEvent
        {
            CurrentEventDispatcher.AddListener<TEventType>(listener);
        }

        public void AddListener<TEvent>(Type eventType, Action<TEvent> listener) where TEvent : ICustomEvent
        {
            CurrentEventDispatcher.AddListener(eventType, listener);
        }

        public void RemoveListener<TEventType>(Action<TEventType> listener) where TEventType : class, ICustomEvent
        {
            CurrentEventDispatcher.RemoveListener(listener);
        }

        public void RemoveListener<TEventType>(Action listener) where TEventType : class, ICustomEvent
        {
            CurrentEventDispatcher.RemoveListener<TEventType>(listener);
        }

        public void RemoveListener<TEventType>(Type eventType, Action<TEventType> listener) where TEventType : class, ICustomEvent
        {
            CurrentEventDispatcher.RemoveListener(eventType, listener);
        }

        public void RemoveAllListenersOf<TEventType>() where TEventType : class, ICustomEvent
        {
            CurrentEventDispatcher.RemoveAllListenersOf<TEventType>();
        }

        public void Dispatch<TEventType>(Action<TEventType> initializer = null) where TEventType : class, ICustomEvent
        {
            CurrentEventDispatcher.Dispatch(initializer);
        }

        public void Clear()
        {
            CurrentEventDispatcher.Clear();
        }
    }
}