using System;
using RedSunsetCore.Attributes;
using RedSunsetCore.Constants;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.Event.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.Event
{
    [CreateAssetMenu(menuName = StringConstants.Factories.MENU_CREATE_EVENT_DISPATCHER_FACTORY, fileName = StringConstants.Files.EVENT_DISPATCHER_FACTORY)]
    public class EventDispatcherFactory : RedSunsetInstaller<EventDispatcherFactory>
    {
        [ClassEnum(typeof(IEventDispatcher))]
        public string serviceType;
        
        override public void InstallBindings()
        {
            var eventServiceType = Type.GetType(serviceType);
            Container.Bind<IEventDispatcher>().To(eventServiceType).AsTransient();
        }
    }
}