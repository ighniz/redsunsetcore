using System;
using System.Collections.Generic;
using ModestTree;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Zenject;
using Zenject;

namespace RedSunsetCore.Services.Event
{
    public class DynamicEventPool
    {
        [Inject]
        private IInjectingService injectingService;
        private Dictionary<Type, Queue<ICustomEvent>> pooledEvents = new Dictionary<Type, Queue<ICustomEvent>>();
        private Dictionary<Type, Queue<ICustomEvent>> busyEvents = new Dictionary<Type, Queue<ICustomEvent>>();

        public TEventType Get<TEventType>() where TEventType : class, ICustomEvent
        {
            Type eventType = typeof(TEventType);
            ICustomEvent targetEventTypeInstance;

            if (!pooledEvents.ContainsKey(eventType))
            {
                Queue<ICustomEvent> busyQueue = new Queue<ICustomEvent>();
                targetEventTypeInstance = injectingService.Create<TEventType>();

                pooledEvents.Add(eventType, new Queue<ICustomEvent>());
                busyEvents.Add(eventType, busyQueue);
                
                busyQueue.Enqueue(targetEventTypeInstance);
            }
            else
            {
                if (pooledEvents[eventType].IsEmpty())
                {
                    targetEventTypeInstance = injectingService.Create<TEventType>();
                }
                else
                {
                    targetEventTypeInstance = pooledEvents[eventType].Dequeue();
                }

                //TODO: Maybe we can to add a "Cleaning System" or something like that in order to release empty slots.

                busyEvents[eventType].Enqueue(targetEventTypeInstance);
            }
            return targetEventTypeInstance as TEventType;
        }

        public void Release<TEventType>(TEventType eventTypeInstance) where TEventType : ICustomEvent
        {
            Type eventType = typeof(TEventType);
            busyEvents[eventType].Dequeue();

            if (!pooledEvents.ContainsKey(eventType))
            {
                pooledEvents.Add(eventType, new Queue<ICustomEvent>());
            }
            pooledEvents[eventType].Enqueue(eventTypeInstance);
        }
    }
}