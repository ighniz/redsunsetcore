﻿using System;
using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Services.Event
{
    public class Caller<T> : ICaller<T> where T : ICustomEvent
    {
        public object Target { get; set; }
        public Action<T> Method { get; set; }
        public Action MethodWithOutParams { get; set; }

        public void Call(ICustomEvent customEvent)
        {
            if (Method != null)
            {
                Method.Invoke((T) customEvent);
            }
            else
            {
                MethodWithOutParams.Invoke();
            }
        }
    }
}