using System;
using RedSunsetCore.Attributes;
using RedSunsetCore.Constants;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.Scenes.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.Scenes
{
    [CreateAssetMenu(fileName = StringConstants.Files.SCENE_SERVICE_INSTALLER, menuName = StringConstants.Installers.OPTION_SCENE_SERVICE_INSTALLER)]
    public class SceneServiceInstaller : RedSunsetInstaller<SceneServiceInstaller>
    {
        [ClassEnum(typeof(ISceneService))]
        public string defaultServiceType = StringConstants.Core.DEFAULT_NAME_NONE_SERVICE;

        override public void InstallBindings()
        {
            Container.Bind<ISceneService>().To(Type.GetType(defaultServiceType)).AsSingle().NonLazy();
        }
    }
}