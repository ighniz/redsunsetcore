using RedSunsetCore.Services.Event.Interfaces;
using UnityEngine.SceneManagement;

namespace RedSunsetCore.Services.Scenes.Events
{
	public class LoadSceneRequestEvent : ICustomEvent
	{
		public string SceneName{ get; set; }
		public LoadSceneMode LoadSceneMode { get; set; }

		public void Reset()
		{
			
		}
	}
}