namespace RedSunsetCore.Services.Scenes.Interfaces
{
    public interface ISceneService : IService<ISceneService>
    {
        void LoadScene(string sceneName);
    }
}