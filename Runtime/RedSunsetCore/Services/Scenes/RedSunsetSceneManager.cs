using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Scenes.Events;
using RedSunsetCore.Services.Scenes.Interfaces;
using UnityEngine.SceneManagement;
using Zenject;

namespace RedSunsetCore.Services.Scenes
{
	public class RedSunsetSceneManager : ISceneService
	{
		private IEventService mEventService;
		
		[Inject]
		private void Initialize(IEventService eventService)
		{
			mEventService = eventService;
			SceneManager.sceneLoaded += OnSceneLoaded;

			mEventService.AddListener<LoadSceneRequestEvent>(OnLoadSceneRequest);
		}
		
		public void LoadScene(string sceneName)
		{
			SceneManager.LoadScene(sceneName);
		}

		private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
		{
			mEventService.Dispatch<SceneLoadedEvent>(ev =>
			{
				ev.Scene = scene;
				ev.LoadSceneMode = loadSceneMode;
			});
		}

		private void OnLoadSceneRequest(LoadSceneRequestEvent ev)
		{
			
		}
	}
}