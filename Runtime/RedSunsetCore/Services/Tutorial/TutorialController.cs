using System.Collections.Generic;
using RedSunsetCore.Constants;
using RedSunsetCore.Services.DebugTools;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Tutorial.Events;
using RedSunsetCore.Services.Tutorial.Interfaces;
using UnityEngine.Assertions;
using Zenject;

namespace RedSunsetCore.Services.Tutorial
{
	public class TutorialController : ITutorialService
	{
		private IEventService mEventService;
		private IDebugService mDebugService;
		private Dictionary<string, ITutorialData> mTutorialsById = new ();
		private ITutorialData mCurrentTutorial;
		private int mCurrentTutorialStep;

		public TutorialServiceConfig Config { get; set; }

		[Inject]
		private void Initialize(
			IEventService eventService,
			IDebugService debugService)
		{
			mEventService = eventService;
			mDebugService = debugService;

			foreach (ITutorialData configTutorial in Config.Tutorials)
			{
				mTutorialsById.Add(configTutorial.TutorialId, configTutorial);
			}
		}

		void ITutorialService.Start(string tutorialId, int step)
		{
			if (mTutorialsById.TryGetValue(tutorialId, out ITutorialData tutorialData))
			{
				mCurrentTutorial = tutorialData;
				mCurrentTutorialStep = step;
				mEventService.Dispatch<TutorialStartedEvent>(tutorialStartedEvent =>
				{
					tutorialStartedEvent.StepIndex = step;
					tutorialStartedEvent.TutorialData = tutorialData;
				});
			}
			else
			{
				mDebugService.LogError($"The tutorial {tutorialId} doesn't exists.");
			}
		}

		void ITutorialService.Skip(string tutorialId)
		{
			throw new System.NotImplementedException();
		}

		void ITutorialService.NextStep()
		{
			Assert.IsNotNull(mCurrentTutorial, StringConstants.Debug.GetFormattedMessage("There is not an active tutorial."));

			if (mCurrentTutorialStep + 1 < mCurrentTutorial.Steps.Length)
			{
				UnloadCurrentStep();

				mCurrentTutorialStep++;
				mEventService.Dispatch<TutorialStepChangedEvent>(tutorialStepChangedEvent =>
				{
					tutorialStepChangedEvent.TutorialData = mCurrentTutorial;
					tutorialStepChangedEvent.StepIndex = mCurrentTutorialStep;
				});
			}
			else
			{
				mEventService.Dispatch<TutorialEndedEvent>(tutorialEndedEvent =>
				{
					tutorialEndedEvent.TutorialData = mCurrentTutorial;
				});
				mCurrentTutorial = null;
				mCurrentTutorialStep = -1;
			}
		}

		void ITutorialService.GoToStep(int step)
		{
			Assert.IsNotNull(mCurrentTutorial, StringConstants.ErrorMessages.NoTutorialLoaded);
			Assert.IsTrue(step >= 0 && step < mCurrentTutorial.Steps.Length, string.Format(StringConstants.ErrorMessages.TutorialStepOutOfRange, mCurrentTutorial.Steps.Length, step));

			if (step >= 0 && step < mCurrentTutorial.Steps.Length)
			{
				UnloadCurrentStep();
				mCurrentTutorialStep = step;
				mEventService.Dispatch<TutorialStepChangedEvent>(tutorialStepChangedEvent =>
				{
					tutorialStepChangedEvent.TutorialData = mCurrentTutorial;
					tutorialStepChangedEvent.StepIndex = mCurrentTutorialStep;
				});
			}
		}

		private void UnloadCurrentStep()
		{
			if (mCurrentTutorial != null && mCurrentTutorialStep != -1)
			{
				mEventService.Dispatch<TutorialStepUnloadedEvent>(ev =>
				{
					ev.TutorialData = mCurrentTutorial;
					ev.CurrentStepIndex = mCurrentTutorialStep;
				});
			}
		}
	}
}