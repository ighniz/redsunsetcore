using System;
using System.Collections.Generic;
using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.Tutorial.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.Tutorial
{
	[CreateAssetMenu(fileName = nameof(TutorialData), menuName = StringConstants.Config.MENU_CONFIG + nameof(TutorialData))]
	public class TutorialData : RedSunsetScriptableConfig<TutorialData>, ITutorialData
	{
		[SerializeField]
		private string tutorialId;
		public string TutorialId => tutorialId;

		[SerializeField]
		private GameObject[] steps;
		public GameObject[] Steps => steps;

#if UNITY_EDITOR
		private void OnValidate()
		{
			if (steps != null)
			{
				for (var i = 0; i < steps.Length; i++)
				{
					GameObject step = steps[i];
					if (step != null && step.GetComponent<ITutorialStep>() == null)
					{
						UnityEditor.EditorUtility.DisplayDialog("Tutorial Data Error", $"All prefabs added should have a component that implement {nameof(ITutorialStep)} interface.", "Got it!");
						steps[i] = null;
					}
				}
			}
		}
#endif

		override public void Reset()
		{
			tutorialId = string.Empty;
			steps = new GameObject[]{};
		}
	}
}