using RedSunsetCore.Constants;
using RedSunsetCore.Core;
using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.Services.Event.ExtensionMethods;
using RedSunsetCore.Services.Tutorial.Events;
using RedSunsetCore.Services.Tutorial.Interfaces;
using UnityEngine;
using UnityEngine.Assertions;

namespace RedSunsetCore.Services.Tutorial.View
{
	[RequireComponent(typeof(RectTransform), typeof(CanvasRenderer))]
	public class ViewTutorialController : RedsunsetCoreBehaviour
	{
		public ViewTutorialOverlayComponent overlayComponent;
		private GameObject mCurrentGameObjectActive;
		private int mCurrentStepIndex = -1;
		private ITutorialData mCurrentTutorial;
		private ITutorialStep mCurrentTutorialStep;
		private ITutorialData mNextTutorialData;
		private int mNextTutorialStepIndex = -1;
		private RectTransform rectTransform;

		override protected void Start()
		{
			base.Start();
			rectTransform = GetComponent<RectTransform>();
			rectTransform.Stretch();

			mEventService.AddListener<TutorialStartedEvent>(OnTutorialStarted);
			mEventService.AddListener<TutorialSkippedEvent>(OnTutorialSkipped);
			mEventService.AddListener<TutorialStepChangedEvent>(OnTutorialStepChanged);
			mEventService.AddListener<TutorialEndedEvent>(OnTutorialEnded);
			mEventService.AddListener<TutorialStepUnloadedEvent>(OnTutorialStepUnloaded);
		}

		private void OnTutorialStarted(TutorialStartedEvent ev)
		{
			overlayComponent.gameObject.SetActive(true);
			if (mCurrentGameObjectActive == null)
			{
				ActivateStep(ev.TutorialData, ev.StepIndex);
			}
			else
			{
				mNextTutorialData = ev.TutorialData;
				mNextTutorialStepIndex = ev.StepIndex;
			}
		}

		private void ActivateStep(ITutorialData tutorialData, int stepIndex)
		{
			Assert.IsNotNull(tutorialData, StringConstants.Debug.GetFormattedMessage("There is not a current active tutorial"));

			if (mCurrentGameObjectActive == null)
			{
				print("ACTIVE WITH NULL");
				mCurrentTutorial = tutorialData;
				mCurrentStepIndex = stepIndex;
				mCurrentGameObjectActive = mInjector.InstantiatePrefab(mCurrentTutorial.Steps[mCurrentStepIndex], overlayComponent.TutorialsContainer);
				mCurrentTutorialStep = mCurrentGameObjectActive.GetComponent<ITutorialStep>();
				mCurrentTutorialStep.AddListener<TutorialStepExitCompletedEvent>(OnTutorialStepExitCompleted);
				mCurrentTutorialStep.OnEnter();
			}
			else
			{
				print("ACTIVE WAITING");
				mNextTutorialData = tutorialData;
				mNextTutorialStepIndex = stepIndex;
			}
		}

		private void OnTutorialStepExitCompleted(TutorialStepExitCompletedEvent ev)
		{
			print("OnTutorialStepExitCompleted");
			mCurrentTutorialStep.RemoveListener<TutorialStepExitCompletedEvent>(OnTutorialStepExitCompleted);
			Destroy(mCurrentGameObjectActive);
			mCurrentTutorialStep = null;
			mCurrentGameObjectActive = null;

			if (mNextTutorialData != null)
			{
				ActivateStep(mNextTutorialData, mNextTutorialStepIndex);
				mNextTutorialData = null;
				mNextTutorialStepIndex = -1;
			}
			else
			{
				overlayComponent.gameObject.SetActive(false);
			}
		}

		private void RemoveCurrentStep()
		{
			if (mCurrentTutorialStep != null)
			{
				mCurrentTutorialStep.OnExit();
			}
		}

		private void OnTutorialStepChanged(TutorialStepChangedEvent ev)
		{
			ActivateStep(ev.TutorialData, ev.StepIndex);
		}

		private void OnTutorialEnded(TutorialEndedEvent ev)
		{
			RemoveCurrentStep();
		}

		private void OnTutorialSkipped(TutorialSkippedEvent ev)
		{
			
		}

		private void OnTutorialStepUnloaded(TutorialStepUnloadedEvent ev)
		{
			RemoveCurrentStep();
		}
	}
}