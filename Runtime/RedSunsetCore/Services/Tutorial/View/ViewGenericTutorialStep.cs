using RedSunsetCore.Core;
using RedSunsetCore.Core.Events;
using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.Services.Async.Interfaces;
using RedSunsetCore.Services.Tutorial.Events;
using RedSunsetCore.Services.Tutorial.Interfaces;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.Tutorial.View
{
	public class ViewGenericTutorialStep : RedsunsetCoreBehaviour, ITutorialStep, IUpdateReceiver
	{
		[Inject]
		private ITutorialService mTutorialService;

		public void OnEnter()
		{
			this.AddToUpdateList();
		}

		public void OnExit()
		{
			this.RemoveFromUpdateList();
			mEventService.AddListener<AnimatorBehaviourExitEvent>(OnAnimatorBehaviourExit);
			animator.SetTrigger("Outro");
		}

		public void OnAnimatorBehaviourExit(AnimatorBehaviourExitEvent ev)
		{
			if (ev.Animator.gameObject == gameObject)
			{
				print("VIEW ON EXIT");
				mEventService.RemoveListener<AnimatorBehaviourExitEvent>(OnAnimatorBehaviourExit);
				Dispatcher.Dispatch<TutorialStepExitCompletedEvent>(tutorialStepExitCompletedEvent =>
				{
					tutorialStepExitCompletedEvent.Step = this;
				});
			}
		}
		
		public void OnUpdate()
		{
			if (Input.GetMouseButtonDown(0))
			{
				mTutorialService.NextStep();
			}
		}
	}
}