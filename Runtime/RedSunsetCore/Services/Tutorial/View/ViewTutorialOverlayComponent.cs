using RedSunsetCore.Core;
using RedSunsetCore.Services.Tutorial.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.Tutorial.View
{
	public class ViewTutorialOverlayComponent : RedsunsetCoreBehaviour, ITutorialOverlay
	{
		[SerializeField]
		private RectTransform tutorialsContainer;
		public RectTransform TutorialsContainer => tutorialsContainer;
	}
}