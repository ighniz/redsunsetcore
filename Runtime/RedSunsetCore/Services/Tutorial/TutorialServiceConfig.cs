using System.Collections.Generic;
using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.Tutorial.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.Tutorial
{
	[CreateAssetMenu(fileName = nameof(TutorialServiceConfig), menuName = StringConstants.Config.MENU_CONFIG + nameof(TutorialServiceConfig))]
	public class TutorialServiceConfig : RedSunsetScriptableServiceConfig<ITutorialService, TutorialServiceConfig>, IServiceConfig<ITutorialService>
	{
		[SerializeField]
		private GameObject overrideOverlayPrefab;
		public GameObject OverrideOverlayPrefab => overrideOverlayPrefab;

		[SerializeField]
		private List<TutorialData> tutorials; 
		public List<TutorialData> Tutorials => tutorials;
	}
}