using RedSunsetCore.Attributes;
using RedSunsetCore.Constants;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.Transforms;
using RedSunsetCore.Services.Tutorial.Interfaces;
using RedSunsetCore.Services.Tutorial.View;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace RedSunsetCore.Services.Tutorial
{
	[CreateAssetMenu(fileName = nameof(TutorialServiceInstaller), menuName = StringConstants.Installers.MENU_INSTALLERS + nameof(TutorialServiceInstaller))]
	public class TutorialServiceInstaller : RedSunsetInstaller<TutorialServiceInstaller, ITutorialService, TutorialServiceConfig>
	{
		[Inject]
		private ITransformService mTransformService;

		[SerializeField]
		private GameObject defaultOverlay;

		[ClassEnum(typeof(ITutorialService))]
		public string serviceType;

		override public void InstallBindings()
		{
			BasicInstallation(serviceType, service =>
			{
				Transform persistableCanvas = mTransformService.Get(nameof(SystemTransformsEnum.PersistableCanvas));
				var tutorialContainer = Container.InstantiateComponentOnNewGameObject<ViewTutorialController>("TutorialContainer");
				var tutorialContainerRectTransform = tutorialContainer.GetComponent<RectTransform>();
				tutorialContainerRectTransform.SetParent(persistableCanvas, false);
				tutorialContainerRectTransform.localPosition = Vector3.zero;
				var viewTutorialController = tutorialContainer.gameObject.GetComponent<ViewTutorialController>();

				Assert.IsNotNull(defaultOverlay, StringConstants.Debug.GetFormattedMessage($"The {nameof(defaultOverlay)} property shouldn't be null."));

				viewTutorialController.overlayComponent = Container.InstantiatePrefab(service.Config.OverrideOverlayPrefab ? service.Config.OverrideOverlayPrefab : defaultOverlay, tutorialContainerRectTransform).GetComponent<ViewTutorialOverlayComponent>();
				viewTutorialController.overlayComponent.gameObject.SetActive(false);
			}).AsSingle().NonLazy();
		}
	}
}