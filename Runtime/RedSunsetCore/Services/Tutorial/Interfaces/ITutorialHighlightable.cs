using UnityEngine;
using UnityEngine.UI;

namespace RedSunsetCore.Services.Tutorial.Interfaces
{
	public interface ITutorialHighlightable
	{
		Image HighlightShape { get; }
		GameObject HighlightTarget { get; }
	}
}