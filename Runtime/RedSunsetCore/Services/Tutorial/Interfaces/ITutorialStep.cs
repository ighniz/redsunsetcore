using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Services.Tutorial.Interfaces
{
	public interface ITutorialStep : IObjectEventDispatcher
	{
		void OnEnter();
		void OnExit();
	}
}