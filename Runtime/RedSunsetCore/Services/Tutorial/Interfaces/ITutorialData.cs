using UnityEngine;

namespace RedSunsetCore.Services.Tutorial.Interfaces
{
	public interface ITutorialData
	{
		string TutorialId { get; }
		GameObject[] Steps { get; }
	}
}