namespace RedSunsetCore.Services.Tutorial.Interfaces
{
	public interface ITutorialService : IService<ITutorialService, TutorialServiceConfig>
	{
		void Start(string tutorialId, int step = 0);
		void Skip(string tutorialId);
		void NextStep();
		void GoToStep(int step);
	}
}