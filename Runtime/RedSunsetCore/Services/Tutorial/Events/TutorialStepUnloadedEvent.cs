using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Tutorial.Interfaces;

namespace RedSunsetCore.Services.Tutorial.Events
{
	public class TutorialStepUnloadedEvent : ICustomEvent
	{
		public ITutorialData TutorialData { get; set; }
		public int CurrentStepIndex { get; set; }

		public void Reset()
		{
			
		}
	}
}