using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Tutorial.Interfaces;

namespace RedSunsetCore.Services.Tutorial.Events
{
	public class TutorialSkippedEvent : ICustomEvent
	{
		public ITutorialData TutorialData { get; set; }

		public void Reset()
		{
			TutorialData = null;
		}
	}
}