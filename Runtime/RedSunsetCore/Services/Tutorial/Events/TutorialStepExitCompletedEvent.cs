using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Tutorial.Interfaces;

namespace RedSunsetCore.Services.Tutorial.Events
{
	public class TutorialStepExitCompletedEvent : ICustomEvent
	{
		public ITutorialStep Step { get; set; }

		public void Reset()
		{
			Step = null;
		}
	}
}