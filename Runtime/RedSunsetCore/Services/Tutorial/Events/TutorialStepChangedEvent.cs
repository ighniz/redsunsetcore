using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Tutorial.Interfaces;

namespace RedSunsetCore.Services.Tutorial.Events
{
	public class TutorialStepChangedEvent : ICustomEvent
	{
		public ITutorialData TutorialData { get; set; }
		public int StepIndex { get; set; }

		public void Reset()
		{
			TutorialData = null;
			StepIndex = 0;
		}
	}
}