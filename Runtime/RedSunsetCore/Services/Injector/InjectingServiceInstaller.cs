using RedSunsetCore.Services.Zenject;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.Injector
{
    [CreateAssetMenu(fileName = "InjectingServiceInstaller", menuName = "RedSunsetCore/Installers/InjectingServiceInstaller")]
    public class InjectingServiceInstaller : ScriptableObjectInstaller<InjectingServiceInstaller>
    {
        override public void InstallBindings()
        {
            Container.Bind<IInjectingService>().To<CustomInjector>().AsSingle().NonLazy();
        }
    }
}