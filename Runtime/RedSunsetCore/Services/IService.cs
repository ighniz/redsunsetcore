﻿namespace RedSunsetCore.Services
{
    public interface IService<TService, TServiceConfig> : IService<TService>
    {
        TServiceConfig Config { get; set; }
    }

    public interface IService<TService> : IService
    {
        
    }
    
    public interface IService
    {
        
    }
}