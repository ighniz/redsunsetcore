using System;
using System.Linq;
using System.Reflection;
using RedSunsetCore.Config;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.Core
{
    [Serializable]
    public class RedSunsetInstaller<TInstaller, TService> : RedSunsetInstaller<TInstaller>
        where TService : class, IService<TService>
        where TInstaller : ScriptableObjectInstaller<TInstaller>
    {
        protected TService Service { get; set; }

        virtual protected ScopeConcreteIdArgConditionCopyNonLazyBinder BasicInstallation(string className, Action<TService> initializer = null)
        {
            var serviceType = SearchType(className);
            return Container.Bind<TService>()
                .FromMethod(() =>
                {
                    if (serviceType != null)
                    {
                        Service = (TService) Activator.CreateInstance(serviceType);
                        initializer?.Invoke(Service);
                        Container.Inject(Service);
                        return Service;
                    }

                    throw new Exception($"{className} doesn't exist!");

                });
        }

        protected ScopeConcreteIdArgConditionCopyNonLazyBinder CreateInstanceAndBasicInstallation(string className, Func<Type, TService> instanceCreator)
        {
            var serviceType = SearchType(className);
            return Container.Bind<TService>()
                .FromMethod(() =>
                {
                    if (serviceType != null)
                    {
                        return instanceCreator.Invoke(serviceType);
                    }

                    throw new Exception($"{className} doesn't exist!");

                });
        }

        protected FromBinderNonGeneric Bind<TInterface>(string className)
        {
            var target = SearchType(className);
            return Container.Bind<TInterface>().To(target);
        }

        protected Type SearchType(string className)
        {
            var targetType = Type.GetType(className);
            if (targetType == null)
            {
                //Searches in all assemblies.
                Assembly targetAssembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(assembly => assembly.GetType(className) != null);
                if (targetAssembly != null)
                {
                    targetType = targetAssembly.GetType(className);
                }
            }

            return targetType;
        }
    }

    [Serializable]
    public class RedSunsetInstaller<TInstaller> : ScriptableObjectInstaller<TInstaller>
        where TInstaller : ScriptableObjectInstaller<TInstaller>
    {
        
    }

    [Serializable]
    public class RedSunsetInstaller<TInstaller, TService, TServiceConfig> : RedSunsetInstaller<TInstaller, TService>
        where TInstaller : ScriptableObjectInstaller<TInstaller>
        where TService : class, IService<TService, TServiceConfig>
        where TServiceConfig : class, IServiceConfig<TService>
    {
        [SerializeField]
        private TServiceConfig serviceConfig;
        private TServiceConfig ServiceConfig
        {
            get
            {
                TServiceConfig targetServiceConfig = null;
                if (GameConfig.TryGetInstance(out GameConfig gameConfig))
                {
                    targetServiceConfig = gameConfig.GetServiceConfig<TService, TServiceConfig>();
                }

                return targetServiceConfig ?? serviceConfig;
            }
        }

        private void InstallConfig()
        {
            TServiceConfig config = ServiceConfig;
            Container.Inject(config);
            Container.Bind(config.GetType()).FromInstance(config).AsSingle();
            Service.Config = config;
        }

        override protected ScopeConcreteIdArgConditionCopyNonLazyBinder BasicInstallation(string className, Action<TService> initializer = null)
        {
            var serviceType = SearchType(className);
            return Container.Bind<TService>()
                .FromMethod(() =>
                {
                    if (serviceType != null)
                    {
                        Service = (TService) Activator.CreateInstance(serviceType);
                        InstallConfig();
                        initializer?.Invoke(Service);
                        Container.Inject(Service);
                        return Service;
                    }

                    throw new Exception($"There is no implementation for {className}!");

                });
        }
    }
}