using System.Collections.Generic;
using RedSunsetCore.Constants;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.Core
{
    [CreateAssetMenu(fileName = StringConstants.Files.CORE_INSTALLER, menuName = StringConstants.Installers.OPTION_CORE_INSTALLER)]
    public class GroupInstaller : RedSunsetInstaller<GroupInstaller>
    {
        public List<ScriptableObjectInstaller> installers = new List<ScriptableObjectInstaller>();

        override public void InstallBindings()
        {
            foreach (ScriptableObjectInstaller currentInstaller in installers)
            {
                Container.Inject(currentInstaller);
                currentInstaller.InstallBindings();
            }
        }
    }
}