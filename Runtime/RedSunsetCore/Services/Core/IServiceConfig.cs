using RedSunsetCore.Config;

namespace RedSunsetCore.Services.Core
{
    public interface IServiceConfig : IConfig
    {
        
    }

    public interface IServiceConfig<TService> : IServiceConfig
        where TService : IService<TService>
    {
        
    }
}