using RedSunsetCore.Services.TaskProgression.Data;
using RedSunsetCore.Services.TaskProgression.Enums;

namespace RedSunsetCore.Services.TaskProgression.ExtensionMethods
{
	static public class UserTaskExtensionMethod
	{
		static public UserTaskType GetUserTaskType(this UserTask source)
		{
			return source switch
			{
				MissionData => UserTaskType.Mission,
				QuestData => UserTaskType.Quest,
				ObjectiveData => UserTaskType.Objective,
				_ => UserTaskType.Generic
			};
		}
	}
}