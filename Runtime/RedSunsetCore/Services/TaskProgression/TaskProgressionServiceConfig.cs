using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.TaskProgression.Config;
using RedSunsetCore.Services.TaskProgression.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.TaskProgression
{
	[CreateAssetMenu(fileName = nameof(TaskProgressionServiceConfig), menuName = StringConstants.Config.MENU_CONFIG + nameof(TaskProgression) + "/" + nameof(TaskProgressionServiceConfig))]
	public class TaskProgressionServiceConfig : RedSunsetScriptableServiceConfig<ITaskProgressionService, TaskProgressionServiceConfig>, IServiceConfig<ITaskProgressionService>
	{
		[SerializeField]
		private ObjectiveScriptableConfig[] allObjectives;
		public ObjectiveScriptableConfig[] AllObjectives => allObjectives;

		[SerializeField]
		private QuestScriptableConfig[] allQuests;
		public QuestScriptableConfig[] AllQuests => allQuests;

		[SerializeField]
		private MissionScriptableConfig[] allMissions;
		public MissionScriptableConfig[] AllMissions => allMissions;
	}
}