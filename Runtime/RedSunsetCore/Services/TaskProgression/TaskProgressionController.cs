using System;
using System.Collections.Generic;
using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.TaskProgression.Data;
using RedSunsetCore.Services.TaskProgression.Events;
using RedSunsetCore.Services.TaskProgression.Interfaces;
using UnityEngine.Assertions;

namespace RedSunsetCore.Services.TaskProgression
{
	public class TaskProgressionController : ITaskProgressionService
	{
		private Dictionary<Type, HashSet<UserTask>> mAllTasksByProgressEvent = new();
		private Dictionary<Type, HashSet<UserTask>> mAllTasksByActivationEvent = new();

		public TaskProgressionServiceConfig Config { get; set; }

		public void RegisterTask<TTask>(TTask task)
			where TTask : UserTask
		{
			AssociateTaskWithEvent(task, task.ProgressEventType, ref mAllTasksByProgressEvent);
			AddListenerFor(task.ProgressEventType, OnTaskProgress);

			if (!task.IsActive && task.ActivatorEventType != null)
			{
				AssociateTaskWithEvent(task, task.ActivatorEventType, ref mAllTasksByActivationEvent);
				AddListenerFor(task.ActivatorEventType, OnActivateTasks);
			}
		}

		private void AssociateTaskWithEvent<TTask>(TTask task, Type eventType, ref Dictionary<Type, HashSet<UserTask>> tasksTable)
			where TTask : UserTask
		{
			if (tasksTable.TryGetValue(eventType, out HashSet<UserTask> userTasks))
			{
				bool existTask = userTasks.Contains(task);
				Assert.IsFalse(existTask, $"The task \"{task.Id}\" already exist.".ToAssertFormat());
				userTasks.Add(task);
			}
			else
			{
				var newUserTasksList = new HashSet<UserTask> { task };
				tasksTable.Add(eventType, newUserTasksList);
			}
		}

		private void DisassociateTaskWithEvent<TTask>(TTask task, Type eventType, ref Dictionary<Type, HashSet<UserTask>> tasksTable)
			where TTask : UserTask
		{
			if (tasksTable.TryGetValue(eventType, out HashSet<UserTask> userTasks))
			{
				bool existTask = userTasks.Contains(task);
				Assert.IsFalse(existTask, $"The task \"{task.Id}\" already exist.".ToAssertFormat());
				userTasks.Remove(task);
				if (userTasks.Count == 0)
				{
					tasksTable.Remove(eventType);
				}
			}
		}

		private void AddListenerFor(Type eventType, Action<ICustomEvent> listener)
		{
			IEventService.Instance.AddListener(eventType, listener);
		}

		private void RemoveListenerFor(Type eventType, Action<ICustomEvent> onProgressionMethod)
		{
			IEventService.Instance.RemoveListener(eventType, onProgressionMethod);
		}
		
		private void OnActivateTasks<TEvent>(TEvent ev)
		{
			HashSet<UserTask> tasks = mAllTasksByActivationEvent[ev.GetType()];
			foreach (UserTask userTask in tasks)
			{
				userTask.Activate();
				RemoveListenerFor(userTask.ActivatorEventType, OnActivateTasks);
				AddListenerFor(userTask.ProgressEventType, OnTaskProgress);
			}
		}

		private void OnTaskProgress<TEvent>(TEvent ev)
			where TEvent : ICustomEvent
		{
			HashSet<UserTask> userTasks = mAllTasksByProgressEvent[ev.GetType()];
			foreach (UserTask userTask in userTasks)
			{
				if (userTask.IsActive)
				{
					userTask.MakeProgress();
					IEventService.Instance.Dispatch<TaskProgressEvent>(x => x.Task = userTask);

					if (userTask.IsCompleted)
					{
						RemoveListenerFor(userTask.ProgressEventType, OnTaskProgress);
						DisassociateTaskWithEvent(userTask, userTask.ProgressEventType, ref mAllTasksByProgressEvent);
						IEventService.Instance.Dispatch<TaskCompletedEvent>(x => x.Task = userTask);
					}
				}
			}
		}
	}
}