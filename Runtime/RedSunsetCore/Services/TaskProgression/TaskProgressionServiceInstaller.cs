using RedSunsetCore.Attributes;
using RedSunsetCore.Constants;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.TaskProgression.Config;
using RedSunsetCore.Services.TaskProgression.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.TaskProgression
{
	[CreateAssetMenu(fileName = nameof(TaskProgressionServiceInstaller), menuName = StringConstants.Installers.MENU_INSTALLERS + nameof(TaskProgressionServiceInstaller))]
	public class TaskProgressionServiceInstaller : RedSunsetInstaller<TaskProgressionServiceInstaller, ITaskProgressionService, TaskProgressionServiceConfig>
	{
		[ClassEnum(typeof(ITaskProgressionService))]
		public string serviceType;

		override public void InstallBindings()
		{
			BasicInstallation(serviceType, service =>
			{
				TaskProgressionServiceConfig serviceConfig = service.Config;
				foreach (ObjectiveScriptableConfig objectiveConfig in serviceConfig.AllObjectives)
				{
					service.RegisterTask(objectiveConfig.TaskData);
				}

				foreach (QuestScriptableConfig questConfig in serviceConfig.AllQuests)
				{
					service.RegisterTask(questConfig.TaskData);
				}

				foreach (MissionScriptableConfig missionConfig in serviceConfig.AllMissions)
				{
					service.RegisterTask(missionConfig.TaskData);
				}
			}).AsSingle();
		}
	}
}