using System;
using RedSunsetCore.Attributes;
using RedSunsetCore.Core.Interfaces;
using RedSunsetCore.Services.Event.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.TaskProgression.Data
{
	[Serializable]
	public class UserTask : ITask
	{
		[SerializeField]
		private string id;
		public string Id => id;

		[ClassEnum(typeof(ICustomEvent))]
		private string progressEventType;
		public Type ProgressEventType
		{
			get => Type.GetType(progressEventType);
			set => progressEventType = $"{value.FullName}, {value.Assembly}";
		}

		private string activatorEventType;
		public Type ActivatorEventType
		{
			get => Type.GetType(activatorEventType);
			set => activatorEventType = $"{value.FullName}, {value.Assembly}";
		}

		[SerializeField]
		private int startValue;
		public int StartValue
		{
			get => startValue;
			set => startValue = value;
		}

		[SerializeField]
		private int targetProgress;
		public int TargetProgress
		{
			get => targetProgress;
			set => targetProgress = value;
		}

		[SerializeField]
		private int step;
		public int Step
		{
			get => step;
			set => step = value;
		}

		public int PercentageComplete => (Progress - StartValue) * 100 / (TargetProgress - StartValue);

		[SerializeField]
		private string description;
		public string Description
		{
			get => description;
			set => description = value;
		}

		[SerializeField]
		private bool isActive;
		public bool IsActive => isActive;

		[SerializeField]
		private string rewardId;
		public string RewardId => rewardId;

		[SerializeField]
		private uint rewardAmount;
		public uint RewardAmount => rewardAmount;

		public int Progress { get; private set; }
		public bool IsCompleted { get; private set; }

		public UserTask(string id)
		{
			this.id = id;
		}

		public void Activate()
		{
			if (!isActive)
			{
				isActive = true;
				Progress = StartValue;
				IsCompleted = false;
			}
		}

		virtual public void MakeProgress()
		{
			Progress += Step;
		}

		virtual public void Complete()
		{
			IsCompleted = true;
		}
	}
}