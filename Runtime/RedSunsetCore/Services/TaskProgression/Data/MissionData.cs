using System;
using System.Collections.Generic;

namespace RedSunsetCore.Services.TaskProgression.Data
{
	[Serializable]
	public class MissionData : QuestData
	{
		public List<QuestData> AllQuests { get; }

		public MissionData(string id) : base(id)
		{
		}
	}
}