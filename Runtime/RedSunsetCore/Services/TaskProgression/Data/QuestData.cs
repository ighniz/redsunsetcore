using System;
using System.Collections.Generic;

namespace RedSunsetCore.Services.TaskProgression.Data
{
	[Serializable]
	public class QuestData : ObjectiveData
	{
		public List<ObjectiveData> AllObjectives { get; }

		public QuestData(string id) : base(id)
		{
		}
	}
}