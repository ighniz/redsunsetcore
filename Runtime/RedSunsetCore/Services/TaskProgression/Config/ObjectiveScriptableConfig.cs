using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.TaskProgression.Data;
using RedSunsetCore.Services.TaskProgression.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.TaskProgression.Config
{
	[CreateAssetMenu(fileName = nameof(ObjectiveScriptableConfig), menuName = StringConstants.Config.MENU_CONFIG + nameof(TaskProgression) + "/" + nameof(ObjectiveScriptableConfig))]
	public class ObjectiveScriptableConfig : RedSunsetScriptableConfig<ObjectiveScriptableConfig>, ITaskProgressionConfig<ObjectiveData>
	{
		[SerializeField]
		private ObjectiveData objectiveData;
		public ObjectiveData TaskData => objectiveData;

		//TODO: Think something similar for Quests and Missions.

		override public void Reset()
		{
			
		}
	}
}