using System;
using RedSunsetCore.Attributes;
using RedSunsetCore.Constants;
using RedSunsetCore.Scriptables;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.TaskProgression.Data;
using RedSunsetCore.Services.TaskProgression.Interfaces;
using UnityEngine;

namespace RedSunsetCore.Services.TaskProgression.Config
{
	[CreateAssetMenu(fileName = nameof(QuestScriptableConfig), menuName = StringConstants.Config.MENU_CONFIG + nameof(TaskProgression) + "/" +  nameof(QuestScriptableConfig))]
	public class QuestScriptableConfig : RedSunsetScriptableConfig<ObjectiveScriptableConfig>, ITaskProgressionConfig<QuestData>
	{
		[ClassEnum(typeof(ICustomEvent))]
		private string eventType;
		public Type EventType => Type.GetType(eventType);

		[SerializeField]
		private QuestData questData;
		public QuestData TaskData => questData;

		//TODO: Think something similar for Quests and Missions.

		override public void Reset()
		{
			
		}
	}
}