using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.TaskProgression.Data;

namespace RedSunsetCore.Services.TaskProgression.Events
{
	public class TaskCompletedEvent : ICustomEvent
	{
		public UserTask Task { get; set; }

		public void Reset()
		{
			Task = null;
		}
	}
}