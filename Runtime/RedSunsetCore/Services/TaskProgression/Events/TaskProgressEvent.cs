using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.TaskProgression.Data;

namespace RedSunsetCore.Services.TaskProgression.Events
{
	public class TaskProgressEvent : ICustomEvent
	{
		public UserTask Task { get; set; }

		public void Reset()
		{
			//
		}
	}
}