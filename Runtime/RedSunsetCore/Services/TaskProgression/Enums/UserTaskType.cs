namespace RedSunsetCore.Services.TaskProgression.Enums
{
	public enum UserTaskType
	{
		Generic = 0,
		Objective,
		Mission,
		Quest
	}
}