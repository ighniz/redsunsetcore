using RedSunsetCore.Services.TaskProgression.Data;

namespace RedSunsetCore.Services.TaskProgression.Interfaces
{
	public interface ITaskProgressionConfig<out TTask>
		where TTask : UserTask
	{
		TTask TaskData { get; }
	}
}