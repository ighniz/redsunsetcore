using RedSunsetCore.Services.TaskProgression.Data;

namespace RedSunsetCore.Services.TaskProgression.Interfaces
{
	public interface ITaskProgressionService : IService<ITaskProgressionService, TaskProgressionServiceConfig>
	{
		void RegisterTask<TTask>(TTask task) where TTask : UserTask;
	}
}