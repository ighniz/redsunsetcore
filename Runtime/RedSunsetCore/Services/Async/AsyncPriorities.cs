﻿using System.Collections.Generic;
using RedSunsetCore.Constants;
using UnityEngine;

namespace RedSunsetCore.Services.Async
{   
    /// <summary>
    /// Serializable class used to persist the behaviours priorities. 
    ///
    /// Extends ScriptableObject
    /// </summary>
    [CreateAssetMenu(fileName = StringConstants.Files.ASYNC_PRIORITIES, menuName = StringConstants.Installers.OPTION_ASYNC_PRIORITIES)]
    public class AsyncPriorities : ScriptableObject
    {
        public List<AsyncReceiverData> awake = new List<AsyncReceiverData>();
        public List<AsyncReceiverData> start = new List<AsyncReceiverData>();
        public List<AsyncReceiverData> update = new List<AsyncReceiverData>();
        public List<AsyncReceiverData> fixedUpdate = new List<AsyncReceiverData>();
        public List<AsyncReceiverData> simulatedFixedUpdate = new List<AsyncReceiverData>();
        public List<AsyncReceiverData> lateUpdate = new List<AsyncReceiverData>();
    }
}
