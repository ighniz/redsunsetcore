using System;
using RedSunsetCore.DesignPatterns.ObserverPattern;
using RedSunsetCore.DesignPatterns.ObserverPattern.Events;
using RedSunsetCore.Services.Async.Enums;
using RedSunsetCore.Services.Async.Interfaces;
using RedSunsetCore.Services.Event.ExtensionMethods;
using RedSunsetCore.Services.Event.Interfaces;
using Zenject;

namespace RedSunsetCore.Services.Async
{
    public class Future<T> : IFuture<T>, IObservable
    {
        protected IObjectEventDispatcher ThisImpl => this;
        
        [Inject]
        IEventDispatcher IObjectEventDispatcher.Dispatcher { get; }

        public FutureStatus Status { get; protected set; }
        public bool HasValue => value != null;

        private Action<T> onValueSet;

        private T value;
        virtual public T Value
        {
            get => value;

            set
            {
                if (Status != FutureStatus.Cancelled)
                {
                    this.value = value;
                    Status = FutureStatus.Completed;
                }

                if (onValueSet != null)
                {
                    onValueSet.Invoke(value);
                    onValueSet = null;
                }

                ThisImpl.Dispatch<ObservableChangedEvent<T>>(ev => ev.Observable = Value);
            }
        }

        virtual public void WithValue(Action<T> action)
        {
            if (HasValue)
            {
                action.Invoke(Value);
            }
            else
            {
                onValueSet += action;
            }
        }
    }
}