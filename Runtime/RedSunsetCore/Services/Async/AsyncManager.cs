﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.Core.Events;
using RedSunsetCore.Services.Async.Interfaces;
using RedSunsetCore.Services.Event.Interfaces;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace RedSunsetCore.Services.Async
{
    /// <summary>
    ///     Use it for control execution order of differents MonoBehaviours callbacks
    ///     like Awake or Update.
    ///
    ///     Notes:
    ///             If BehaviourManager should check if an object is null and delete it, needs iterates all list and
    ///             probably consumes so much resources.
    /// 
    ///     Extends: MonoBehaviour
    /// </summary>
    sealed public class AsyncManager : MonoBehaviour, IAsyncService
    {
        private const string WITHOUT_PRIORITY = "No priority";

        internal static AsyncManager instance;

        internal SortedList<int, AsyncReceiverList<IUpdateReceiver>> updates;
        internal SortedList<int, AsyncReceiverList<ILateUpdateReceiver>> lateUpdates;
        internal SortedList<int, AsyncReceiverList<IFixedUpdateReceiver>> fixedUpdates;
        internal SortedList<int, AsyncReceiverList<IAwakeReceiver>> awakes;
        internal SortedList<int, AsyncReceiverList<IStartReceiver>> starts;
        internal SortedList<int, AsyncReceiverList<ISimulatedFixedUpdateReceiver>> simulatedFixedUpdates;

        private HashSet<Coroutine> allCoroutines;
        private IEventService mEventService;

        [Inject]
        private void Initialize(IEventService eventService)
        {
            mEventService = eventService;
        }

        /// <summary>
        ///     Constructor.
        /// </summary>
        /// <param name="priorities"></param>
        public void Initialize(AsyncPriorities priorities)
        {
            FillPriorities(ref awakes, priorities.awake);
            FillPriorities(ref starts, priorities.start);
            FillPriorities(ref updates, priorities.update);
            FillPriorities(ref lateUpdates, priorities.lateUpdate);
            FillPriorities(ref fixedUpdates, priorities.fixedUpdate);
            FillPriorities(ref simulatedFixedUpdates, priorities.simulatedFixedUpdate);

            SceneManager.sceneLoaded += SceneLoaded;

            allCoroutines = new HashSet<Coroutine>();
            
            instance = this;
        }

        public Coroutine WaitUntil(float seconds, Action action)
        {
            Coroutine routine = StartCoroutine(WaitUntilStarter(seconds, action));
            allCoroutines.Add(routine);
            return routine;
        }

        private IEnumerator WaitUntilStarter(float seconds, Action action)
        {
            var wait = new WaitForSeconds(seconds);
            yield return wait;
            action.Invoke();
        }

        private void OnEnable()
        {
            //StartCoroutine(CallAwakeAndStart());
        }

        /// <summary>
        ///     Calls automatically when scene was loaded.
        /// </summary>
        /// <param name="scene">Scene.</param>
        /// <param name="loadSceneMode">Load scene mode.</param>
        private void SceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            //Call Awake and then, in the next frame, call Start.
            StartCoroutine(CallAwakeAndStart());
        }

        /// <summary>
        ///     Fill a priority list with specific data.
        /// </summary>
        /// <param name="listToFill">List to fill.</param>
        /// <param name="data">Data.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        private void FillPriorities<T>(ref SortedList<int, AsyncReceiverList<T>> listToFill, IReadOnlyList<AsyncReceiverData> data) where T : IAsyncReceiver
        {
            listToFill = new SortedList<int, AsyncReceiverList<T>>();
            for (int i = 0, maxCount = data.Count; i < maxCount; i++)
            {
                var asyncReceiverList = new AsyncReceiverList<T> {priority = i, assemblyName = data[i].descriptor};
                listToFill.Add(asyncReceiverList.priority, asyncReceiverList);
            }

            //Adds one more field for those targets that theren't in the priority list.
            AsyncReceiverList<T> additional = new AsyncReceiverList<T>();
            additional.priority = data.Count;
            additional.assemblyName = WITHOUT_PRIORITY;
            listToFill.Add(additional.priority, additional);
        }

        /// <summary>
        ///     Finds the index for target.
        /// </summary>
        /// <returns>The index for target.</returns>
        /// <param name="target">Target.</param>
        /// <param name="list">List.</param>
        /// <typeparam name="T">The 1st type parameter (the type must inherit from IBehaviourReceiver).</typeparam>
        internal int FindIndexForTarget<T>(IAsyncReceiver target, SortedList<int, AsyncReceiverList<T>> list) where T : IAsyncReceiver
        {
            //Find between all types in list. If there is an object of same type of target, takes the priority of that object.
            IEnumerable<KeyValuePair<int, AsyncReceiverList<T>>> result = list.Where(x => Type.GetType(x.Value.assemblyName) == target.GetType() ||
                                                                                     x.Value.assemblyName == WITHOUT_PRIORITY);
            return result.First().Value.priority;
        }

        /// <summary>
        ///     Calls every Update.
        /// </summary>
        private void Update()
        {
            SortedList<int, AsyncReceiverList<IUpdateReceiver>> receivers = updates;

            foreach (List<IUpdateReceiver> priorityList in receivers.Select(item => item.Value.Receivers))
            {
                for (int i = priorityList.Count - 1; i >= 0; i--)
                {
                    priorityList[i].OnUpdate();
                }
            }
        }

        /// <summary>
        ///     Call after Updates, when all Updates finishes.
        /// </summary>
        private void LateUpdate()
        {
            SortedList<int, AsyncReceiverList<ILateUpdateReceiver>> receivers = this.lateUpdates;

            foreach (List<ILateUpdateReceiver> priorityList in receivers.Select(item => item.Value.Receivers))
            {
                for (int i = priorityList.Count - 1; i >= 0; i--)
                {
                    priorityList[i].OnLateUpdate();
                }
            }
        }

        /// <summary>
        ///     Call Time.fixedDeltaTime's per seconds.
        /// </summary>
        private void FixedUpdate()
        {
            SortedList<int, AsyncReceiverList<IFixedUpdateReceiver>> receivers = this.fixedUpdates;

            foreach (List<IFixedUpdateReceiver> priorityList in receivers.Select(item => item.Value.Receivers))
            {
                for (int i = priorityList.Count - 1; i >= 0; i--)
                {
                    priorityList[i].OnFixedUpdate();
                }
            }
        }

        /// <summary>
        ///     Simulates the physics. Process all physics logic and call all OnCollisionXXX and OnTriggerXXX.
        /// </summary>
        /// <param name="step">Step.</param>
        public void SimulatePhysics(float step)
        {
            Physics2D.Simulate(step);
            SortedList<int, AsyncReceiverList<ISimulatedFixedUpdateReceiver>> receivers = this.simulatedFixedUpdates;

            foreach (List<ISimulatedFixedUpdateReceiver> priorityList in receivers.Select(item => item.Value.Receivers))
            {
                for (int i = priorityList.Count - 1; i >= 0; i--)
                {
                    priorityList[i].OnSimulatedFixedUpdate();
                }
            }
        }

        /// <summary>
        ///     Calls all Awakes, wait a frame and calls all Starts.
        /// </summary>
        private IEnumerator CallAwakeAndStart()
        {
            //Waits two frames until Awake and Start methods of MonoBehaviours are executed.
            yield return new WaitForEndOfFrame();
            
            //Clean all lists.
            CleanList(awakes);
            CleanList(starts);
            CleanList(updates);
            CleanList(lateUpdates);
            CleanList(fixedUpdates);
            CleanList(simulatedFixedUpdates);
            
            //Call all Awakes and clean lists.
            foreach (List<IAwakeReceiver> priorityList in awakes.Select(item => item.Value.Receivers))
            {
                for (int i = 0, maxCount = priorityList.Count; i < maxCount; i++)
                    priorityList[i].OnAwake();
                priorityList.Clear();
            }

            //Wait for a frame for imitate Start method behaviour.
            yield return new WaitForEndOfFrame();

            //Call all Starts and clean lists.
            foreach (List<IStartReceiver> priorityList in this.starts.Select(item => item.Value.Receivers))
            {
                for (int i = 0, maxCount = priorityList.Count; i < maxCount; i++)
                    priorityList[i].OnStart();
                priorityList.Clear();
            }
        }

        /// <summary>
        ///     Clean a sorted list deleting all callbacks that not persist.
        /// </summary>
        /// <param name="listToClean">List to clean.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        private void CleanList<T>(SortedList<int, AsyncReceiverList<T>> listToClean) where T : IAsyncReceiver
        {
            foreach (KeyValuePair<int, AsyncReceiverList<T>> item in listToClean.Where(item => item.Value.Receivers.Count > 0))
                item.Value.Receivers = item.Value.Receivers.FindAll(t => t != null);
        }

        private void OnApplicationQuit()
        {
            mEventService.Dispatch<ApplicationQuitEvent>();
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            mEventService.Dispatch<ApplicationPauseEvent>(ev => ev.IsPaused = pauseStatus);
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            mEventService.Dispatch<ApplicationFocusEvent>(ev => ev.HasFocus = hasFocus);
        }
    }
}