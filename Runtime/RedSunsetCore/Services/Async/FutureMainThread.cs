using System;
using System.Collections.Generic;
using RedSunsetCore.DesignPatterns.ObserverPattern.Events;
using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.Services.Async.Enums;
using RedSunsetCore.Services.Async.Interfaces;
using RedSunsetCore.Services.Event.ExtensionMethods;

namespace RedSunsetCore.Services.Async
{
    public class FutureMainThread<T> : Future<T>, IUpdateReceiver
    {
        private List<Action<T>> listeners = new List<Action<T>>();

        private T value;
        override public T Value
        {
            get => value;
            set
            {
                if (Status != FutureStatus.Cancelled)
                {
                    this.value = value;
                    Status = FutureStatus.Completed;
                }
                
                this.AddToUpdateList();
            }
        }

        void IUpdateReceiver.OnUpdate()
        {
            ThisImpl.Dispatch<ObservableChangedEvent<T>>(ev => ev.Observable = Value);
            ThisImpl.RemoveAllListenersOf<ObservableChangedEvent<T>>();
            this.RemoveFromUpdateList();
        }
    }
}