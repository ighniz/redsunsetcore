namespace RedSunsetCore.Services.Async.Enums
{
    public enum FutureStatus
    {
        InProgress,
        Cancelled,
        Completed
    }
}