﻿namespace RedSunsetCore.Services.Async.Interfaces
{
    public interface IAsyncReceiverList<in T>
    {
        /// <summary>
        ///     Add the specified target to behaviour list.
        /// </summary>
        /// <returns>Object that implement IBehaviourReceiver inteface.</returns>
        /// <param name="target">Target.</param>
        /// <seealso cref="IAsyncReceiver"/>
        void Add(T target);

        /// <summary>
        ///     Remove the specified target from behaviour list.
        /// </summary>
        /// <returns>The remove.</returns>
        /// <param name="target">Target.</param>
        /// <seealso cref="IAsyncReceiver"/>
        void Remove(T target);

        /// <summary>
        ///     Gets or sets the priority of this object in behaviour queue.
        /// </summary>
        /// <value>The priority.</value>
        int priority { get; set; }

        /// <summary>
        /// Gets or sets the class name, including namespace.
        /// </summary>
        /// <value>The name of the assembly: namespace + className.</value>
        string assemblyName { get; set; }
    }
}
