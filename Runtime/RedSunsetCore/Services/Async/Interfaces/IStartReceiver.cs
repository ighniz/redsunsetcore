﻿namespace RedSunsetCore.Services.Async.Interfaces
{
    /// <summary>
    ///     Use to catch Start event.
    /// 
    ///     Extends: IBehaviourReceiverList<T>
    /// </summary>
    public interface IStartReceiver : IAsyncReceiver
    {
        void OnStart();
    }
}