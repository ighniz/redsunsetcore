﻿namespace RedSunsetCore.Services.Async.Interfaces
{
    /// <summary>
    ///     Use to catch Awake event.
    /// </summary>
    public interface IAwakeReceiver : IAsyncReceiver
    {
        void OnAwake();
    }
}
