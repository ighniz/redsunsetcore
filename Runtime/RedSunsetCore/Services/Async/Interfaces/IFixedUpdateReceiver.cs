﻿namespace RedSunsetCore.Services.Async.Interfaces
{
    /// <summary>
    ///     Use to catch FixedUpdate event.
    /// </summary>
    public interface IFixedUpdateReceiver : IAsyncReceiver
    {
        void OnFixedUpdate();
    }
}
