using System;
using RedSunsetCore.DesignPatterns.ObserverPattern;
using RedSunsetCore.Services.Async.Enums;

namespace RedSunsetCore.Services.Async.Interfaces
{
    public interface IFuture<T> : IObservable
    {
        T Value { get; set; }
        FutureStatus Status { get; }
        bool HasValue { get; }
        void WithValue(Action<T> action);
    }
}