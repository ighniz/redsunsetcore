﻿namespace RedSunsetCore.Services.Async.Interfaces
{
    /// <summary>
    ///     Use to catch Update event.
    /// 
    ///     Extends: IBehaviourReceiverList<T>
    /// </summary>
    public interface IUpdateReceiver : IAsyncReceiver
    {
        void OnUpdate();
    }
}
