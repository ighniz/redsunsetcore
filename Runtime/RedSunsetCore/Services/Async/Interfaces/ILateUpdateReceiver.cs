﻿namespace RedSunsetCore.Services.Async.Interfaces
{
    /// <summary>
    ///     Use to catch LateUpdate event.
    /// 
    ///     Extends: IBehaviourReceiverList<T>
    /// </summary>
    public interface ILateUpdateReceiver : IAsyncReceiver
    {
        void OnLateUpdate();
    }
}
