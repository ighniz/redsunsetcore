using System;
using System.Collections;
using UnityEngine;

namespace RedSunsetCore.Services.Async.Interfaces
{
    public interface IAsyncService : IService<IAsyncService>
    {
        void Initialize(AsyncPriorities priorities);
        Coroutine WaitUntil(float seconds, Action action);
        Coroutine StartCoroutine(IEnumerator routine);
        void StopCoroutine(Coroutine routine);
        void StopCoroutine(IEnumerator routine);
    }
}