﻿namespace RedSunsetCore.Services.Async.Interfaces
{
    public interface ISimulatedFixedUpdateReceiver : IAsyncReceiver
    {
        void OnSimulatedFixedUpdate();
    }
}
