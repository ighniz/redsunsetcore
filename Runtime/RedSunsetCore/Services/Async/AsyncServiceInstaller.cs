﻿using System;
using RedSunsetCore.Attributes;
using RedSunsetCore.Constants;
using RedSunsetCore.Services.Async.Interfaces;
using RedSunsetCore.Services.Core;
using UnityEngine;

namespace RedSunsetCore.Services.Async
{
	[CreateAssetMenu(fileName = StringConstants.Files.ASYNC_SERVICE_INSTALLER, menuName = StringConstants.Installers.OPTION_ASYNC_SERVICE_INSTALLER)]
	public class AsyncServiceInstaller : RedSunsetInstaller<AsyncServiceInstaller, IAsyncService>
	{
		[ClassEnum(typeof(IAsyncService))]
		public string defaultServiceType = StringConstants.Core.DEFAULT_NAME_NONE_SERVICE;
		public AsyncPriorities priorities;
		public string nameInScene = "AsyncService";
	
		override public void InstallBindings()
		{
			CreateInstanceAndBasicInstallation(defaultServiceType, type =>
			{
				var gameObject = new GameObject(nameInScene);
				var serviceComponent = Container.InstantiateComponent(type, gameObject) as IAsyncService;
				gameObject.transform.SetParent(Container.DefaultParent);

				if (serviceComponent != null)
				{
					serviceComponent.Initialize(priorities);
					AsyncSingletonAccess.Instance = serviceComponent;
					return serviceComponent;
				}
				
				throw new Exception($"{defaultServiceType} can't be initialized.");
			}).AsSingle().NonLazy();
		}
	}	
}
