﻿using System.Collections.Generic;
using RedSunsetCore.Services.Async.Interfaces;

namespace RedSunsetCore.Services.Async
{
    /// <summary>
    ///     Represents a list of behaviours that can be added to BehaviourManager.
    /// 
    ///     Extends: IBehaviourReceiverList<T>
    /// </summary>
    /// <seealso cref="AsyncManager"/>
    sealed public class AsyncReceiverList<T> : IAsyncReceiverList<T>
    {
        #region fields

        #region public:

        /// <summary>
        ///     Gets list of methods that will be called for BehaviourManager.
        /// </summary>
        public List<T> Receivers { get; set; }

        /// <summary>
        ///     Gets or sets the priority that it have in BehaviourManager queue.
        /// </summary>
        /// <value>The priority.</value>
        public int priority { get; set; }

        /// <summary>
        ///     Gets or sets the name of the assembly including namespace: namespace + className.
        /// </summary>
        public string assemblyName { get; set; }

        #endregion

        #endregion

        #region methods

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:PandaWorld.Services.Behaviours.ReceiverList`1"/> class.
        /// </summary>
        public AsyncReceiverList()
        {
            this.Receivers = new List<T>();
        }

        public void Add(T target)
        {
            if (!this.Receivers.Contains(target))
                this.Receivers.Add(target);
        }

        public void Remove(T target)
        {
            this.Receivers.Remove(target);
        }

        #endregion
    }
}
