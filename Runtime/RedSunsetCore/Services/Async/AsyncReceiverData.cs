﻿using System;

namespace RedSunsetCore.Services.Async
{   
    /// <summary>
    /// Serializable class that holds the data that describes a behaviour
    /// receiver in a persisted priority list
    /// </summary>
    [Serializable]
	public class AsyncReceiverData
    {
        public string descriptor;
	}
}
