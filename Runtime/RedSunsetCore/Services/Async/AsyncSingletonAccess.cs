using RedSunsetCore.Services.Async.Interfaces;

namespace RedSunsetCore.Services.Async
{
    static internal class AsyncSingletonAccess
    {
        static internal IAsyncService Instance { get; set; }
    }
}