using RedSunsetCore.Attributes;
using RedSunsetCore.Services.Core;

namespace RedSunsetCore.Services.GameActionSystem
{
    public class GameActionSystemInstaller : RedSunsetInstaller<GameActionSystemInstaller, IGameActionService>
    {
        [ClassEnum(typeof(IGameActionService))]
        public string serviceType;
        
        override public void InstallBindings()
        {
            BasicInstallation(serviceType, service =>
            {
                
            });
        }
    }
}