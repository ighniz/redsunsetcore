using UnityEngine;

namespace RedSunsetCore.Services.GameActionSystem.Presets
{
    public class GameActionQuitApp : GameAction
    {
        override public void DoAction()
        {
            Application.Quit();
        }
    }
}