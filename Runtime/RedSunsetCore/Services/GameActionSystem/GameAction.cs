﻿using System;
using RedSunsetCore.Services.Event.Interfaces;
using Zenject;

namespace RedSunsetCore.Services.GameActionSystem
{
	[Serializable]
	abstract public class GameAction : IObjectEventDispatcher
	{
		public string Name { get; private set; }
		public bool IsBusy { get; private set; }
		public bool WasFinished { get; private set; }
		public int IndexInGameActionGroup { get; internal set; }
		public GameActionGroup Parent { get; private set; }
		public string Id { get; private set; }

        abstract public void DoAction();
        [Inject]
		public IEventDispatcher Dispatcher { get; }

		public void NotifyFinish(GameActionEvent.GameActionState state = GameActionEvent.GameActionState.Completed)
		{
			Dispatcher.Dispatch<GameActionEvent>(ev =>
			{
				ev.gameAction = this;
				ev.gameActionGroup = Parent;
				ev.state = state;
			});
		}
	}
}
