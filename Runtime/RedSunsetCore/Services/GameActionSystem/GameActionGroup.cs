﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.Services.Event.ExtensionMethods;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Utils.Interfaces.JSON;
using UnityEngine;
using Zenject;

namespace RedSunsetCore.Services.GameActionSystem
{
	/// <summary>
	/// 	Represents a group of actions that can be execute.
	/// </summary>
	public class GameActionGroup : IEnumerable<GameAction>, IObjectEventDispatcher, IJson
	{
		public List<GameAction> AllActions { get; private set; } = new List<GameAction>();
		public int CurrentActionIndex { get; private set; }
		public bool AutomaticMode { get; private set; }
		
		public void AddAction(GameAction action)
		{
			if (action != null)
			{
				AllActions.Add(action);
				action.IndexInGameActionGroup = AllActions.Count - 1;
			}else
				Debug.LogWarningFormat("[{0}] The GameAction can't be null.", this);
		}
		
		public bool RemoveAction(GameAction action)
		{
			return AllActions.Remove(action);
		}

		public void RemoveActionAt(int index)
		{
			AllActions.RemoveAt(index);
		}

		public bool RemoveActionBy(Predicate<GameAction> predicate)
		{
			for (var i = 0; i < Count; i++)
			{
				if (predicate(this[i]))
				{
					AllActions.Remove(this[i]);
					return true;
				}
			}

			return false;
		}

		public T GetAction<T>() where T : GameAction
		{
			return (T)AllActions.Find(action => action is T);
		}

		public IEnumerable<T> GetAllActions<T>() where T : GameAction
		{
			return AllActions.OfType<T>();
		}

		public void DoAllActions()
		{
			RunAction(this[0]);
			AutomaticMode = true;
		}

		public void DoSomeActions(Predicate<GameAction> predicate)
		{
			for (var i = 0; i < Count; i++)
			{
				GameAction action = this[i];
				if (predicate(action))
					RunAction(action);
			}
		}

		public void DoActionByName(string actionName)
		{
			RunAction(this[actionName]);
		}

		public void DoActionByType<T>() where T : GameAction
		{
			GameAction gameAction = AllActions.Find( action => action is T );
			if(gameAction != null)
				RunAction(gameAction);
			else
				Debug.LogWarningFormat("[{0}] The GameAction {1} can't be null.", this, typeof(T));
		}

		virtual protected void RunAction(GameAction gameAction)
		{
			if (gameAction != null)
			{
				CurrentActionIndex = gameAction.IndexInGameActionGroup;
				gameAction.AddListener<GameActionEvent>(OnGameActionFinish);
				gameAction.DoAction();
			}
			else
				Debug.LogWarningFormat("[{0}] The action doesn't exist.", this);
		}

		virtual protected void OnGameActionFinish(GameActionEvent gameActionEvent)
		{
			if (AutomaticMode && CurrentActionIndex + 1 < Count)
			{
				this.Dispatch<GameActionEvent>(ev =>
				{
					ev.gameAction = gameActionEvent.gameAction;
					ev.gameActionGroup = this;
					ev.groupAlreadyFinished = false;
				}); //Dispatch event before run the next GameAction.
				RunAction(AllActions[CurrentActionIndex + 1]);
			}
			else
			{
				this.Dispatch<GameActionEvent>(ev =>
				{
					ev.gameAction = gameActionEvent.gameAction;
					ev.gameActionGroup = this;
					ev.groupAlreadyFinished = true;
				});
				
				this.Dispatch<GameActionGroupEvent>(ev =>
				{
					ev.group = this;
				});
			}
		}

		public GameAction this[int index]
		{
			get => 0 <= index && index < AllActions.Count ? AllActions[index] : null;
			set => AllActions[index] = value;
		}

		public GameAction this[string key]
		{
			get => AllActions.Find(action => action.Name == key);
			set => AllActions[AllActions.FindIndex(action => action.Name == key)] = value;
		}

		public bool Contains(string actionName)
		{
			return this[actionName] != null;
		}

		public bool Contains(GameAction action)
		{
			return Contains(action.Name);
		}

		public IEnumerator<GameAction> GetEnumerator()
		{
			return ((IEnumerable<GameAction>) AllActions).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
		
		public int Count => AllActions.Count;

		[Inject]
		public IEventDispatcher Dispatcher { get; }
	}
}
