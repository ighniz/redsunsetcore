﻿using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Services.GameActionSystem
{
	/// <summary>
	/// 	Represents a GameAction or GameActionGroup event.
	/// </summary>
	public class GameActionEvent: ICustomEvent
	{
		public GameAction gameAction;
		public GameActionGroup gameActionGroup;
		public GameActionState state;
		public bool groupAlreadyFinished = false;
		
		public void Reset()
		{

		}

		public enum GameActionState
		{
			Idle,
			InProgress,
			Completed,
			Fail
		}
	}
}