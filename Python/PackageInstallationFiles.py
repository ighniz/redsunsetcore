from pathlib import Path

engine_extension = ".rscinstallationfile"
folders = Path("./../InstallationFiles/").glob("**/*")

print("Packaging files...")

for path in folders:
        if path.is_file():
            old_name = path.stem
            old_extension = path.suffix
            if old_extension != engine_extension and old_extension != ".meta":
                directory = path.parent
                new_name = old_name + old_extension + engine_extension
                path.rename(Path(directory, new_name))

print("Done!")