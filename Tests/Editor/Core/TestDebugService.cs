using IngameDebugConsole;
using RedSunsetCore.Services.DebugTools;
using UnityEngine;

namespace RedSunsetCore.Tests.Editor.Core
{
	public class TestDebugService : IDebugService
	{
		public DebugLogManager LogManager => throw new System.NotImplementedException();

		public void SetActive(bool active)
		{
			
		}

		public void Log(string message)
		{
			Debug.Log(message);
		}

		public void LogWarning(string message)
		{
			Debug.LogWarning(message);
		}

		public void LogError(string message)
		{
			Debug.LogError(message);
		}
	}
}