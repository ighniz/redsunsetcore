using System;
using System.Collections.Generic;
using System.Linq;
using RedSunsetCore.Services.DebugTools;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Zenject;
using Zenject;

namespace RedSunsetCore.Tests.Editor.Core
{
	public class RedSunsetUnitTest<TSut> : ZenjectUnitTestFixture
	{
		protected IInjectingService mInjector;
		protected IEventService mEventService;
		protected IDebugService mDebugService;
		protected List<TSut> mAllImplementations = new();

		override public void Setup()
		{
			base.Setup();

			mAllImplementations.Clear();

			Container.Bind<IInjectingService>()
				.To<CustomInjector>()
				.AsSingle()
				.NonLazy();

			RegisterService<IEventService, Services.Event.EventSystem>();
			RegisterService<IDebugService, TestDebugService>();

			mInjector = Container.Resolve<IInjectingService>();
			mEventService = Container.Resolve<IEventService>();
			mDebugService = Container.Resolve<IDebugService>();

			CreateInstancesForAllImplementations();
		}

		public void RegisterService<TService, TImplementation>()
			where TImplementation : TService
		{
			Container.Bind<TService>()
				.To<TImplementation>()
				.AsSingle();
		}

		virtual protected void CreateInstancesForAllImplementations()
		{
			Type sutType = typeof(TSut);
			if (sutType.IsInterface)
			{
				List<Type> allTypes = AppDomain.CurrentDomain.GetAssemblies()
					.SelectMany(assembly => assembly.GetTypes())
					.Where(applicable => sutType.IsAssignableFrom(applicable) && !applicable.IsInterface)
					.ToList();
			
				foreach (Type implementationType in allTypes)
				{
					CreateAndRegisterInstance(implementationType);
				}
			}
			else
			{
				CreateAndRegisterInstance(sutType);
			}
		}

		virtual protected void CreateAndRegisterInstance(Type implementationType)
		{
			var implementationInstance = (TSut)Activator.CreateInstance(implementationType);
			mInjector.InjectDependencies(implementationInstance);
			mAllImplementations.Add(implementationInstance);
		}

		virtual protected void Run(Action<TSut> action)
		{
			mDebugService.Log($"Starting test with {mAllImplementations.Count} implementations for {typeof(TSut)}...");
			foreach (TSut implementation in mAllImplementations)
			{
				mDebugService.Log($"Running test with {implementation.GetType()} implementation...");
				action.Invoke(implementation);
				mDebugService.Log($"Test with {implementation.GetType()}... finished.");
			}
		}

		virtual protected void Run<TTargetSut>(Action<TTargetSut> action) where TTargetSut : TSut
		{
			mDebugService.Log($"Starting test with {mAllImplementations.Count} implementations for {typeof(TSut)}...");
			foreach (TTargetSut implementation in mAllImplementations)
			{
				mDebugService.Log($"Running test with {implementation.GetType()} implementation...");
				action.Invoke(implementation);
				mDebugService.Log($"Test with {implementation.GetType()}... finished.");
			}
		}
	}
}