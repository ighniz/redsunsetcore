using NUnit.Framework;
using RedSunsetCore.DesignPatterns.PoolPattern.Enums;
using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.Tests.Editor.Core;
using Tests.Editor.PoolPattern;

namespace RedSunsetCore.Tests.Editor.PoolPattern
{
	public class TestPoolPattern : RedSunsetUnitTest<ITestObjectPool>
	{
		[SetUp]
		override public void Setup()
		{
			base.Setup();
		}

		[TestCase(0u)]
		[TestCase(1u)]
		[TestCase(5u)]
		public void TestCreation(uint amount)
		{
			Run(pool =>
			{
				pool.CreatorCallback = () => new TestObjectForPool();
				pool.CreateCache(amount);
				Assert.AreEqual(amount, (uint)pool.AvailableObjects, $"The amount of available objects should be {amount} but it is {pool.AvailableObjects}".ToAssertFormat());
				Assert.AreEqual(0, pool.BusyObjects, $"The amount of busy objects should be 0 for this test and it is {pool.BusyObjects}".ToAssertFormat());
				Assert.AreEqual(amount, (uint)pool.TotalObjects, $"The amount of total objects should be {amount} but it is {pool.TotalObjects}".ToAssertFormat());
			});
		}

		[Test]
		public void TestObjectPickupAndRelease()
		{
			Run(pool =>
			{
				pool.CreatorCallback = () => new TestObjectForPool();
				pool.CreateCache(1);
				
				var obj = pool.Pick();
				Assert.IsNotNull(obj, $"The pool pickup object should not be null.".ToAssertFormat());
				Assert.AreEqual(0, pool.AvailableObjects, $"Just one object was created and picked, so it shouldn't be available objects ({nameof(pool.AvailableObjects)} = {pool.AvailableObjects}).".ToAssertFormat());
				Assert.AreEqual(1, pool.BusyObjects, $"It should be 1 busy object in the pool (${nameof(pool.BusyObjects)} = {pool.BusyObjects}).)".ToAssertFormat());

				pool.Release(obj);
				Assert.AreEqual(1, pool.AvailableObjects, $"One object was picked and released, so it should be 1 available object (${nameof(pool.AvailableObjects)} = {pool.AvailableObjects}).)".ToAssertFormat());
				Assert.AreEqual(0, pool.BusyObjects, $"One object was picked and released, so it shouldn't be busy objects ({nameof(pool.BusyObjects)} = {pool.BusyObjects}).)".ToAssertFormat());
			});
		}

		[Test]
		public void TestPoolExpansion()
		{
			Run(pool =>
			{
				pool.CreatorCallback = () => new TestObjectForPool();
				pool.CreateCache(1);
				
				TestObjectForPool obj1 = pool.Pick();
				TestObjectForPool obj2 = pool.Pick();
				
				Assert.IsNotNull(obj1, "The picked object should not be null.".ToAssertFormat());
				Assert.IsNotNull(obj2, "The second picked object should not be null. The pool should be expanded.".ToAssertFormat());
				Assert.AreEqual(0, pool.AvailableObjects, $"It should be 0 available objects ({nameof(pool.AvailableObjects)} = {pool.AvailableObjects}))");
				Assert.AreEqual(2, pool.BusyObjects, $"Two objects were created and picked, so it should be 2 busy objects ({nameof(pool.BusyObjects)} = {pool.BusyObjects}).");
			});
		}

		[Test]
		public void TestPoolMetrics()
		{
			Run(pool =>
			{
				pool.CreatorCallback = () => new TestObjectForPool();
				pool.CreateCache(2);

				TestObjectForPool obj1 = pool.Pick();
				Assert.AreEqual(1, pool.AvailableObjects, $"2 objects were created and 1 picked so just one should be available but {nameof(pool.AvailableObjects)} = {pool.AvailableObjects}.");
				Assert.AreEqual(1, pool.BusyObjects, $"2 objects were created and 1 picked so just one should be busy but {nameof(pool.BusyObjects)} = {pool.BusyObjects}.");

				TestObjectForPool obj2 = pool.Pick();
				Assert.AreEqual(0, pool.AvailableObjects, $"2 objects were created and 2 picked so it should be available objects but {nameof(pool.AvailableObjects)} = {pool.AvailableObjects}.");
				Assert.AreEqual(2, pool.BusyObjects, $"2 objects were created and 2 picked so it should be 2 objects busy but {nameof(pool.BusyObjects)} = {pool.BusyObjects}.");

				pool.Release(obj1);
				Assert.AreEqual(1, pool.AvailableObjects, $"2 objects were created and picked and 1 was released. So It should be 1 available but {nameof(pool.AvailableObjects)} = {pool.AvailableObjects}.");
				Assert.AreEqual(1, pool.BusyObjects, $"2 objects were created and picked and 1 was released. So it should be 1 busy but {nameof(pool.BusyObjects)} = {pool.BusyObjects}.");

				pool.Release(obj2);
				Assert.AreEqual(2, pool.AvailableObjects, $"2 objects were created, picked and released. So it should be 2 available but {nameof(pool.AvailableObjects)} = {pool.AvailableObjects}.");
				Assert.AreEqual(0, pool.BusyObjects, $"2 objects were created, picked and released. So it should be 0 busy but {nameof(pool.BusyObjects)} = {pool.BusyObjects}.");
			});
		}

		[Test]
		public void TestObjectStateTransition()
		{
			Run(pool =>
			{
				pool.CreatorCallback = () => new TestObjectForPool();
				pool.CreateCache(2);

				TestObjectForPool obj1 = pool.Pick();
				Assert.AreEqual(PooleableObjectState.Picked, obj1.State, $"The picked object should have been picked but the state is {obj1.State}.");

				pool.Release(obj1);
				Assert.AreEqual(PooleableObjectState.Released, obj1.State, $"The picked object was released but the state is {obj1.State}.");
			});
		}
	}
}