using RedSunsetCore.DesignPatterns.PoolPattern.Interfaces;

namespace Tests.Editor.PoolPattern
{
	public interface ITestObjectPool : IObjectPool<TestObjectForPool>
	{
		
	}
}