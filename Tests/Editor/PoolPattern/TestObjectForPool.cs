using RedSunsetCore.DesignPatterns.PoolPattern.Enums;
using RedSunsetCore.DesignPatterns.PoolPattern.Interfaces;

namespace Tests.Editor.PoolPattern
{
	public class TestObjectForPool : IPooleableObject
	{
		public PooleableObjectState State { get; private set; }

		void IPooleableObject.OnPicked()
		{
			State = PooleableObjectState.Picked;
		}

		void IPooleableObject.OnReleased()
		{
			State = PooleableObjectState.Released;
		}
	}
}