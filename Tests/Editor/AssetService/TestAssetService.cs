using RedSunsetCore.Services.Assets;
using RedSunsetCore.Tests.Editor.Core;

namespace RedSunsetCore.Tests.Editor.AssetService
{
	public class TestAssetService : RedSunsetUnitTest<IAssetService>
	{
		private const string ASSET_TEST_GAMEOBJECT = "AssetServiceTestGameObject";
		private const string ASSET_TEST_MATERIAL = "AssetServiceTestMaterial";
		private const string ASSET_TEST_MATERIAL_BLUE = "AssetServiceTestMaterialVariationBlue";
		private const string ASSET_TEST_MATERIAL_RED = "AssetServiceTestMaterialVariationRed";
		private const string ASSET_TEST_SCENE = "AssetServiceTestScene";
		private const string ASSET_TEST_SQUARE = "AssetServiceTestSquare";
		private const string ASSET_TEST_TRIANGLE = "AssetServiceTestTriangle";
		private const string VARIATION_RED = "tests_blue";
		private const string VARIATION_BLUE = "tests_red";

		//[Test]
		public void InlineSimpleLoad()
		{
			/*Run(assetService =>
			{
				var square = assetService.LoadAsset<Sprite>(ASSET_TEST_SQUARE);
				var triangle = assetService.LoadAsset<Sprite>(ASSET_TEST_TRIANGLE);
				Assert.Equals(square.name, ASSET_TEST_SQUARE);
				Assert.Equals(triangle.name, ASSET_TEST_TRIANGLE);
			});*/
		}

		//[Test]
		public void InlineLoadVariation()
		{
			/*Run(assetService =>
			{
				var material = assetService.LoadAsset<Material>(ASSET_TEST_MATERIAL);
				var materialBlue = assetService.LoadAsset<Material>(ASSET_TEST_MATERIAL_BLUE);
				var materialRed = assetService.LoadAsset<Material>(ASSET_TEST_MATERIAL_RED);
			});*/
		}

		//[Test]
		public void InlineLoadScene()
		{
		}

		//[UnityTest]
		/*public IEnumerator AsyncLoad()
		{
			yield return RunAsync(assetService =>
			{
				for (var i = 0; i < 10; i++)
				{
					Debug.Log($"Value: {i}");
				}

				canContinueRun = true;
			});
		}*/
	}
}