using RedSunsetCore.Services.TaskProgression.Data;

namespace RedSunsetCore.Tests.Editor.TaskProgression
{
	public class TestTask : UserTask
	{
		static private int cumulativeId = 0;
		public TestTask(string id) : base(id + cumulativeId++)
		{
			
		}
	}
}