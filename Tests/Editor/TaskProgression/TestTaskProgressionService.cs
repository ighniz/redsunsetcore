using NUnit.Framework;
using RedSunsetCore.Services.TaskProgression.Interfaces;
using RedSunsetCore.Tests.Editor.Core;

namespace RedSunsetCore.Tests.Editor.TaskProgression
{
	[TestFixture]
	public class TestTaskProgressionService : RedSunsetUnitTest<ITaskProgressionService>
	{
		private const string TASK_ID = "task_for_test";
		
		[SetUp]
		override public void Setup()
		{
			base.Setup();
		}

		[TestCase(0, 1, 4, 25)]
		[TestCase(0, 2, 4, 50)]
		[TestCase(0, 4, 4, 100)]
		[TestCase(0, 0, 4, 0)]
		[TestCase(4, -1, 0, 25)]
		public void TestListenProgress(int startValue, int step, int targetProgress, int percentageComplete)
		{
			Run(taskService =>
			{
				var task = new TestTask(TASK_ID)
				{
					ProgressEventType = typeof(TestProgressTaskEvent),
					StartValue = startValue,
					Step = step,
					TargetProgress = targetProgress
				};
				task.Activate();
				taskService.RegisterTask(task);
				mEventService.Dispatch<TestProgressTaskEvent>();
				Assert.AreEqual(task.PercentageComplete, percentageComplete, $"With {nameof(startValue)}: {startValue} and {nameof(targetProgress)}: {targetProgress}, making {step} progress, the result should be {percentageComplete}% (but it's {task.PercentageComplete}%).");
			});
		}

		[Test]
		public void TestAutoActivation()
		{
			Run(taskService =>
			{
				var task = new TestTask(TASK_ID)
				{
					StartValue = 0,
					Step = 1,
					TargetProgress = 4,
					ActivatorEventType = typeof(TestActivationTaskEvent),
					ProgressEventType = typeof(TestProgressTaskEvent)
				};
			
				Assert.IsFalse(task.IsActive, $"The task {task.Id} should start deactivated.");
				taskService.RegisterTask(task);
			
				mEventService.Dispatch<TestProgressTaskEvent>();
				Assert.IsTrue(task.PercentageComplete == 0, $"The task {task.Id} shouldn't make progress.");
			
				mEventService.Dispatch<TestActivationTaskEvent>();
				Assert.IsTrue(task.IsActive, $"The task {task.Id} should be active after the activation event.");
			
				mEventService.Dispatch<TestProgressTaskEvent>();
				Assert.IsTrue(task.PercentageComplete == 25, "The task should has 25% completed.");
			});
		}
	}
}