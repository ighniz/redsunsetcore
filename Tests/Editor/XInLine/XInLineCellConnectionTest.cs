using System.Collections.Generic;
using RedSunsetCore.GameTools.XInLineTools.Interfaces;

namespace RedSunsetCore.Tests.Editor.XInLine
{
	public class XInLineCellConnectionTest : ICellConnection<MatrixCellForTest, XInLineTileTest>
	{
		public HashSet<MatrixCellForTest> ConnectedCells { get; }
	}
}