using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using RedSunsetCore.GameTools.Orientation;
using RedSunsetCore.GameTools.XInLineTools.Factory;
using RedSunsetCore.GameTools.XInLineTools.Matrix;
using RedSunsetCore.Services.Event;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Tests.Editor.Core;
using UnityEngine;

namespace RedSunsetCore.Tests.Editor.XInLine
{
	[TestFixture]
	public class XInLineMatrixTest : RedSunsetUnitTest<XInLineMatrixTest>
	{
		private XInLineMatrix<MatrixCellForTest, XInLineTileTest, XInLineCellConnectionTest> board;
		private CellFactory<MatrixCellForTest, XInLineTileTest> factory;

		[SetUp]
		override public void Setup()
		{
			base.Setup();
			factory = mInjector.Create<CellFactory<MatrixCellForTest, XInLineTileTest>>();
			Container.Bind<IEventDispatcher>().To<EventDispatcher>().AsTransient();
		}

		[TestCase(10, 10)]
		[TestCase(15, 10)]
		[TestCase(10, 15)]
		public void TestFill(int rowsCount, int columnsCount)
		{
			board = new XInLineMatrix<MatrixCellForTest, XInLineTileTest, XInLineCellConnectionTest>(rowsCount, columnsCount, DPadOrientation.Plus);
			mInjector.InjectDependencies(board);
			board.Fill(factory, true);
			Assert.True(board.Count == board.Capacity);
		}

		[TestCase(5, 5, 10, 10)]
		[TestCase(0, 0, 10, 10)]
		[TestCase(9, 9, 10, 10)]
		[TestCase(0, 9, 10, 10)]
		[TestCase(9, 0, 10, 10)]
		[TestCase(5, 5, 15, 10)]
		[TestCase(0, 0, 15, 10)]
		[TestCase(9, 9, 15, 10)]
		[TestCase(0, 9, 15, 10)]
		[TestCase(9, 0, 15, 10)]
		[TestCase(5, 5, 15, 15)]
		[TestCase(0, 0, 15, 15)]
		[TestCase(9, 9, 15, 15)]
		[TestCase(0, 9, 15, 15)]
		[TestCase(9, 0, 15, 15)]
		public void TestConnections(int row, int column, int rowsCount, int columnsCount)
		{
			board = new XInLineMatrix<MatrixCellForTest, XInLineTileTest, XInLineCellConnectionTest>(rowsCount, columnsCount, DPadOrientation.Plus);
			mInjector.InjectDependencies(board);
			board.Fill(factory, true);
			HashSet<MatrixCellForTest> allNeighbors = board[row, column].DirectNeighbors;
			foreach (MatrixCellForTest neighbor in allNeighbors)
			{
				Assert.True(Mathf.Abs(neighbor.Position.Column - column) <= 1 &&
				            Mathf.Abs(neighbor.Position.Row - row) <= 1);
			}
		}

		[TestCase(10, 10)]
		[TestCase(15, 10)]
		[TestCase(10, 15)]
		public void TestMatch(int rowsCount, int columnsCount)
		{
			board = new XInLineMatrix<MatrixCellForTest, XInLineTileTest, XInLineCellConnectionTest>(rowsCount, columnsCount, DPadOrientation.Plus);
			mInjector.InjectDependencies(board);
			board.Fill(factory, true);

			board[0, 0].AddTile(new XInLineTileTest { id = 1 });
			board[0, 1].AddTile(new XInLineTileTest { id = 1 });
			board[0, 2].AddTile(new XInLineTileTest { id = 1 });
			board[1, 0].AddTile(new XInLineTileTest { id = 1 });
			board[2, 0].AddTile(new XInLineTileTest { id = 1 });

			List<MatrixCellForTest> result = board.GetMatchesOf(board[0, 0]).ToList();
			Assert.AreEqual(4, result.Count);
			Assert.Contains(board[0, 1], result);
			Assert.Contains(board[0, 2], result);
			Assert.Contains(board[1, 0], result);
			Assert.Contains(board[2, 0], result);
		}

		[TestCase(0, 0, 0, 1, 10, 10)]
		[TestCase(0, 0, 1, 1, 10, 10)]
		[TestCase(0, 0, 1, 0, 10, 10)]
		[TestCase(0, 0, 0, 1, 10, 10)]
		[TestCase(2, 0, 0, 2, 10, 10)]
		[TestCase(2, 0, 0, 0, 10, 10)]
		[TestCase(1, 2, 1, 1, 10, 10)]
		[TestCase(0, 0, 0, 0, 10, 10)]
		[TestCase(0, 0, 0, 1, 15, 10)]
		[TestCase(0, 0, 1, 1, 15, 10)]
		[TestCase(0, 0, 1, 0, 15, 10)]
		[TestCase(0, 0, 0, 1, 15, 10)]
		[TestCase(2, 0, 0, 2, 15, 10)]
		[TestCase(2, 0, 0, 0, 15, 10)]
		[TestCase(1, 2, 1, 1, 15, 10)]
		[TestCase(0, 0, 0, 0, 15, 10)]
		[TestCase(0, 0, 0, 1, 10, 15)]
		[TestCase(0, 0, 1, 1, 10, 15)]
		[TestCase(0, 0, 1, 0, 10, 15)]
		[TestCase(0, 0, 0, 1, 10, 15)]
		[TestCase(2, 0, 0, 2, 10, 15)]
		[TestCase(2, 0, 0, 0, 10, 15)]
		[TestCase(1, 2, 1, 1, 10, 15)]
		[TestCase(0, 0, 0, 0, 10, 15)]
		public void TestSwap(int rowA, int columnA, int rowB, int columnB, int rowsCount, int columnsCount)
		{
			board = new XInLineMatrix<MatrixCellForTest, XInLineTileTest, XInLineCellConnectionTest>(rowsCount, columnsCount, DPadOrientation.Plus);
			mInjector.InjectDependencies(board);
			board.Fill(factory, true);
			board[0, 0].AddTile(new XInLineTileTest { id = 1 });
			board[0, 1].AddTile(new XInLineTileTest { id = 1 });
			board[0, 2].AddTile(new XInLineTileTest { id = 1 });
			board[1, 0].AddTile(new XInLineTileTest { id = 2 });
			board[2, 0].AddTile(new XInLineTileTest { id = 2 });

			MatrixCellForTest cellA = board[rowA, columnA];
			MatrixCellForTest cellB = board[rowB, columnB];
			int beforeNeighborsForA = cellA.DirectNeighbors.Count;
			int beforeNeighborsForB = cellB.DirectNeighbors.Count;

			board.Swap(cellA, cellB);

			int afterNeighborsForA = cellA.DirectNeighbors.Count;
			int afterNeighborsForB = cellB.DirectNeighbors.Count;

			Assert.AreEqual(beforeNeighborsForA, afterNeighborsForB);
			Assert.AreEqual(beforeNeighborsForB, afterNeighborsForA);
			Assert.AreEqual(cellA.DirectNeighbors.Count, beforeNeighborsForB);
			Assert.AreEqual(cellB.DirectNeighbors.Count, beforeNeighborsForA);
		}

		[TestCase(10, 10)]
		[TestCase(15, 10)]
		[TestCase(10, 15)]
		public void TestSwapAndMatch(int rowsCount, int columnsCount)
		{
			board = new XInLineMatrix<MatrixCellForTest, XInLineTileTest, XInLineCellConnectionTest>(rowsCount, columnsCount, DPadOrientation.Plus);
			mInjector.InjectDependencies(board);
			board.Fill(factory, true);
			board[0, 0].AddTile(new XInLineTileTest { id = 1 });
			board[0, 1].AddTile(new XInLineTileTest { id = 1 });
			board[0, 2].AddTile(new XInLineTileTest { id = 2 });
			board[0, 3].AddTile(new XInLineTileTest { id = 1 });
			board[0, 4].AddTile(new XInLineTileTest { id = 2 });

			MatrixCellForTest cellA = board[0, 0];

			Assert.AreEqual(1, board.GetMatchesOf(cellA).Count);
			board.Swap(board[0, 2], board[0, 3]);
			Assert.AreEqual(2, board.GetMatchesOf(cellA).Count);
		}

		[TestCase(10, 10)]
		[TestCase(15, 10)]
		[TestCase(10, 15)]
		public void TestHorizontalRemoveAndMove(int rowsCount, int columnsCount)
		{
			board = new XInLineMatrix<MatrixCellForTest, XInLineTileTest, XInLineCellConnectionTest>(rowsCount, columnsCount, DPadOrientation.Plus);
			mInjector.InjectDependencies(board);

			board.Fill(factory, true);
			board[0, 0].AddTile(new XInLineTileTest { id = 1 });
			board[0, 1].AddTile(new XInLineTileTest { id = 1 });
			board[0, 2].AddTile(new XInLineTileTest { id = 1 });
			board[0, 3].AddTile(new XInLineTileTest { id = 1 });
			board[0, 4].AddTile(new XInLineTileTest { id = 1 });
			board[1, 0].AddTile(new XInLineTileTest { id = 3 });
			board[1, 1].AddTile(new XInLineTileTest { id = 3 });
			board[1, 2].AddTile(new XInLineTileTest { id = 3 });
			board[1, 3].AddTile(new XInLineTileTest { id = 3 });
			board[1, 4].AddTile(new XInLineTileTest { id = 3 });

			//Matcheable cell.
			board[5, 5].AddTile(new XInLineTileTest { id = 3 });

			Assert.False(board[0, 0].IsMatchableWith(board[5, 5]));
			Assert.False(board[0, 1].IsMatchableWith(board[5, 5]));
			Assert.False(board[0, 2].IsMatchableWith(board[5, 5]));
			Assert.False(board[0, 3].IsMatchableWith(board[5, 5]));
			Assert.False(board[0, 4].IsMatchableWith(board[5, 5]));

			HashSet<MatrixCellForTest> matches = board.GetMatchesOf(board[0, 1], true);
			HashSet<MatrixCellForTest> cellsToMove = board.RemoveAndMove(matches, DPadOrientation.Down);

			Assert.AreEqual(5, matches.Count);
			Assert.AreEqual((rowsCount - 1) * 5, cellsToMove.Count);

			Assert.True(board[0, 0].IsMatchableWith(board[5, 5]));
			Assert.True(board[0, 1].IsMatchableWith(board[5, 5]));
			Assert.True(board[0, 2].IsMatchableWith(board[5, 5]));
			Assert.True(board[0, 3].IsMatchableWith(board[5, 5]));
			Assert.True(board[0, 4].IsMatchableWith(board[5, 5]));
		}

		[TestCase(10, 10)]
		[TestCase(15, 10)]
		[TestCase(10, 15)]
		public void TestVerticalRemoveAndMove(int rowsCount, int columnsCount)
		{
			board = new XInLineMatrix<MatrixCellForTest, XInLineTileTest, XInLineCellConnectionTest>(rowsCount, columnsCount, DPadOrientation.Plus);
			mInjector.InjectDependencies(board);

			board.Fill(factory, true);
			board[0, 0].AddTile(new XInLineTileTest { id = 1 });
			board[1, 0].AddTile(new XInLineTileTest { id = 1 });
			board[2, 0].AddTile(new XInLineTileTest { id = 1 });
			board[3, 0].AddTile(new XInLineTileTest { id = 1 });
			board[4, 0].AddTile(new XInLineTileTest { id = 1 });
			board[5, 0].AddTile(new XInLineTileTest { id = 3 });
			board[6, 0].AddTile(new XInLineTileTest { id = 3 });
			board[7, 0].AddTile(new XInLineTileTest { id = 3 });
			board[8, 0].AddTile(new XInLineTileTest { id = 3 });
			board[9, 0].AddTile(new XInLineTileTest { id = 3 });

			//Matcheable cell.
			board[5, 5].AddTile(new XInLineTileTest { id = 3 });

			Assert.False(board[0, 0].IsMatchableWith(board[5, 5]), $"The cell {board[0, 0]} SHOULDN'T BE matchable with {board[5, 5]}");
			Assert.False(board[1, 0].IsMatchableWith(board[5, 5]), $"The cell {board[1, 0]} SHOULDN'T BE matchable with {board[5, 5]}");
			Assert.False(board[2, 0].IsMatchableWith(board[5, 5]), $"The cell {board[2, 0]} SHOULDN'T BE matchable with {board[5, 5]}");
			Assert.False(board[3, 0].IsMatchableWith(board[5, 5]), $"The cell {board[3, 0]} SHOULDN'T BE matchable with {board[5, 5]}");
			Assert.False(board[4, 0].IsMatchableWith(board[5, 5]), $"The cell {board[4, 0]} SHOULDN'T BE matchable with {board[5, 5]}");

			HashSet<MatrixCellForTest> matches = board.GetMatchesOf(board[1, 0], true);
			HashSet<MatrixCellForTest> cellsToMove = board.RemoveAndMove(matches, DPadOrientation.Down);

			Assert.AreEqual(5, matches.Count, $"The matches count SHOULD BE 5, but was {matches.Count}");
			int expectedCellsToMoveCount = rowsCount - matches.Count;
			Assert.AreEqual(expectedCellsToMoveCount, cellsToMove.Count, $"The cells counts to move SHOULD BE {expectedCellsToMoveCount}, but was {cellsToMove.Count}");

			Assert.True(board[0, 0].IsMatchableWith(board[5, 5]), $"The cell {board[0, 0]} SHOULD BE matchable with {board[5, 5]}");
			Assert.True(board[1, 0].IsMatchableWith(board[5, 5]), $"The cell {board[1, 0]} SHOULD BE matchable with {board[5, 5]}");
			Assert.True(board[2, 0].IsMatchableWith(board[5, 5]), $"The cell {board[2, 0]} SHOULD BE matchable with {board[5, 5]}");
			Assert.True(board[3, 0].IsMatchableWith(board[5, 5]), $"The cell {board[3, 0]} SHOULD BE matchable with {board[5, 5]}");
			Assert.True(board[4, 0].IsMatchableWith(board[5, 5]), $"The cell {board[4, 0]} SHOULD BE matchable with {board[5, 5]}");
		}

		[TestCase(10, 10)]
		[TestCase(15, 10)]
		[TestCase(10, 15)]
		public void TestSwapRemoveAndMove(int rowsCount, int columnsCount)
		{
			board = new XInLineMatrix<MatrixCellForTest, XInLineTileTest, XInLineCellConnectionTest>(rowsCount, columnsCount, DPadOrientation.Plus);
			mInjector.InjectDependencies(board);
			board.Fill(factory, true);
			board[0, 0].AddTile(new XInLineTileTest { id = 3 });
			board[0, 1].AddTile(new XInLineTileTest { id = 1 });
			board[0, 2].AddTile(new XInLineTileTest { id = 1 });
			board[0, 3].AddTile(new XInLineTileTest { id = 1 });
			board[0, 4].AddTile(new XInLineTileTest { id = 1 });
			board[1, 0].AddTile(new XInLineTileTest { id = 1 });
			board[1, 1].AddTile(new XInLineTileTest { id = 3 });
			board[1, 2].AddTile(new XInLineTileTest { id = 3 });
			board[1, 3].AddTile(new XInLineTileTest { id = 3 });
			board[1, 4].AddTile(new XInLineTileTest { id = 3 });

			//Matcheable cell.
			board[5, 5].AddTile(new XInLineTileTest { id = 3 });

			MatrixCellForTest cellA = board[0, 0];
			MatrixCellForTest cellB = board[1, 0];
			board.Swap(cellA, cellB);

			HashSet<MatrixCellForTest> matches = board.GetMatchesOf(cellB, true);
			HashSet<MatrixCellForTest> cellsToMove = board.RemoveAndMove(matches, DPadOrientation.Down);

			Assert.AreEqual(5, matches.Count);
			Assert.AreEqual((rowsCount - 1) * matches.Count, cellsToMove.Count);

			Assert.True(board[0, 0].IsMatchableWith(board[5, 5]));
			Assert.True(board[0, 1].IsMatchableWith(board[5, 5]));
			Assert.True(board[0, 2].IsMatchableWith(board[5, 5]));
			Assert.True(board[0, 3].IsMatchableWith(board[5, 5]));
			Assert.True(board[0, 4].IsMatchableWith(board[5, 5]));
		}

		[Test]
		public void TestMatchingTilesRecursively()
		{
			board = new XInLineMatrix<MatrixCellForTest, XInLineTileTest, XInLineCellConnectionTest>(5, 5, DPadOrientation.Plus);
			mInjector.InjectDependencies(board);
			board.Fill(factory, true);
			CellPosition targetA = new(3, 2);
			CellPosition targetB = new(3, 4);
			board[targetA].AddTile(new XInLineHorizontalTileTest { id = 3, Matrix = board, Cell = board[targetA] });
			board[targetB].AddTile(new XInLineHorizontalTileTest { id = 3, Matrix = board, Cell = board[targetB] });

			board[targetA].Pop();
		}
	}
}