using System.Collections.Generic;
using RedSunsetCore.GameTools.XInLineTools.Matrix;

namespace RedSunsetCore.Tests.Editor.XInLine
{
	public class XInLineHorizontalTileTest : XInLineTileTest
	{
		public XInLineMatrix<MatrixCellForTest, XInLineTileTest, XInLineCellConnectionTest> Matrix { get; set; }
		public MatrixCellForTest Cell { get; set; }
		public HashSet<MatrixCellForTest> MatchingGluppops { get; set; }

		override public void OnCellPopped()
		{
			MatchingGluppops = new HashSet<MatrixCellForTest>();

			CollectTowardTo(CellPosition.Right);
			CollectTowardTo(CellPosition.Left);

			Matrix.Remove(MatchingGluppops);
		}

		private void CollectTowardTo(CellPosition direction)
		{
			CellPosition neighborPosition = Cell.Position + direction;
			while (Matrix.IsValidPosition(neighborPosition))
			{
				MatrixCellForTest targetCell = Matrix[neighborPosition];
				if (targetCell != null)
				{
					MatchingGluppops.Add(targetCell);
				}

				neighborPosition += direction;
			}
		}
	}
}