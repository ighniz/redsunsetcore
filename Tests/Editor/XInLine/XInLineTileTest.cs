using RedSunsetCore.GameTools.XInLineTools.Interfaces;

namespace RedSunsetCore.Tests.Editor.XInLine
{
	public class XInLineTileTest : IXInLineTileComponent<XInLineTileTest>
	{
		public int id;

		virtual public bool IsMatchableWith(XInLineTileTest other)
		{
			return other.id == id;
		}

		virtual public void OnCellPopped()
		{
			//throw new NotImplementedException();
		}
	}
}