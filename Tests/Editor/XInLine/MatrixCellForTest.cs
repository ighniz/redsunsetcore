using System.Linq;
using RedSunsetCore.GameTools.XInLineTools.Matrix;

namespace RedSunsetCore.Tests.Editor.XInLine
{
	public class MatrixCellForTest : XInLineMatrixCell<MatrixCellForTest, XInLineTileTest>
	{
		override public bool IsMatchableWith(MatrixCellForTest cell)
		{
			return AllTiles.Any(myTile => cell.AllTiles.Any(otherTile => otherTile.IsMatchableWith(myTile)));
		}
	}
}