using NUnit.Framework;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Tests.Editor.Core;

namespace RedSunsetCore.Tests.Editor.EventSystem
{
	[TestFixture]
	public class TestEventSystem : RedSunsetUnitTest<IEventService>
	{
		private const string VALUE = "Testing Value";
		private const int TRIES_COUNT = 7;

		private int successDispatchCounter;
		private int dispatchCount;

		[Test]
		public void AddListeners()
		{
			Run(eventService =>
			{
				eventService.AddListener<TestEvent>(OnTestEvent);
				eventService.AddListener<TestEvent>(OnTestEvent);
				eventService.AddListener<TestEvent>(OnTestEvent);
				eventService.AddListener<TestEvent>(OnTestEvent);
			});
		}

		[Test]
		public void RemoveListeners()
		{
			Run(eventService =>
			{
				eventService.RemoveListener<TestEvent>(OnTestEvent);
				eventService.AddListener<TestEvent>(OnTestEvent);
				eventService.RemoveAllListenersOf<TestEvent>();
			});
		}

		[Test]
		public void RemoveAllListenersWithoutExisting()
		{
			Run(eventService =>
			{
				eventService.AddListener<TestEvent>(OnTestEvent);
				eventService.RemoveAllListenersOf<TestEvent>();
			});
		}

		[Test]
		public void DispatchEvents()
		{
			Run(eventService =>
			{
				eventService.AddListener<TestEvent>(OnTestEvent);

				successDispatchCounter = 0;
				dispatchCount = 0;

				while (dispatchCount < TRIES_COUNT)
				{
					dispatchCount++;
					eventService.Dispatch<TestEvent>(t =>
					{
						if (t.value == null)
						{
							Assert.Fail("The value shouldn't be null. The EventSystem doesn't reset the value before dispatch the event.");
						}
						else
						{
							t.value = VALUE;
						}
					});
				}

				if (successDispatchCounter == 0)
				{
					Assert.Pass("All dispatches are success, but the events wasn't heard.");
				}
				else if (successDispatchCounter < dispatchCount)
				{
					Assert.Fail($"{dispatchCount} events are dispatched but only {successDispatchCounter} were heard.");
				}
				else if (successDispatchCounter > dispatchCount)
				{
					Assert.Fail($"{dispatchCount} were dispatched, but {successDispatchCounter} were heard.");
				}

				successDispatchCounter = 0;
				eventService.RemoveAllListenersOf<TestEvent>();
				eventService.Dispatch<TestEvent>();
				if (successDispatchCounter > 0)
				{
					Assert.Fail("Not all listeners were removed.");
				}
			});
		}

		[Test]
		public void RemoveAllListeners()
		{
			Run(eventService =>
			{
				eventService.AddListener<TestEvent>(test => { Assert.Fail("All events was cleared so the event system shouldn't execute this line."); });
				eventService.Clear();

				eventService.Dispatch<TestEvent>(t => { Assert.Fail("All events was cleared so the event system shouldn't execute this line."); });

				eventService.Dispatch<TestEvent>();
			});
		}

		private void OnTestEvent(TestEvent testEvent)
		{
			if (testEvent.value != VALUE)
			{
				Assert.Fail("The value should be {0}, but is {1}.", nameof(testEvent.value), testEvent.value);
			}
			else
			{
				successDispatchCounter++;
			}
		}

		[Test]
		public void DispatchingInsideADispatch()
		{
			Run(eventService =>
			{
				eventService.AddListener<TestEvent>(OnDispatchingInsideADispatch);
				eventService.Dispatch<TestEvent>();
			});
		}

		private void OnDispatchingInsideADispatch()
		{
			Run(eventService =>
			{
				eventService.RemoveListener<TestEvent>(OnDispatchingInsideADispatch);
				eventService.Dispatch<TestEvent>();
			});
		}

		class TestEvent : ICustomEvent
		{
			public string value = null;

			public void Reset()
			{
				value = string.Empty;
			}
		}
	}
}