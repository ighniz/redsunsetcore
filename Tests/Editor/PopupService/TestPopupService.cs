﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.Popup;
using RedSunsetCore.Services.Popup.Events;
using RedSunsetCore.Services.Popup.Interfaces;
using RedSunsetCore.Services.Zenject;
using Zenject;

namespace RedSunsetCore.Tests.Editor.PopupService
{
	[TestFixture]
	public class TestPopupService : ZenjectUnitTestFixture
	{
		private IPopupService popupService;
		private IEventService eventService;

		[SetUp]
		public void Initialize()
		{
			Container.Bind<IInjectingService>().To<CustomInjector>().AsSingle();
			Container.Bind<IEventService>().To<RedSunsetCore.Services.Event.EventSystem>().AsSingle();
			Container.Bind<IPopupService>().To<PopupController>().AsSingle();
			eventService = Container.Resolve<IEventService>();
			popupService = Container.Resolve<IPopupService>();
		}

		[Test]
		public void TestEnqueueAndShowInOrder()
		{
			var buttons = new List<string> { "btn1", "btn2", "btn3" };
			var acceptanceCriteria = new List<string>();
			var popupsConfig = new[]
			{
				new PopupConfig()
					.AddButton(buttons[0], () => acceptanceCriteria.Add(buttons[0])),
				new PopupConfig()
					.AddButton(buttons[1], () => acceptanceCriteria.Add(buttons[1])),
				new PopupConfig()
					.AddButton(buttons[2], () => acceptanceCriteria.Add(buttons[2])),
			};

			popupService.Enqueue(popupsConfig[0]);
			popupService.Enqueue(popupsConfig[1]);
			popupService.Enqueue(popupsConfig[2]);

			eventService.Dispatch<PopupButtonClickedEvent>(ev =>
			{
				ev.ButtonName = buttons[0];
				ev.PopupConfig = popupsConfig[0];
			});
			eventService.Dispatch<PopupButtonClickedEvent>(ev =>
			{
				ev.ButtonName = buttons[1];
				ev.PopupConfig = popupsConfig[1];
			});
			eventService.Dispatch<PopupButtonClickedEvent>(ev =>
			{
				ev.ButtonName = buttons[2];
				ev.PopupConfig = popupsConfig[2];
			});

			Assert.That(acceptanceCriteria.SequenceEqual(buttons), Is.True);
		}
	}
}