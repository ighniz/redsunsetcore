using System.Collections.Generic;
using System.IO;
using RedSunsetCore.Constants;
using RedSunsetCore.Editor.Core.Tools.StatusViewer;
using RedSunsetCore.Editor.FileSystem;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEditor.Callbacks;
using UnityEngine;

namespace RedSunsetCore.Editor.Core.Builder
{
    static public class CoreBuilder
    {
        static private Dictionary<string, string> commandLineArgsTable = new Dictionary<string, string>();
        
        static public bool IsDirty { get; private set; }

        [DidReloadScripts]
        static public void AutoBuildCore()
        {
            if (IsDirty)
            {
                BuildEngine();
                IsDirty = false;
            }
        }

        [MenuItem(StringConstants.Builder.MENU_INSTALL_ENGINE)]
        static public void InstallEngine()
        {
            FilesSystemUtils.CopyFilesRecursively(
                StringConstants.Paths.FullEngineInstallationFiles,
                Directory.GetCurrentDirectory() + "/", 
                StringConstants.Files.INSTALLATION_FILE_TYPE);

            var resourcesFolder = $"{StringConstants.Paths.FullEnginePath}Resources/";
            FilesSystemUtils.CopyFilesRecursively(
                resourcesFolder,
                Directory.GetCurrentDirectory() + "/Assets/Resources/",
                ".prefab",
                ".prefab");
        }
        
        [MenuItem(StringConstants.Builder.MENU_BUILD_ENGINE_OPTION)]
        static public void BuildEngine()
        {
            //Build entire core.
            StatusViewerWindow.ShowWindow();
            
            //1) Build bundles and assets settings.
            AssetsBuilder.Build();
            
            //2) Build all enums.
            EnumsBuilder.Build();
        }

        [MenuItem(StringConstants.Builder.MENU_BUILD_POPUPS)]
        static public void BuildPopups()
        {
            //Load and record all IPopup here.
            
        }

        static public void ParseCommandLineArguments()
        {
            /*string[] allArguments = Environment.GetCommandLineArgs();
            string execName = allArguments[0];
            for (var i = 1; i < allArguments.Length; i++)
            {
                commandLineArgsTable.Add(allArguments[i].Replace("-", ""), allArguments[i+1]);
            }*/
        }

        static public void SetSDKs()
        {
            EditorPrefs.SetString("AndroidSdkRoot", "");
            EditorPrefs.SetString("AndroidNdkRoot", "");
            EditorPrefs.SetString("JdkPath", "");
        }

        [MenuItem(StringConstants.Builder.MENU_BUILD_ANDROID)]
        static public void BuildAndroid()
        {
            ParseCommandLineArguments();
            BuildPlayerOptions options = new BuildPlayerOptions();
            options.options = BuildOptions.None;
            options.target = BuildTarget.Android;

            string buildName = commandLineArgsTable.ContainsKey("buildName")
                ? commandLineArgsTable["buildName"]
                : Application.productName;
            
            options.locationPathName = $"./Builds/{options.target}/{buildName}.apk";

            if (BuildPipeline.BuildPlayer(options).summary.result != BuildResult.Succeeded)
            {
                EditorApplication.Exit(1);
            }
        }

        static public void BuildStandalone()
        {
            
        }

        static public void Build(RuntimePlatform platform)
        {
            
            //If the build process fail...
            //EditorApplication.Exit(1);
        }
    }
}