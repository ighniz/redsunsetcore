using System.Collections.Generic;
using System.Globalization;
using RedSunsetCore.Editor.Core.Tools.CodeGenerator;
using RedSunsetCore.Editor.Core.Tools.CodeGenerator.Data;
using UnityEngine;
using Zenject;
using UnityEditor;
using System;
using System.Reflection;
using RedSunsetCore.Config;
using RedSunsetCore.Constants;
using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.Services.Assets.Data;
using RedSunsetCore.Services.Core;
using RedSunsetCore.Services.GameActionSystem;
using RedSunsetCore.Services.Inputs;
using UnityEngine.InputSystem;

namespace RedSunsetCore.Editor.Core.Builder
{
    static public class EnumsBuilder
    {
        //[MenuItem(StringConstants.Builder.MENU_BUILD_ALL_ENUMS)]
        [InitializeOnLoadMethod]
        static public void Build()
        {
            Debug.Log($"[{nameof(EnumsBuilder)}] Building Enums...");
            BuildInstallersEnum();
            BuildAssetsEnums();
            BuildGameActionsEnum();
            BuildInputActionsEnum();
        }

        static private void BuildGameActionsEnum()
        {
            var enumData = new EnumData(StringConstants.Core.EnumsNames.GAME_ACTIONS,
                                        StringConstants.Core.EnumsModifiers.DEFAULT);
            
            foreach (Type type in typeof(GameAction).GetAllSubtypes())
            {
                if (!type.IsGenericType)
                {
                    enumData.AddName(type.Name);                    
                }
            }
            
            enumData.AddLength();
            EnumCreator.Create(enumData, StringConstants.Core.Namespaces.CORE_ENUMS);
        }
        
        static private void BuildInstallersEnum()
        {
            var enumData = new EnumData(StringConstants.Core.EnumsNames.INSTALLER_TYPE,
                                        StringConstants.Core.EnumsModifiers.DEFAULT);
            
            var projectContext = Resources.Load<ProjectContext>("ProjectContext");
            foreach (ScriptableObjectInstaller installer in projectContext.ScriptableObjectInstallers)
            {
                if (installer is GroupInstaller installerGroup)
                {
                    foreach (ScriptableObjectInstaller subInstaller in installerGroup.installers)
                    {
                        enumData.AddName(subInstaller.name);
                    }
                }
                else
                {
                    enumData.AddName(installer.name);
                }
            }
            
            enumData.AddLength();
            EnumCreator.Create(enumData, StringConstants.Core.Namespaces.CORE_ENUMS);
        }

        static public void BuildAssetsEnums()
        {
            var assetsConfig = CoreFrameworkConfig.MainCoreFrameworkConfig?.GetConfig<AssetsConfig>();
            
            if (assetsConfig != null)
            {
                //Assets Labels...
                var enumLabelData = new EnumData(StringConstants.Core.EnumsNames.ASSETS_LABELS,
                    StringConstants.Core.EnumsModifiers.DEFAULT);
            
                string noLabelEnumField = StringConstants.Labels.ASSET_WITH_NO_LABEL;
                noLabelEnumField = noLabelEnumField.Replace("[", "");
                noLabelEnumField = noLabelEnumField.Replace("]", "");
            
                foreach (KeyValuePair<string,AssetSourceDataDictionary> assetPair in assetsConfig.assetsByLabels)
                {
                    if (assetPair.Key == StringConstants.Labels.ASSET_WITH_NO_LABEL)
                    {
                        enumLabelData.AddName(noLabelEnumField);
                    }
                    else
                    {
                        enumLabelData.AddName(assetPair.Key);
                    }
                }
            
                enumLabelData.AddLength();
                EnumCreator.Create(enumLabelData, StringConstants.Core.Namespaces.CORE_ENUMS);
            
                //Assets Variations...
                var enumVariationsData = new EnumData(StringConstants.Core.EnumsNames.ASSETS_VARIATIONS,
                    StringConstants.Core.EnumsModifiers.DEFAULT);
                foreach (KeyValuePair<string,AssetBundleSourceData> bundlesPair in assetsConfig.bundles)
                {
                    string normalizedText = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(bundlesPair.Value.extension);
                    enumVariationsData.AddName(normalizedText); //Bundle extension represents the "variation".
                }
                enumVariationsData.AddLength();
                EnumCreator.Create(enumVariationsData, StringConstants.Core.Namespaces.CORE_ENUMS);
            }
        }

        static public void BuildInputActionsEnum()
        {
            var enumInputActionsData = new EnumData(StringConstants.Core.EnumsNames.INPUT_ACTIONS, StringConstants.Core.EnumsModifiers.DEFAULT);
            var projectContext = Resources.Load<ProjectContext>(nameof(ProjectContext));
            if (projectContext)
            {
                foreach (ScriptableObjectInstaller installer in projectContext.ScriptableObjectInstallers)
                {
                    var installerType = installer.GetType();
                    if (installerType == typeof(GroupInstaller))
                    {
                        var groupInstaller = (GroupInstaller)installer;
                        foreach (ScriptableObjectInstaller installerInGroup in groupInstaller.installers)
                        {
                            FieldInfo serviceConfigPropertyInfo = installerInGroup.GetType().BaseType?.GetField("serviceConfig", BindingFlags.NonPublic | BindingFlags.Instance);
                            if (serviceConfigPropertyInfo != null)
                            {
                                var inputServiceConfig = serviceConfigPropertyInfo.GetValue(installerInGroup) as InputServiceConfig;
                                if (inputServiceConfig)
                                {
                                    foreach (InputActionMap actionMap in inputServiceConfig.InputActionAsset.actionMaps)
                                    {
                                        foreach (InputAction inputAction in actionMap.actions)
                                        {
                                            enumInputActionsData.AddName($"{actionMap.name}_{inputAction.name}");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            enumInputActionsData.AddLength();
            EnumCreator.Create(enumInputActionsData, StringConstants.Core.Namespaces.CORE_ENUMS);
        }
    }
}