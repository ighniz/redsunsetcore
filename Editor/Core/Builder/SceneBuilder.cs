using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using Zenject.Internal;

namespace RedSunsetCore.Editor.Core.Builder
{
	[InitializeOnLoad]
	static public class SceneBuilder
	{
		static SceneBuilder()
		{
			EditorSceneManager.newSceneCreated += OnNewSceneCreated;
		}

		static private void OnNewSceneCreated(Scene scene, NewSceneSetup setup, NewSceneMode mode)
		{
			ZenMenuItems.CreateSceneContext(null);
		}
	}
}