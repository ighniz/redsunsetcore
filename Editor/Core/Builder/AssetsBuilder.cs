using System;
using System.Collections.Generic;
using System.IO;
using RedSunsetCore.Config;
using RedSunsetCore.Constants;
using RedSunsetCore.Editor.Core.Tools.StatusViewer;
using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.Services.Assets.Data;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace RedSunsetCore.Editor.Core.Builder
{
    static public class AssetsBuilder
    {
        static private StatusViewerTaskData taskData;

        static public void Build()
        {
            taskData = new StatusViewerTaskData();
            StatusViewerWindow.AddTask(taskData);
            
            var config = CoreFrameworkConfig.MainCoreFrameworkConfig.GetConfig<AssetsConfig>();
            BuildInstance(config);
        }
        
        static public void BuildInstance(AssetsConfig assetsConfig)
        {
            BuildAllBundles();
            assetsConfig.Reset();
            
            assetsConfig.bundles        = BuildAllAssetManifests();
            assetsConfig.assetsByLabels = BuildAssetsBundlesByLabels(assetsConfig.bundles);
                
            assetsConfig.assetsByPath = new AssetSourceDataDictionary();
            foreach (KeyValuePair<string,AssetBundleSourceData> castedTargetBundle in assetsConfig.bundles)
            {
                foreach (KeyValuePair<string,AssetSourceData> assetPair in castedTargetBundle.Value.Assets)
                {
                    assetsConfig.assetsByPath[assetPair.Value.relativePath] = assetPair.Value;
                }
            }
        }
        
        [MenuItem(StringConstants.Builder.MENU_BUILD_ALL_ASSET_BUNDLES)]
        static private void BuildAllBundles()
        {
            taskData.state = StatusViewerTaskState.InProgress;
            taskData.titleText = StringConstants.Dialogs.PROGRESS_MESSAGE_BUILD_ALL_BUNDLES;
            BuildAssetBundles(PathAssetBundlesFolder,
                              BuildAssetBundleOptions.ChunkBasedCompression       |
                              BuildAssetBundleOptions.AppendHashToAssetBundleName |
                              BuildAssetBundleOptions.ForceRebuildAssetBundle,
                              EditorUserBuildSettings.activeBuildTarget);
        }

        static private void FormatDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }

            Directory.CreateDirectory(path);
        }

        static private void BuildAssetBundles(string path, BuildAssetBundleOptions options, BuildTarget target)
        {
            FormatDirectory(path);
            BuildPipeline.BuildAssetBundles(path, options, target);
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        }

        [MenuItem(StringConstants.Builder.MENU_BUILD_ASSETS_MANIFESTS)]
        static internal AssetBundlesSourceDataDictionary BuildAllAssetManifests()
        {
            taskData.state = StatusViewerTaskState.InProgress;
            taskData.titleText = StringConstants.Dialogs.UNLOAD_BUNDLES_BUILD_BUNDLES_DATA;
            AssetBundle.UnloadAllAssetBundles(true);

            taskData.titleText = StringConstants.Dialogs.LOADING_MAIN_MANIFEST.GetFormatted(PathMainAssetBundles);
            AssetBundle manifestBundle = AssetBundle.LoadFromFile(PathMainAssetBundles);

            taskData.titleText = StringConstants.Dialogs.LOADING_ALL_ASSETS_OF_BUNDLE.GetFormatted(PathMainAssetBundles);
            AssetBundleManifest manifest = manifestBundle.LoadAsset<AssetBundleManifest>(StringConstants.Files.MAIN_ASSET_MANIFEST_NAME);
            string[] allBundles = manifest.GetAllAssetBundlesWithVariant();
            var allParsedBundles = new AssetBundlesSourceDataDictionary();

            taskData.titleText = StringConstants.Dialogs.BUILDING_ASSET_BUNDLE_SOURCE_DATA.GetFormatted(0, allBundles.Length);
            for(var bundleIndex=0; bundleIndex<allBundles.Length; bundleIndex++)
            {
                taskData.infoText = StringConstants.Dialogs.CURRENT_BUNDLE_X_OF_Y.GetFormatted(allBundles[bundleIndex], bundleIndex+1, allBundles.Length);
                
                string currentBundlePath = allBundles[bundleIndex];
                string currentBundleAbsolutePath = GetFilePathFromAssetBundlesFolder(currentBundlePath);
                AssetBundle assetBundle = AssetBundle.LoadFromFile(currentBundleAbsolutePath);
                string[] bundleAssetsNames = assetBundle.GetAllAssetNames();
                var assetBundleSourceData = new AssetBundleSourceData(assetBundle.name)
                {
                    relativePath = currentBundlePath,
                    hash128 = manifest.GetAssetBundleHash(assetBundle.name)
                };
                
                for(var assetIndex=0; assetIndex<bundleAssetsNames.Length; assetIndex++)
                {
                    string assetPath = bundleAssetsNames[assetIndex];
                    
                    taskData.infoSubText = StringConstants.Dialogs.CURRENT_ASSET_X_OF_Y.GetFormatted(assetPath, assetIndex+1, bundleAssetsNames.Length);
                    
                    int lastSlashIndex = assetPath.LastIndexOf("/", StringComparison.Ordinal)+1;
                    string assetName = assetPath.Substring(lastSlashIndex, assetPath.Length - lastSlashIndex);

                    var assetSourceData = new AssetSourceData(assetName)
                    {
                        relativePath = RemoveFolderFromString(bundleAssetsNames[assetIndex], "Assets")
                    };

                    assetBundleSourceData.AddAssetSourceData(assetSourceData);

                    taskData.infoSubTextCompletedPercentage = (float)assetIndex / bundleAssetsNames.Length;
                }
                allParsedBundles.Add(assetBundleSourceData.completeName, assetBundleSourceData);
                assetBundle.Unload(true);

                taskData.infoTextCompletedPercentage = (float) bundleIndex / allBundles.Length;
            }

            return allParsedBundles;
        }

        static private void ShowAssetBundlesProgressBar(int bundleIndex, int bundlesCount, string currentBundlePath, string assetPath, int assetIndex, int bundleAssetsNamesCount)
        {
            /*
            string progressTitle = string.Format(
                                                 StringConstants.Dialogs.TITLE_BUILD_BUNDLES_DATA,
                                                 bundleIndex + 1,
                                                 bundlesCount,
                                                 currentBundlePath); 
            string progressInfo = string.Format(StringConstants.Dialogs.PROGRESS_MESSAGE_BUILD_BUNDLES_DATA, assetPath);
            float  percentage   = (float)assetIndex / bundleAssetsNamesCount;
            */
            
            //EditorUtility.DisplayCancelableProgressBar(progressTitle, progressInfo, percentage);
        }

        static private AssetSourceDataByLabels BuildAssetsBundlesByLabels(AssetBundlesSourceDataDictionary bundles)
        {
            var assetsByLabels = new AssetSourceDataByLabels();
            assetsByLabels.Add(StringConstants.Labels.ASSET_WITH_NO_LABEL, new AssetSourceDataDictionary());
            
            foreach(KeyValuePair<string, AssetBundleSourceData> sourceData in bundles)
            {
                AssetBundle.UnloadAllAssetBundles(true);
                AssetBundle currentBundle =
                    AssetBundle.LoadFromFile(GetFilePathFromAssetBundlesFolder(sourceData.Value.relativePath));

                //Is it a scene bundle?
                if (currentBundle.isStreamedSceneAssetBundle)
                {
                    string[] allScenePaths = currentBundle.GetAllScenePaths();
                    foreach (string scenePath in allScenePaths)
                    {
                        var scene = AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePath);
                        string[] allSceneLabels = AssetDatabase.GetLabels(scene);
                        foreach (string sceneLabel in allSceneLabels)
                        {
                            AssetSourceData sceneAssetSourceData = sourceData.Value.GetAssetSourceData(scene.name);
                            if (!assetsByLabels.ContainsKey(sceneLabel))
                            {
                                assetsByLabels.Add(sceneLabel, new AssetSourceDataDictionary());
                            }

                            assetsByLabels[sceneLabel].AddIfNotExist(sceneAssetSourceData.relativePath, sceneAssetSourceData);
                        }
                    }
                }
                else
                {
                    Object[] allAssetsInBundle = currentBundle.LoadAllAssets();
                    foreach (Object asset in allAssetsInBundle)
                    {
                        string[] allAssetLabels = AssetDatabase.GetLabels(asset);
                        AssetSourceData assetSourceData = sourceData.Value.GetAssetSourceData(asset.name);
                        if (allAssetLabels.Length > 0)
                        {
                            foreach (string label in allAssetLabels)
                            {
                                if (!assetsByLabels.ContainsKey(label))
                                {
                                    assetsByLabels.Add(label, new AssetSourceDataDictionary());
                                }

                                assetsByLabels[label].AddIfNotExist(assetSourceData.relativePath, assetSourceData);
                            }
                        }
                        else
                        {
                            assetsByLabels[StringConstants.Labels.ASSET_WITH_NO_LABEL].AddIfNotExist(assetSourceData.relativePath, assetSourceData);
                        }
                    }
                }
            }

            if (assetsByLabels[StringConstants.Labels.ASSET_WITH_NO_LABEL].Count == 0)
            {
                assetsByLabels.Remove(StringConstants.Labels.ASSET_WITH_NO_LABEL);
            }
            
            return assetsByLabels;
        }

        [MenuItem(StringConstants.Builder.MENU_DELETE_ALL_ASSET_BUNDLES_CACHE)]
        static public void CleanAllAssetsBundlesCache()
        {
            Debug.Log($"Cache cleared? {Caching.ClearCache()}.");
            Debug.Log("Asset Bundles cache deleted.");
        }

        static private string PathAssetBundlesFolder =>
            string.Format(StringConstants.Paths.AssetBundlesFolder,
                          EditorUserBuildSettings.activeBuildTarget);

        static private string PathMainAssetBundles =>
            string.Format(StringConstants.Paths.MainAssetBundles,
                          EditorUserBuildSettings.activeBuildTarget);

        static private string GetFilePathFromAssetBundlesFolder(string relativePath)
        {
            return string.Format(StringConstants.Paths.AssetBundlesFolder, EditorUserBuildSettings.activeBuildTarget) +
                   relativePath;
        }

        static private string RemoveFolderFromString(string path, string folderName)
        {
            folderName = folderName.ToLowerInvariant() + "/";
            int index = path.IndexOf(folderName, StringComparison.Ordinal);
            string str = string.Empty;
            if (index != -1)
            {
                str = path.Substring(index + folderName.Length, path.Length - folderName.Length);
            }

            return str;
        }

        static private void BuildLabelsEnumClass()
        {
            
        }

        static private void BuildVariantsEnumClass()
        {
            
        }
    }
}