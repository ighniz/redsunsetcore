using System;
using RedSunsetCore.Core;
using UnityEditor;

namespace RedSunsetCore.Editor.Core
{
    [CustomEditor(typeof(RedsunsetCoreBehaviour))]
    public class RedSunsetCoreBehaviourEditor : RedSunsetEditor
    {
    }
}