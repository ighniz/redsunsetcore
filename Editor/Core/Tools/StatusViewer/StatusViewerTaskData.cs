namespace RedSunsetCore.Editor.Core.Tools.StatusViewer
{
    public class StatusViewerTaskData
    {
        public string titleText = string.Empty;
        public string infoText = string.Empty;
        public float infoTextCompletedPercentage = 0f;
        public string infoSubText = string.Empty;
        public float infoSubTextCompletedPercentage = 0f;
        public float globalCompletedPercentage = 0f;
        public string successText = string.Empty;
        public string failureText = string.Empty;
        public StatusViewerTaskState state;
    }
}