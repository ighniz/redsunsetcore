namespace RedSunsetCore.Editor.Core.Tools.StatusViewer
{
    public enum StatusViewerTaskState
    {
        Nothing,
        Waiting,
        InProgress,
        Completed,
        Failure
    }
}