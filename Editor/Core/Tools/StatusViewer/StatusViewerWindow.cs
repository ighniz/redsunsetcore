using System.Collections.Generic;
using RedSunsetCore.Constants;
using UnityEditor;
using UnityEngine;

namespace RedSunsetCore.Editor.Core.Tools.StatusViewer
{
    public class StatusViewerWindow : EditorWindow
    {
        static private StatusViewerWindow thisWindow;
        static readonly public HashSet<StatusViewerTaskData> allCoreStatus = new HashSet<StatusViewerTaskData>();

        [MenuItem(StringConstants.Core.Tools.ENGINE_STATUS_VIEWER)]
        static public void ShowWindow()
        {
            thisWindow = GetWindow<StatusViewerWindow>();
            thisWindow.position = Rect.zero;
            thisWindow.Show();
        }

        static public void AddTask(StatusViewerTaskData taskData)
        {
            allCoreStatus.Add(taskData);
        }

        private void OnGUI()
        {
            foreach (StatusViewerTaskData taskData in allCoreStatus)
            {
                EditorGUILayout.LabelField(taskData.titleText);
                EditorGUI.ProgressBar(new Rect(0, EditorGUIUtility.singleLineHeight*2, 200, 50),
                                      taskData.globalCompletedPercentage,
                                      taskData.titleText);
                EditorGUI.ProgressBar(new Rect(0, EditorGUIUtility.singleLineHeight*3, 200, 50),
                                      taskData.infoTextCompletedPercentage,
                                      taskData.infoText);
                EditorGUI.ProgressBar(new Rect(0, EditorGUIUtility.singleLineHeight *4, 200, 50),
                                      taskData.infoSubTextCompletedPercentage,
                                      taskData.infoSubText);
            }

            /*
             * TODO:
             * - Make a ReorderableList and move finished task to the end.
             * - Add a "CheckBox" for clean completed tasks.
             */
            //EditorGUI.ProgressBar(new Rect(0, 0, 200, 50), .5f, "Building...");
        }
    }
}