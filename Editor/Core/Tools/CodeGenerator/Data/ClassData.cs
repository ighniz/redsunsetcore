namespace RedSunsetCore.Editor.Core.Tools.CodeGenerator.Data
{
    public class ClassData : BaseBuildableStructure
    {
        public enum ScopeType
        {
            Private,
            Protected,
            Internal,
            Public
        }

        public string name;
        public ScopeType scope;
        public bool isStatic;
        public NamespaceData namespaceData;
        override public string Build()
        {
            throw new System.NotImplementedException();
        }
    }
}