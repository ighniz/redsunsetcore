using System;
using System.Collections.Generic;
using System.Text;

namespace RedSunsetCore.Editor.Core.Tools.CodeGenerator.Data
{
    abstract public class BaseBuildableStructure
    {
        static readonly protected int TAB_INDEX = 0;
        static readonly protected string TAB = "{" + TAB_INDEX + "}";
        static readonly protected int HEADER_INDEX = 1;
        static readonly protected string HEADER = "{" + HEADER_INDEX + "}\n";
        static readonly protected int BODY_INDEX = 2;
        static readonly protected string BODY = TAB + "{{" + "\n" +
                                                TAB + "\t" + "{" + BODY_INDEX + "}\n" + 
                                                TAB + "}}\n";
        
        readonly private List<BaseBuildableStructure> allContent = new List<BaseBuildableStructure>();
        protected string bodyStructure;
        private int indentLevel = 0;
        protected string header;

        public BaseBuildableStructure()
        {
            bodyStructure = HEADER + BODY;
        }

        public BaseBuildableStructure AddContent(BaseBuildableStructure content)
        {
            content.indentLevel = indentLevel + 1;
            allContent.Add(content);

            return this;
        }

        protected StringBuilder GetIndentString()
        {
            var indent = new StringBuilder();
            for (int i = 0; i < indentLevel; i++)
            {
                indent.Append("\t");
            }

            return indent;
        }

        virtual public string Build()
        {
            var outputStructure = new StringBuilder();

            StringBuilder myIndent = GetIndentString();
            foreach (BaseBuildableStructure content in allContent)
            {
                outputStructure.Append(content.Build());
            }

            return outputStructure.ToString();
        }

        virtual protected string Format(string input, string headerContent, string body, params string[] others)
        {
            return string.Format(input, GetIndentString(), headerContent, body, others);
        }
    }
}