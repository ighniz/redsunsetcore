using System.Text;

namespace RedSunsetCore.Editor.Core.Tools.CodeGenerator.Data
{
    public class NamespaceData : BaseBuildableStructure
    {
        private string name;

        public NamespaceData(string name)
        {
            this.name = "namespace " + name;
        }
        
        override public string Build()
        {
            var builder = new StringBuilder();
            builder.Append(Format(bodyStructure, name, base.Build()));
            return builder.ToString();
        }
    }
}