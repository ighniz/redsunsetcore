using System.Collections.Generic;
using System.Text;

namespace RedSunsetCore.Editor.Core.Tools.CodeGenerator.Data
{
    public class EnumData : BaseBuildableStructure
    {
        private const string LENGTH = "Length";
        
        private int biggerValue = 0;
        readonly private List<EnumNameValuePair> structure = new List<EnumNameValuePair>();
        
        readonly public string name;

        public EnumData(string name, string accessModifiers = "", int startingValue = 0)
        {
            this.name = name;
            header = $"{accessModifiers} enum {name}";
            biggerValue = startingValue;
        }
        
        public void AddNameAndValue(string fieldName, int fieldValue)
        {
            AddNameAndValue(new EnumNameValuePair {name = fieldName, value = fieldValue});
        }
        
        public void AddName(string fieldName)
        {
            AddNameAndValue(fieldName, biggerValue++);
        }

        public void AddNameAndValue(EnumNameValuePair nameValuePair)
        {
            if (!structure.Exists(field => field.name == nameValuePair.name))
            {
                structure.Add(nameValuePair);

                if (nameValuePair.value > biggerValue)
                {
                    biggerValue = nameValuePair.value;
                }
            }
        }

        public void AddLength()
        {
            RemoveLength();
            AddNameAndValue(LENGTH, structure.Count);
        }

        public void RemoveName(string fieldName)
        {
            int index = structure.FindIndex(match => match.name == fieldName);
            if (index != -1)
            {
                structure.RemoveAt(index);
            }
        }

        public void RemoveLength()
        {
            RemoveName(LENGTH);
        }

        override public string Build()
        {
            var namesBuilder = new StringBuilder();
            StringBuilder myIndent = GetIndentString();
            foreach (EnumNameValuePair enumPair in structure)
            {
                namesBuilder.Append(enumPair.name + " = " + enumPair.value + ",\n" + myIndent + "\t");
            }

            return Format(bodyStructure, header, namesBuilder.ToString());
        }
        
        public struct EnumNameValuePair
        {
            public string name;
            public int value;
        }
    }
}