using System.Collections.Generic;
using System.IO;
using RedSunsetCore.Editor.Core.Tools.CodeGenerator.Data;
using UnityEditor;
using UnityEngine;

namespace RedSunsetCore.Editor.Core.Tools.CodeGenerator
{
    static public class EnumCreator
    {
        static public void Create(string enumName, string enumNamespace, string accessModifiers, IEnumerable<EnumData.EnumNameValuePair> nameValuePair)
        {
            var enumData = new EnumData(enumName, accessModifiers);
            
            foreach (EnumData.EnumNameValuePair enumPairs in nameValuePair)
            {
                enumData.AddNameAndValue(enumPairs);
            }

            Create(enumData, enumNamespace);
        }

        static public void Create(EnumData enumData, string enumNamespace = "")
        {
            string output = !string.IsNullOrEmpty(enumNamespace)
                ? new NamespaceData(enumNamespace).AddContent(enumData).Build()
                : enumData.Build();

            string folder = Directory.GetCurrentDirectory() + "/Assets/" +
                            enumNamespace.Replace(".", "/") + "/";

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            File.WriteAllText(folder + enumData.name + ".cs", output);
            AssetDatabase.Refresh(ImportAssetOptions.Default);
        }
    }
}