using System.IO;
using UnityEditor;

namespace RedSunsetCore.Editor.FileSystem
{
    static public class FilesSystemUtils
    {
        static public void CopyFilesRecursively(string sourcePath, string targetPath, string searchPattern = "", string patternReplacement = "")
        {
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(dirPath.Replace(sourcePath, targetPath));
            }

            //Copy all the files & Replaces any files with the same name
            foreach (string filePath in Directory.GetFiles(sourcePath, "*.*",SearchOption.AllDirectories))
            {
                string targetFilePath = filePath.Replace(sourcePath, targetPath);
                string newFilePath = targetFilePath.Replace(searchPattern, patternReplacement);
                File.Copy(filePath, newFilePath, true);
            }
            
            AssetDatabase.Refresh();
        }

        static public void RenameFiles(string filePath, string oldPattern, string newPattern, bool includeSubfolders = false)
        {
            var directory = new DirectoryInfo(filePath);
            FileInfo[] files = directory.GetFiles(oldPattern, SearchOption.AllDirectories);
            foreach(FileInfo file in files)
            {
                File.Move(file.FullName, file.FullName.Replace("abc_",""));
            }
        }
    }
}