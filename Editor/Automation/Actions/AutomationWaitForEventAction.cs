using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.GameActionSystem;
using Zenject;

namespace RedSunsetCore.Editor.Automation.Actions
{
    public class AutomationWaitForEventAction<TEvent> : AutomationScriptAction where TEvent : class, ICustomEvent
    {
        [Inject]
        private IEventService eventService;

        override public void DoAction()
        {
            eventService.AddListener<TEvent>(OnEventDispatched);
        }

        private void OnEventDispatched(TEvent ev)
        {
            NotifyFinish();
        }
    }
}