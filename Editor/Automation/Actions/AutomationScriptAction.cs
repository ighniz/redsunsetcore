using RedSunsetCore.Editor.Automation.Interfaces;
using RedSunsetCore.Services.GameActionSystem;

namespace RedSunsetCore.Editor.Automation.Actions
{
    abstract public class AutomationScriptAction : GameAction, IAutomationAction
    {
        
    }
}