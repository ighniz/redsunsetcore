using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.Services.Async.Interfaces;

namespace RedSunsetCore.Editor.Automation.Actions
{
    public class AutomationDelayAction : AutomationScriptAction, IUpdateReceiver
    {
        private float mDelay;

        public AutomationDelayAction(float delayInSeconds)
        {
            mDelay = delayInSeconds;
        }

        override public void DoAction()
        {
            this.AddToUpdateList();
        }

        public void OnUpdate()
        {
            
        }
    }
}