using RedSunsetCore.Services.GameActionSystem;

namespace RedSunsetCore.Editor.Automation.Interfaces
{
    public interface IAutomationAction
    {
        void NotifyFinish(GameActionEvent.GameActionState state = GameActionEvent.GameActionState.Completed);
    }
}