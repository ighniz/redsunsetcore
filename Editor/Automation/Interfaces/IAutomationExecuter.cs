using RedSunsetCore.Services.Event.Interfaces;

namespace RedSunsetCore.Editor.Automation.Interfaces
{
    public interface IAutomationExecuter
    {
        AutomationScriptExecuter AddDelay(float delay);
        AutomationScriptExecuter AddWaitForEventStep<TEvent>() where TEvent : class, ICustomEvent;
        AutomationScriptExecuter AddStep<TStep>(TStep step) where TStep : IAutomationAction;
        void Execute();
    }
}