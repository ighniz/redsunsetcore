using RedSunsetCore.Services.Zenject;

namespace RedSunsetCore.Editor.Automation
{
    static public class AutomationScriptBuilder
    {
        static public AutomationScriptExecuter Create()
        {
            return IInjectingService.Instance.CreateFromType<AutomationScriptExecuter>(typeof(AutomationScriptExecuter));
        }
    }
}