using RedSunsetCore.Editor.Automation.Actions;
using RedSunsetCore.Editor.Automation.Interfaces;
using RedSunsetCore.Services.Event.Interfaces;
using RedSunsetCore.Services.GameActionSystem;

namespace RedSunsetCore.Editor.Automation
{
    public class AutomationScriptExecuter : GameActionGroup, IAutomationExecuter
    {
        public AutomationScriptExecuter AddDelay(float delay)
        {
            AddAction(new AutomationDelayAction(delay));
            return this;
        }

        public AutomationScriptExecuter AddWaitForEventStep<TEvent>() where TEvent : class, ICustomEvent
        {
            AddAction(new AutomationWaitForEventAction<TEvent>());
            return this;
        }

        public AutomationScriptExecuter AddStep<TStep>(TStep step) where TStep : IAutomationAction
        {
            AddAction(step as GameAction);
            return this;
        }

        public void Execute()
        {
            DoAllActions();
        }
    }
}