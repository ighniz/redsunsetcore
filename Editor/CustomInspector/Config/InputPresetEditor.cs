using RedSunsetCore.Editor.Core;
using RedSunsetCore.Services.Inputs.Presets;
using UnityEditor;

namespace RedSunsetCore.Editor.CustomInspector.Config
{
    [CustomEditor(typeof(InputPreset))]
    public class InputPresetEditor : RedSunsetEditor
    {
        override public void OnInspectorGUI()
        {
            
        }
    }
}