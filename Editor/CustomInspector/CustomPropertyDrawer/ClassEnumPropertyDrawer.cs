using ModestTree;
using RedSunsetCore.Attributes;
using UnityEditor;
using UnityEngine;

namespace RedSunsetCore.Editor.CustomInspector.CustomPropertyDrawer
{
    [UnityEditor.CustomPropertyDrawer(typeof(ClassEnumAttribute))]
    public class ClassEnumPropertyDrawer : PropertyDrawer
    {
        override public void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var castedAttribute = attribute as ClassEnumAttribute;
            int selectedIndex = EditorGUI.Popup(position,
                                                   label.text,
                                                   castedAttribute.allSubclassesAsString.IndexOf(property.stringValue),
                                                   castedAttribute.allSubclassesAsString);

            property.stringValue = selectedIndex != -1 ? castedAttribute.allSubclassesAsString[selectedIndex] : string.Empty;
        }
    }
}