﻿using System;
using System.Collections.Generic;
using System.Reflection;
using RedSunsetCore.Services.Async;
using RedSunsetCore.Services.Async.Interfaces;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace RedSunsetCore.Editor.CustomInspector.Services.Async
{
    /// <summary>
    /// Implements the tool to edit the behaviours priorities. 
    ///
    /// Extends Editor.
    /// </summary>
    [CustomEditor(typeof(AsyncPriorities))]
    public class AsyncPrioritiesEditor : UnityEditor.Editor
    {
        const string RELATIVE_PROPERTY_PATH = "descriptor";

        const char NAMESPACE_SEPARATOR = '.';
        const char MENU_SEPARATOR = '/';

        const string SAVE_BTN_TEXT = "Apply";

        const int RM_BTN_SIZE = 22;
        const string RM_BTN_TOOLTIP = "Remove item";
        const string RM_BTN_ICON_NAME = "Toolbar Minus";
        const string RM_BTN_STYLE = "InvisibleButton"; 

        private AsyncPriorities Priorities => target as AsyncPriorities;

        readonly private List<ReorderableList> priorities = new List<ReorderableList>();
        readonly private Queue<Action> preRenderActions = new Queue<Action>();
        override public void OnInspectorGUI()
        {
            // Process remove actions 
            if (0 < preRenderActions.Count)
            {
                foreach (Action action in preRenderActions)
                {
                    action();
                }
                preRenderActions.Clear();
            }

            // Update reorderable lists
            serializedObject.Update();
            foreach (ReorderableList list in priorities)
            {
                list.DoLayoutList();
            }
            serializedObject.ApplyModifiedProperties();

            // Force to save Update Priorites asset
            if (GUILayout.Button(SAVE_BTN_TEXT, new GUILayoutOption[0]))
            {
                AssetDatabase.SaveAssets();
            }
        }

        private void OnEnable()
        {
            // Create reorderable lists based on target public fields
            Type targetType = target.GetType();
            BindingFlags publics = BindingFlags.Public | BindingFlags.Instance;
            foreach(FieldInfo property in targetType.GetFields(publics))
            {
                var list = new ReorderableList(serializedObject,
                    serializedObject.FindProperty(property.Name)) {displayRemove = false};

                list.drawHeaderCallback = (Rect rect) => {
                            DrawListHeader(rect, list);
                };

                list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
                        DrawListElement(rect, index, list);
                };

                list.onAddDropdownCallback = AddElementWithDropDown;
                priorities.Add(list);
            }
        }

        /// <summary>
        /// Draws the list header.
        /// </summary>
        /// <param name="rect">Rect where the header will be drawn.</param>
        /// <param name="list">List whose header will be drawn.</param>
        private void DrawListHeader(Rect rect, ReorderableList list)
        {
            EditorGUI.LabelField(rect, list.serializedProperty.name);
        }

        /// <summary>
        /// Draws a list element.
        /// </summary>
        /// <param name="rect">Rect where the item will be drawn.</param>
        /// <param name="index">Index of the element to be drawn.</param>
        /// <param name="list">List that holds the element to be drawn.</param>
        private void DrawListElement(Rect rect, int index, ReorderableList list)
        {
            // Add a remove button
            Rect removeRect = new Rect(rect.x + rect.width - RM_BTN_SIZE,
                                       rect.y + EditorGUIUtility.standardVerticalSpacing, RM_BTN_SIZE,
                                       EditorGUIUtility.singleLineHeight);

            GUIContent removeIcon = EditorGUIUtility.IconContent(RM_BTN_ICON_NAME, RM_BTN_TOOLTIP); 
            if (GUI.Button(removeRect, removeIcon, RM_BTN_STYLE))
            {
                preRenderActions.Enqueue(() => RemoveElement(list, index)); 
            }

            // Display the descriptor value
            SerializedProperty serializedList = list.serializedProperty;
            SerializedProperty element = serializedList.GetArrayElementAtIndex(index);

            Rect descriptorRect = new Rect(rect.x, rect.y + EditorGUIUtility.standardVerticalSpacing,
                                           EditorGUIUtility.currentViewWidth - removeRect.width,
                                           EditorGUIUtility.singleLineHeight);

            EditorGUI.LabelField(descriptorRect, element.FindPropertyRelative(RELATIVE_PROPERTY_PATH).stringValue);
        }

        /// <summary>
        /// Removes an element from a reorderable list.
        /// </summary>
        /// <param name="list">List where the element will be removed from.</param>
        /// <param name="index">Index of the element to be removed.</param>
        private void RemoveElement(ReorderableList list, int index)
        {
            SerializedProperty serializedList = list.serializedProperty;
            serializedList.MoveArrayElement(index, serializedList.arraySize - 1);
            serializedList.arraySize--;
            serializedObject.ApplyModifiedProperties();
        }

        /// <summary>
        /// Adds an element with drop down.
        /// </summary>
        /// <param name="buttonRect">Button rect.</param>
        /// <param name="list">Reorderable list.</param>
        private void AddElementWithDropDown(Rect buttonRect, ReorderableList list)
        {
            GenericMenu menu = new GenericMenu();
            foreach(Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach(Type type in assembly.GetTypes())
                {
                    if (IsValidForDisplay(type))
                    {
                        menu.AddItem(
                            new GUIContent(GetMenuItemText(type.FullName)),
                            false,
                            OnMenuItemClick,
                            new OnMenuItemClickParams{ list = list, descriptor = type.FullName }
                        );
                    }
                }
            }
            menu.ShowAsContext();
        }

        /// <summary>
        /// Checks if a Type is valid to be displayed in the dropdown menu.
        /// </summary>
        /// <returns><c>true</c>, if a Type is valid, <c>false</c> otherwise.</returns>
        /// <param name="type">Type to be checked.</param>
        private bool IsValidForDisplay(Type type)
        {
            return typeof(IAsyncReceiver).IsAssignableFrom(type);
        }

        /// <summary>
        /// Gets the menu item text.
        /// </summary>
        /// <returns>The menu item text.</returns>
        /// <param name="fullname">Fullname.</param>
        private string GetMenuItemText(string fullname)
        {
            return fullname.Replace(NAMESPACE_SEPARATOR, MENU_SEPARATOR);
        }

        /// <summary>
        /// Creates a new element in a reorderable list
        /// </summary>
        /// <param name="obj">Object.</param>
        private void OnMenuItemClick(object obj)
        {
            var data = (OnMenuItemClickParams) obj;
            SerializedProperty serializedList = data.list.serializedProperty;

            int elementIndex = serializedList.arraySize;
            serializedList.arraySize++;

            // Update index of the displayed reorderable list
            data.list.index = elementIndex;

            // Add new element to the serialized list
            SerializedProperty element = serializedList.GetArrayElementAtIndex(elementIndex);
            element.FindPropertyRelative(RELATIVE_PROPERTY_PATH).stringValue = data.descriptor;

            serializedObject.ApplyModifiedProperties();
        }

        /// <summary>
        /// Struct used to pass multiple parameters to OnMenuItemClick
        /// </summary>
        private struct OnMenuItemClickParams
        {
            public ReorderableList list;
            public string descriptor;
        }
    }
}
