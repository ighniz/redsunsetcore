using RedSunsetCore.Editor.Core;
using RedSunsetCore.UnityComponents.Layout.Base;
using UnityEditor;

namespace RedSunsetCore.Editor.CustomInspector.UnityComponents.Layouts
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(LayoutElementComponent), true)]
    public class UnityLayoutElementComponentEditor : RedSunsetCoreBehaviourEditor
    {
        private LayoutElementComponent castedTarget;
        
        protected void OnEnable()
        {
            castedTarget = (LayoutElementComponent) target;
            NotifyParent();
        }

        private void NotifyParent()
        {
            if (castedTarget.transform.parent != null)
            {
                var layoutParent = castedTarget.transform.parent.GetComponent<LayoutComponent>();
                if (layoutParent != null)
                {
                    layoutParent.UpdateLayout();
                }
            }
        }
        
        override public void OnInspectorGUI()
        {
            SerializedProperty propertyIterator = serializedObject.GetIterator();
            propertyIterator.NextVisible(true);
            while (propertyIterator.NextVisible(false))
            {
                EditorGUILayout.PropertyField(propertyIterator);
            }

            if (serializedObject.hasModifiedProperties)
            {
                serializedObject.ApplyModifiedProperties();
                castedTarget.isDirty = true;
                NotifyParent();
            }
        }
    }
}