using System;
using RedSunsetCore.Editor.Core;
using RedSunsetCore.UnityComponents.Layout.Base;
using UnityEditor;

namespace RedSunsetCore.Editor.CustomInspector.UnityComponents.Layouts
{
    [CustomEditor(typeof(LayoutComponent), true)]
    public class UnityLayoutComponentEditor : RedSunsetCoreBehaviourEditor
    {
        private LayoutComponent castedTarget;
        
        private void OnEnable()
        {
            castedTarget = (LayoutComponent) target;
        }

        private void OnDisable()
        {
            castedTarget = null;
        }

        override public void OnInspectorGUI()
        {
            SerializedProperty propertyIterator = serializedObject.GetIterator();
            propertyIterator.NextVisible(true);
            while (propertyIterator.NextVisible(false))
            {
                EditorGUILayout.PropertyField(propertyIterator);
            }

            if (serializedObject.hasModifiedProperties)
            {
                serializedObject.ApplyModifiedProperties();
                castedTarget.UpdateLayout();
            }
        }
    }
}