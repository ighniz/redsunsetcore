using RedSunsetCore.UnityComponents.Layout;
using UnityEditor;
using UnityEngine;

namespace RedSunsetCore.Editor.CustomInspector.UnityComponents.Layouts
{
    [CustomEditor(typeof(GridLayoutComponent))]
    public class GridLayoutComponentEditor : UnityLayoutComponentEditor
    {
        private GridLayoutComponent castedTarget;
        
        private void OnEnable()
        {
            castedTarget = (GridLayoutComponent) target;
        }

        private void OnDisable()
        {
            castedTarget = null;
        }
        
        override public void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Apply"))
            {
                castedTarget.UpdateLayout();
            }
        }
    }
}