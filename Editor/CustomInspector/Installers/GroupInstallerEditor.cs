using RedSunsetCore.Services.Core;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace RedSunsetCore.Editor.CustomInspector.Installers
{
    [CustomEditor(typeof(GroupInstaller))]
    public class GroupInstallerEditor : UnityEditor.Editor
    {
        private ReorderableList reorderableList;

        private void OnEnable()
        {
            var castedTarget = (GroupInstaller) target;
            reorderableList = new ReorderableList(serializedObject,
                                                  serializedObject.FindProperty(nameof(castedTarget.installers)),
                                                  true,
                                                  true,
                                                  true,
                                                  true);

            reorderableList.drawHeaderCallback = DrawHeader;
            reorderableList.drawElementCallback = DrawElement;
        }

        private void DrawHeader(Rect rect)
        {
            EditorGUI.LabelField(rect, "Sub-Installers");
        }
        
        private void DrawElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            EditorGUI.PropertyField(rect, reorderableList.serializedProperty.GetArrayElementAtIndex(index), GUIContent.none);
        }

        override public void OnInspectorGUI()
        {
            serializedObject.Update();
            reorderableList.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }
    }
}