using System;
using System.Linq;
using RedSunsetCore.Config;
using RedSunsetCore.Editor.Core;
using UnityEditor;
using UnityEngine;

namespace RedSunsetCore.Editor.ScriptableData
{
    [CustomEditor(typeof(UnityEngineConfig))]
    public class UnityEngineConfigEditor : RedSunsetScriptableEditor
    {
        private UnityEngineConfig castedTarget;
        
        private void OnEnable()
        {
            castedTarget = (UnityEngineConfig) target;
        }

        private void OnDisable()
        {
            castedTarget = null;
        }
        
        override public void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (IsValidEnvironmentName(castedTarget.environmentVarName))
            {
                if (GUILayout.Button("Write Environment Var"))
                {
                    if (IsValidEnvironmentName(castedTarget.lastEnvironmentVarName))
                    {
                        Environment.SetEnvironmentVariable(castedTarget.lastEnvironmentVarName, null, EnvironmentVariableTarget.Machine);
                    }

                    Environment.SetEnvironmentVariable(castedTarget.environmentVarName, castedTarget.location, EnvironmentVariableTarget.Machine);
                    castedTarget.lastEnvironmentVarName = castedTarget.environmentVarName;
                }
            }
            else
            {
                var style = new GUIStyle {normal = {textColor = Color.red}};
                GUILayout.Label("THE CURRENT \"environment var name\" DOESN'T VALID.", style);
            }
        }

        static private bool IsValidEnvironmentName(string environmentName)
        {
            return !string.IsNullOrWhiteSpace(environmentName) &&
                   environmentName.All(character => char.IsLetterOrDigit(character) || character.Equals('_'));
        }
    }
}