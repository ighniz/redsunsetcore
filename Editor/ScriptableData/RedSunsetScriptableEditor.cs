using System;
using System.IO;
using Newtonsoft.Json;
using RedSunsetCore.Config;
using RedSunsetCore.Constants;
using RedSunsetCore.Editor.Core;
using RedSunsetCore.Scriptables;
using UnityEditor;
using UnityEngine;

namespace RedSunsetCore.Editor.ScriptableData
{
    [CustomEditor(typeof(RedSunsetScriptable), true)]
    public class RedSunsetScriptableEditor : RedSunsetEditor
    {
        private RedSunsetScriptable castedTarget;
        
        private void OnEnable()
        {
            castedTarget = (RedSunsetScriptable) target;
        }

        private void OnDisable()
        {
            castedTarget = null;
        }
        
        override public void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUILayout.Space(10);
            if (GUILayout.Button("Export JSON"))
            {
                ApplyJsonLibraryConfig();
                try
                {
                    File.WriteAllText(StringConstants.Paths.JsonConfig + castedTarget.name + ".json",castedTarget.ToJson());
                }
                catch (UnauthorizedAccessException unauthorizedAccessException)
                {
                    Debug.LogError($"Unauthorized Access. Try running Unity as an Administrator. \n\n {unauthorizedAccessException.Message}");
                }
            }

            GUILayout.Space(10);
            if (GUILayout.Button("Copy JSON To Clipboard"))
            {
                ApplyJsonLibraryConfig();
                GUIUtility.systemCopyBuffer = castedTarget.ToJson();
            }
        }

        private void ApplyJsonLibraryConfig()
        {
            var gameConfig = Resources.Load<GameConfig>(nameof(GameConfig));
            if (gameConfig != null)
            {
                gameConfig.ApplyJsonLibraryConfig();
            }
        }
    }
}